#!/usr/bin/php
<?php
require_once('vendor/autoload.php');

$part = $argv[1] ?? 'a';
$day = $argv[2] ?? date('d');
$y = $argv[3] ?? date('Y');

$inputSuffix = substr($part, 1);
$part = strtoupper($part[0]);

$cls = sprintf('App\\Y%04s\\D%02s', $y, $day);

if (class_exists($cls)) {
    /** @var \App\Day $dayObj */
    $dayObj = new $cls;
    $dayObj->_argv = array_slice($argv, 4);
    $dayObj->_suffix = $inputSuffix;

    match ($part) {
        'A' => $dayObj->run(),
        'B' => $dayObj->runB(),
    };

} else {
    die("No such day\n");
}
