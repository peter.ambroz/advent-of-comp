# Advent of Code

Behold my collection of AoC solutions.

## Before first run

Year 2024:
Python 3.12

Years 2015-2023:
PHP version 8.2 is required.
```
composer install
chmod u+x run.php
```

## Usage
For python, run the actual file of the day
`python3 d[day].py`

or

`./run.php [part] [day] [year]`

- *part* is either `a` or `b`. Defaults to `a`
- *day* defaults to today
- *year* defaults to current year

## Results
| Year | Stars   |
|------|---------|
| 2024 | **50*** |
| 2023 | **50*** |
| 2022 | **50*** |
| 2021 | **50*** |
| 2020 | **50*** |
| 2019 | **50*** |
| 2018 | **50*** |
| 2017 | **50*** |
| 2016 | **50*** |
| 2015 | **50*** |

## Remark
The solution is sometimes a combined effort of paper with a help of computer. Therefore the puzzle results are not 100%
usable directly as a solution.
