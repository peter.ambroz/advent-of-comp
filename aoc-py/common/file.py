import sys
from pathlib import Path

def input_file_name(suffix: str = '') -> dict[str, str]:
    path = Path(sys.argv[0])
    name = path.name.split('.')[0]

    return {
        "full": str(path.parent / "input" / f"{name}{suffix}.txt"),
        "day_name": name,
    }

def input_lines(call = None, suffix: str = '') -> list[str]:
    input_data = input_file_name(suffix)

    with open(input_data["full"], 'r') as file:
        lines = file.read().splitlines()
    if call is not None:
        return list(map(call, lines))
    else:
        return lines
