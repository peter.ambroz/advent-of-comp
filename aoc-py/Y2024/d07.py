from itertools import product
from common.file import input_lines

def parse(line: str) -> dict:
    vals = line.split(" ")
    return {
        "result": int(vals[0].split(":")[0]),
        "values": [int(v) for v in vals[1:]],
    }

def calc(test: dict, part2: bool = False) -> bool:
    operations = "+*"
    if part2:
        operations = "+*|"
    for ops in product(operations, repeat=len(test["values"])-1):
        left = test["values"][0]
        for i, op in enumerate(ops):
            match op:
                case '+':
                    left += test["values"][i+1]
                case '*':
                    left *= test["values"][i+1]
                case '|':
                    left = int(str(left) + str(test["values"][i+1]))

        if left == test["result"]:
            return True
    return False

print(sum([t["result"] for t in input_lines(parse) if calc(t)]))
print(sum([t["result"] for t in input_lines(parse) if calc(t, True)]))
