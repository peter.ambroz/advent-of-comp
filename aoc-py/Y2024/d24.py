from common.file import input_lines

def parse():
    data, ops = {}, {}
    definitions = True
    for line in input_lines():
        if len(line) == 0:
            definitions = False
            continue
        if definitions:
            k, v = line.split(': ')
            data[k] = int(v)
        else:
            i1, op, i2, _, o = line.split(' ')
            ops[o] = [i1, op, i2]
    return data, ops

def calc(var: str) -> int:
    if data.get(var, -1) == -1:
        match ops[var][1]:
            case 'AND':
                return calc(ops[var][0]) & calc(ops[var][2])
            case 'OR':
                return calc(ops[var][0]) | calc(ops[var][2])
            case 'XOR':
                return calc(ops[var][0]) ^ calc(ops[var][2])
            case _:
                raise ValueError
    return data[var]

def check(id: int):
    for k in data:
        data[k] = 0
    for x in [0, 1]:
        data[f"x{id:02d}"] = x
        for y in [0, 1]:
            data[f"y{id:02d}"] = y
            expect = x + y
            actual = calc(f"z{id:02d}") + 2 * calc(f"z{id+1:02d}")
            if expect != actual:
                print(f"{id} expected {expect} but got {actual}")

data, ops = parse()
print(sum([calc(f"z{i:02d}") * 2**i for i in range(46)]))

# part 2 - check each individual adder for errors. The rest is manual checking of affected parts
for i in range(44):
    check(i)