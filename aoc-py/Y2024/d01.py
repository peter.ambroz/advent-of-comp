from common.file import input_lines

data = input_lines(lambda x : [int(y) for y in x.split('   ')])
l = zip(sorted([y[0] for y in data]), sorted([y[1] for y in data]))
print(sum([abs(z[0] - z[1]) for z in l]))
counts = {}

for y in data:
    counts[y[1]] = counts.get(y[1], 0) + 1

print(sum([x[0] * counts.get(x[0], 0) for x in data]))
