from common.file import input_lines

def safe(l: list) -> int:
    if l[0] > l[1]: up = False
    elif l[0] < l[1]: up = True
    else: return 0

    for i,x in enumerate(l):
        if i == 0: continue
        diff = l[i] - l[i-1]

        if up and (diff < 1 or diff > 3):
            return 0
        if not up and (diff > -1 or diff < -3):
            return 0
    return 1

def damp_safe(l: list) -> int:
    if safe(l) == 1: return 1

    for i,x in enumerate(l):
        new_l = l.copy()
        del new_l[i]
        if safe(new_l):
            return 1

    return 0

def run():
    data = input_lines(lambda x: [int(y) for y in x.split(' ')])
    print(sum([safe(y) for y in data]))
    print(sum([damp_safe(y) for y in data]))

run()