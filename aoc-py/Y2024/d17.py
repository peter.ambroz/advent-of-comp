"""
Part 1 - hand-compiled from instructions into python code
Part 2 - iterative guessing of octal digits that produce desired output
"""

def part_1(a: int) -> list[int]:
    output = []
    while True:
        b = (a % 8) ^ 1
        c = a // (2**b)
        b = b ^ 5 ^ c
        a = a // 8
        output.append(b % 8)
        if a == 0:
            break
    return output

expected = [2, 4, 1, 1, 7, 5, 1, 5, 0, 3, 4, 4, 5, 5, 3, 0]

print(part_1(23999685))

# part 2
num = 0
i = len(expected) - 1
while i >= 0:
    ex = expected[i:]
    for j in range(8):
        pt1 = part_1(num + j)
        if pt1 == ex:
            num = (num + j) * 8
            break
    else:
        num += 8
        i += 1
    i -= 1
num //= 8

print(num)
