from common.file import input_lines

def parse(suffix: str = '', part2: bool = False) -> tuple[list, list, str]:
    parse_room = True
    start, room, moves = [0, 0], [], ''
    for j, line in enumerate(input_lines(suffix=suffix)):
        if parse_room:
            if len(line) <= 0:
                parse_room = False
                continue
            if part2:
                line = line.replace('#','##').replace('O','[]').replace('.','..').replace('@','@.')
            room.append(list(line))
            if (sx:=line.find('@')) >= 0:
                start = [sx, j]
        else: # parse moves
            moves += line
    return start, room, moves

def can_move(pos: list, room: list, dir: str) -> bool:
    x, y = pos[0] + V[dir][0], pos[1] + V[dir][1]
    match room[y][x]:
        case 'O':
            return can_move([x,y], room, dir)
        case '[':
            if dir == '>':
                return can_move([x+1,y], room, dir)
            return can_move([x+1,y], room, dir) & can_move([x,y], room, dir)
        case ']':
            if dir == '<':
                return can_move([x-1,y], room, dir)
            return can_move([x-1,y], room, dir) & can_move([x, y], room, dir)
        case '.':
            return True
    return False

def do_move(pos: list, room: list, dir: str) -> list:
    x, y = pos[0] + V[dir][0], pos[1] + V[dir][1]
    match room[y][x]:
        case 'O':
            do_move([x,y], room, dir)
        case '[':
            do_move([x+1,y], room, dir)
            do_move([x,y], room, dir)
        case ']':
            do_move([x-1,y], room, dir)
            do_move([x, y], room, dir)
        case '.':
            pass
        case _:
            return pos

    room[y][x] = room[pos[1]][pos[0]]
    room[pos[1]][pos[0]] = '.'
    return [x, y]


V = {'^': [0, -1], 'v': [0, 1], '<': [-1, 0], '>': [1, 0]}
position, room, moves = parse()
position2, room2, _ = parse(part2=True)
for move in moves:
    if can_move(position, room, move):
        position = do_move(position, room, move)
    if can_move(position2, room2, move):
        position2 = do_move(position2, room2, move)

print(sum([100*j + i for j,row in enumerate(room) for i,c in enumerate(row) if c == 'O']))
print(sum([100*j + i for j,row in enumerate(room2) for i,c in enumerate(row) if c == '[']))