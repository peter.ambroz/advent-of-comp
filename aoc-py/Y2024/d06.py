from common.file import input_lines

def get(x: int, y: int) -> str:
    if x == extra_x and y == extra_y:
        return "#"
    if (0 <= y < len(map)) and (0 <= x < len(map[0])):
        return map[y][x]
    return ""

def move() -> bool:
    global p_x, p_y
    global dir, visit
    next_x, next_y = p_x + V[dir][0], p_y + V[dir][1]
    visit[f"{p_y}:{p_x}"] = True
    match get(next_x, next_y):
        case "#":
            dir = (dir + 1) % 4
        case "":
            return False
        case _:
            p_x, p_y = next_x, next_y

    return True

V = [[0, -1], [1, 0], [0, 1], [-1, 0]]
map = input_lines()
visit, dir = {}, 0
extra_x, extra_y = -1, -1

# Find guard starting position
for j, row in enumerate(map):
    i = row.find('^')
    if not i == -1:
        p_x, p_y = i, j
        break

start_x, start_y = p_x, p_y

# Solve for part 1
while move(): pass
print(len(visit))

# Solve for part 2
wall_positions = 0
for j, row in enumerate(map):
    for i, c in enumerate(row):
        if i == start_x and j == start_y:
            continue
        dir, steps = 0, 0
        extra_x, extra_y = i, j
        p_x, p_y = start_x, start_y
        while True:
            if move(): # guard proceeds to move
                steps += 1
                if steps > 17000: # very generous cycle detection, but runs under 2 mins, so meh..
                    wall_positions += 1
                    break
                continue
            else: # guard exited, try another wall placement
                break

print(wall_positions)