from collections import defaultdict
from time import sleep

from common.file import input_lines

SX, SY = 101, 103
HX, HY = 50, 51

def process(line: str) -> dict:
    p, v = [x[2:] for x in line.split(' ')]
    return {
        'p': [int(x) for x in p.split(',')],
        'v': [int(x) for x in v.split(',')],
    }

def simulate(robot: dict, steps: int) -> list:
    return [
        (robot['p'][0] + steps * robot['v'][0]) % SX,
        (robot['p'][1] + steps * robot['v'][1]) % SY,
    ]

def quadrant(pos: list) -> int:
    if pos[0] == HX or pos[1] == HY:
        return -1
    return (2 if pos[0] > HX else 0) + (1 if pos[1] > HY else 0)

def display(positions: list[list]) -> None:
    buffer = [['.' for _ in range(SX)] for _ in range(SY)]
    for p in positions:
        buffer[p[1]][p[0]] = '#'
    for row in buffer:
        for c in row:
            print(c, end='')
        print()

data = [quadrant(simulate(x, 100)) for x in input_lines(process)]
print(data.count(0) * data.count(1) * data.count(2) * data.count(3))

"""
Part 2
Here I visualized every step. Then I noticed that the pattern looks interesting
at steps 16, 119, 222, .. with increments of 103. So I changed the simulation
to only show in increments of 103, starting with 16. In a few seconds I spotted
the picture, noted down the exact step number (8050 in my case), and submitted.
"""
robots = input_lines(process)
for i in range(10000):
    print(i)
    display([simulate(x, i) for x in robots])
    sleep(0.1)
    print()