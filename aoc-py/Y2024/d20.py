from common.file import input_lines

def calc_distances():
    pos, steps = start, 0
    while True:
        steps += 1
        for dir in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
            x, y = pos[0] + dir[0], pos[1] + dir[1]
            if track[y][x] != '#' and dist.get((x, y), -1) == -1:
                dist[x, y] = steps
                pos = x, y
                if track[y][x] == 'E':
                    return
                break

def cheat(px: int, py: int) -> int:
    if track[py][px] != '#':
        return 0
    for dir in [(1, 0), (0, 1)]:
        s1, s2 = (px + dir[0], py + dir[1]), (px - dir[0], py - dir[1])
        d1, d2 = dist.get(s1, -1), dist.get(s2, -1)
        if d1 == -1 or d2 == -1:
            continue
        if abs(d1 - d2) - 2 >= 100:
            return 1
    return 0

def cheat_more(px: int, py: int) -> int:
    if track[py][px] == '#':
        return 0
    savings = 0
    for y in range(max(py-20, 1), min(py+21, len(track))):
        for x in range(max(px-20, 1), min(px+21, len(track[0]))):
            cheat_size = abs(x - px) + abs(y - py)
            if cheat_size > 20:
                continue
            d1, d2 = dist.get((px, py), -1), dist.get((x, y), -1)
            if d1 == -1 or d2 == -1:
                continue
            if d1 - d2 - cheat_size >= 100:
                savings += 1
    return savings

track = input_lines()
dist, start = {}, (0, 0)
for y, line in enumerate(track):
    for x, c in enumerate(line):
        if c == 'S':
            start = x, y
            dist[x, y] = 0

calc_distances()
print(sum([cheat(x, y) for y in range(1, len(track)-1) for x in range(1, len(track)-1)]))
print(sum([cheat_more(x, y) for y in range(1, len(track)-1) for x in range(1, len(track)-1)]))
