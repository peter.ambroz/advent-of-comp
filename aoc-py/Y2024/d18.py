from common.file import input_lines

SX, SY, LOAD = 71, 71, 1024
begin, end = (0, 0), (SX - 1, SY - 1)

def bfs(start, goal) -> list|None:
    explored = []
    queue = [[start]]
    while queue:
        path = queue.pop(0)
        node = path[-1]
        if node not in explored:
            for dir in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
                npos = (node[0] + dir[0], node[1] + dir[1])
                if 0 <= npos[0] < SX and 0 <= npos[1] < SY and npos not in memory:
                    new_path = list(path)
                    new_path.append(npos)
                    queue.append(new_path)
                    if npos == goal:
                        return new_path
            explored.append(node)
    return None

memory = set()
for i, line in enumerate(input_lines()):
    if i == LOAD:
        break
    memory.add(tuple(int(n) for n in line.split(',')))
# part 1
print(len(bfs(begin, end)) - 1)
# part 2
for i, line in enumerate(input_lines()):
    if i < LOAD:
        continue
    memory.add(tuple(int(n) for n in line.split(',')))
    if i >= 2700 and bfs(begin, end) is None:
        print(line)
        break