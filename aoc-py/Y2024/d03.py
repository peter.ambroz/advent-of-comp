from common.file import input_lines
import re

mul_enabled = True

def calc(mul: str) -> int:
    global mul_enabled
    if mul == "do()":
        mul_enabled = True
        return 0

    if mul == "don't()":
        mul_enabled = False
        return 0

    if not mul_enabled:
        return 0

    data = re.findall(r'\((.*),(.*)\)', mul)
    return int(data[0][0]) * int(data[0][1])

def run():
    regex = [
        r'mul\(\d{1,3},\d{1,3}\)',
        r'mul\(\d{1,3},\d{1,3}\)|do\(\)|don\'t\(\)',
    ]

    for regex_part in regex:
        muls = [item for row in input_lines(lambda x: re.findall(regex_part, x)) for item in row]
        print(sum([calc(x) for x in muls]))

run()