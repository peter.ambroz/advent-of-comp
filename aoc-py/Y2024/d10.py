from common.file import input_lines

def path(pos: tuple) -> list:
    result = []
    c = int(data[pos[1]][pos[0]])
    if c == 9:
        return [pos]
    for dir in [(0, -1), (1, 0), (0, 1), (-1, 0)]:
        x, y = pos[0] + dir[0], pos[1] + dir[1]
        if 0 <= x < len(data[0]) and 0 <= y < len(data) and int(data[y][x]) == c + 1:
            result += path((x, y))
    return result

data = input_lines()
zeroes = [(i, j) for j, row in enumerate(data) for i, c in enumerate(row) if c == '0']
paths = [path(z) for z in zeroes]
print(sum(len(set(x)) for x in paths)) # part 1
print(sum(len(x) for x in paths)) # part 2
