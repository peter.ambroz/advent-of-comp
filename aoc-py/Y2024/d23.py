from collections import defaultdict
from common.file import input_lines

def has_t(nodes: list[str]) -> bool:
    for node in nodes:
        if node.startswith('t'):
            return True
    return False

nodes = defaultdict(list)
edges = []
for line in input_lines(lambda l: l.split('-')):
    nodes[line[0]].append(line[1])
    nodes[line[1]].append(line[0])
    edges.append({line[0], line[1]})

tri = set()
for n, e in nodes.items():
    for v1, v2 in edges:
        if v1 in e and v2 in e:
            x = [n, v1, v2]
            if has_t(x):
                tri |= {tuple(sorted(x))}
print(len(tri))
# part 2
max_party = {}
max_len = 0
for seed in nodes:
    party = {seed}
    for n, e in nodes.items():
        for p in party:
            if p not in e:
                break
        else:
            party.add(n)
    if len(party) > max_len:
        max_len = len(party)
        max_party = party

print(','.join(sorted(max_party)))