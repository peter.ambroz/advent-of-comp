from functools import cache
from common.file import input_lines

@cache
def process(stone: str, n: int) -> int:
    if n < 1:
        return 1
    s, l = 0, len(stone)
    if stone == '0':
        s += process('1', n - 1)
    elif l % 2 == 0:
        s += process(str(int(stone[:l//2])), n - 1)
        s += process(str(int(stone[l//2:])), n - 1)
    else:
        s += process(str(int(stone) * 2024), n - 1)

    return s

data = input_lines()[0].split(' ')
print(sum([process(x, 25) for x in data]))
print(sum([process(x, 75) for x in data]))