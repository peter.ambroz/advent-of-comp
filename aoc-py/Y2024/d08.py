from collections import defaultdict
from common.file import input_lines

data = input_lines()
anti1, anti2 = {}, {}
antennae = defaultdict(list)
for j, row in enumerate(data):
    for i, c in enumerate(row):
        if not c == '.':
            antennae[c].append((i, j))

for k, v in antennae.items():
    for (i, j) in v:
        for (x, y) in v:
            if not (i, j) == (x, y):
                dx, dy = i - x, j - y
                anti_x1, anti_y1 = i + dx, j + dy
                anti_x2, anti_y2 = x - dx, y - dy
                if 0 <= anti_x1 < len(data) and 0 <= anti_y1 < len(data):
                    anti1[f"{anti_x1}:{anti_y1}"] = True
                if 0 <= anti_x2 < len(data) and 0 <= anti_y2 < len(data):
                    anti1[f"{anti_x2}:{anti_y2}"] = True

                for z in range(-len(data), len(data)):
                    anti_x, anti_y = dx*z + x, dy*z + y
                    if 0 <= anti_x < len(data) and 0 <= anti_y < len(data):
                        anti2[f"{anti_x}:{anti_y}"] = True

print(len(anti1), len(anti2))