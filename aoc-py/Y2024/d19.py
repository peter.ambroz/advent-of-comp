from functools import cache
from common.file import input_lines

@cache
def make_design(design: str) -> int:
    if len(design) == 0:
        return 1
    found = 0
    for t in patterns:
        if design.startswith(t):
            found += make_design(design[len(t):])
    return found

data = input_lines()
patterns = data[0].split(', ')
print(sum([1 for d in data[2:] if make_design(d)]))
print(sum([make_design(d) for d in data[2:]]))