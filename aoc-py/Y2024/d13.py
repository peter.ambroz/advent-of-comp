from common.file import input_lines

# process lines into a list of equation systems
# each 4 lines define 1 system of equations with 2 unknowns (A, B)
def process_input(bias: int = 0, suffix: str = '') -> list:
    data = []
    system = [[0, 0, 0], [0, 0, 0]]
    for line in input_lines(suffix=suffix):
        parts = line.split(' ')
        match len(parts):
            case 1:
                data.append(system)
                system = [[0, 0, 0], [0, 0, 0]]
            case 3: # prize
                system[0][2] = int(parts[1][2:-1]) + bias
                system[1][2] = int(parts[2][2:]) + bias
            case 4: # button
                if parts[1] == 'A:':
                    system[0][0] = int(parts[2][2:-1])
                    system[1][0] = int(parts[3][2:])
                elif parts[1] == 'B:':
                    system[0][1] = int(parts[2][2:-1])
                    system[1][1] = int(parts[3][2:])

    return data

# Solve system of 2 equations with 2 unknowns by matrix elimination
def solve(s: list) -> tuple[int, int]:
    coef = s[0][1] / s[1][1]
    dividend = s[0][0] - coef * s[1][0]
    # equations are colinear, prevent division by 0
    if abs(dividend) < 0.0001:
        return 0, 0
    a = (s[0][2] - coef * s[1][2]) / dividend
    b = (s[0][2] - a * s[0][0]) / s[0][1]
    # accept only integer solutions
    if abs(a - round(a)) < 0.01 and abs(b - round(b)) < 0.01:
        return round(a), round(b)
    return 0, 0

# part 1
cost = 0
for system in process_input():
    a, b = solve(system)
    if 0 <= a <= 100 and 0 <= b <= 100:
        cost += 3*a + b
print(cost)

# part 2
cost = 0
for system in process_input(10000000000000):
    a, b = solve(system)
    if 0 <= a and 0 <= b:
        cost += 3*a + b
print(cost)