from common.file import input_lines

def get(x: int, y: int) -> str:
    if (0 <= y < len(data)) and (0 <= x < len(data)):
        return data[y][x]
    return ""

def find(x: int, y: int) -> None:
    global cnt1
    word = "XMAS"
    if get(x, y) != word[0]:
        return
    for yd in range(-1, 2):
        for xd in range(-1, 2):
            for l in range(1, len(word)):
                if get(x + xd * l, y + yd * l) != word[l]:
                    break
            else:
                cnt1 += 1

def find2(x: int, y: int) -> None:
    global cnt2
    if get(x, y) != "A":
        return
    hash = get(x-1, y-1) + get(x+1, y-1) + get(x-1, y+1) + get(x+1, y+1)
    if hash in ["MMSS", "SSMM", "MSMS", "SMSM"]:
        cnt2 += 1

data = input_lines()
cnt1, cnt2 = (0, 0)
[[[find(x, y), find2(x, y)] for x, c in enumerate(row)] for y, row in enumerate(data)]

print(cnt1, cnt2)