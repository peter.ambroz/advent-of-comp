from common.file import input_lines

class Block:
    def __init__(self, id: int, file: bool, position: int, length: int):
        self.id = id
        self.file = file
        self.position = position
        self.length = length

    def value(self) -> int:
        return (self.id * self.length * (2 * self.position + self.length - 1)) // 2 if self.file else 0

data = input_lines()[0]

# part1
chksum, position = 0, 0
i, end = 0, len(data) - 1
end_block_length, end_id = 0, 0

while i <= end:
    id = i // 2
    block_length = int(data[i])
    # count the current file block checksum
    chksum += (id * block_length * (2 * position + block_length - 1)) // 2
    position += block_length

    free_length = int(data[i + 1])
    if id + 1 == end_id: # last id from the beginning has been checksummed, the end is now free
        free_length = end_block_length

    while free_length > 0 and i <= end:
        if end_block_length == 0:
            end_id = end // 2
            end_block_length = int(data[end])
            end -= 2

        process_length = min(free_length, end_block_length)
        free_length -= process_length
        end_block_length -= process_length
        # count checksum of the part that fits in
        chksum += (end_id * process_length * (2 * position + process_length - 1)) // 2
        position += process_length

    i += 2

print(chksum)

# part2
blocks = []
position = 0
for i,c in enumerate(data):
    length = int(c)
    blocks.append(Block(
        i // 2,
        i % 2 == 0,
        position,
        length,
    ))
    position += length

for b in reversed(blocks):
    if b.file:
        for f in blocks:
            if not f.file and f.length >= b.length and f.position < b.position:
                b.position = f.position
                f.length -= b.length
                f.position += b.length

print(sum([b.value() for b in blocks]))
