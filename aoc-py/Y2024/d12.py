from collections import defaultdict
from functools import cmp_to_key
from common.file import input_lines

def explore(x: int, y: int, letter: str, id: int) -> bool:
    if not (0 <= x < len(data[0]) and 0 <= y < len(data)) or not letter == data[y][x]:
        return True
    if visited.get((x, y), False):
        return False
    visited[(x, y)] = True
    map[id]['area'] += 1
    for dir, dx, dy in [('r', 1, 0), ('d', 0, 1), ('l', -1, 0), ('u', 0, -1)]:
        if explore(x + dx, y + dy, letter, id):
            map[id]['perimeter'] += 1
            map[id]['border'] |= {(dir, x, y)} if dir in ['l', 'r'] else {(dir, y, x)}

    return False

def compare(k1: tuple, k2: tuple) -> int:
    if not k1[0] == k2[0]:
        return ord(k1[0]) - ord(k2[0])
    if not k1[1] == k2[1]:
        return k1[1] - k2[1]
    return k1[2] - k2[2]

id = -1
visited = {}
map = defaultdict(lambda: {'area': 0, 'perimeter': 0, 'border': set()})
data = input_lines()
for j, row in enumerate(data):
    for i, c in enumerate(row):
        if not visited.get((i, j), False):
            explore(i, j, c, id := id + 1)
# part1
print(sum([a['area'] * a['perimeter'] for a in map.values()]))

# part2
sum = 0
for a in map.values():
    last_dir, last_x, last_y = '!', -1, -1
    perimeter = 0
    for dir, x, y in sorted(a['border'], key=cmp_to_key(compare)):
        if dir != last_dir or x != last_x or y - last_y != 1:
            perimeter += 1
            last_dir, last_x = dir, x
        last_y = y
    sum += perimeter * a['area']
print(sum)