from common.file import input_lines

def is_correct(pages: list) -> bool:
    for i, p1 in enumerate(pages):
        for j, p2 in enumerate(pages):
            if i < j and [p2, p1] in rules:
                return False
    return True

def correct(pages: list) -> int:
    relevant_rules = [r[0] for r in rules if r[0] in pages and r[1] in pages]
    middle = [x for x in pages if relevant_rules.count(x) == len(pages)//2]
    return middle[0]

data = input_lines()
_sep = data.index('')
rules = [[int(x) for x in r.split('|')] for r in data[0:_sep]]
manuals = [[int(x) for x in m.split(',')] for m in data[_sep+1:]]

print(sum([m[len(m)//2] for m in manuals if is_correct(m)]))
print(sum(correct(m) for m in manuals if not is_correct(m)))
