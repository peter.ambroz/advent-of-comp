from common.file import input_lines

keys, locks, type = [], [], '?'
buffer = [0 for _ in range(5)]
for line in input_lines():
    if line == '':
        if type == 'key':
            keys.append(buffer)
        else:
            locks.append(buffer)
        type = '?'
        buffer = [0 for _ in range(5)]
        continue
    if type == '?':
        type = 'key' if line[0] == '.' else 'lock'
        continue
    for i, c in enumerate(line):
        if c == '#':
            buffer[i] += 1

fit = 0
for k in keys:
    for l in locks:
        for i in range(5):
            if k[i] + l[i] > 6:
                break
        else:
            fit += 1
print(fit)