from collections import defaultdict
from common.file import input_lines

def next(num: int) -> int:
    num ^= (num * 64) % 16777216
    num ^= (num // 32) % 16777216
    return num ^ ((num * 2048) % 16777216)

pt1 = 0
pt2 = defaultdict(dict)
for j, num in enumerate(input_lines(lambda x: int(x))):
    prev = num % 10
    diffs = []
    for i in range(2000):
        num = next(num)
        cur = num % 10
        diffs.append(cur - prev)
        if i >= 3:
            key = tuple(diffs[i-3:i+1])
            monkeys = pt2[key]
            if monkeys.get(j, 0) == 0:
                monkeys[j] = cur
        prev = cur
    pt1 += num

print(pt1)
key = max(pt2, key=lambda x: sum(pt2[x].values()))
print(sum(pt2[key].values()))