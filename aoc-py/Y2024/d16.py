# I didn't enjoy this one
import sys
import heapq
from common.file import input_lines

def solve(pos: tuple[int, int], cur_dir: int = 0, cost: int = 0):
    visited[pos] = cost
    if pos == end_pos:
        return
    for dir, dx, dy in [[0, 1, 0], [1, 0, -1], [2, -1, 0], [3, 0, 1]]:
        nx, ny = pos[0] + dx, pos[1] + dy
        if maze[ny][nx] != '#':
            new_cost = cost + 1
            if abs(dir - cur_dir) == 2:
                new_cost += 2000
            elif dir != cur_dir:
                new_cost += 1000
            if new_cost <= visited.get((nx, ny), 200000):
                solve((nx, ny), dir, new_cost)


sys.setrecursionlimit(10**5)
maze = input_lines(suffix='')
start_pos = (1, len(maze) - 2)
end_pos = (len(maze[0]) - 2, 1)

visited = {}
solve(start_pos)
print(visited[end_pos])

visited = {}
pq = [(0, (*start_pos, 1), [start_pos])]
paths = []
best_score = sys.maxsize

while pq and pq[0][0] <= best_score:
    score, (x, y, dir), path = heapq.heappop(pq)

    if (x, y) == end_pos:
        best_score = score
        paths.append(path)
        continue

    if visited.get((x, y, dir), sys.maxsize) < score:
        continue
    visited[x, y, dir] = score

    for i, dx, dy in [(0, -1, 0), (1, 0, 1), (2, 1, 0), (3, 0, -1)]:
        nx, ny = x + dx, y + dy
        if maze[ny][nx] != '#' and (nx, ny) not in path:
            cost = 1 if i == dir else 1001
            heapq.heappush(pq, (score + cost, (nx, ny, i), path + [(nx, ny)]))

places = set()
for path in paths:
    places |= set(path)
print(len(places))

