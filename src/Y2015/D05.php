<?php
declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D05 extends Day
{
    public function verify(string $line): int
    {
        $vow = 0;
        $dd = false;
        $fb = false;

        $prev = '';
        for ($i = 0; $i < strlen($line); $i++) {
            $c = $line[$i];
            if (in_array($c, ['a', 'e', 'i', 'o', 'u'])) {
                $vow++;
            }
            if ($c === $prev) {
                $dd = true;
            }
            $prev = $c;
            if ($i > 0) {
                $cc = $line[$i-1] . $c;
                if (in_array($cc, ['ab', 'cd', 'pq', 'xy'])) {
                    $fb = true;
                }
            }
        }

        return ($vow >= 3 && $dd && !$fb) ? 1 : 0;
    }

    public function verify2(string $line): int
    {
        $dif = false;
        $ob = false;
        for ($i = 0; $i < strlen($line) - 3; $i++) {
            $diftong1 = $line[$i] . $line[$i+1];
            for ($j = $i+2; $j < strlen($line) - 1; $j++) {
                $diftong2 = $line[$j] . $line[$j+1];
                if ($diftong1 === $diftong2) {
                    $dif = true;
                    break 2;
                }
            }
        }

        for ($i = 0; $i < strlen($line) - 2; $i++) {
            if ($line[$i] === $line[$i+2]) {
                $ob = true;
                break;
            }
        }

        return ($dif && $ob) ? 1 : 0;
    }

    public function run(): void
    {
        printf("%d\n", array_sum($this->inputLines([$this, 'verify'])));
    }

    public function runB(): void
    {
        printf("%d\n", array_sum($this->inputLines([$this, 'verify2'])));
    }
}