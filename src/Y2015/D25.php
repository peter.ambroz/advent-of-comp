<?php
declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D25 extends Day
{
    public function run(): void
    {
        $row = 2981;
        $column = 3075;
        $mul = 252533;
        $mod = 33554393;
        $init = 20151125;

        $inc = 1;
        $num = 1;
        for ($j = 2; $j <= $row; $j++) {
            $num += $inc;
            $inc++;
        }
        $inc++;
        for ($i = 2; $i <= $column; $i++) {
            $num += $inc;
            $inc++;
        }

        printf("%d\n", $num); // 18331560
        $num--;

        $q = $mul;
        $muls = [];
        $bnum = decbin($num);
        foreach (str_split(strrev($bnum)) as $i => $coef) {
            if ($i > 0) {
                $q = ($q * $q) % $mod;
            }
            if ($coef === '1') {
                $muls[$i] = $q;
            } else {
                $muls[$i] = 1;
            }
        }

        printf("%d\n", $prod = array_reduce($muls, function(int $c, int $x) use ($mod): int {
            return ($c * $x) % $mod;
        }, 1));

        printf("%d\n", ($prod * $init) % $mod);
    }
}