<?php
declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D04 extends Day
{
    public function run(): void
    {
        $input = 'iwrupvqb';
        $pwd = '';
        $c = 0;

        while (strlen($pwd) < 1) {
            $c++;
            $h = md5($input . $c);
            if (substr($h, 0, 6) === '000000') {
                $pwd .= $h[6];
            }
        }

        printf("%d\n", $c);

    }
}