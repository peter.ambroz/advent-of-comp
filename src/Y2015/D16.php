<?php

declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D16 extends Day
{
    public function run(): void
    {
        $query = [
            'children' => 3,
            'cats' => 7,
            'samoyeds' => 2,
            'pomeranians' => 3,
            'akitas' => 0,
            'vizslas' => 0,
            'goldfish' => 5,
            'trees' => 3,
            'cars' => 2,
            'perfumes' => 1,
        ];

        $index = [];

        foreach ($this->inputLines() as $lineNo => $line) {
            foreach (explode(', ', $line, 3) as $part) {
                [$name, $count] = explode(': ', $part, 2);
                $count = (int) $count;

                if (!isset($index[$name])) {
                    $index[$name] = [];
                }

                if (!isset($index[$name][$count])) {
                    $index[$name][$count] = [];
                }

                $index[$name][$count][] = $lineNo;
            }
        }

        $results = [];

        foreach ($query as $qName => $qItems) {
            foreach ($index[$qName][$qItems] as $lady) {
                $results[$lady] = ($results[$lady] ?? 0) + 1;
            }
        }

        asort($results);

        var_dump($results);
    }

    public function runB(): void
    {
        $query = [
            'children' => 3,
            'cats' => 7, // +
            'samoyeds' => 2,
            'pomeranians' => 3, // -
            'akitas' => 0,
            'vizslas' => 0,
            'goldfish' => 5, // -
            'trees' => 3, // +
            'cars' => 2,
            'perfumes' => 1,
        ];

        $data = [];

        foreach ($this->inputLines() as $lineNo => $line) {
            $d = [];

            foreach (explode(', ', $line, 3) as $part) {
                [$name, $count] = explode(': ', $part, 2);
                $count = (int) $count;
                $d[$name] = $count;
            }

            $data[$lineNo] = $d;
        }

        foreach ($data as $lady => $datum) {
            foreach ($datum as $item => $value) {
                $match = match ($item) {
                    'cats', 'trees' => ($query[$item] < $value),
                    'pomeranians', 'goldfish' => ($query[$item] > $value),
                    default => ($query[$item] === $value),
                };

                if (!$match) {
                    break;
                }
            }

            if ($match) {
                printf("Lady %d: [%s] = [%s]\n", $lady + 1, implode(', ', array_keys($datum)), implode(', ', array_values($datum)));
            }
        }
    }
}