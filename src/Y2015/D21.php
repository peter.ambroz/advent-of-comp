<?php

declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D21 extends Day
{
    private const BOSS_HP = 100;
    private const BOSS_DAM = 8;
    private const BOSS_ARM = 2;

    private const SHOP = [
        'weapon' => [
            4 => 8,
            5 => 10,
            6 => 25,
            7 => 40,
            8 => 74,
        ],
        'armor' => [
            1 => 13,
            2 => 31,
            3 => 53,
            4 => 75,
            5 => 102,
        ],
        'ringw' => [
            1 => 25,
            2 => 50,
            3 => 100,
        ],
        'ringa' => [
            1 => 20,
            2 => 40,
            3 => 80,
        ],
    ];

    public function run(): void
    {
        for ($dam = 0; $dam <= 13; $dam++) {
            for ($arm = 0; $arm <= 10; $arm++) {
                if ($this->fight(100, $dam, $arm)) {
                    printf("DAM %d ; ARM %d\n", $dam, $arm);
                    break;
                }
            }
        }
    }

    public function runB(): void
    {
        var_dump($this->fight(100, 4, 5));

        for ($dam = 13; $dam >= 4; $dam--) {
            for ($arm = 10; $arm >= 0; $arm--) {
                if (!$this->fight(100, $dam, $arm)) {
                    printf("DAM %d ; ARM %d\n", $dam, $arm);
                    break;
                }
            }
        }
    }

    private function fight(int $hp, int $dam, int $arm): bool
    {
        $pl = ['hp' => $hp, 'd' => $dam, 'a' => $arm];
        $bo = ['hp' => self::BOSS_HP, 'd' => self::BOSS_DAM, 'a' => self::BOSS_ARM];

        while (true) {
            $pdam = max($pl['d'] - $bo['a'], 1);
            $bhp = $bo['hp'] - $pdam;
            if ($bhp <= 0) {
                //printf("Player won with %d HP\n", $pl['hp']);
                return true;
            }
            $bo['hp'] = $bhp;

            $bdam = max($bo['d'] - $pl['a'], 1);
            $plhp = $pl['hp'] - $bdam;
            if ($plhp <= 0) {
                //printf("Boss won with %d HP\n", $bo['hp']);
                return false;
            }
            $pl['hp'] = $plhp;
        }
    }
}

/*

DAM 9 ; ARM 0
DAM 8 ; ARM 1
DAM 7 ; ARM 2 = 148 (more!)
DAM 6 ; ARM 3 = 138
DAM 5 ; ARM 4 = 110 / 126
DAM 4 ; ARM 5 = 128

 */