<?php

declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D09 extends Day
{
    private array $d = [];
    private array $list = [];

    public function parse(string $line): int
    {
        [$from, $to, $dist] = explode(' ', $line);
        if (!isset($this->d[$from])) {
            $this->d[$from] = [$from => 1000];
        }
        if (!isset($this->d[$to])) {
            $this->d[$to] = [$to => 1000];
        }

        $this->d[$from][$to] = (int)$dist;
        $this->d[$to][$from] = (int)$dist;

        return 1;
    }

    private function perm($items, $perms = [])
    {
        if (empty($items)) {
            $this->list[] = $perms;
        } else {
            for ($i = count($items)-1; $i >= 0; --$i) {
                $newitems = $items;
                $newperms = $perms;
                [$foo] = array_splice($newitems, $i, 1);
                array_unshift($newperms, $foo);
                $this->perm($newitems, $newperms);
            }
        }
        return $this->list;
    }

    public function run(): void
    {
        $this->inputLines([$this, 'parse']);
        $cities = array_keys($this->d);

        $min = 9999999;
        $minp = '';
        foreach ($this->perm($cities) as $item) {
            $l = 0;
            for ($i = 0; $i < 7; $i++) {
                $l += $this->d[$item[$i]][$item[$i+1]];
            }
            if ($l < $min) {
                $min = $l;
                $minp = implode('', $item);
            }
        }

        printf("%d %s\n", $min, $minp);
    }

    public function runB(): void
    {
        $this->inputLines([$this, 'parse']);
        $cities = array_keys($this->d);

        $max = -1;
        $maxp = '';
        foreach ($this->perm($cities) as $item) {
            $l = 0;
            for ($i = 0; $i < 7; $i++) {
                $l += $this->d[$item[$i]][$item[$i+1]];
            }
            if ($l > $max) {
                $max = $l;
                $maxp = implode('', $item);
            }
        }

        printf("%d %s\n", $max, $maxp);
    }
}