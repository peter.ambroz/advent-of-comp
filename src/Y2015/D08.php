<?php
declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D08 extends Day
{
    private int $total = 0;
    private int $real = 0;
    private int $bloat = 0;

    public function process(string $line): int
    {
        $this->total += strlen($line);
        $this->real += strlen(eval('return ' . $line . ';'));
//        printf("%s %d %d\n", $line, strlen($line), strlen(eval('return ' . $line . ';')));
        return 1;
    }

    public function processB(string $line): int
    {
        $this->total += strlen($line);
        $this->bloat += strlen(addslashes($line)) + 2;
        printf("%s %d %d\n", $line, strlen($line), strlen(addslashes($line)) + 2);
        return 1;
    }

    public function run(): void
    {
        $this->inputLines([$this, 'process']);
        printf("%d\n", $this->total - $this->real);
    }

    public function runB(): void
    {
        $this->inputLines([$this, 'processB']);
        printf("%d\n", $this->bloat - $this->total);
    }
}