<?php
declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D03 extends Day
{
    public function run(): void
    {
        $x = 0;
        $y = 0;
        $a = ['0;0' => 1];
        $instr = str_split(file_get_contents($this->inputFileName()));

        foreach ($instr as $dir) {
            switch ($dir) {
                case '^':
                    $y--;
                    break;
                case '>':
                    $x++;
                    break;
                case 'v':
                    $y++;
                    break;
                case '<':
                    $x--;
                    break;
            }

            $idx = $x . ';' . $y;
            if (!isset($a[$idx])) {
                $a[$idx] = 1;
            } else {
                $a[$idx]++;
            }
        }

        printf("%d\n", count($a));
    }

    public function runB(): void
    {
        $x = [0, 0];
        $y = [0, 0];
        $a = ['0;0' => 2];
        $instr = str_split(file_get_contents($this->inputFileName()));

        foreach ($instr as $par => $dir) {
            $p = $par % 2;
            switch ($dir) {
                case '^':
                    $y[$p]--;
                    break;
                case '>':
                    $x[$p]++;
                    break;
                case 'v':
                    $y[$p]++;
                    break;
                case '<':
                    $x[$p]--;
                    break;
            }

            $idx = $x[$p] . ';' . $y[$p];
            if (!isset($a[$idx])) {
                $a[$idx] = 1;
            } else {
                $a[$idx]++;
            }
        }

        printf("%d\n", count($a));
    }
}