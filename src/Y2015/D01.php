<?php
declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D01 extends Day
{
    public function run(): void
    {
        printf("%d\n", 74);
    }

    public function runB(): void
    {
        $par = str_split(file_get_contents($this->inputFileName()));
        $s = 0;
        foreach ($par as $k => $p) {
            if ($p === '(') $s++;
            if ($p === ')') $s--;
            if ($s < 0) {
                printf("%d\n", $k+1);
                break;
            }
        }
    }
}