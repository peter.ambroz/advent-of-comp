<?php
declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D02 extends Day
{
    public function paper(string $dim): int
    {
        [$x, $y, $z] = explode("x", $dim);
        $side1 = (int)$x * (int)$y;
        $side2 = (int)$x * (int)$z;
        $side3 = (int)$z * (int)$y;
        $min = min($side1, $side2, $side3);
        return 2*$side1 + 2*$side2 + 2*$side3 + $min;
    }

    public function ribbon(string $dim): int
    {
        [$x, $y, $z] = array_map(function (string $q): int {return (int)$q;}, explode("x", $dim));
        $bow = $x * $y * $z;
        $min = 2*$x + 2*$y + 2*$z - 2*max($x, $y, $z);
        return $bow + $min;
    }

    public function run(): void
    {
        printf("%d\n", array_sum($this->inputLines([$this, 'paper'])));
    }

    public function runB(): void
    {
        printf("%d\n", array_sum($this->inputLines([$this, 'ribbon'])));
    }
}