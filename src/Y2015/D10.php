<?php
declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D10 extends Day
{
    private function iter(string $txt): string
    {
        $ns = '';
        $len = strlen($txt);
        $lc = $txt[0];
        $lcc = 0;
        for ($i = 0; $i < $len; $i++) {
            $cc = $txt[$i];
            if ($cc === $lc) {
                $lcc++;
            } else {
                $ns .= $lcc . $lc;
                $lc = $cc;
                $lcc = 1;
            }
        }
        $ns .= $lcc . $lc;

        return $ns;
    }

    public function run(): void
    {
        $txt = '1113122113';
        for ($j = 0; $j < 40; $j++) {
            $txt = $this->iter($txt);
//            printf("%s\n", $txt);
        }
        printf("%d\n", strlen($txt));
    }

    public function runB(): void
    {
        $txt = '1113122113';
        for ($j = 0; $j < 50; $j++) {
            $txt = $this->iter($txt);
        }
        printf("%d\n", strlen($txt));
    }
}