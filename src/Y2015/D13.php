<?php

declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D13 extends Day
{
    public array $g = [];

    public function run(): void
    {
        $this->parse();
        foreach ($this->g as $p1 => $rel) {
            printf("%s\n", $p1);
            foreach ($rel as $p2 => $value) {
                printf("  %s: %d\n", $p2, $value);
            }
            printf("\n");
        }
    }

    private function parse()
    {
        foreach ($this->inputLines() as $line) {
            [$p1, , $dir, $amount, , , , , , , $p2] = explode(' ', $line);

            $amount = (int) $amount;
            if ($dir === 'lose') {
                $amount *= -1;
            }

            if (!isset($this->g[$p1])) {
                $this->g[$p1] = [];
            }

            $this->g[$p1][$p2] = $amount;

            if (isset($this->g[$p2][$p1])) {
                $this->g[$p1][$p2] += $this->g[$p2][$p1];
                $this->g[$p2][$p1] += $amount;
            }
        }
    }
}
/*
Alice
  Bob: 57
  Carol: -164
  David: 25
  Eric: 38
  Frank: -38
  George: 83
  Mallory: -130
Bob
  Alice: 57
  Carol: -62
  David: -6
  Eric: 33
  Frank: 54
  George: -107
  Mallory: 87
Carol
  Alice: -164
  Bob: -62
  David: 83
  Eric: 94
  Frank: 149
  George: -42
  Mallory: -5
David
  Alice: 25
  Bob: -6
  Carol: 83
  Eric: -163
  Frank: 41
  George: 98
  Mallory: 46
Eric
  Alice: 38
  Bob: 33
  Carol: 94
  David: -163
  Frank: 23
  George: -68
  Mallory: 100
Frank
  Alice: -38
  Bob: 54
  Carol: 149
  David: 41
  Eric: 23
  George: 59
  Mallory: 15
George
  Alice: 83
  Bob: -107
  Carol: -42
  David: 98
  Eric: -68
  Frank: 59
  Mallory: -10
Mallory
  Alice: -130
  Bob: 87
  Carol: -5
  David: 46
  Eric: 100
  Frank: 15
  George: -10

Alice + George 83
Alice + Bob 57

Bob + Mallory 87
Bob + Alice 57

Carol + Frank 149
Carol + Eric 94

! David + George 98
! David + Carol 83

Eric + Mallory 100
Eric + Carol 94

! Frank + Carol 149
! Frank + George 59

George + David 98
George + Alice 83

Mallory + Eric 100
Mallory + Bob 87

G + A + B + M + E + C + F + D
83  57  87  100 94  149 41  98
 */