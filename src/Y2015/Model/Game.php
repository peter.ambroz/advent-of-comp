<?php

declare(strict_types=1);

namespace App\Y2015\Model;

class Game
{
    public int $onMove;
    public array $boss;
    public array $pl;
    public array $spell;
    public int $manaCost;
    public int $penalty;

    public function __construct(int $bossHp, int $bossDam, int $plHp, int $mana, int $penalty = 0)
    {
        $this->onMove = 0;
        $this->boss = ['hp' => $bossHp, 'dam' => $bossDam];
        $this->pl = ['hp' => $plHp, 'mana' => $mana];
        $this->spell = ['shield' => 0, 'poison' => 0, 'recharge' => 0];
        $this->manaCost = 0;
        $this->penalty = $penalty;
    }
}
