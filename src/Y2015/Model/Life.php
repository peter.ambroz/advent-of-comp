<?php

declare(strict_types=1);

namespace App\Y2015\Model;

class Life
{
    public function __construct(private array $map)
    {
    }

    public function step(): void
    {
        $newmap = [];
        foreach ($this->map as $j => $row) {
            $newrow = [];
            foreach ($row as $i => $c) {
                $neigh = 0;
                foreach ([[-1,-1], [0,-1], [1,-1], [-1,0], [1,0], [-1,1], [0,1], [1,1]] as $d) {
                    if (($this->map[$j + $d[1]][$i + $d[0]] ?? '') === '#') {
                        $neigh++;
                    }
                }
                if (($c === '#' and ($neigh === 2 || $neigh === 3)) || ($c === '.' and $neigh === 3)) {
                    $newrow[] = '#';
                } else {
                    $newrow[] = '.';
                }
            }
            $newmap[] = $newrow;
        }
        $this->map = $newmap;
    }

    public function addCorners(): void
    {
        $sx = count($this->map[0]);
        $sy = count($this->map);
        $this->map[0][0] = '#';
        $this->map[$sy-1][0] = '#';
        $this->map[$sy-1][$sx-1] = '#';
        $this->map[0][$sx-1] = '#';
    }

    public function countIf(string $char): int
    {
        return array_sum(array_map(fn ($row) => array_count_values($row)[$char] ?? 0, $this->map));
    }

    public function display(): void
    {
        foreach ($this->map as $row) {
            printf("%s\n", implode('', $row));
        }
        printf("\n");
    }
}
