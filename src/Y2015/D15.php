<?php

declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D15 extends Day
{
    public const C = [
        [3, 0, 0, -3],
        [-3, 3, 0, 0],
        [-1, 0, 4, 0],
        [0, 0, -2, 2],
    ];

    public function run(): void
    {
        $am = [0, 0, 100, 0];
        $s = self::r($am);

        for ($i = 0; $i < 95; $i++) {
            $mini = 0;
            $minv = 10000;
            $maxi = 0;
            $maxv = 0;
            foreach ($s as $j => $v) {
                if ($j > 3) {
                    continue;
                }
                if ($v < $minv) {
                    $minv = $v;
                    $mini = $j;
                }
                if ($v > $maxv) {
                    $maxv = $v;
                    $maxi = $j;
                }
            }

            $am[$maxi]--;
            $am[$mini]++;
            $s = self::r($am);
        }

        echo array_product($s) . "\n";
        var_dump($s);
        var_dump($am);
    }

    private static function m(array $a, int $b): array
    {
        return array_map(fn ($x) => $x * $b, $a);
    }

    private static function r(array $am): array
    {
        $s = [0, 0, 0, 0];
        foreach ($am as $i => $x) {
            $row = self::m(self::C[$i], $x);
            for ($j = 0; $j < 4; $j++) {
                $s[$j] += $row[$j];
            }
        }

        return $s;
    }
}
