<?php

declare(strict_types=1);

namespace App\Y2015;

use App\Day;
use App\Y2015\Model\Life;

class D18 extends Day
{
    public function run(): void
    {
        $map = $this->inputLines(str_split(...));
        $sim = new Life($map);
        for ($i = 0; $i < 100; $i++) {
            $sim->step();
        }
        printf("%d\n", $sim->countIf('#'));
    }

    public function runB(): void
    {
        $map = $this->inputLines(str_split(...));
        $sim = new Life($map);
        $sim->addCorners();
        for ($i = 0; $i < 100; $i++) {
            $sim->step();
            $sim->addCorners();
        }
        printf("%d\n", $sim->countIf('#'));
    }
}
