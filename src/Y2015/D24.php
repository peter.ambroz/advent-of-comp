<?php

declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D24 extends Day
{
    private int $capacity;
    private int $minCount = 1000;
    private int $minQuantum = 0;

    public function run(): void
    {
        $boxes = array_reverse($this->inputLines(fn ($x) => (int)$x));
        $this->capacity = intdiv(array_sum($boxes), 3);
        $this->find(array_combine($boxes, $boxes), $this->capacity, [], []);

        printf("%d\n", $this->minQuantum);
    }

    public function runB(): void
    {
        $boxes = array_reverse($this->inputLines(fn ($x) => (int)$x));
        $this->capacity = intdiv(array_sum($boxes), 4);
        $this->find(array_combine($boxes, $boxes), $this->capacity, [], []);

        printf("%d\n", $this->minQuantum);
    }

    private function find(array $boxes, int $target, array $used, array $history)
    {
        if ($target === 0) {
            $this->consider2($used);
            return;
        }

        if (count($used) > 6) {
            return;
        }

        foreach ($boxes as $box) {
            if ($box <= $target) {
                $newBoxes = $boxes;
                unset($newBoxes[$box]);
                $cu = count($used);
                if ($cu === 0 || $used[$cu - 1] < $box) {
                    $this->find($newBoxes, $target - $box, array_merge($used, [$box]), $history);
                }
            } else {
                return;
            }
        }
    }

    private function consider2(array $used)
    {
        $l = count($used);
        $p = (int) array_product($used);

        if ($l < $this->minCount) {
            $this->minCount = $l;
            $this->minQuantum = $p;
            printf("better count %d\n", $this->minCount);
        } elseif ($l === $this->minCount && $p < $this->minQuantum) {
            $this->minQuantum = $p;
            printf("better quantum %d\n", $this->minQuantum);
        }
    }
}
