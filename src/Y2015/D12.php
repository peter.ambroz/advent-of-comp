<?php

declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D12 extends Day
{
    public function run(): void
    {
        $sum = 0;
        $a = json_decode(file_get_contents($this->inputFileName()), true);
        array_walk_recursive($a, function ($v, $k) use (&$sum) {
            if (is_numeric($v)) {
                $sum += $v;
            }
        });

        printf("%d\n", $sum);
    }

    public function runB(): void
    {
        $sum = 0;
        $a = json_decode(file_get_contents($this->inputFileName()), true);
        printf("%d\n", $this->visit($a));
    }

    private function visit(array $a): int
    {
        $sum = 0;

        foreach ($a as $k => $v) {
            if (is_numeric($v)) {
                $sum += $v;
            } elseif (is_array($v)) {
                $sum += $this->visit($v);
            } elseif ($v === 'red' && !is_numeric($k)) {
                return 0;
            }
        }

        return $sum;
    }
}
