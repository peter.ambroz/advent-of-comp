<?php

declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D19 extends Day
{
    private function common()
    {
        $rules = [];
        $state = false;
        $molecule = '';
        foreach ($this->inputLines() as $line) {
            if (!strlen($line)) {
                $state = true;
                continue;
            }
            if (!$state) {
                $rules[] = explode(' => ', $line);
            } else {
                $molecule = $line;
            }
        }

        return [$rules, $molecule];
    }

    public function run(): void
    {
        [$rules, $molecule] = $this->common();
        $found = [];
        foreach ($rules as $rule) {
            $pos = -1;
            while (($pos = strpos($molecule, $rule[0], $pos + 1)) !== false) {
                $news = substr($molecule, 0, $pos) . $rule[1] . substr($molecule, $pos + 1);
                $found[$news] = true;
            }
        }

        printf("%d\n", count($found));
    }

    public function runB(): void
    {
        [$rules, $mol] = $this->common();
        $counts = [];
        foreach (str_split($mol) as $c) {
            $counts[$c] = ($counts[$c] ?? 0) + 1;
        }
        printf("M: %d, F: %d, C: %d, D: %d\n", strlen($mol), $counts['F'], $counts['C'], $counts['D']);
    }
}

/*
6x F (+30)
30x DC (+90)
291 - 90 - 30 = 171

 */
