<?php

declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D20 extends Day
{
    public function run(): void
    {
        $input = 3400000;

        $f = 800000;
        while ($f >= 700000) {
            $sum = $f + 1;

            for ($i = 2; $i <= $f / 2; $i++) {
                if ($f % $i === 0) {
                    $sum += $i;
                }
            }

            printf("%d %d\n", $f, $sum);

            if ($sum >= $input) {
                printf("%d %d\n", $f, $sum);
                break;
            }


            $f--;
        }
    }

    public function runB(): void
    {
        $input = 3090910;

        $f = 830000;
        while ($f <= 860000) {
            $sum = $f;

            for ($i = intdiv($f, 50); $i <= $f / 2; $i++) {
                if ($f % $i === 0) {
                    $sum += $i;
                }
            }

            printf("%d %d\n", $f, $sum);

            if ($sum >= $input) {
                printf("%d %d\n", $f, $sum);
                break;
            }


            $f++;
        }
    }
}
