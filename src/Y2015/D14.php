<?php

declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D14 extends Day
{
    public function run(): void
    {
        $rd = $this->inputLines([$this, 'distance']);
        $bestName = '';
        $bestDist = 0;

        foreach ($rd as $r) {
            $rounds = intdiv(2503, $r['totalTime']);
            $dist = ($rounds * $r['distance']) + (min(2503 % $r['totalTime'], $r['time']) * $r['speed']);

            if ($dist > $bestDist) {
                $bestDist = $dist;
                $bestName = $r['name'];
            }
        }

        printf("%s %d\n", $bestName, $bestDist);
    }

    public function runB(): void
    {
        $rd = $this->inputLines([$this, 'distance']);

        for ($i = 1; $i <= 2503; $i++) {
            $best = 0;
            foreach ($rd as &$r) {
                if ($r['state'] === 1) {
                    if ($r['timer'] < $r['time']) {
                        $r['timer']++;
                        $r['position'] += $r['speed'];
                    } else {
                        $r['timer'] = 1;
                        $r['state'] = 0;
                    }
                } else {
                    if ($r['timer'] < $r['rest']) {
                        $r['timer']++;
                    } else {
                        $r['timer'] = 1;
                        $r['position'] += $r['speed'];
                        $r['state'] = 1;
                    }
                }

                if ($r['position'] > $best) {
                    $best = $r['position'];
                }
            }
            unset($r);

            foreach ($rd as &$r) {
                if ($r['position'] === $best) {
                    $r['score']++;
                }
            }
            unset($r);
        }

        $bestScore = 0;
        $bestName = '';
        foreach ($rd as $r) {
            if ($r['score'] > $bestScore) {
                $bestScore = $r['score'];
                $bestName = $r['name'];
            }
        }

        printf("%s %d\n", $bestName, $bestScore);
    }

    public function distance(string $line): array
    {
        [$name, $speed, $time, $rest] = explode(' ', $line);
        $speed *= 1;
        $time *= 1;
        $rest *= 1;
        $totalTime = $time + $rest;
        $dist = $speed * $time;

        return ['name' => $name, 'speed' => $speed, 'time' => $time, 'rest' => $rest, 'totalTime' => $totalTime, 'distance' => $dist, 'score' => 0, 'position' => 0, 'timer' => 0, 'state' => 1];
    }
}
