<?php
declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D06 extends Day
{
    private array $l;

    public function action(string $cmd): int
    {
        $dif = 0;
        $cparts = explode(' ', $cmd);

        for ($y = $cparts[2]; $y <= $cparts[5]; $y++) {
            for ($x = $cparts[1]; $x <= $cparts[4]; $x++) {
                switch ($cparts[0]) {
                    case 'on':
                        if (!$this->l[$y][$x]) {
                            $dif++;
                            $this->l[$y][$x] = true;
                        }
                        break;
                    case 'off':
                        if ($this->l[$y][$x]) {
                            $dif--;
                            $this->l[$y][$x] = false;
                        }
                        break;
                    case 'toggle':
                        if ($this->l[$y][$x]) {
                            $dif--;
                            $this->l[$y][$x] = false;
                        } else {
                            $dif++;
                            $this->l[$y][$x] = true;
                        }
                        break;
                }
            }
        }

        return $dif;
    }

    public function actionB(string $cmd): int
    {
        $dif = 0;
        $cparts = explode(' ', $cmd);

        for ($y = $cparts[2]; $y <= $cparts[5]; $y++) {
            for ($x = $cparts[1]; $x <= $cparts[4]; $x++) {
                switch ($cparts[0]) {
                    case 'on':
                        $this->l[$y][$x]++;
                        $dif++;
                        break;
                    case 'off':
                        if ($this->l[$y][$x] > 0) {
                            $dif--;
                            $this->l[$y][$x]--;
                        }
                        break;
                    case 'toggle':
                        $this->l[$y][$x] += 2;
                        $dif += 2;
                        break;
                }
            }
        }

        return $dif;
    }

    public function run(): void
    {
        $this->l = array_fill(0, 1000, array_fill(0, 1000, false));
        printf("%d\n", array_sum($this->inputLines([$this, 'action'])));
    }

    public function runB(): void
    {
        $this->l = array_fill(0, 1000, array_fill(0, 1000, 0));
        printf("%d\n", array_sum($this->inputLines([$this, 'actionB'])));
    }
}