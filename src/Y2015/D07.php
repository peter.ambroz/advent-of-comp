<?php
declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D07 extends Day
{
    private const O_SET = 'SET';
    private const O_LSHIFT = 'LSHIFT';
    private const O_RSHIFT = 'RSHIFT';
    private const O_AND = 'AND';
    private const O_OR = 'OR';
    private const O_NOT = 'NOT';

    private array $bk = [];

    public function load(string $line): bool
    {
        [$op, $res] = explode(' -> ', $line);
        $oparts = explode(' ', $op);
        switch (count($oparts)) {
            case 1:
                $opname = self::O_SET;
                $opvalues = $oparts;
                break;
            case 2:
                $opname = array_shift($oparts);
                $opvalues = $oparts;
                break;
            case 3:
                $opname = $oparts[1];
                $opvalues = [$oparts[0], $oparts[2]];
                break;
            default:
                $opname = 'NOP';
                $opvalues = [];
                break;
        }

        $this->bk[$res] = [
            'op' => $opname,
            'val' => $opvalues,
        ];

        return true;
    }

    private function resolve(string $wire): int
    {
        printf('resolve %s: ', $wire);

        foreach ($this->bk[$wire]['val'] as &$datum) {
            if (!is_numeric($datum)) {
                printf(" (%s) need %s\n", $wire, $datum);
                //usleep(100000);
                $datum = $this->resolve($datum);
            } else {
                printf(" (%s) provided %d\n", $wire, $datum);
                $datum = (int)$datum;
            }
        }
        unset($datum);

        $data = $this->bk[$wire];

        switch ($data['op']) {
            case self::O_SET:
                $val = $data['val'][0];
                break;
            case self::O_AND:
                $val = ($data['val'][0] & $data['val'][1]);
                break;
            case self::O_OR:
                $val = ($data['val'][0] | $data['val'][1]);
                break;
            case self::O_NOT:
                $val = (~$data['val'][0]) + 65536;
                break;
            case self::O_RSHIFT:
                $val = ($data['val'][0] >> $data['val'][1]);
                break;
            case self::O_LSHIFT:
                $val = ($data['val'][0] << $data['val'][1]);
                break;
            default:
                throw new \Exception('Invalid opcode');
        }

        return $val;
    }

    public function run(): void
    {
        $this->inputLines([$this, 'load']);

        printf("%d\n", $this->resolve('a'));
    }

    public function runB(): void
    {
        $this->inputLines([$this, 'load']);
        $this->bk['b'] = [
            'op' => self::O_SET,
            'val' => [3176],
        ];

        printf("%d\n", $this->resolve('a'));
    }
}