<?php

declare(strict_types=1);

namespace App\Y2015;

use App\Day;

class D17 extends Day
{
    private int $ways = 0;

    private function common(?int $limit = null): int
    {
        $containers = $this->inputLines(fn ($x) => (int)$x);
        $len = count($containers);
        $min = 10000;
        for ($i = 0; $i < 2**$len; $i++) {
            $bits = array_map(fn ($x) => (int)$x, str_split(sprintf("%0{$len}s", decbin($i))));
            $bitcount = array_sum($bits);
            if ($limit !== null && $bitcount !== $limit) {
                continue;
            }
            $sum = array_sum(array_map(fn ($a, $b) => $a * $b, $containers, $bits));
            if ($sum === 150) {
                $this->ways++;
                if ($bitcount < $min) {
                    $min = $bitcount;
                }
            }
        }

        return $min;
    }

    public function run(): void
    {
        $this->common();
        printf("%d\n", $this->ways);
    }

    public function runB(): void
    {
        $min = $this->common();
        $this->ways = 0;
        $this->common($min);
        printf("%d\n", $this->ways);
    }
}
