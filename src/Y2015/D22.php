<?php

declare(strict_types=1);

namespace App\Y2015;

use App\Day;
use App\Y2015\Model\Game;
use Exception;

class D22 extends Day
{
    private const BOSS_HP = 58;
    private const BOSS_DAM = 9;
    private const PL_HP = 50;
    private const PL_MANA = 500;

    private const LOSE = 0;
    private const WIN = 1;
    private const CONT = 2;

    private const COST = [
        'missile' => 53, // It instantly does 4 damage.
        'drain' => 73, // It instantly does 2 damage and heals you for 2 hit points.
        'shield' => 113, // It starts an effect that lasts for 6 turns. While it is active, your armor is increased by 7.
        'poison' => 173, // It starts an effect that lasts for 6 turns. At the start of each turn while it is active, it deals the boss 3 damage.
        'recharge' => 229, // It starts an effect that lasts for 5 turns. At the start of each turn while it is active, it gives you 101 new mana.
    ];

    private int $bestCost;

    public function run(): void
    {
        $this->bestCost = 1000000;
        $this->game(
            new Game(self::BOSS_HP, self::BOSS_DAM, self::PL_HP, self::PL_MANA),
        );
        printf("%d\n", $this->bestCost);
    }

    public function runB(): void
    {
        $this->bestCost = 1000000;
        $this->game(
            new Game(self::BOSS_HP, self::BOSS_DAM, self::PL_HP, self::PL_MANA, 1),
        );
        printf("%d\n", $this->bestCost);
    }

    private function game(Game $g): void
    {
        if ($g->manaCost > $this->bestCost) {
            return;
        }
        foreach (self::COST as $spell => $value) {
            $newG = clone $g;
            try {
                $newG->pl['hp'] -= $newG->penalty;
                if ($newG->pl['hp'] <= 0) {
                    $out = self::LOSE;
                } else {
                    $out = $this->turn($newG, $spell);
                }
                if ($out === self::CONT) {
                    $out = $this->turn($newG); // boss
                }
                if ($out === self::WIN) {
                    if ($this->bestCost > $newG->manaCost) {
                        $this->bestCost = $newG->manaCost;
                    }
                    break;
                } elseif ($out === self::LOSE) {
                    continue;
                }
                $this->game($newG);
            } catch (Exception) {
            }
        }
    }

    private static function turn(Game $g, ?string $spell = null, bool $debug = false): int
    {
        if ($debug) printf("-- %s --\n", $g->onMove ? 'Boss' : "Player");
        $armor = 0;
        foreach ($g->spell as $type => &$ttl) {
            if ($ttl > 0) {
                switch ($type) {
                    case 'shield':
                        $armor = 7;
                        if ($debug) printf("(SHIELD [%d]) Armor = 7\n", $ttl);
                        break;
                    case 'poison':
                        if ($debug) printf("(POISON [%d]) Boss damage = 3\n", $ttl);
                        $g->boss['hp'] -= 3;
                        break;
                    case 'recharge':
                        if ($debug) printf("(RECHARGE [%d]) Add 101 mana\n", $ttl);
                        $g->pl['mana'] += 101;
                        break;
                }
                $ttl--;
            }
        }
        unset($ttl);

        if ($g->onMove) {
            if ($debug) printf("- Boss HP: %d\n", $g->boss['hp']);
        } else {
            if ($debug) printf("- Player HP: %d, MANA: %d, ARMOR: %d\n", $g->pl['hp'], $g->pl['mana'], $armor);
        }

        if ($g->boss['hp'] <= 0) {
            if ($debug) printf("+ Boss died, Player wins!\n");
            return self::WIN;
        }

        if ($g->onMove) {
            $damage = max($g->boss['dam'] - $armor, 1);
            $g->pl['hp'] -= $damage;
            if ($debug) printf("Boss deals %d damage\n", $damage);
        } else {
            if ($g->pl['mana'] < self::COST['missile']) {
                if ($debug) printf("+ Player has insufficient mana, Boss wins!\n");
                return self::LOSE;
            }

            if ($spell === null) {
                throw new Exception("X Out of spells");
            }

            if (!isset(self::COST[$spell])) {
                throw new Exception(sprintf("X Invalid spell %s", $spell));
            }

            if ($g->pl['mana'] < self::COST[$spell]) {
                throw new Exception(sprintf("X Not enough mana. Need %d for %s", self::COST[$spell], $spell));
            }

            if (($g->spell[$spell] ?? 0) > 0) {
                throw new Exception(sprintf("Spell %s in progress", $spell));
            }

            $g->pl['mana'] -= self::COST[$spell];
            $g->manaCost += self::COST[$spell];
            if ($debug) printf("Player casts %s\n", $spell);

            switch ($spell) {
                case 'missile':
                    $g->boss['hp'] -= 4;
                    if ($debug) printf("Player deals 4 damage\n");
                    break;
                case 'drain':
                    $g->boss['hp'] -= 2;
                    $g->pl['hp'] += 2;
                    if ($debug) printf("Player deals 2 damage and heals 2 HP\n");
                    break;
                case 'shield':
                    $g->spell['shield'] = 6;
                    break;
                case 'poison':
                    $g->spell['poison'] = 6;
                    break;
                case 'recharge':
                    $g->spell['recharge'] = 5;
                    break;
            }
        }

        if ($g->pl['hp'] <= 0) {
            if ($debug) printf("+ Player died, Boss wins!\n");
            return self::LOSE;
        }

        if ($g->boss['hp'] <= 0) {
            if ($debug) printf("+ Boss died, Player wins!\n");
            return self::WIN;
        }

        $g->onMove = 1 - $g->onMove;

        return self::CONT;
    }
}
