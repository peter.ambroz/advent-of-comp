<?php

declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D16 extends Day
{
    private function doRun(int $len): void
    {
        $data = '01111001100111011';

        while (strlen($data) < $len) {
            $data = self::expand($data);
        }

        $data = substr($data, 0, $len);

        while (strlen($data) % 2 === 0) {
            $data = self::checksum($data);
        }

        printf("Checksum %d: %s\n", $len, $data);
    }

    public function run(): void
    {
        $this->doRun(272);
    }

    public function runB(): void
    {
        $this->doRun(35651584);
    }

    private static function expand(string $init): string
    {
        return $init . '0' . strtr(strrev($init), ['0' => '1', '1' => '0']);
    }

    private static function checksum(string $data): string
    {
        if (strlen($data) % 2 !== 0) {
            return $data;
        }

        $r = '';
        for ($i = 0; $i < (strlen($data) - 1); $i += 2) {
            $r .= ($data[$i] == $data[$i + 1]) ? '1' : '0';
        }

        return $r;
    }
}
