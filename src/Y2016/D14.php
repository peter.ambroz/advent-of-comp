<?php

declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D14 extends Day
{
    private const SALT = 'ihaygndm';
    private const L3 = 0;
    private const L5 = 1;

    private array $cache = [];

    public function run(): void
    {
        $i = -1;
        $h = 0;
        while ($h < 64) {
            $i++;
            if ($this->isKey($i, false)) {
                $h++;
            }
        }

        printf("Index %d\n", $i);
    }

    public function runB(): void
    {
        $i = -1;
        $h = 0;
        while ($h < 64) {
            $i++;
            if ($this->isKey($i, true)) {
                $h++;
            }
        }

        printf("Index %d\n", $i);
    }

    private function isKey(int $index, bool $stretch): bool
    {
        $cur = $this->get($index, $stretch);
        $key = $cur[self::L3];
        if ($key === null) {
            return false;
        }

        for ($i = $index + 1; $i <= $index + 1000; $i++) {
            $try = $this->get($i, $stretch);

            if (isset($try[self::L5][$key])) {
                return true;
            }
        }

        return false;
    }

    private function get(int $index, bool $stretch): array
    {
        if (!isset($this->cache[$index])) {
            $hash = md5(self::SALT . $index);

            if ($stretch) {
                for ($i = 0; $i < 2016; $i++) {
                    $hash = md5($hash);
                }
            }

            $this->cache[$index] = self::explore($hash);
        }

        return $this->cache[$index];
    }

    private static function explore(string $text): array
    {
        $found = [self::L3 => null, self::L5 => []];
        $last = '!';
        $size = 1;
        for ($i = 0; $i < strlen($text); $i++) {
            if ($last === $text[$i]) {
                $size++;
            } else {
                $last = $text[$i];
                $size = 1;
            }

            if ($size === 3 && $found[self::L3] === null) {
                $found[self::L3] = $last;
            }

            if ($size === 5) {
                $found[self::L5][$last] = true;
            }
        }

        return $found;
    }
}
