<?php

declare(strict_types=1);

namespace App\Y2016;

use App\Day;
use App\Y2016\Model\Equipment;

class D11 extends Day
{
    private const NONE = 0;
    private const PROMETHIUM = 1;
    private const COBALT = 2;
    private const CURIUM = 4;
    private const RUTHENIUM = 8;
    private const PLUTONIUM = 16;
    private const ELERIUM = 32;
    private const DILITHIUM = 64;

    private const ALL1 = self::PROMETHIUM | self::COBALT | self::CURIUM | self::RUTHENIUM | self::PLUTONIUM;
    private const ALL2 = self::PROMETHIUM | self::COBALT | self::CURIUM | self::RUTHENIUM | self::PLUTONIUM | self::ELERIUM | self::DILITHIUM;

    private array $s;
    private int $target;

    public function run(): void
    {
        $data = new Equipment([
            self::PROMETHIUM,
            self::NONE,
            self::COBALT | self::CURIUM | self::RUTHENIUM | self::PLUTONIUM,
            self::NONE,
        ], [
            self::PROMETHIUM,
            self::COBALT | self::CURIUM | self::RUTHENIUM | self::PLUTONIUM,
            self::NONE,
            self::NONE,
        ], 0);

        $this->s = [];
        $this->target = (new Equipment([0,0,0,self::ALL1], [0,0,0,self::ALL1], 3))->id();
        $this->solve($data, 0);
        printf("%d\n", $this->s[$this->target]);
    }

    public function runB(): void
    {
        $data = new Equipment([
            self::PROMETHIUM | self::ELERIUM | self::DILITHIUM,
            self::NONE,
            self::COBALT | self::CURIUM | self::RUTHENIUM | self::PLUTONIUM,
            self::NONE,
        ], [
            self::PROMETHIUM | self::ELERIUM | self::DILITHIUM,
            self::COBALT | self::CURIUM | self::RUTHENIUM | self::PLUTONIUM,
            self::NONE,
            self::NONE,
        ], 0);

        $this->s = [];
        $this->target = (new Equipment([0,0,0,self::ALL2], [0,0,0,self::ALL2], 3))->id();
        $this->solve($data, 0);
        printf("%d\n", $this->s[$this->target]);
    }

    private function solve(Equipment $e, int $steps): void
    {
        if ($steps > 60) {
            return;
        }
        $id = $e->id();
        $st = $this->s[$id] ?? null;
        if ($st !== null && $st <= $steps) {
            return;
        }
        $this->s[$id] = $steps;

        if ($id === $this->target) {
            return;
        }

        foreach ([1, -1] as $dir) {
            foreach ($e->elevatorCombos() as $combo) {
                $ne = clone $e;
                if ($ne->move($combo[0], $combo[1], $dir)) {
                    $this->solve($ne, $steps + 1);
                }
            }
        }
    }
}
