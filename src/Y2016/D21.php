<?php

declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D21 extends Day
{
    private string $pass;
    private int $mod;
    private int $ptr = 0;
    public function run(): void
    {
        $this->pass = 'abcdefgh';
        $this->mod = strlen($this->pass);

        foreach ($this->inputLines(fn ($l) => explode(' ', $l)) as $line) {
            $cmd = $line[0] . ' ' . $line[1];
            switch ($cmd) {
                case 'swap position':
                    $this->swapPosition((int) $line[2], (int) $line[5]);
                    break;

                case 'swap letter':
                    $this->swapLetter($line[2], $line[5]);
                    break;

                case 'rotate left':
                    $this->rotateLeft((int) $line[2]);
                    break;

                case 'rotate right':
                    $this->rotateRight((int) $line[2]);
                    break;

                case 'rotate based':
                    $this->rotateBased($line[6]);
                    break;

                case 'reverse positions':
                    $this->reversePositions((int) $line[2], (int) $line[4]);
                    break;

                case 'move position':
                    $this->movePosition((int) $line[2], (int) $line[5]);
                    break;
            }
        }

        $this->normalize();

        printf("%s\n", $this->pass);
    }

    public function runB(): void
    {
        $this->pass = 'fbgdceah';
        $this->mod = strlen($this->pass);

        foreach (array_reverse($this->inputLines(fn ($l) => explode(' ', $l))) as $line) {
            $cmd = $line[0] . ' ' . $line[1];
            switch ($cmd) {
                case 'swap position':
                    $this->swapPosition((int) $line[5], (int) $line[2]);
                    break;

                case 'swap letter':
                    $this->swapLetter($line[5], $line[2]);
                    break;

                case 'rotate left':
                    $this->rotateRight((int) $line[2]);
                    break;

                case 'rotate right':
                    $this->rotateLeft((int) $line[2]);
                    break;

                case 'rotate based':
                    $this->undoRotateBased($line[6]);
                    break;

                case 'reverse positions':
                    $this->reversePositions((int) $line[2], (int) $line[4]);
                    break;

                case 'move position':
                    $this->movePosition((int) $line[5], (int) $line[2]);
                    break;
            }
        }

        $this->normalize();

        printf("%s\n", $this->pass);
    }

    private function ap(int $i): int
    {
        return ($this->ptr + $i) % $this->mod;
    }

    private function get(int $i): string
    {
        return $this->pass[$this->ap($i)];
    }

    private function set(int $i, string $c): void
    {
        $this->pass[$this->ap($i)] = $c;
    }

    private function swapPosition(int $i, int $j): void
    {
        $tmp = $this->get($i);
        $this->set($i, $this->get($j));
        $this->set($j, $tmp);
    }

    private function swapLetter(string $a, string $b): void
    {
        $this->pass = strtr($this->pass, [$a => $b, $b => $a]);
    }

    private function rotateLeft(int $steps): void
    {
        $this->ptr = ($this->ptr + $steps) % $this->mod;
    }

    private function rotateRight(int $steps): void
    {
        $this->ptr -= $steps;
        while ($this->ptr < 0) {
            $this->ptr += $this->mod;
        }
    }

    private function rotateBased(string $letter): void
    {
        $i = strpos($this->pass, $letter) - $this->ptr;
        while ($i < 0) {
            $i += $this->mod;
        }
        if ($i >= 4) {
            $i++;
        }
        $this->rotateRight($i + 1);
    }

    private function undoRotateBased(string $letter): void
    {
        $t = [
            0 => 1,
            1 => 1,
            2 => 6,
            3 => 2,
            4 => 7,
            5 => 3,
            6 => 0,
            7 => 4,
        ];
        $this->normalize();

        $i = strpos($this->pass, $letter);
        $this->rotateLeft($t[$i]);
    }

    private function reversePositions(int $i, int $j): void
    {
        $this->normalize();
        $this->pass = substr($this->pass, 0, $i) . strrev(substr($this->pass, $i, $j - $i + 1)) . substr($this->pass, $j + 1);
    }

    private function movePosition(int $i, int $j): void
    {
        $this->normalize();
        $ri = $this->ap($i);
        $rj = $this->ap($j);
        $char = $this->pass[$ri];
        $this->pass = substr($this->pass, 0, $ri) . substr($this->pass, $ri + 1);
        $this->pass = substr($this->pass, 0, $rj) . $char . substr($this->pass, $rj);
    }

    private function normalize(): void
    {
        $this->pass = substr($this->pass, $this->ptr) . substr($this->pass, 0, $this->ptr);
        $this->ptr = 0;
    }
}
