<?php
declare(strict_types=1);

namespace App\Y2016;

use App\Day;
use App\Y2016\Model\BunnyCode;

class D23 extends Day
{
    public function run(): void
    {
        $bunny = new BunnyCode();
        $this->inputLines([$bunny, 'load']);

        $bunny->run([7,0,0,0]);
        printf("%d\n", $bunny->getRegs()[0]);
    }

    public function runB(): void
    {
        /*
        $bunny = new BunnyCode();
        $this->inputLines([$bunny, 'load']);

        $bunny->run([12,0,0,0]);
        printf("%d\n", $bunny->getRegs()[0]);
        */
        printf("%d\n", 12 * 110 * 72 * 42 * 20 * 6 + 81 * 90);
    }
}