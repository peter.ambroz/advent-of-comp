<?php

declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D18 extends Day
{
    public function run(): void
    {
        $count = 53;
        $line = $this->inputLines()[0];

        for ($i = 1; $i < 40; $i++) {
            $line = $this->next($line);

            foreach (str_split($line) as $char) {
                if ($char === '.') {
                    $count++;
                }
            }
        }

        printf("%d\n", $count);
    }

    public function runB(): void
    {
        $count = 53;
        $line = $this->inputLines()[0];

        for ($i = 1; $i < 400000; $i++) {
            $line = $this->next($line);

            foreach (str_split($line) as $char) {
                if ($char === '.') {
                    $count++;
                }
            }
        }

        printf("%d\n", $count);
    }

    private function next(string $line): string
    {
        $chars = str_split($line);
        $nchars = $chars;

        $nchars[0] = $chars[1];
        $nchars[99] = $chars[98];

        for ($i = 1; $i < 99; $i++) {
            if ($chars[$i-1] === $chars[$i+1]) {
                $nchars[$i] = '.';
            } else {
                $nchars[$i] = '^';
            }
        }

        return implode('', $nchars);
    }
}
