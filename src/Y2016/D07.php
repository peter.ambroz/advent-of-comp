<?php
declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D07 extends Day
{
    public function valid(string $line): int
    {
        return 1;
    }

    public function validB(string $line): int
    {
        $t1 = [];
        $t2 = [];
        $parts = explode('[', $line);
        $t1[] = array_shift($parts);
        foreach ($parts as $part) {
            $parts2 = explode(']', $part);
            $t1[] = $parts2[1];
            $t2[] = $parts2[0];
        }

        file_put_contents('/tmp/D07.txt', implode('  ', $t1) . '[' . implode('  ', $t2) . ']' . "\n", FILE_APPEND);
        return 1;
    }

    public function run(): void
    {
        printf("%d\n", array_sum($this->inputLines([$this, 'valid'])));
    }

    public function runB(): void
    {
        printf("%d\n", array_sum($this->inputLines([$this, 'validB'])));
    }
}