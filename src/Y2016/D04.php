<?php
declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D04 extends Day
{
    private array $roomlist = [];

    public function validateRoom(string $room): int
    {
        $parts = explode('-', $room);
        $lastPart = array_pop($parts);
        $str = str_split(implode('', $parts));
        $name = implode(' ', $parts);
        [$id, $rest] = explode('[', $lastPart);
        $check = substr($rest, 0, -1);

        $counts = array_combine(str_split('abcdefghijklmnopqrstuvwxyz'), array_fill(0, 26, 0));
        foreach ($str as $ch) {
            $counts[$ch]++;
        }

        $best = '';
        for ($j = 0; $j < 5; $j++) {
            $max = -1;
            $maxi = '';
            foreach ($counts as $key => $val) {
                if ($val > $max) {
                    $max = $val;
                    $maxi = $key;
                }
            }
            $best .= $maxi;
            unset($counts[$maxi]);
        }

        if ($check !== $best) {
            return 0;
        }

        $this->roomlist[] = [$name, $id];

        return (int)$id;
    }

    public function run(): void
    {
        printf("%d\n", array_sum($this->inputLines([$this, 'validateRoom'])));
    }

    public function runB(): void
    {
        $this->inputLines([$this, 'validateRoom']);

        foreach ($this->roomlist as $roomdata) {
            printf("%s / %d\n", implode('', array_map(function (string $ch) use ($roomdata) : string {
                if ($ch === ' ') return ' ';
                return chr(((ord($ch) - 97) + $roomdata[1]) % 26 + 97);
            }, str_split($roomdata[0]))), $roomdata[1]);
        }
    }
}