<?php

declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D19 extends Day
{
    public function run(): void
    {
        $elves = 3017957;
        $elves = intdiv($elves, 2);

        $n = 1;
        while ($n < $elves) {
            $elves -= $n;
            $n *= 2;
        }

        printf("%d\n", 4 * $elves - 1);
    }

    public function runB(): void
    {
        // too slow
        $elves = 3017957;
        $e = '';
        for ($i = 0; $i < $elves; $i++) {
            $e .= sprintf("%07d", $i);
        }

        $ep = 0;
        while ($elves > 1) {
            if ($elves % 1000 === 0) {
                printf("%d\n", $elves);
            }
            $victim = (intdiv($elves,  2) + $ep) % $elves;
            $e = substr($e, 0, 7*$victim) . substr($e, 7*($victim + 1));
            $elves--;
            $ep = ($ep + 1) % $elves;
        }

        printf("Last elf: %s\n", $e);
    }
}
