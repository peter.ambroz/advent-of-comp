<?php
declare(strict_types=1);

namespace App\Y2016;

use App\Day;
use App\Model\Point;

class D13 extends Day
{
    private const OFFSET = 1358;
    private const WALL = -2;
    private const SPACE = -1;

    /** @var int[] */
    private array $map;

    public function run(): void
    {
        echo $this->find(new Point(1, 1), 0);
        echo "\n";
    }

    public function runB(): void
    {
        $this->find(new Point(1, 1), 0, 50);

        echo count(array_filter($this->map, fn ($val) => $val >= 0));
        echo "\n";
    }

    private function get(Point $p): int
    {
        if (isset($this->map[(string)$p])) {
            return $this->map[(string)$p];
        }
        $x = $p->x;
        $y = $p->y;
        $v = $x*$x + 3*$x + 2*$x*$y + $y + $y*$y + self::OFFSET;
        $w = array_sum(str_split(decbin($v))) % 2 === 1;
        return $this->map[(string) $p] = $w ? self::WALL : self::SPACE;
    }

    private function find(Point $p, int $dist, int $limit = 100): ?int
    {
        if ($dist > $limit) {
            return null;
        }

        $this->map[(string) $p] = $dist;

        if ($p->x === 31 && $p->y === 39) {
            return $dist;
        }

        $low = null;
        foreach ($p->neighbors() as $n) {
            if ($n->x < 0 || $n->y < 0 || $n->x > 100 || $n->y > 100) {
                continue;
            }

            $nval = $this->get($n);

            if ($nval === self::WALL) {
                continue;
            }

            if ($nval >= 0 && $nval <= $dist) {
                continue;
            }

            $v = $this->find($n, $dist + 1, $limit);
            if ($v !== null && ($low === null || $v < $low)) {
                $low = $v;
            }
        }

        return $low;
    }
}
