<?php
declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D09 extends Day
{
    private const RE = '/\((\d+)x(\d+)\)/';

    public function process(string $line): int
    {
        $o = 0;

        while (preg_match(self::RE, $line, $m)) {
            $i = strpos($line, $m[0]);
            $o += $i;
            $line = substr($line, $i + strlen($m[0]) + (int) $m[1]);
            $o += (int) $m[1] * (int) $m[2];
        }

        $o += strlen($line);

        return $o;
    }

    public function processB(string $line): int
    {
        $o = 0;

        while (preg_match(self::RE, $line, $m)) {
            $i = strpos($line, $m[0]);
            $o += $i;
            $ext = substr($line, $i + strlen($m[0]), (int) $m[1]);
            $o += $this->processB($ext) * (int) $m[2];

            $line = substr($line, $i + strlen($m[0]) + (int) $m[1]);
        }

        $o += strlen($line);

        return $o;
    }

    public function run(): void
    {
        printf("%d\n", array_sum($this->inputLines([$this, 'process'])));
    }

    public function runB(): void
    {
        printf("%d\n", array_sum($this->inputLines([$this, 'processB'])));
    }
}