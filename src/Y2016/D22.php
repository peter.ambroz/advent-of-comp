<?php

declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D22 extends Day
{
    private const KEYS = ['x', 'y', 'size', 'used', 'avail', 'perc'];

    public function run(): void
    {
        $data = $this->inputLines(function ($x) {
            return array_combine(self::KEYS, array_map(fn ($y) => (int) $y, explode(' ', $x)));
        });

        $viable = 0;

        foreach ($data as $d1 => $v1) {
            foreach ($data as $d2 => $v2) {
                if ($d1 !== $d2 && $v1['used'] !== 0 && $v1['used'] <= $v2['avail']) {
                    printf("%d %d %d %d\n", $v1['x'], $v1['y'], $v2['x'], $v2['y']);
                    $viable++;
                }
            }
        }

        printf("%d\n", $viable);
    }

    public function runB(): void
    {
        $data = $this->inputLines(function ($x) {
            return array_combine(self::KEYS, array_map(fn ($y) => (int) $y, explode(' ', $x)));
        });

        foreach ($data as $n) {
            $c = '.';

            if ($n['used'] === 0) {
                $c = '_';
            } elseif ($n['used'] > 100) {
                $c = '#';
            }

            if ($n['y'] === 0) {
                echo "\n";
            }
            echo $c;
        }
        echo "\n";

        echo 5 + 34 + 6 + 28*5 . "\n";
    }
}
