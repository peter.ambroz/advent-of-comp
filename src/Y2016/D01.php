<?php

declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D01 extends Day
{
    private int $x = 0;
    private int $y = 0;
    private int $dir = 0;
    private array $v = [0 => [0]];

    private function visit(int $amt): void
    {
        for ($i = 0; $i < $amt; $i++) {
            switch ($this->dir) {
                case 0:
                    $this->y--;
                    break;
                case 1:
                    $this->x++;
                    break;
                case 2:
                    $this->y++;
                    break;
                case 3:
                    $this->x--;
                    break;
            }

            if (!isset($this->v[$this->y])) {
                $this->v[$this->y] = [];
            }

            if (isset($this->v[$this->y][$this->x])) {
                printf("Visit again %d %d\n", $this->x, $this->y);
            }

            $this->v[$this->y][$this->x] = true;
        }
    }

    public function run(): void
    {
        $instr = explode(", ", file_get_contents($this->inputFileName()));
        foreach ($instr as $i) {
            switch ($i[0]) {
                case 'L':
                    $this->dir = ($this->dir + 3)%4;
                    break;
                case 'R':
                    $this->dir = ($this->dir + 1)%4;
                    break;
            }

            $amt = (int)substr($i, 1);
            $this->visit($amt);
        }

        printf("%d\n", abs($this->x) + abs($this->y));
    }
}