<?php
declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D06 extends Day
{
    private array $freq;

    public function addToFreq(string $line): bool
    {
        foreach (str_split($line) as $pos => $char) {
            if (!isset($this->freq[$pos][$char])) {
                $this->freq[$pos][$char] = 0;
            }

            $f = $this->freq[$pos][$char] + 1;
            $this->freq[$pos][$char] = $f;
            if ($f > $this->freq[$pos]['max']) {
                $this->freq[$pos]['max'] = $f;
                $this->freq[$pos]['maxc'] = $char;
            }
        }

        return true;
    }

    public function addToFreqB(string $line): bool
    {
        foreach (str_split($line) as $pos => $char) {
            if (!isset($this->freq[$pos][$char])) {
                $this->freq[$pos][$char] = 0;
            }

            $this->freq[$pos][$char]++;
        }

        return true;
    }

    public function run(): void
    {
        $this->freq = array_fill(0, 8, ['max' => -1, 'maxc' => '']);
        $this->inputLines([$this, 'addToFreq']);
        foreach ($this->freq as $posData) {
            echo $posData['maxc'];
        }
        echo "\n";
    }

    public function runB(): void
    {
        $this->freq = array_fill(0, 8, []);
        $this->inputLines([$this, 'addToFreqB']);
        foreach ($this->freq as $posData) {
            $min = 999999;
            $minc = '';
            foreach ($posData as $char => $amt) {
                if ($amt < $min) {
                    $min = $amt;
                    $minc = $char;
                }
            }
            echo $minc;
        }
        echo "\n";
    }
}