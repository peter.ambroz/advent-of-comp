<?php
declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D08 extends Day
{
    private array $s;

    private function rect(int $x, int $y): void
    {
        for ($j = 0; $j < $y; $j++) {
            for ($i = 0; $i < $x; $i++) {
                $this->s[$j][$i] = 1;
            }
        }
    }

    private function rotateRow(int $y, int $amount): void
    {
        $off = array_splice($this->s[$y], -$amount, $amount);
        array_splice($this->s[$y], 0, 0, $off);
    }

    private function rotateColumn(int $x, int $amount): void
    {
        for ($j = 0; $j < $amount; $j++) {
            $tmp = $this->s[5][$x];
            for ($i = 4; $i >= 0; $i--) {
                $this->s[$i + 1][$x] = $this->s[$i][$x];
            }
            $this->s[0][$x] = $tmp;
        }
    }

    public function process(string $line): int
    {
        $data = explode(' ', $line);
        switch ($data[0]) {
            case 'rect':
                [$x, $y] = explode('x', $data[1]);
                $this->rect((int)$x, (int)$y);
                break;
            case 'rotate':
                if ($data[1] === 'row') {
                    $this->rotateRow((int)$data[2], (int)$data[4]);
                } else {
                    $this->rotateColumn((int)$data[2], (int)$data[4]);
                }
                break;
        }

        return 1;
    }

    public function show(): void
    {
        $cnt = 0;
        foreach ($this->s as $column) {
            foreach ($column as $char) {
                if ($char === 1) {
                    $cnt++;
                    echo '#';
                } else {
                    echo ' ';
                }
            }
            echo "\n";
        }
        printf("%d\n", $cnt);
    }

    public function run(): void
    {
        $this->s = array_fill(0, 6, array_fill(0, 50, 0));
        $this->inputLines([$this, 'process']);
        $this->show();
    }
}