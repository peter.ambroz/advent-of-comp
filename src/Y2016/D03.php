<?php
declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D03 extends Day
{
    public function validate3(string $line): int
    {
        [$a, $b, $c] = explode(' ', $line);
        if ($a+$b > $c && $a+$c > $b && $b+$c > $a) {
            return 1;
        }
        return 0;
    }

    public function run(): void
    {
        printf("%d\n", array_sum($this->inputLines([$this, 'validate3'])));
    }

    public function runB(): void
    {
        $tr = $this->inputLines(function (string $line): array {
            return explode(' ', $line);
        });

        $s = 0;
        for ($j = 0; $j < 3; $j++) {
            for ($i = 0; $i < count($tr)/3; $i++) {
                $a = $tr[$i*3][$j];
                $b = $tr[$i*3+1][$j];
                $c = $tr[$i*3+2][$j];
                if ($a+$b > $c && $a+$c > $b && $b+$c > $a) {
                    $s++;
                }
            }
        }

        printf("%d\n", $s);
    }
}