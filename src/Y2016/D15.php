<?php

declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D15 extends Day
{
    private array $disc;
    public function doRun(bool $b): void
    {
        $this->init($b);
        $this->rotate();

        $time = 0;
        foreach ($this->disc as $d) {
            while ($time % $d['mod'] !== $d['pos']) {
                $time += $d['mul'];
            }
        }

        printf("time: %d\n", $time);
    }

    public function run(): void
    {
        $this->doRun(false);
    }

    public function runB(): void
    {
        $this->doRun(true);
    }

    private function init(bool $b = false): void
    {
        foreach ($this->inputLines(fn ($l) => explode(' ', $l)) as $line) {
            $pos = substr($line[11], 0, -1);
            $this->disc[] = [
                'mod' => (int) $line[3],
                'pos' => (int) $pos,
            ];
        }

        if ($b) {
            $this->disc[] = [
                'mod' => 11,
                'pos' => 0,
            ];
        }
    }

    private function rotate(): void
    {
        $oldMul = 1;
        foreach ($this->disc as $i => &$d) {
            $d['pos'] = (2 * $d['mod'] - $d['pos'] - $i - 1) % $d['mod'];
            $d['mul'] = $oldMul;
            $oldMul *= $d['mod'];
        }
        unset($d);
    }
}

/*
    T-  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21
11   5  6  7  8  9 10 11 12 13 14 15 16  0  1  2  3  4  5  6  7  8  9 10
 9   8  9 10 11 12 13 14 15 16 17 18  0  1  2  3  4  5  6  7  8  9 10 11
 3   1  2  3  4  5  6  0  1  2  3  4  5  6  0  1  2  3  4  5  6  0  1  2
 2   7  8  9 10 11 12  0  1  2  3  4  5  6  7  8  9 10 11 12  0  1  2  3
 4   1  2  3  4  0  1  2  3  4  0  1  2  3  4  0  1  2  3  4  0  1  2  3
 0   0  1  2  0  1  2  0  1  2  0  1  2  0  1  2  0  1  2  0  1  2  0  1

17a + 11 = 11
19b +  9 = 28
 7c +  3 = 997
13d +  2 = 16824
 5e +  4 = 16824
 3f +  0 = 16824

 */