<?php
declare(strict_types=1);

namespace App\Y2016\Model;

class BunnyCode
{
    private const INC = 0;
    private const DEC = 1;
    private const CPY = 2;
    private const JNZ = 3;
    private const OUT = 4;
    private const HLT = 5;
    private const TGL = 6;

    /** @var int[] */
    private array $reg;
    private int $ip;
    /** @var array[] */
    private array $mem;

    public function __construct()
    {
        $this->reg = [0, 0, 0, 0];
        $this->ip = 0;
        $this->mem = [];
    }

    public function load(string $line): bool
    {
        $data = explode(' ', $line);
        switch ($data[0]) {
            case 'inc':
                $this->mem[] = [self::INC, $data[1], 0];
                break;
            case 'dec':
                $this->mem[] = [self::DEC, $data[1], 0];
                break;
            case 'cpy':
                $this->mem[] = [self::CPY, $data[1], $data[2]];
                break;
            case 'jnz':
                $this->mem[] = [self::JNZ, $data[1], $data[2]];
                break;
            case 'out':
                $this->mem[] = [self::OUT, $data[1], 0];
                break;
            case 'tgl':
                $this->mem[] = [self::TGL, $data[1], 0];
                break;
            default:
                throw new \Exception('Unknown instruction ' . $data[0]);
        }

        return true;
    }

    /**
     * @param int[] $regs
     */
    private function setRegs(array $regs): void
    {
        if (count($regs) === 4) {
            $this->reg = $regs;
        } else {
            throw new \Exception('Invalid registers');
        }
    }

    private function regId(string $reg): int
    {
        if ($reg >= 'a' && $reg <= 'd') {
            return ord($reg) - ord('a');
        }
        throw new \Exception('Invalid register access: ' . $reg);
    }

    private function getReg(string $reg): int
    {
        return $this->reg[$this->regId($reg)];
    }

    private function setReg(string $reg, int $val): void
    {
        $this->reg[$this->regId($reg)] = $val;
    }

    /**
     * @return int[]
     */
    public function getRegs(): array
    {
        return $this->reg;
    }

    public function run(array $regs = [0, 0, 0, 0]): void
    {
        $this->setRegs($regs);
        while (true) {
            $instr = $this->mem[$this->ip] ?? [self::HLT, 0, 0];
            switch ($instr[0]) {
                case self::INC:
                    $this->setReg($instr[1], $this->getReg($instr[1]) + 1);
                    break;
                case self::DEC:
                    $this->setReg($instr[1], $this->getReg($instr[1]) - 1);
                    break;
                case self::CPY:
                    if (is_numeric($instr[2])) {
                        break;
                    }

                    if (is_numeric($instr[1])) {
                        $this->setReg($instr[2], (int) $instr[1]);
                    } else {
                        $this->setReg($instr[2], $this->getReg($instr[1]));
                    }
                    break;
                case self::JNZ:
                    if (is_numeric($instr[1])) {
                        $cmp = (int) $instr[1];
                    } else {
                        $cmp = $this->getReg($instr[1]);
                    }

                    if (is_numeric($instr[2])) {
                        $jmp = $instr[2];
                    } else {
                        $jmp = $this->getReg($instr[2]);
                    }

                    if ($cmp !== 0) {
                        $this->ip += ($jmp - 1);
                    }
                    break;
                case self::OUT:
                    printf("OUT %d\n", $this->getReg($instr[1]));
                    break;
                case self::HLT:
                    return;
                case self::TGL:
                    $map = [
                        self::INC => self::DEC,
                        self::DEC => self::INC,
                        self::CPY => self::JNZ,
                        self::JNZ => self::CPY,
                        self::OUT => self::INC,
                        self::TGL => self::INC,
                    ];

                    if (is_numeric($instr[1])) {
                        $jmp = $instr[1];
                    } else {
                        $jmp = $this->getReg($instr[1]);
                    }

                    printf("TGL %d: ", $jmp);

                    $ip = $this->ip + $jmp;
                    if (!isset($this->mem[$ip])) {
                        printf("-\n");
                        break;
                    }
                    $ti = $this->mem[$ip];
                    printf("%d -> ", $ti[0]);
                    $ti[0] = $map[$ti[0]];
                    printf("%d\n", $ti[0]);
                    $this->mem[$ip] = $ti;
                    break;
                default:
                    throw new \Exception('Invalid instruction at ' . $this->ip . ': ' . $instr[0]);
            }

            $this->ip++;
        }
    }
}