<?php

declare(strict_types=1);

namespace App\Y2016\Model;

use Exception;

final class Equipment
{
    /**
     * @param int[] $chips
     * @param int[] $rtgs
     */
    public function __construct(
        private array $chips,
        private array $rtgs,
        private int $floor,
    ) {
    }

    public function move(int $chips, int $rtgs, int $dir): bool
    {
        $dest = $this->floor + $dir;
        if ($dest < 0 || $dest > 3) {
            return false;
        }

        if (array_sum(str_split(decbin($chips))) + array_sum(str_split(decbin($rtgs))) > 2) {
            return false;
        }

        if ((($this->chips[$this->floor] & $chips) !== $chips)
            || (($this->rtgs[$this->floor] & $rtgs) !== $rtgs)
        ) {
            return false;
        }

        if (!self::isSafe($chips, $rtgs)) {
            return false;
        }

        $currentChips = $this->chips[$this->floor] & ~$chips;
        $currentRtgs = $this->rtgs[$this->floor] & ~$rtgs;
        $newChips = $this->chips[$dest] | $chips;
        $newRtgs = $this->rtgs[$dest] |= $rtgs;
        if (!self::isSafe($currentChips, $currentRtgs) || !self::isSafe($newChips, $newRtgs)) {
            return false;
        }

        $this->chips[$this->floor] = $currentChips;
        $this->rtgs[$this->floor] = $currentRtgs;
        $this->chips[$dest] = $newChips;
        $this->rtgs[$dest] = $newRtgs;
        $this->floor = $dest;

        return true;
    }

    /**
     * @return int[]
     */
    public function items(): array
    {
        return [
            $this->chips[$this->floor],
            $this->rtgs[$this->floor],
        ];
    }

    public function floor(): int
    {
        return $this->floor;
    }

    /**
     * @return int[][]
     */
    public function elevatorCombos(): array
    {
        $floorChips = $this->chips[$this->floor];
        $floorRtgs = $this->rtgs[$this->floor];
        $res = [];
        for ($i = 0; $i < 14; $i++) {
            for ($j = $i + 1; $j < 14; $j++) {
                $chips = 0;
                $rtgs = 0;
                $items = [1 << $i, 1 << $j];
                foreach ($items as $item) {
                    if ($item < 128) {
                        $add = $floorChips & $item;
                        if ($add === 0) {
                            continue 2;
                        }
                        $chips |= $add;
                    } else {
                        $add = $floorRtgs & ($item >> 7);
                        if ($add === 0) {
                            continue 2;
                        }
                        $rtgs |= $add;
                    }
                }

                $possibleChips = $floorChips & ~$chips;
                $possibleRtgs = $floorRtgs & ~$rtgs;
                if (self::isSafe($chips, $rtgs) && self::isSafe($possibleChips, $possibleRtgs)) {
                    $res[] = [$chips, $rtgs];
                }
            }
        }

        for ($k = 0; $k < 14; $k++) {
            $chips = 0;
            $rtgs = 0;
            $item = 1 << $k;
            if ($item < 128) {
                $add = $floorChips & $item;
                if ($add === 0) {
                    continue;
                }
                $chips |= $add;
            } else {
                $add = $floorRtgs & ($item >> 7);
                if ($add === 0) {
                    continue;
                }
                $rtgs |= $add;
            }

            $possibleChips = $floorChips & ~$chips;
            $possibleRtgs = $floorRtgs & ~$rtgs;
            if (self::isSafe($chips, $rtgs) && self::isSafe($possibleChips, $possibleRtgs)) {
                $res[] = [$chips, $rtgs];
            }
        }

        return $res;
    }

    public function id(): int
    {
        $sum = 0;
        for ($i = 0; $i < 4; $i++) {
            $sum = $sum << 14;
            $sum += ($this->chips[$i] << 7) + $this->rtgs[$i];
        }
        return ($sum << 2) + $this->floor;
    }

    private static function isSafe(int $chips, int $rtgs): bool
    {
        $bareChips = $chips - ($chips & $rtgs);
        return $bareChips === 0 || $rtgs === 0;
    }
}
