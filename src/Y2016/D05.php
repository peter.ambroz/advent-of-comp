<?php
declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D05 extends Day
{
    public function run(): void
    {
        $input = 'ugkcyxxp';
        $pwd = '';
        $c = 0;

        while (strlen($pwd) < 8) {
            $h = md5($input . $c);
            if (substr($h, 0, 5) === '00000') {
                $pwd .= $h[5];
            }
            $c++;
        }

        printf("%s %d\n", $pwd, $c);
    }

    public function runB(): void
    {
        $input = 'ugkcyxxp';
        $pwd = ['_', '_', '_', '_', '_', '_', '_', '_'];
        $c = 0;
        $x = 0;

        while ($x < 8) {
            $h = md5($input . $c);
            if (substr($h, 0, 5) === '00000') {
                if (array_key_exists($h[5], $pwd) && $pwd[$h[5]] === '_') {
                    $pwd[$h[5]] = $h[6];
                    printf("%s %d\n", implode('', $pwd), $c);
                    $x++;
                }
            }
            $c++;
        }
    }
}