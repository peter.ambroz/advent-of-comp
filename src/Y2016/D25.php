<?php
declare(strict_types=1);

namespace App\Y2016;

use App\Day;
use App\Y2016\Model\BunnyCode;

class D25 extends Day
{
    public function run(): void
    {
        $bunny = new BunnyCode();
        $this->inputLines([$bunny, 'load']);

        $bunny->run([196, 0, 0, 0]);
    }
}