<?php

declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D20 extends Day
{
    public function run(): void
    {
        $range = $this->init();

        $cover = -1;
        for ($i = 0; $i < count($range); $i++) {
            if ($range[$i][0] > ($cover + 1)) {
                printf("First IP: %d\n", $cover + 1);
                return;
            }

            if ($range[$i][1] > $cover) {
                $cover = $range[$i][1];
            }
        }

        printf("First IP: %d\n", $range[count($range) - 1][1] + 1);
    }

    public function runB(): void
    {
        $range = $this->init();
        $ips = 0;

        $cover = -1;
        for ($i = 0; $i < count($range); $i++) {
            if ($range[$i][0] > ($cover + 1)) {
                $ips += $range[$i][0] - ($cover + 1);
            }

            if ($range[$i][1] > $cover) {
                $cover = $range[$i][1];
            }
        }

        printf("Allowed IPs: %d\n", $ips);
    }

    private function init(): array
    {
        $range = $this->inputLines(fn ($l) => array_map(fn ($x) => (int) $x, explode('-', $l)));
        usort($range, fn ($a, $b) => $a[0] <=> $b[0]);
        return $range;
    }
}
