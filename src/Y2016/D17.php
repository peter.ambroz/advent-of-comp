<?php

declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D17 extends Day
{
    private const PW = 'yjjvjgan';

    private string $shortestPath;

    private int $shortestLength;

    private int $longestLength;

    public function run(): void
    {
        $this->shortestLength = 100000;
        $this->longestLength = 0;

        $this->explore('', 0, 0);

        printf("%s\n%d\n", $this->shortestPath, $this->longestLength);
    }

    private function explore(string $path, int $x, int $y): void
    {
        if ($x === 3 && $y === 3) {
            if (strlen($path) < $this->shortestLength) {
                $this->shortestLength = strlen($path);
                $this->shortestPath = $path;
            }

            if (strlen($path) > $this->longestLength) {
                $this->longestLength = strlen($path);
            }
            return;
        }

        foreach (self::md($path) as $dir => $open) {
            $nx = $x;
            $ny = $y;
            if ($open) {
                switch ($dir) {
                    case 'U':
                        $ny--;
                        break;
                    case 'D':
                        $ny++;
                        break;
                    case 'L':
                        $nx--;
                        break;
                    case 'R':
                        $nx++;
                        break;
                }
                if ($nx >= 0 && $nx < 4 && $ny >= 0 && $ny < 4) {
                    $this->explore($path . $dir, $nx, $ny);
                }
            }
        }
    }

    private static function md(string $path): array
    {
        $r = [];
        $m = md5(self::PW . $path);
        foreach (['U', 'D', 'L', 'R'] as $pos => $dir) {
            $r[$dir] = in_array($m[$pos], ['b', 'c', 'd', 'e', 'f']);
        }

        return $r;
    }
}
