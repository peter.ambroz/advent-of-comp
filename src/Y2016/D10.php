<?php
declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D10 extends Day
{
    private array $b = [];
    private array $o = [];

    private function initBot(int $botId): void
    {
        $this->b[$botId] = ['h' => [], 'f' => []];
    }

    public function process(string $line): int
    {
        $parts = explode(' ', $line);
        if ($parts[0] === 'value') {
            $botId = (int) $parts[5];
            $value = (int) $parts[1];
            if (!isset($this->b[$botId])) {
                $this->initBot($botId);
            }
            $this->b[$botId]['h'][] = $value;
        } else {
            $botId = (int) $parts[1];
            $low = (int) $parts[6];
            if ($parts[5] === 'output') {
                $low = ($low + 1000) * -1;
            }
            $high = (int) $parts[11];
            if ($parts[10] === 'output') {
                $high = ($high + 1000) * -1;
            }
            if (!isset($this->b[$botId])) {
                $this->initBot($botId);
            }
            $this->b[$botId]['f'] = [$low, $high];
        }
        return 1;
    }

    private function sim(bool $b = false): void
    {
        $this->inputLines([$this, 'process']);
        while (true) {
            foreach ($this->b as $id => $bot) {
                $f = $bot['f'];
                $h = $bot['h'];
                if (count($h) === 2) {
                    printf("B %d: ", $id);
                    if (empty($f)) {
                        throw new \Exception(sprintf('Bot %d stuck', $id));
                    }
                    if ($f[0] >= 0) {
                        printf("%d -> B %d, ", min($h), $f[0]);
                        $this->b[$f[0]]['h'][] = min($h);
                    } else {
                        $o = ($f[0] * -1) - 1000;
                        printf("%d -> OUT %d, ", min($h), $o);
                        $this->o[$o] = min($h);
                    }
                    if ($f[1] >= 0) {
                        printf("%d -> B %d\n", max($h), $f[1]);
                        $this->b[$f[1]]['h'][] = max($h);
                    } else {
                        $o = ($f[1] * -1) - 1000;
                        printf("%d -> OUT %d\n", max($h), $o);
                        $this->o[$o] = max($h);
                    }
                    $this->b[$id]['h'] = [];

                    if (!$b && min($h) === 17 && max($h) === 61) {
                        break 2;
                    }

                    if ($b && isset($this->o[0], $this->o[1], $this->o[2])) {
                        printf("%d\n", $this->o[0] * $this->o[1] * $this->o[2]);
                        break 2;
                    }
                } elseif (count($h) > 2) {
                    throw new \Exception('Overful hands %d', $id);
                }
            }
        }
    }

    public function run(): void
    {
        $this->sim();
    }

    public function runB(): void
    {
        $this->sim(true);
    }
}