<?php
declare(strict_types=1);

namespace App\Y2016;

use App\Day;
use App\Y2016\Model\BunnyCode;

class D12 extends Day
{
    public function run(): void
    {
        $bunny = new BunnyCode();
        $this->inputLines([$bunny, 'load']);

        $bunny->run();
        printf("%d\n", $bunny->getRegs()[0]);
    }

    public function runB(): void
    {
        $bunny = new BunnyCode();
        $this->inputLines([$bunny, 'load']);

        $bunny->run([0, 0, 1, 0]);
        printf("%d\n", $bunny->getRegs()[0]);
    }
}