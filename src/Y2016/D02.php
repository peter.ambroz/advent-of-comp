<?php

declare(strict_types=1);

namespace App\Y2016;

use App\Day;

class D02 extends Day
{
    private int $x = 1;
    private int $y = 1;

    public function parseLine(string $line): int
    {
        $kp = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9],
        ];
        foreach (str_split($line) as $ch) {
            switch ($ch) {
                case 'U':
                    $this->y = max($this->y - 1, 0);
                    break;
                case 'R':
                    $this->x = min($this->x + 1, 2);
                    break;
                case 'D':
                    $this->y = min($this->y + 1, 2);
                    break;
                case 'L':
                    $this->x = max($this->x - 1, 0);
                    break;
            }
        }

        return $kp[$this->y][$this->x];
    }

    public function parseLineB(string $line): string
    {
        $kp = [
            ['0', '0', '1', '0', '0'],
            ['0', '2', '3', '4', '0'],
            ['5', '6', '7', '8', '9'],
            ['0', 'A', 'B', 'C', '0'],
            ['0', '0', 'D', '0', '0'],
        ];
        foreach (str_split($line) as $ch) {
            switch ($ch) {
                case 'U':
                    $y = max($this->y - 1, 0);
                    if ($kp[$y][$this->x] !== '0') {
                        $this->y = $y;
                    }
                    break;
                case 'R':
                    $x = min($this->x + 1, 4);
                    if ($kp[$this->y][$x] !== '0') {
                        $this->x = $x;
                    }
                    break;
                case 'D':
                    $y = min($this->y + 1, 4);
                    if ($kp[$y][$this->x] !== '0') {
                        $this->y = $y;
                    }
                    break;
                case 'L':
                    $x = max($this->x - 1, 0);
                    if ($kp[$this->y][$x] !== '0') {
                        $this->x = $x;
                    }
                    break;
            }
        }

        return $kp[$this->y][$this->x];
    }

    public function run(): void
    {
        foreach ($this->inputLines([$this, 'parseline']) as $code) {
            printf("%d", $code);
        }
        printf("\n");
    }

    public function runB(): void
    {
        foreach ($this->inputLines([$this, 'parseLineB']) as $code) {
            printf("%s", $code);
        }
        printf("\n");
    }
}