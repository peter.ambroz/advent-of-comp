<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;
use App\Y2020\Model\Edge;
use App\Y2020\Model\Tile;

class D20 extends Day
{
    /** @var Tile[] */
    private array $tiles = [];
    /** @var Edge[]  */
    private array $edges = [];

    /** @var string[][] */
    private array $ltmp;
    private int $tid;

    public function process(string $line): int
    {
        if (!strlen($line)) {
            $this->tiles[$this->tid] = $t = new Tile($this->tid, $this->ltmp);
            foreach ($t->getCanonicalEdges() as $edge) {
                $e = $this->edges[$edge] ?? null;
                if ($e === null) {
                    $this->edges[$edge] = $e = new Edge($edge);
                }

                $e->tiles[] = $t;
            }
            return 0;
        }

        if (substr($line, 0, 4) === 'Tile') {
            $this->tid = (int) substr($line, 5, 4);
            $this->ltmp = [];
            return 1;
        }

        $this->ltmp[] = str_split($line);

        return 1;
    }

    private function findTile(int $edgeId, int $edgeVal, int $notThis): Tile
    {
        foreach ($this->tiles as $tile) {
            if ($tile->id === $notThis) {
                continue;
            }

            if (in_array($edgeVal, $tile->getAllEdges())) {
                $tile->align($edgeId, $edgeVal);
                return $tile;
            }
        }

        throw new \Exception(sprintf('Tile not found %d %d %d', $edgeId, $edgeVal, $notThis));
    }

    public function run(): void
    {
        $this->inputLines([$this, 'process']);
        $count = [];
        foreach ($this->edges as $edge) {
            if (count($edge->tiles) === 1) {
                $tid = $edge->tiles[0]->id;
                if (!isset($count[$tid])) {
                    $count[$tid] = 0;
                }
                $count[$tid]++;
            }
        }

        $corners = 1;
        foreach ($count as $tid => $cnt) {
            if ($cnt === 2) {
                $corners *= $tid;
            }
        }

        printf("%d\n", $corners);
    }

    public function runB(): void
    {
        $this->inputLines([$this, 'process']);

        /** @var Tile[][] $map */
        $map = [];
        $upperId = -1;
        $upperAlign = 402;

        for ($j = 0; $j < 12; $j++) {
            $firstTile = $this->findTile(Tile::E_UP, $upperAlign, $upperId);
            $map[$j] = [$firstTile];

            $leftId = $firstTile->id;
            $leftAlign = $firstTile->edges[Tile::E_RIGHT];

            for ($i = 1; $i < 12; $i++) {
                $nextTile = $this->findTile(Tile::E_LEFT, $leftAlign, $leftId);
                $map[$j][$i] = $nextTile;

                $leftId = $nextTile->id;
                $leftAlign = $nextTile->edges[Tile::E_RIGHT];
            }

            $upperId = $firstTile->id;
            $upperAlign = $firstTile->edges[Tile::E_DOWN];
        }

        $count = 0;
        foreach ($map as $tileRow) {
            foreach ($tileRow as $tile) {
                foreach ($tile->image as $irow) {
                    foreach ($irow as $ichar) {
                        if ($ichar === '#') {
                            $count++;
                        }
                    }
                }
            }
        }

        printf("%d\n", $count - 225);
    }
}