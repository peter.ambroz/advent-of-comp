<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D24 extends Day
{
    private array $hex = [];
    /** @var int[] */
    private array $size;

    private function idx(array $ptr): string
    {
        return implode(',', $ptr);
    }

    private function countAdjacent(string $idx): int
    {
        [$x, $y] = explode(',', $idx);
        $x = (int)$x;
        $y = (int)$y;
        $c = 0;
        for ($i = -1; $i <= 1; $i++) {
            for ($j = -1; $j <= 1; $j++) {
                if ($i + $j === 0) {
                    continue;
                }
                if (isset($this->hex[$this->idx([$x + $i, $y + $j])])) {
                    $c++;
                }
            }
        }
        return $c;
    }

    private function step(): void
    {
        $n = [];
        $ns = ['xmi' => 999999, 'xma' => -999999, 'ymi' => 999999, 'yma' => -999999];
        for ($i = $this->size['xmi'] - 1; $i <= $this->size['xma'] + 1; $i++) {
            for ($j = $this->size['ymi'] - 1; $j <= $this->size['yma'] + 1; $j++) {
                $id = $this->idx([$i, $j]);
                $c = $this->countAdjacent($id);
                if ((isset($this->hex[$id]) && ($c === 1 || $c === 2)) || (!isset($this->hex[$id]) && ($c === 2))) {
                    $n[$id] = true;
                    if ($i < $ns['xmi']) $ns['xmi'] = $i;
                    if ($i > $ns['xma']) $ns['xma'] = $i;
                    if ($j < $ns['ymi']) $ns['ymi'] = $j;
                    if ($j > $ns['yma']) $ns['yma'] = $j;
                }
            }
        }
        $this->size = $ns;
        $this->hex = $n;
    }

    public function process(string $line): int
    {
        $cmd = '';
        $ptr = [0, 0];
        foreach (str_split($line) as $char) {
            $cmd .= $char;
            if ($char === 's' || $char === 'n') {
                continue;
            }

            switch ($cmd) {
                case 'e':
                    $ptr[1]++;
                    break;
                case 'ne':
                    $ptr[0]--;
                    break;
                case 'nw':
                    $ptr[0]--;
                    $ptr[1]--;
                    break;
                case 'w':
                    $ptr[1]--;
                    break;
                case 'sw':
                    $ptr[0]++;
                    break;
                case 'se':
                    $ptr[0]++;
                    $ptr[1]++;
                    break;
            }

            $cmd = '';
        }

        $id = $this->idx($ptr);
        if (isset($this->hex[$id])) {
            unset($this->hex[$id]);
        } else {
            $this->hex[$id] = true;

            if ($ptr[0] > $this->size['yma']) {
                $this->size['yma'] = $ptr[0];
            }
            if ($ptr[0] < $this->size['ymi']) {
                $this->size['ymi'] = $ptr[0];
            }
            if ($ptr[1] > $this->size['xma']) {
                $this->size['xma'] = $ptr[1];
            }
            if ($ptr[1] < $this->size['xmi']) {
                $this->size['xmi'] = $ptr[1];
            }
        }

        return 1;
    }

    public function run(): void
    {
        $this->size = ['xmi' => 9999, 'xma' => -9999, 'ymi' => 9999, 'yma' => -9999];
        $this->inputLines([$this, 'process']);
        printf("%d\n", count($this->hex));
        for ($i = $this->size['ymi']; $i <= $this->size['yma']; $i++) {
            for ($j = $this->size['xmi']; $j <= $this->size['xma']; $j++) {
                if (isset($this->hex[$this->idx([$i, $j])])) {
                    echo '#';
                } else {
                    echo '.';
                }
            }
            echo "\n";
        }
    }

    public function runB(): void
    {
        $this->size = ['xmi' => 9999, 'xma' => -9999, 'ymi' => 9999, 'yma' => -9999];
        $this->inputLines([$this, 'process']);

        for ($i = 0; $i < 100; $i++) {
            $this->step();
        }

        printf("%d\n", count($this->hex));
    }
}