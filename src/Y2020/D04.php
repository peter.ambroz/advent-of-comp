<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;
use App\Y2020\Model\Passport;

class D04 extends Day
{
    public function run(): void
    {
        $pass = new Passport();
        $valid = 0;
        foreach (explode("\n", file_get_contents($this->inputFileName())) as $line) {
            if (!strlen($line)) {
                if ($pass->isValid()) {
                    $valid++;
                }
                $pass = new Passport();
            } else {
                foreach (explode(" ", $line) as $token) {
                    [$tname, $tval] = explode(':', $token);
                    $pass->$tname = $tval;
                }
            }
        }

        printf("%d\n", $valid);
    }

    public function runB(): void
    {
        $pass = new Passport();
        $valid = 0;
        foreach (explode("\n", file_get_contents($this->inputFileName())) as $line) {
            if (!strlen($line)) {
                if ($pass->attributesValid()) {
                    $valid++;
                }
                $pass = new Passport();
            } else {
                foreach (explode(" ", $line) as $token) {
                    [$tname, $tval] = explode(':', $token);
                    $pass->$tname = $tval;
                }
            }
        }

        printf("%d\n", $valid);
    }
}