<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D17 extends Day
{
    /** @var int[] */
    private array $size;
    private array $g = [];
    private int $y = 0;

    private function idx(int $x, int $y, int $z, $w): string
    {
        return sprintf("%d:%d:%d:%d", $x, $y, $z, $w);
    }

    private function countAdjacent(string $idx): int
    {
        [$x, $y, $z, $w] = explode(':', $idx);
        $x = (int)$x;
        $y = (int)$y;
        $z = (int)$z;
        $w = (int)$w;
        $c = 0;
        for ($i = -1; $i <= 1; $i++) {
            for ($j = -1; $j <= 1; $j++) {
                for ($k = -1; $k <= 1; $k++) {
                    for ($l = -1; $l <= 1; $l++) {
                        if ($i === 0 && $j === 0 && $k === 0 && $l === 0) {
                            continue;
                        }
                        if (isset($this->g[$this->idx($x + $i, $y + $j, $z + $k, $w + $l)])) {
                            $c++;
                        }
                    }
                }
            }
        }
        return $c;
    }

    public function init(string $line): int
    {
        foreach (str_split($line) as $x => $ch) {
            if ($ch === '#') {
                $this->g[$this->idx($x, $this->y, 0, 0)] = 1;
            }
        }

        $this->y++;

        return 1;
    }

    private function step(): void
    {
        $n = [];
        $ns = ['xmi' => 999999, 'xma' => -999999, 'ymi' => 999999, 'yma' => -999999, 'zmi' => 999999, 'zma' => -999999];
        for ($i = $this->size['xmi'] - 1; $i <= $this->size['xma'] + 1; $i++) {
            for ($j = $this->size['ymi'] - 1; $j <= $this->size['yma'] + 1; $j++) {
                for ($k = $this->size['zmi'] - 1; $k <= $this->size['zma'] + 1; $k++) {
                    $id = $this->idx($i, $j, $k, 0);
                    $c = $this->countAdjacent($id);
                    if ((isset($this->g[$id]) && ($c === 2 || $c === 3)) || (!isset($this->g[$id]) && ($c === 3))) {
                        $n[$id] = 1;
                        if ($i < $ns['xmi']) $ns['xmi'] = $i;
                        if ($i > $ns['xma']) $ns['xma'] = $i;
                        if ($j < $ns['ymi']) $ns['ymi'] = $j;
                        if ($j > $ns['yma']) $ns['yma'] = $j;
                        if ($k < $ns['zmi']) $ns['zmi'] = $k;
                        if ($k > $ns['zma']) $ns['zma'] = $k;
                    }
                }
            }
        }
        $this->size = $ns;
        $this->g = $n;
    }

    private function stepB(): void
    {
        $n = [];
        $ns = ['xmi' => 999999, 'xma' => -999999, 'ymi' => 999999, 'yma' => -999999, 'zmi' => 999999, 'zma' => -999999, 'wmi' => 999999, 'wma' => -999999];
        for ($i = $this->size['xmi'] - 1; $i <= $this->size['xma'] + 1; $i++) {
            for ($j = $this->size['ymi'] - 1; $j <= $this->size['yma'] + 1; $j++) {
                for ($k = $this->size['zmi'] - 1; $k <= $this->size['zma'] + 1; $k++) {
                    for ($l = $this->size['wmi'] - 1; $l <= $this->size['wma'] + 1; $l++) {
                        $id = $this->idx($i, $j, $k, $l);
                        $c = $this->countAdjacent($id);
                        if ((isset($this->g[$id]) && ($c === 2 || $c === 3)) || (!isset($this->g[$id]) && ($c === 3))) {
                            $n[$id] = 1;
                            if ($i < $ns['xmi']) $ns['xmi'] = $i;
                            if ($i > $ns['xma']) $ns['xma'] = $i;
                            if ($j < $ns['ymi']) $ns['ymi'] = $j;
                            if ($j > $ns['yma']) $ns['yma'] = $j;
                            if ($k < $ns['zmi']) $ns['zmi'] = $k;
                            if ($k > $ns['zma']) $ns['zma'] = $k;
                            if ($l < $ns['wmi']) $ns['wmi'] = $l;
                            if ($l > $ns['wma']) $ns['wma'] = $l;
                        }
                    }
                }
            }
        }
        $this->size = $ns;
        $this->g = $n;
    }

    public function run(): void
    {
        $this->inputLines([$this, 'init']);
        $this->size = ['xmi' => 0, 'xma' => 7, 'ymi' => 0, 'yma' => 7, 'zmi' => 0, 'zma' => 0];

        for ($i = 0; $i < 6; $i++) {
            $this->step();
        }

        printf("%d\n", array_sum($this->g));
    }

    public function runB(): void
    {
        $this->inputLines([$this, 'init']);
        $this->size = ['xmi' => 0, 'xma' => 7, 'ymi' => 0, 'yma' => 7, 'zmi' => 0, 'zma' => 0, 'wmi' => 0, 'wma' => 0];

        for ($i = 0; $i < 6; $i++) {
            $this->stepB();
        }

        printf("%d\n", array_sum($this->g));
    }
}