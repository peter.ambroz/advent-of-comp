<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D09 extends Day
{
    private array $buf;
    private int $iter;

    public function verify(string $num): bool
    {
        if ($this->iter < 25) {
            $this->buf[$this->iter++] = $num;
            return true;
        }

        foreach ($this->buf as $i1 => $val1) {
            foreach ($this->buf as $i2 => $val2) {
                if ($i1 <= $i2) {
                    break;
                }
                if (bccomp(bcadd($val1, $val2), $num) === 0) {
                    $ptr = $this->iter % 25;
                    $this->buf[$ptr] = $num;
                    $this->iter++;
                    return true;
                }
            }
        }

        throw new \Exception($num);
    }

    public function run(): void
    {
        $this->iter = 0;
        $this->buf = array_fill(0, 25, 0);
        try {
            $this->inputLines([$this, 'verify']);
        } catch (\Exception $e) {
            printf("%s\n", $e->getMessage());
        }
    }

    public function runB(): void
    {
        $input = 14360655;
        $sum = 0;
        $min = 0;
        $max = -1;
        $buf = $this->inputLines();
        while ($sum != $input) {
            if ($sum < $input) {
                $sum += (int)$buf[++$max];
            } elseif ($sum > $input) {
                $sum -= (int)$buf[$min++];
            }
        }

        $minv = 99999999;
        $maxv = -9999;
        for ($i = $min; $i<= $max; $i++) {
            if ((int)$buf[$i] > $maxv) {
                $maxv = (int)$buf[$i];
            }

            if ((int)$buf[$i] < $minv) {
                $minv = (int)$buf[$i];
            }
        }
        printf("%d %d-%d\n", $minv + $maxv, $min, $max);
    }
}