<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D19 extends Day
{
    private array $r;
    private bool $m;

    private function expand(int $ruleId): array
    {
        $newr = [];
        foreach ($this->r[$ruleId] as $rulePart) {
            if (!is_numeric($rulePart)) {
                $newr[] = $rulePart;
            } else {
                $newr[] = '(';
                $newr = array_merge($newr, $this->expand((int) $rulePart));
                $newr[] = ')';
            }
        }
        $this->r[$ruleId] = $newr;
        return $newr;
    }

    private function match(string $line, bool $b = false): int
    {
        $re = '/^(((b(b(((b(ab|ba)|a((b|a)b|aa))b|(b(ab|ba)|a(ba|aa))a)a|(b((aa|ab)b)|a(a(ab)))b)|a((a((bb)a|(bb)b)|b((ab)a|(aa|bb)b))b|((b((b|a)a|ab))a|(b(ba)|a(bb))b)a))|a(b(b((((b|a)(b|a))b|(ba|aa)a)b|(a(ba))a)|a(b(((b|a)b|ba)(b|a))|a(((b|a)(b|a))a|(bb|ab)b)))|a(a(b((aa|bb)a|((b|a)b|ba)b)|a((bb)b|(ba)a))|b(a(b(ba)|a(bb))|b(b((b|a)b|aa)|a(ab|ba))))))b|(b((((((b|a)(b|a))a|((b|a)a|ab)b)b|(b(ba|aa)|a(ba))a)b|(a((ab)a|(ab)b)|b(a((b|a)b|ba)|b(bb|ab)))a)b|((a((aa|bb)b|(aa|ab)a)|b((bb|ab)a|(aa|ab)b))a|(((aa|bb)a|((b|a)b|ba)b)b|((aa|ab)(b|a))a)b)a)|a(a(b((((b|a)(b|a))a|(bb|ab)b)b|(a(aa|bb)|b((b|a)(b|a)))a)|a(b(b((b|a)b|ba)|a((b|a)a|ab))|a(((b|a)(b|a))b|(aa|ab)a)))|b(b(((ba)a|((b|a)b|ba)b)a|((ab)b|(ba|aa)a)b)|a(((ab)b|(aa|bb)a)b|((bb|ab)a|((b|a)a|ab)b)a))))a))(((b(b(((b(ab|ba)|a((b|a)b|aa))b|(b(ab|ba)|a(ba|aa))a)a|(b((aa|ab)b)|a(a(ab)))b)|a((a((bb)a|(bb)b)|b((ab)a|(aa|bb)b))b|((b((b|a)a|ab))a|(b(ba)|a(bb))b)a))|a(b(b((((b|a)(b|a))b|(ba|aa)a)b|(a(ba))a)|a(b(((b|a)b|ba)(b|a))|a(((b|a)(b|a))a|(bb|ab)b)))|a(a(b((aa|bb)a|((b|a)b|ba)b)|a((bb)b|(ba)a))|b(a(b(ba)|a(bb))|b(b((b|a)b|aa)|a(ab|ba))))))b|(b((((((b|a)(b|a))a|((b|a)a|ab)b)b|(b(ba|aa)|a(ba))a)b|(a((ab)a|(ab)b)|b(a((b|a)b|ba)|b(bb|ab)))a)b|((a((aa|bb)b|(aa|ab)a)|b((bb|ab)a|(aa|ab)b))a|(((aa|bb)a|((b|a)b|ba)b)b|((aa|ab)(b|a))a)b)a)|a(a(b((((b|a)(b|a))a|(bb|ab)b)b|(a(aa|bb)|b((b|a)(b|a)))a)|a(b(b((b|a)b|ba)|a((b|a)a|ab))|a(((b|a)(b|a))b|(aa|ab)a)))|b(b(((ba)a|((b|a)b|ba)b)a|((ab)b|(ba|aa)a)b)|a(((ab)b|(aa|bb)a)b|((bb|ab)a|((b|a)a|ab)b)a))))a)((((b(b((bb|ab)a|(bb)b)|a(b(ba)|a(ba|aa)))|a(b(((b|a)(b|a))a|(bb|ab)b)|a(b(ba|aa)|a(ba))))b|(b((((b|a)(b|a))a|(bb|ab)b)a|((ab)b|(aa|ab)a)b)|a(b(b(bb|ab)|a(ab|ba))))a)b|(b(b(b(a((b|a)a|ab)|b(ab|ba))|a(b(ab)|a((b|a)b|aa)))|a(a((bb)a|(bb)b)|b(((b|a)a|ab)a|(ab)b)))|a(a((a(ab)|b(aa))b|(a(bb|ab)|b(ab))a)|b(b(a((b|a)a|ab)|b(ba))|a(b(aa|bb)|a(aa)))))a)a|(((b(((b|a)((b|a)(b|a)))a|(a(aa|bb)|b((b|a)(b|a)))b)|a(b(a((b|a)a|ab)|b(bb|ab))|a(b(aa|ab)|a((b|a)a|ab))))a|(a((a(ba))b|((aa)a|(ab)b)a)|b(b((bb|ab)a|((b|a)b|aa)b)|a(a((b|a)a|ab)|b(ab|ba))))b)b|(a(b((b((b|a)(b|a)))b|((bb)b|((b|a)b|ba)a)a)|a(b(((b|a)a|ab)a|(aa|bb)b)|a(b(ab|ba)|a((b|a)b|aa))))|b(a(a(((b|a)(b|a))a|((b|a)a|ab)b)|b(((b|a)b|aa)b|(bb)a))|b(a((bb|ab)a|(ba)b)|b((aa|bb)(b|a)))))a)b))$/';
        $reb = '/^((b(b(((b(ab|ba)|a((b|a)b|aa))b|(b(ab|ba)|a(ba|aa))a)a|(b((aa|ab)b)|a(a(ab)))b)|a((a((bb)a|(bb)b)|b((ab)a|(aa|bb)b))b|((b((b|a)a|ab))a|(b(ba)|a(bb))b)a))|a(b(b((((b|a)(b|a))b|(ba|aa)a)b|(a(ba))a)|a(b(((b|a)b|ba)(b|a))|a(((b|a)(b|a))a|(bb|ab)b)))|a(a(b((aa|bb)a|((b|a)b|ba)b)|a((bb)b|(ba)a))|b(a(b(ba)|a(bb))|b(b((b|a)b|aa)|a(ab|ba))))))b|(b((((((b|a)(b|a))a|((b|a)a|ab)b)b|(b(ba|aa)|a(ba))a)b|(a((ab)a|(ab)b)|b(a((b|a)b|ba)|b(bb|ab)))a)b|((a((aa|bb)b|(aa|ab)a)|b((bb|ab)a|(aa|ab)b))a|(((aa|bb)a|((b|a)b|ba)b)b|((aa|ab)(b|a))a)b)a)|a(a(b((((b|a)(b|a))a|(bb|ab)b)b|(a(aa|bb)|b((b|a)(b|a)))a)|a(b(b((b|a)b|ba)|a((b|a)a|ab))|a(((b|a)(b|a))b|(aa|ab)a)))|b(b(((ba)a|((b|a)b|ba)b)a|((ab)b|(ba|aa)a)b)|a(((ab)b|(aa|bb)a)b|((bb|ab)a|((b|a)a|ab)b)a))))a)+(((b(b(((b(ab|ba)|a((b|a)b|aa))b|(b(ab|ba)|a(ba|aa))a)a|(b((aa|ab)b)|a(a(ab)))b)|a((a((bb)a|(bb)b)|b((ab)a|(aa|bb)b))b|((b((b|a)a|ab))a|(b(ba)|a(bb))b)a))|a(b(b((((b|a)(b|a))b|(ba|aa)a)b|(a(ba))a)|a(b(((b|a)b|ba)(b|a))|a(((b|a)(b|a))a|(bb|ab)b)))|a(a(b((aa|bb)a|((b|a)b|ba)b)|a((bb)b|(ba)a))|b(a(b(ba)|a(bb))|b(b((b|a)b|aa)|a(ab|ba))))))b|(b((((((b|a)(b|a))a|((b|a)a|ab)b)b|(b(ba|aa)|a(ba))a)b|(a((ab)a|(ab)b)|b(a((b|a)b|ba)|b(bb|ab)))a)b|((a((aa|bb)b|(aa|ab)a)|b((bb|ab)a|(aa|ab)b))a|(((aa|bb)a|((b|a)b|ba)b)b|((aa|ab)(b|a))a)b)a)|a(a(b((((b|a)(b|a))a|(bb|ab)b)b|(a(aa|bb)|b((b|a)(b|a)))a)|a(b(b((b|a)b|ba)|a((b|a)a|ab))|a(((b|a)(b|a))b|(aa|ab)a)))|b(b(((ba)a|((b|a)b|ba)b)a|((ab)b|(ba|aa)a)b)|a(((ab)b|(aa|bb)a)b|((bb|ab)a|((b|a)a|ab)b)a))))a)(?148)?((((b(b((bb|ab)a|(bb)b)|a(b(ba)|a(ba|aa)))|a(b(((b|a)(b|a))a|(bb|ab)b)|a(b(ba|aa)|a(ba))))b|(b((((b|a)(b|a))a|(bb|ab)b)a|((ab)b|(aa|ab)a)b)|a(b(b(bb|ab)|a(ab|ba))))a)b|(b(b(b(a((b|a)a|ab)|b(ab|ba))|a(b(ab)|a((b|a)b|aa)))|a(a((bb)a|(bb)b)|b(((b|a)a|ab)a|(ab)b)))|a(a((a(ab)|b(aa))b|(a(bb|ab)|b(ab))a)|b(b(a((b|a)a|ab)|b(ba))|a(b(aa|bb)|a(aa)))))a)a|(((b(((b|a)((b|a)(b|a)))a|(a(aa|bb)|b((b|a)(b|a)))b)|a(b(a((b|a)a|ab)|b(bb|ab))|a(b(aa|ab)|a((b|a)a|ab))))a|(a((a(ba))b|((aa)a|(ab)b)a)|b(b((bb|ab)a|((b|a)b|aa)b)|a(a((b|a)a|ab)|b(ab|ba))))b)b|(a(b((b((b|a)(b|a)))b|((bb)b|((b|a)b|ba)a)a)|a(b(((b|a)a|ab)a|(aa|bb)b)|a(b(ab|ba)|a((b|a)b|aa))))|b(a(a(((b|a)(b|a))a|((b|a)a|ab)b)|b(((b|a)b|aa)b|(bb)a))|b(a((bb|ab)a|(ba)b)|b((aa|bb)(b|a)))))a)b))$/';
        return preg_match($b ? $reb : $re, $line);
    }

    public function parse(string $line): int
    {
        if (!strlen($line)) {
            $this->m = true;
            return 0;
        }

        if ($this->m) {
            return $this->match($line);
        }

        return 0;
    }

    public function parseB(string $line): int
    {
        if (!strlen($line)) {
            $this->m = true;
            return 0;
        }

        if ($this->m) {
            return $this->match($line, true);
        }

        return 0;
    }

    public function simpleParse(string $line): int
    {
        if (!strlen($line)) {
            $this->m = true;
            return 0;
        }

        if ($this->m) {
            return 0;
        } else {
            [$ruleno, $ruledata] = explode(': ', $line);
            $this->r[$ruleno] = explode(' ', $ruledata);
            return 0;
        }
    }

    public function run(): void
    {
        $this->m = false;
        printf("%d\n" , array_sum($this->inputLines([$this, 'parse'])));
    }

    public function runB(): void
    {
//        $this->r = array_fill(0, 132, []);
        $this->m = false;
        printf("%d\n" , array_sum($this->inputLines([$this, 'parseB'])));
//        $this->expand(0);
        /*
        foreach ($this->r as $rule) {
            $newrule = sprintf("/^%s$/\n", str_replace(['(a)', '(b)'], ['a', 'b'], implode('', $rule)));
            file_put_contents($this->inputFileName() . '.2.txt', $newrule, FILE_APPEND);
        }*/

    }
}