<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D07 extends Day
{
    private array $bagTree = [];
    private array $bagFwd = [];

    public function parseBag(string $bagline): array
    {
        [$outer, $innerline] = explode(' bags contain ', $bagline);
        $innerline = substr($innerline, 0, -1);
        $inner = [];
        if ($innerline !== 'no other bags') {
            foreach (explode(', ', $innerline) as $innerbag) {
                $innerparts = explode(' ', $innerbag);
                $amount = (int)array_shift($innerparts);
                array_pop($innerparts);
                $color = implode(' ', $innerparts);

                $inner[$color] = $amount;

                if (!isset($this->bagTree[$color])) {
                    $this->bagTree[$color] = [];
                }
                $this->bagTree[$color][] = $outer;
            }
        }

        return [$outer => $inner];
    }

    public function collect(string $start): array
    {
        $children = $this->bagTree[$start] ?? [];
        $collection = $children;
        foreach ($children as $item) {
            $collection = array_unique(array_merge($collection, $this->collect($item)));
        }

        return $collection;
    }

    public function count(string $start): int
    {
        $c = 1;
        $children = $this->bagFwd[$start] ?? [];
        foreach ($children as $color => $cnt) {
            $c += $cnt * $this->count($color);
        }

        return $c;
    }

    public function run(): void
    {
        $this->inputLines([$this, 'parseBag']);
        printf("%d\n", count($this->collect('shiny gold')));
    }

    public function runB(): void
    {
        foreach ($this->inputLines([$this, 'parseBag']) as $baginfo) {
            $a = array_keys($baginfo);
            $b = array_values($baginfo);
            $this->bagFwd[$a[0]] = $b[0];
        }

        printf("%d\n", $this->count('shiny gold') - 1);
    }
}