<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D22 extends Day
{
    private array $deckSave;

    private function score(array $cards): int
    {
        $s = 0;
        $i = count($cards);
        foreach ($cards as $c) {
            $s += $c * $i;
            $i--;
        }
        return $s;
    }

    public function run(): void
    {
        $deck = [
            [1, 10, 28, 29, 13, 11, 35, 7, 43, 8, 30, 25, 4, 5, 17, 32, 22, 39, 50, 46, 16, 26, 45, 38, 21],
            [19, 40, 2, 12, 49, 23, 34, 47, 9, 14, 20, 24, 42, 37, 48, 44, 27, 6, 33, 18, 15, 3, 36, 41, 31],
        ];

        while (count($deck[0]) && count($deck[1])) {
            $t1 = array_shift($deck[0]);
            $t2 = array_shift($deck[1]);
            if ($t1 > $t2) {
                $deck[0][] = $t1;
                $deck[0][] = $t2;
            } else {
                $deck[1][] = $t2;
                $deck[1][] = $t1;
            }
        }

        printf("%d %d\n", $this->score($deck[0]), $this->score($deck[1]));
    }

    private function hash(array $deck): string
    {
        return md5(sprintf('[%s][%s]', implode(',', $deck[0]), implode(',', $deck[1])));
    }

    private function subGame(array $deck): int
    {
        $previous = [];
        $winner = -1;

        while (count($deck[0]) && count($deck[1])) {
            $gameHash = $this->hash($deck);
            if (isset($previous[$gameHash])) {
                $this->deckSave = $deck;
                return 0;
            }
            $previous[$gameHash] = true;

            $top = [array_shift($deck[0]), array_shift($deck[1])];
            if ($top[0] <= count($deck[0]) && $top[1] <= count($deck[1])) {
                $winner = $this->subGame([array_slice($deck[0], 0, $top[0]), array_slice($deck[1], 0, $top[1])]);
            } else {
                if ($top[0] > $top[1]) {
                    $winner = 0;
                } else {
                    $winner = 1;
                }
            }

            $deck[$winner][] = $top[$winner];
            $deck[$winner][] = $top[1 - $winner];
        }

        $this->deckSave = $deck;
        return $winner;
    }

    public function runB(): void
    {
        $deck = [
            [1, 10, 28, 29, 13, 11, 35, 7, 43, 8, 30, 25, 4, 5, 17, 32, 22, 39, 50, 46, 16, 26, 45, 38, 21],
            [19, 40, 2, 12, 49, 23, 34, 47, 9, 14, 20, 24, 42, 37, 48, 44, 27, 6, 33, 18, 15, 3, 36, 41, 31],
        ];

        $w = $this->subGame($deck);

        printf("%d %d %d\n", $this->score($this->deckSave[0]), $this->score($this->deckSave[1]), $w);
    }
}