<?php

declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D06 extends Day
{
    public function run(): void
    {
        $lines = $this->inputLines();

        $gsum = 0;
        $gq = [];
        foreach ($lines as $line) {
            if (!strlen($line)) {
                $gsum += count($gq);
                $gq = [];
            } else {
                foreach (str_split($line) as $ch) {
                    $gq[$ch] = true;
                }
            }
        }

        printf("%d\n", $gsum);
    }

    public function runB(): void
    {
        $lines = $this->inputLines();

        $gsum = 0;
        $gq = str_split('abcdefghijklmnopqrstuvwxyz');
        foreach ($lines as $line) {
            if (!strlen($line)) {
                $gsum += count($gq);
                $gq = str_split('abcdefghijklmnopqrstuvwxyz');
            } else {
                $gq = array_intersect($gq, str_split($line));
            }
        }

        printf("%d\n", $gsum);
    }
}