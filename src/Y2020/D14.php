<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D14 extends Day
{
    private array $mem = [];
    private array $mask = [];

    private function applyMask(int $value): int
    {
        foreach ($this->mask as $k => $v) {
            $p = 35 - $k;
            if ($v === '0') {
                $value &= ~(1 << $p);
            } elseif ($v === '1') {
                $value |= (1 << $p);
            }
        }
        return $value;
    }

    private function applyAddrMask(int $value): array
    {
        $xbits = [];
        foreach ($this->mask as $k => $v) {
            $p = 35 - $k;
            if ($v === '1') {
                $value |= (1 << $p);
            } elseif ($v === 'X') {
                $value &= ~(1 << $p);
                $xbits[] = $p;
            }
        }

        $addrs = [];
        $cnt = 0;
        $max = (1 << count($xbits));
        while ($cnt < $max) {
            $tmpA = $value;
            foreach ($xbits as $i => $x) {
                if ($cnt & (1 << $i)) {
                    $tmpA |= (1 << $x);
                } else {
                    $tmpA &= ~(1 << $x);
                }
            }
            $addrs[] = $tmpA;
            $cnt++;
        }

        return $addrs;
    }

    public function process(string $line): int
    {
        [$l, $r] = explode(' = ', $line);
        if ($l === 'mask') {
            $this->mask = str_split($r);
        } else {
            [, $pt2] = explode('[', $l);
            [$addr, ] = explode(']', $pt2);
            $addr = (int) $addr;
            $r = (int) $r;
            $this->mem[$addr] = $this->applyMask($r);
        }

        return 1;
    }

    public function processB(string $line): int
    {
        [$l, $r] = explode(' = ', $line);
        if ($l === 'mask') {
            $this->mask = str_split($r);
        } else {
            [, $pt2] = explode('[', $l);
            [$addr, ] = explode(']', $pt2);
            $addr = (int) $addr;
            $r = (int) $r;
            foreach ($this->applyAddrMask($addr) as $addrVal) {
                $this->mem[$addrVal] = $r;
            }
        }

        return 1;
    }

    public function run(): void
    {
        $this->inputLines([$this, 'process']);
        printf("%d\n", array_sum($this->mem));
    }

    public function runB(): void
    {
        $this->inputLines([$this, 'processB']);
        printf("%d\n", array_sum($this->mem));
    }
}