<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D18 extends Day
{
    private function tokenize(string $line): array
    {
        $res = [];
        $toks = explode(' ', $line);
        foreach ($toks as $tok) {
            while (substr($tok, 0, 1) === '(') {
                $res[] = '(';
                $tok = substr($tok, 1);
            }

            $tmp = 0;
            while (substr($tok, -1, 1) === ')') {
                $tmp++;
                $tok = substr($tok, 0, -1);
            }

            $res[] = $tok;

            for ($i = 0; $i < $tmp; $i++) {
                $res[] = ')';
            }
        }

        return $res;
    }

    private function apply(string $op, int $a, int $b): int
    {
        switch ($op) {
            case '+':
                return $a + $b;
            case '*':
                return $a * $b;
            default:
                throw new \Exception('Unknown OP');
        }
    }

    public function evaluate(string $line): int
    {
        $st = [];
        $op = [];
        foreach ($this->tokenize($line) as $t) {
            switch ($t) {
                case '(':
                    $op[] = '(';
                    break;
                case ')':
                    while (($top = array_pop($op)) !== '(') {
                        $st[] = $this->apply($top, array_pop($st), array_pop($st));
                    }
                    break;
                case '+':
                case '*':
                    while (count($op)) {
                        $top = array_pop($op);
                        if ($top === '(') {
                            $op[] = '(';
                            break;
                        }
                        $st[] = $this->apply($top, array_pop($st), array_pop($st));
                    }
                    $op[] = $t;
                    break;
                default:
                    $st[] = (int)$t;
                    break;
            }
        }

        while (count($op)) {
            $top = array_pop($op);
            $st[] = $this->apply($top, array_pop($st), array_pop($st));
        }

        return $st[0];
    }

    public function evaluateB(string $line): int
    {
        $st = [];
        $op = [];
        foreach ($this->tokenize($line) as $t) {
            switch ($t) {
                case '(':
                    $op[] = '(';
                    break;
                case ')':
                    while (($top = array_pop($op)) !== '(') {
                        $st[] = $this->apply($top, array_pop($st), array_pop($st));
                    }
                    break;
                case '+':
                case '*':
                    while (count($op)) {
                        $top = array_pop($op);
                        if ($top === '(') {
                            $op[] = '(';
                            break;
                        }
                        if ($top === '*' && $t === '+') {
                            $op[] = $top;
                            break;
                        }
                        $st[] = $this->apply($top, array_pop($st), array_pop($st));
                    }
                    $op[] = $t;
                    break;
                default:
                    $st[] = (int)$t;
                    break;
            }
        }

        while (count($op)) {
            $top = array_pop($op);
            $st[] = $this->apply($top, array_pop($st), array_pop($st));
        }

        return $st[0];
    }

    public function run(): void
    {
        printf("%d\n", array_sum($this->inputLines([$this, 'evaluate'])));
    }

    public function runB(): void
    {
        printf("%d\n", array_sum($this->inputLines([$this, 'evaluateB'])));
    }
}