<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D13 extends Day
{
    public function run(): void
    {
        $departure = 1000001;
        $buses = [13, 17, 19, 23, 29, 37, 41, 577, 601];

        $next = [];
        $wait = $departure + 700;
        $bestBus = -1;
        foreach ($buses as $bus) {
            $nd = ceil($departure / $bus) * $bus;
            if ($nd < $wait) {
                $wait = $nd;
                $bestBus = $bus;
            }
        }

        printf("%d %d %d\n", $wait, $bestBus, ($wait - $departure) * $bestBus);
    }
    
    public function runB(): void
    {
        $buses = [
            29 => 0,
            41 => 22,
            577 => 548,
            13 => 10,
            17 => 8,
            19 => 9,
            23 => 17,
            601 => 541,
            37 => 14,
        ];
        $t = 1;
        foreach ($buses as $bus => $mod) {
            $t *= $bus;
        }

        $f = 0;
        foreach ($buses as $bus => $mod) {
            for ($i = 0; $i < $bus; $i++) {
                if ((($t/$bus) * $i) % $bus === $mod) {
                    $f += ($i * ($t/$bus)) % $t;
                    printf("%d\n", $i * ($t/$bus));
                }
            }

        }
        printf("%d\n", $f % $t);
    }
}