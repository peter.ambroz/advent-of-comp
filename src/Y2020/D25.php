<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D25 extends Day
{
    public function run(): void
    {
        $input = [9789649, 3647239];
        $loop = [14587082, 12387410];
        $mod = 20201227;
/*
        $n = 1;
        $i = 0;
        while ($n !== $input[0]) {
            $n = ($n * 7) % $mod;
            $i++;
        }

        $m = 1;
        $j = 0;
        while ($m !== $input[1]) {
            $m = ($m * 7) % $mod;
            $j++;
        }

        printf("%d %d\n", $i, $j);
*/

        $n = 1;
        for ($i = 0; $i < $loop[0]; $i++) {
            $n = ($n * $input[1]) % $mod;
        }

        $m = 1;
        for ($j = 0; $j < $loop[1]; $j++) {
            $m = ($m * $input[0]) % $mod;
        }


        printf("%d %d\n", $n, $m);
    }
}