<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D15 extends Day
{
    private int $last = 6;
    private array $said = [
        0 => 4,
        1 => 1,
        2 => 0,
        10 => 2,
        11 => 3,
    ];

    public function turn(int $i): void
    {
        $saw = $this->said[$this->last] ?? $i;
        $this->said[$this->last] = $i;
        $this->last = $i - $saw;
    }

    public function run(): void
    {
        for ($i = 6; $i < 2020; $i++) {
            $this->turn($i - 1);
        }

        printf("%d\n", $this->last);
    }

    public function runB(): void
    {
        for ($i = 6; $i < 30000000; $i++) {
            $this->turn($i - 1);
        }

        printf("%d\n", $this->last);
    }
}