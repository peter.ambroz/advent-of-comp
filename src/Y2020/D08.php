<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D08 extends Day
{
    private const JMP = 'jmp';
    private const ACC = 'acc';
    private const NOP = 'nop';
    private const HLT = 'hlt';

    private int $ip = 0;
    private int $acc = 0;
    private array $instr = [];

    public function parse(string $instr): array
    {
        $x = explode(" ", $instr);
        $x[1] = (int)$x[1];
        return $x;
    }

    private function exec(): void
    {
        $i = $this->instr[$this->ip];
        switch ($i[0]) {
            case self::JMP:
                $this->ip += $i[1];
                break;
            case self::ACC:
                $this->acc += $i[1];
                $this->ip++;
                break;
            case self::NOP:
                $this->ip++;
                break;
            case self::HLT:
                throw new \Exception("HALT", 0);
        }
    }

    public function run(): void
    {
        $this->instr = $this->inputLines([$this, 'parse']);

        $visited = [true];
        while (true) {
            $this->exec();
            if (!isset($visited[$this->ip])) {
                $visited[$this->ip] = true;
            } else {
                printf("%d\n", $this->acc);
                break;
            }
        }
    }

    public function runB(): void
    {
        $this->instr = $this->inputLines([$this, 'parse']);
        $this->instr[] = [self::HLT, 0];

        foreach ($this->instr as $ip => &$op) {
            if ($op[0] === self::JMP) {
                $op[0] = self::NOP;
            } elseif ($op[0] === self::NOP) {
                $op[0] = self::JMP;
            }

            $this->ip = 0;
            $this->acc = 0;
            $visited = [true];
            while (true) {
                try {
                    $this->exec();
                } catch (\Exception $e) {
                    if ($e->getCode() === 0) {
                        printf("HALTED ACC = %d\n", $this->acc);
                        break 2;
                    }
                }

                if (!isset($visited[$this->ip])) {
                    $visited[$this->ip] = true;
                } else {
                    //printf("Loop at IP = %d\n", $this->ip);
                    break;
                }
            }

            if ($op[0] === self::JMP) {
                $op[0] = self::NOP;
            } elseif ($op[0] === self::NOP) {
                $op[0] = self::JMP;
            }
        }
        unset($op);
    }
}