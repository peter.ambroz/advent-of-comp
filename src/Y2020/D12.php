<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D12 extends Day
{
    private int $x = 0;
    private int $y = 0;
    private int $wx = 10;
    private int $wy = -1;
    private int $dir = 0;

    public function move(string $line): bool
    {
        $dirmap = [
            0 => 'E',
            90 => 'N',
            180 => 'W',
            270 => 'S',
        ];

        $cmd = $line[0];
        $num = (int)substr($line, 1);

        if ($cmd === 'F') {
            $cmd = $dirmap[$this->dir];
        }

        switch ($cmd) {
            case 'N':
                $this->y -= $num;
                break;
            case 'S':
                $this->y += $num;
                break;
            case 'E':
                $this->x += $num;
                break;
            case 'W':
                $this->x -= $num;
                break;
            case 'L':
                $this->dir = ($this->dir + $num) % 360;
                break;
            case 'R':
                $this->dir = ($this->dir - $num + 360) % 360;
                break;
        }

        return true;
    }

    private function rotate(int $num): void
    {
        switch ($num) {
            case 90:
                $tmp = $this->wx;
                $this->wx = $this->wy;
                $this->wy = $tmp * -1;
                break;
            case 180:
                $this->wx *= -1;
                $this->wy *= -1;
                break;
            case 270:
                $tmp = $this->wx;
                $this->wx = $this->wy * -1;
                $this->wy = $tmp;
                break;
        }
    }

    public function moveB(string $line): bool
    {
        $cmd = $line[0];
        $num = (int)substr($line, 1);

        switch ($cmd) {
            case 'N':
                $this->wy -= $num;
                break;
            case 'S':
                $this->wy += $num;
                break;
            case 'E':
                $this->wx += $num;
                break;
            case 'W':
                $this->wx -= $num;
                break;
            case 'L':
                $this->rotate($num);
                break;
            case 'R':
                $this->rotate(360 - $num);
                break;
            case 'F':
                $this->x += $this->wx * $num;
                $this->y += $this->wy * $num;
                break;
        }

        return true;
    }

    public function run(): void
    {
        $this->inputLines([$this, 'move']);
        printf("%d\n", abs($this->x) + abs($this->y));
    }

    public function runB(): void
    {
        $this->inputLines([$this, 'moveB']);
        printf("%d\n", abs($this->x) + abs($this->y));
    }
}