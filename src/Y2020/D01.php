<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D01 extends Day
{
    public function run(): void
    {
        $nums = array_map(function (string $x): int {
            return (int)$x;
        }, explode("\n", file_get_contents($this->inputFileName())));

        foreach ($nums as $i1 => $x) {
            foreach ($nums as $i2 => $y) {
                if ($i1 >= $i2) {
                    continue;
                }

                if ($x + $y === 2020) {
                    printf("%d %d %d\n", $x, $y, $x * $y);
                    break 2;
                }
            }
        }
    }

    public function runB(): void
    {
        $nums = array_map(function (string $x): int {
            return (int)$x;
        }, explode("\n", file_get_contents($this->inputFileName())));

        foreach ($nums as $i1 => $x) {
            foreach ($nums as $i2 => $y) {
                foreach ($nums as $i3 => $z) {
                    if ($i1 >= $i2 || $i2 >= $i3 || $i1 >= $i3) {
                        continue;
                    }

                    if ($x + $y + $z === 2020) {
                        printf("%d %d %d %d\n", $x, $y, $z, $x * $y * $z);
                        break 3;
                    }
                }
            }
        }
    }
}
