<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D16 extends Day
{
    /** @var int[] */
    private array $myTicket;
    /** @var int[][] */
    private array $tickets = [];
    /** @var int[][] */
    private array $validTickets = [];
    /** @var int[][]  */
    private array $constraints = [];

    public function parse(string $line): int
    {
        if (strpos($line, 'or') !== false) {
            [$name, $value] = explode(': ', $line);
            $x = array_map(function (string $range): array {
                return explode('-', $range);
            }, explode(' or ', $value));
            $this->constraints[] = array_merge($x[0], $x[1]);
        } else {
            $this->tickets[] = array_map(function (string $num): int {
                return (int)$num;
            }, explode(',', $line));
        }
        return 1;
    }

    public function anyValid(int $num): bool
    {
        foreach ($this->constraints as $constraint) {
            if (($num >= $constraint[0] && $num <= $constraint[1]) || ($num >= $constraint[2] && $num <= $constraint[3])) {
                return true;
            }
        }
        return false;
    }

    public function columnValid(array $data): array
    {
        $validConstraints = [];
        foreach ($this->constraints as $key => $constraint) {
            foreach ($data as $num) {
                if (($num < $constraint[0] || $num > $constraint[1]) && ($num < $constraint[2] || $num > $constraint[3])) {
                    continue 2;
                }
            }
            $validConstraints[] = $key;
        }
        return $validConstraints;
    }

    public function run(): void
    {
        $this->inputLines([$this, 'parse']);
        $this->myTicket = array_shift($this->tickets);

        $invalid = 0;
        foreach ($this->tickets as $ticket) {
            foreach ($ticket as $value) {
                if (!$this->anyValid($value)) {
                    $invalid += $value;
                }
            }
        }

        printf("%d\n", $invalid);
    }

    public function runB(): void
    {
        $this->inputLines([$this, 'parse']);
        $this->myTicket = array_shift($this->tickets);

        foreach ($this->tickets as $ticket) {
            foreach ($ticket as $value) {
                if (!$this->anyValid($value)) {
                    continue 2;
                }
            }
            $this->validTickets[] = $ticket;
        }

        printf("Valid: %d\n", count($this->validTickets));

        $validConstaints = [];
        foreach (array_keys($this->validTickets[0]) as $key) {
            $validConstaints[$key] = $this->columnValid(array_column($this->validTickets, $key));
            printf("C %d valid: %s\n", $key, implode(', ', $validConstaints[$key]));
        }

        printf("%d\n", $this->myTicket[17] * $this->myTicket[8] * $this->myTicket[10] * $this->myTicket[11] * $this->myTicket[0] * $this->myTicket[19]);
    }
}