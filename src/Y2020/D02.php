<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D02 extends Day
{
    private function match(string $line): bool
    {
        [$range, $char, $pwd] = explode(' ', $line);
        [$min, $max] = explode('-', $range);
        $char = substr($char, 0, 1);

        $cnt = 0;
        foreach (str_split($pwd) as $c) {
            if ($c === $char) {
                $cnt++;
            }
        }

        return ($cnt >= $min && $cnt <= $max);
    }

    private function matchB(string $line): bool
    {
        [$range, $char, $pwd] = explode(' ', $line);
        [$min, $max] = explode('-', $range);
        $char = substr($char, 0, 1);

        return (($pwd[$min-1] === $char && $pwd[$max-1] !== $char) || ($pwd[$min-1] !== $char && $pwd[$max-1] === $char));
    }

    public function run(): void
    {
        $i = 0;
        foreach (explode("\n", file_get_contents($this->inputFileName())) as $line) {
            if ($this->match($line)) {
                $i++;
            }
        }

        printf("%d\n", $i);
    }

    public function runB(): void
    {
        $i = 0;
        foreach (explode("\n", file_get_contents($this->inputFileName())) as $line) {
            if ($this->matchB($line)) {
                $i++;
            }
        }

        printf("%d\n", $i);
    }
}