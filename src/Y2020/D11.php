<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D11 extends Day
{
    /** @var string[][]  */
    private array $s;

    private function count(int $x, int $y): int
    {
        $cnt = 0;
        for ($j = -1; $j <= 1; $j++) {
            if (!isset($this->s[$y + $j])) {
                continue;
            }
            for ($i = -1; $i <= 1; $i++) {
                if ($i === 0 && $j === 0) {
                    continue;
                }
                if (!isset($this->s[$y + $j][$x + $i])) {
                    continue;
                }
                if ($this->s[$y + $j][$x + $i] === '#') {
                    $cnt++;
                }
            }
        }
        return $cnt;
    }

    private function countB(int $x, int $y): int
    {
        $cnt = 0;
        for ($j = -1; $j <= 1; $j++) {
            if (!isset($this->s[$y + $j])) {
                continue;
            }
            for ($i = -1; $i <= 1; $i++) {
                if ($i === 0 && $j === 0) {
                    continue;
                }

                $hit = '.';
                $dist = 1;
                while ($hit === '.') {
                    $cy = $y + $dist * $j;
                    $cx = $x + $dist * $i;
                    if (!isset($this->s[$cy])) {
                        break;
                    }
                    if (!isset($this->s[$cy][$cx])) {
                        break;
                    }

                    $hit = $this->s[$cy][$cx];
                    $dist++;
                }
                if ($hit === '#') {
                    $cnt++;
                }
            }
        }
        return $cnt;
    }

    /**
     * @param string[][] $d
     */
    private function diff(array $d): bool
    {
        foreach ($d as $y => $line) {
            foreach ($line as $x => $char) {
                if ($this->s[$y][$x] !== $char) {
                    return false;
                }
            }
        }
        return true;
    }

    private function iterate(): array
    {
        $o = [];
        foreach ($this->s as $y => $line) {
            $l = [];
            foreach ($line as $x => $char) {
                if ($char === '.') {
                    $l[] = '.';
                    continue;
                }

                $cnt = $this->count($x, $y);
                if ($char === 'L' && $cnt === 0) {
                    $l[] = '#';
                } elseif ($char === '#' && $cnt >= 4) {
                    $l[] = 'L';
                } else {
                    $l[] = $char;
                }
            }
            $o[] = $l;
        }
        return $o;
    }

    private function iterateB(): array
    {
        $o = [];
        foreach ($this->s as $y => $line) {
            $l = [];
            foreach ($line as $x => $char) {
                if ($char === '.') {
                    $l[] = '.';
                    continue;
                }

                $cnt = $this->countB($x, $y);
                if ($char === 'L' && $cnt === 0) {
                    $l[] = '#';
                } elseif ($char === '#' && $cnt >= 5) {
                    $l[] = 'L';
                } else {
                    $l[] = $char;
                }
            }
            $o[] = $l;
        }
        return $o;
    }

    public function prn(): void
    {
        foreach ($this->s as $line) {
            foreach ($line as $char) {
                echo $char;
            }
            echo "\n";
        }
        echo "\n";
    }

    public function run(): void
    {
        $d = $this->inputLines('str_split');
        $i = 0;
        do {
            $this->s = $d;
            //$this->prn();
            $d = $this->iterate();
            $i++;
        } while (!$this->diff($d));

        printf("%d %d\n", $i, array_sum(array_map(function (array $line): int {
            return array_sum(array_map(function (string $char): int {
                return $char === '#' ? 1 : 0;
            }, $line));
        }, $this->s)));
    }

    public function runB(): void
    {
        $d = $this->inputLines('str_split');
        $i = 0;
        do {
            $this->s = $d;
            //$this->prn();
            $d = $this->iterateB();
            $i++;
        } while (!$this->diff($d));

        printf("%d %d\n", $i, array_sum(array_map(function (array $line): int {
            return array_sum(array_map(function (string $char): int {
                return $char === '#' ? 1 : 0;
            }, $line));
        }, $this->s)));
    }
}