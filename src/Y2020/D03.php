<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D03 extends Day
{
    /** @var string[] */
    private array $map;

    private int $xs;

    private int $ys;

    private function load(): void
    {
        $this->map = explode("\n", file_get_contents($this->inputFileName()));
        $this->xs = strlen($this->map[0]);
        $this->ys = count($this->map);
    }

    private function slopeCount(int $dx, int $dy): int
    {
        $x = 0; $y = 0;
        $cnt = 0;
        while ($y < $this->ys) {
            if ($this->map[$y][$x] === '#') {
                $cnt++;
            }

            $y += $dy;
            $x = ($x + $dx) % $this->xs;
        }

        return $cnt;
    }

    public function run(): void
    {
        $this->load();
        printf("%d\n", $this->slopeCount(3, 1));
    }

    public function runB(): void
    {
        $this->load();
        $a = $this->slopeCount(1, 1);
        $b = $this->slopeCount(3, 1);
        $c = $this->slopeCount(5, 1);
        $d = $this->slopeCount(7, 1);
        $e = $this->slopeCount(1, 2);
        printf("%d %d %d %d %d %d\n", $a, $b, $c, $d, $e, $a*$b*$c*$d*$e);
    }
}