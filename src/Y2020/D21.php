<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;

class D21 extends Day
{
    private array $allergens = [];
    private array $all = [];

    public function process(string $line): int
    {
        [$ingredientList, $allergenList] = explode(' (contains ', $line);
        $ingredients = explode(' ', $ingredientList);
        $allergens = explode(', ', substr($allergenList, 0, -1));

        foreach ($allergens as $allergen) {
            if (!isset($this->allergens[$allergen])) {
                $this->allergens[$allergen] = [];
            }

            $this->allergens[$allergen][] = $ingredients;
        }

        $this->all = array_merge($this->all, $ingredients);
        return 1;
    }

    public function run(): void
    {
        $this->inputLines([$this, 'process']);
/*
        $found = [];
        foreach ($this->allergens as $allergen => $ingredientLists) {
            $found[$allergen] = array_intersect(...$ingredientLists);
        }
        var_dump($found);
*/

        $map = [
            "shellfish" => "nqbnmzx",
            "soy" => "ntggc",
            "eggs" => "pgblcd",
            "sesame" => "dstct",
            "fish" => "xhkdc",
            "dairy" => "dhfng",
            "wheat" => "znrzgs",
            "peanuts" => "ghlzj",
        ];
        printf("%d\n", count(array_filter($this->all, function (string $i) use ($map): bool {
            return !in_array($i, $map);
        })));
    }

    public function runB(): void
    {
        $map = [
            "dairy" => "dhfng",
            "eggs" => "pgblcd",
            "fish" => "xhkdc",
            "peanuts" => "ghlzj",
            "sesame" => "dstct",
            "shellfish" => "nqbnmzx",
            "soy" => "ntggc",
            "wheat" => "znrzgs",
        ];
        printf("%s\n", implode(',', $map));
    }
}