<?php
declare(strict_types=1);

namespace App\Y2020\Model;

class Cup
{
    private int $id;

    public ?Cup $next;

    public function __construct(int $id)
    {
        $this->id = $id;
        $this->next = null;
    }

    public function getId(): int
    {
        return $this->id;
    }
}