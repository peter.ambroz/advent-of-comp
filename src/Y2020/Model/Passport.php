<?php
declare(strict_types=1);

namespace App\Y2020\Model;

class Passport
{
    public string $byr;
    public string $iyr;
    public string $eyr;
    public string $hgt;
    public string $hcl;
    public string $ecl;
    public string $pid;
    public ?string $cid;

    public function isValid(): bool
    {
        return isset($this->byr, $this->eyr, $this->iyr, $this->hgt, $this->hcl, $this->ecl, $this->pid);
    }

    public function attributesValid(): bool
    {
        if (!$this->isValid()) {
            return false;
        }

        if (strlen($this->byr) !== 4) {
            return false;
        }
        $byr = (int)$this->byr;
        if ($byr < 1920 || $byr > 2002) {
            return false;
        }

        if (strlen($this->iyr) !== 4) {
            return false;
        }
        $iyr = (int)$this->iyr;
        if ($iyr < 2010 || $iyr > 2020) {
            return false;
        }

        if (strlen($this->eyr) !== 4) {
            return false;
        }
        $eyr = (int)$this->eyr;
        if ($eyr < 2020 || $eyr > 2030) {
            return false;
        }

        $unit = substr($this->hgt, -2, 2);
        $hgt = (int)substr($this->hgt, 0, -2);
        if ($unit === 'cm') {
            if ($hgt < 150 || $hgt > 193) {
                return false;
            }
        } elseif ($unit == 'in') {
            if ($hgt < 59 || $hgt > 76) {
                return false;
            }
        } else {
            return false;
        }

        if (!preg_match('/^#[0-9a-f]{6}$/', $this->hcl)) {
            return false;
        }

        if (!in_array($this->ecl, ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'])) {
            return false;
        }

        if (!preg_match('/^[0-9]{9}$/', $this->pid)) {
            return false;
        }

        return true;
    }
}