<?php
declare(strict_types=1);

namespace App\Y2020\Model;

class Edge
{
    public int $id;

    /** @var Tile[]  */
    public array $tiles;

    public function __construct(int $id)
    {
        $this->id = $id;
        $this->tiles = [];
    }
}