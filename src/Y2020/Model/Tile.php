<?php
declare(strict_types=1);

namespace App\Y2020\Model;

class Tile
{
    public const E_UP = 0;
    public const E_RIGHT = 1;
    public const E_DOWN = 2;
    public const E_LEFT = 3;

    public int $id;

    /** @var string[][] */
    public array $image;

    /** @var int[] */
    public array $edges;

    /**
     * @param string[][] $image
     */
    public function __construct(int $id, array $image)
    {
        $this->id = $id;
        $this->edges = [0, 0, 0, 0];
        $this->image = $this->detectAndSetEdges($image);
    }

    public function getCanonicalEdges(): array
    {
        $ce = [];
        foreach ($this->edges as $eid => $e) {
            $altE = $this->flipEdge($e);
            $ce[$eid] = min($e, $altE);
        }

        return $ce;
    }

    public function getAllEdges(): array
    {
        $ce = [];
        foreach ($this->edges as $eid => $e) {
            $altE = $this->flipEdge($e);
            $ce[$eid] = $altE;
        }

        return array_merge($this->edges, $ce);
    }

    public function align(int $edgeId, int $edgeVal): void
    {
        for ($i = 0; $i < 4; $i++) {
            if ($this->edges[$edgeId] === $edgeVal) {
                return;
            }
            $this->rotate();
        }

        $this->flip();

        for ($i = 0; $i < 4; $i++) {
            if ($this->edges[$edgeId] === $edgeVal) {
                return;
            }
            $this->rotate();
        }

        throw new \Exception(sprintf('Align failed %d %d %d', $this->id, $edgeId, $edgeVal));
    }

    private function flip(): void
    {
        $n = [];

        foreach (array_keys($this->image[0]) as $x) {
            $n[] = array_column($this->image, $x);
        }

        $e = [
            self::E_UP => $this->edges[self::E_LEFT],
            self::E_RIGHT => $this->edges[self::E_DOWN],
            self::E_DOWN => $this->edges[self::E_RIGHT],
            self::E_LEFT => $this->edges[self::E_UP],
        ];

        $this->image = $n;
        $this->edges = $e;
    }

    private function rotate(): void
    {
        $n = [];

        foreach (array_keys($this->image[0]) as $x) {
            $n[] = array_reverse(array_column($this->image, $x));
        }

        $e = [
            self::E_UP => $this->flipEdge($this->edges[self::E_LEFT]),
            self::E_RIGHT => $this->edges[self::E_UP],
            self::E_DOWN => $this->flipEdge($this->edges[self::E_RIGHT]),
            self::E_LEFT => $this->edges[self::E_DOWN],
        ];

        $this->image = $n;
        $this->edges = $e;
    }

    private function flipEdge(int $edge): int
    {
        return bindec(strrev(sprintf('%010s', decbin($edge))));
    }

    /**
     * @param string[][] $image
     * @return string[][]
     */
    private function detectAndSetEdges(array $image): array
    {
        $imageContent = [];
        $e3 = [];
        $e1 = [];
        foreach ($image as $lno => $line) {
            if ($lno === 0) {
                $this->setEdge($line, self::E_UP);
            } elseif ($lno === 9) {
                $this->setEdge($line, self::E_DOWN);
            } else {
                $imageContent[] = array_slice($line, 1, 8);
            }

            $e1[] = $line[9];
            $e3[] = $line[0];
        }
        $this->setEdge($e1, self::E_RIGHT);
        $this->setEdge($e3, self::E_LEFT);

        return $imageContent;
    }

    private function setEdge(array $edge, int $edgeId): void
    {
        $this->edges[$edgeId] = bindec(str_replace(['.', '#'], ['0', '1'], implode('', $edge)));
    }
}