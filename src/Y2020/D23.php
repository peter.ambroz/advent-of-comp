<?php
declare(strict_types=1);

namespace App\Y2020;

use App\Day;
use App\Y2020\Model\Cup;

class D23 extends Day
{
    private ?Cup $cups = null;
    /** @var Cup[] */
    private array $index = [];

    /**
     * @param int[] $sequence
     */
    private function init(array $sequence, int $size): void
    {
        $this->index = array_fill(0, $size, null);
        $lastCup = null;
        foreach ($sequence as $item) {
            $cup = new Cup($item);
            $this->index[$item] = $cup;

            if ($this->cups === null) {
                $this->cups = $cup;
            } else {
                $lastCup->next = $cup;
            }

            $lastCup = $cup;
        }

        for ($i = count($sequence) + 1; $i <= $size; $i++) {
            $cup = new Cup($i);
            $this->index[$i] = $cup;
            $lastCup->next = $cup;
            $lastCup = $cup;
        }

        $lastCup->next = $this->cups;
    }

    private function stepL(Cup $start, int $size): void
    {
        $cv = $start->getId() - 1;
        if ($cv === 0) {
            $cv = $size;
        }

        $nv = $start->next;
        $nvEnd = $nv->next->next;
        $start->next = $nvEnd->next;

        while ($this->inNext3($nv, $cv)) {
            if (--$cv === 0) {
                $cv = $size;
            }
        }

        $start = $this->index[$cv];

        $nvEnd->next = $start->next;
        $start->next = $nv;
    }

    private function inNext3(Cup $head, int $val): bool
    {
        for ($i = 0; $i < 3; $i++) {
            if ($head->getId() === $val) {
                return true;
            }
            $head = $head->next;
        }

        return false;
    }

    public function run(): void
    {
        $this->init([9, 6, 3, 2, 7, 5, 4, 8, 1], 9);

        $current = $this->cups;
        for ($i = 0; $i < 100; $i++) {
            $this->stepL($current, 9);
            $current = $current->next;
        }

        $cup = $this->cups;
        do {
            echo $cup->getId();
            $cup = $cup->next;
        } while ($cup !== $this->cups);
        echo "\n";
    }

    public function runB(): void
    {
        $tmStart = hrtime(true);
        $this->init([9, 6, 3, 2, 7, 5, 4, 8, 1], 1000000);

        $current = $this->cups;
        for ($i = 0; $i < 10000000; $i++) {
            $this->stepL($current, 1000000);
            $current = $current->next;
        }

        $current = $this->index[1];
        $two = $current->next;
        $three = $two->next;

        printf("%d %d %d\n", $two->getId(), $three->getId(), $two->getId() * $three->getId());
        printf("Took %d\n", hrtime(true) - $tmStart);
    }
}