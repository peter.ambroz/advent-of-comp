<?php
declare(strict_types=1);

namespace App;

abstract class Day
{
    /** @var string[] */
    public array $_argv = [];

    public string $_suffix = '';

    abstract public function run(): void;

    protected function inputFileName(string $suffix = ''): string
    {
        [, $year, $className] = explode('\\', static::class);
        return sprintf('%s/../input/%s/%s%s.txt', __DIR__, $year, $className, $suffix);
    }

    protected function inputLines(callable $call = null, string $suffix = ''): array
    {
        if (!strlen($suffix)) {
            $suffix = $this->_suffix;
        }
        $lines = explode("\n", file_get_contents($this->inputFileName($suffix)));

        if ($call !== null) {
            return array_map($call, $lines);
        } else {
            return $lines;
        }
    }

    public function runB(): void
    {
        echo "Not implemented\n";
    }
}
