<?php

declare(strict_types=1);

namespace App\Y2022;

use App\Day;
use App\Y2022\Model\Inode;

class D07 extends Day
{
    private Inode $fs;
    private ?Inode $cd = null;

    public function run(): void
    {
        $this->fs = new Inode(null, '/', Inode::DIR);
        $this->inputLines($this->process(...));

        printf("%d\n", $this->findSmallDirs($this->fs, 100000));
    }

    public function runB(): void
    {
        $total = 70000000;
        $required = 30000000;

        $this->fs = new Inode(null, '/', Inode::DIR);
        $this->inputLines($this->process(...));

        $used = $this->fs->getSize();
        $free = $total - $used;

        $freeUp = $required - $free;

        printf("req: %d\nfreed: %d\n", $freeUp, $this->findSmallestLargeEnough($this->fs, $freeUp));
    }

    private function process(string $line): int
    {
        if (str_starts_with($line, '$ cd')) {
            [, , $dir] = explode(' ', $line);

            if ($dir === '/') {
                $this->cd = $this->fs;
            } elseif ($dir === '..') {
                $this->cd = $this->cd->getParent();
            } else {
                $this->cd = $this->cd->getOrCreateSubdir($dir);
            }

        } elseif ($line !== '$ ls') {
            [$data, $name] = explode(' ', $line);

            if ($data === 'dir') {
                $ino = new Inode($this->cd, $name, Inode::DIR);
            } else {
                $ino = new Inode($this->cd, $name, Inode::FILE, (int) $data);
            }

            $this->cd->addChild($ino);
        }

        return 0;
    }

    private function findSmallDirs(Inode $dir, int $maxSize): int
    {
        $sum = 0;

        foreach ($dir->getSubdirectories() as $child) {
            if ($child->getSize() <= $maxSize) {
                $sum += $child->getSize();
            }

            $sum += $this->findSmallDirs($child, $maxSize);
        }

        return $sum;
    }

    private function findSmallestLargeEnough(Inode $dir, int $minSize): int
    {
        $min = 9999999999;

        foreach ($dir->getSubdirectories() as $child) {
            if ($child->getSize() >= $minSize) {
                if ($child->getSize() < $min) {
                    $min = $child->getSize();
                }

                $rec = $this->findSmallestLargeEnough($child, $minSize);

                if ($rec < $min) {
                    $min = $rec;
                }
            }
        }

        return $min;
    }
}
