<?php

declare(strict_types=1);

namespace App\Y2022;

use App\Day;
use App\Y2022\Model\Forest;
use App\Y2022\Model\Tree;

class D08 extends Day
{
    public function run(): void
    {
        $data = new Forest($this->inputLines(str_split(...)));

        for ($i = 0; $i < 4; $i++) {
            $this->visible($data);
            $data->rotate90();
        }

        printf("%d\n", $data->visibleCount());
    }

    public function runB(): void
    {
        $data = new Forest($this->inputLines(str_split(...)));

        $max = 0;

        foreach ($data as $y => $row) {
            /** @var Tree $tree */
            foreach ($row as $x => $tree) {
                $score = $this->score($data, $tree->get(), $x, $y);
                if ($score > $max) {
                    $max = $score;
                }
            }
        }

        printf("%d\n", $max);
    }

    private function visible(Forest $data): void
    {
        foreach ($data as $y => $row) {
            $maxH = -1;

            /** @var Tree $tree */
            foreach ($row as $x => $tree) {
                if ($tree->get() > $maxH) {
                    $data->setVisible($x, $y);
                    $maxH = $tree->get();
                }
            }
        }
    }

    private function score(Forest $data, int $pivot, int $x, int $y): int
    {
        $score = [
            Forest::DIR_RIGHT => 0,
            Forest::DIR_UP => 0,
            Forest::DIR_LEFT => 0,
            Forest::DIR_DOWN => 0,
        ];

        foreach ($score as $dir => &$dirScore) {
            foreach ($data->getRay($x, $y, $dir) as $cur) {
                $dirScore++;

                if ($cur >= $pivot) {
                    break;
                }
            }
        }

        return array_product($score);
    }
}
