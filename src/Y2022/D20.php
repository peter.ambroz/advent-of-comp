<?php

declare(strict_types=1);

namespace App\Y2022;

use App\Day;
use App\Y2022\Model\ListItem;

class D20 extends Day
{
    public function run(): void
    {
        $this->doRun(1, 1);
    }

    public function runB(): void
    {
        $this->doRun(811589153, 10);
    }

    public function doRun(int $scale, int $repeat): void
    {
        /** @var ListItem[] $data */
        $data = $this->inputLines(fn ($x) => new ListItem(((int) $x) * $scale));
        $zero = $this->link($data);

        $mod = count($data) - 1;

        for ($i = 0; $i < $repeat; $i++) {
            foreach ($data as $item) {
                $diff = $item->value % $mod;
                if ($diff < 0) {
                    $diff += $mod;
                }

                $item->move($diff);
            }
        }

        $sum = 0;
        for ($j = 0; $j < 3; $j++) {
            for ($i = 0; $i < 1000; $i++) {
                $zero = $zero->next;
            }

            $sum += $zero->value;
            echo $zero->value . ' ';
        }
        echo "\n";

        printf("%d\n", $sum);
    }

    /**
     * Link into a doubly-linked list and return the item with 0 value
     *
     * @param ListItem[] $data
     */
    private function link(array $data): ListItem
    {
        $zero = null;

        foreach ($data as $i => $item) {
            if ($i > 0) {
                $ptr = $i - 1;
            } else {
                $ptr = count($data) - 1;
            }

            $data[$ptr]->next = $item;
            $item->prev = $data[$ptr];

            if ($item->value === 0) {
                $zero = $item;
            }
        }

        return $zero;
    }
}
