<?php

namespace App\Y2022;

use App\Day;

class D10 extends Day
{
    private int $rx = 1;

    private int $tick = 0;

    private int $result = 0;

    private bool $crt = false;

    public function run(): void
    {
        $this->inputLines($this->exec(...));

        printf("%d\n", $this->result);
    }

    public function runB(): void
    {
        $this->crt = true;
        $this->inputLines($this->exec(...));

        echo "\n";
    }

    private function exec(string $line): int
    {
        if ($line === 'noop') {
            $this->onTick();
            return 0;
        }

        [, $add] = explode(' ', $line);
        $add = (int) $add;

        $this->onTick();
        $this->onTick();
        $this->rx += $add;

        return 0;
    }

    private function onTick(): void
    {
        $this->tick++;

        if ($this->crt) {
            $t = ($this->tick - 1) % 40;
            if ($t === 0) {
                echo "\n";
            }

            if ($t >= ($this->rx - 1) && $t <= ($this->rx + 1)) {
                echo '#';
            } else {
                echo ' ';
            }
        } else {
            if ($this->tick < 221 && ($this->tick % 40) === 20) {
                $this->result += $this->rx * $this->tick;
            }
        }
    }
}
