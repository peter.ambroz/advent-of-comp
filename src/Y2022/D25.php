<?php

declare(strict_types=1);

namespace App\Y2022;

use App\Day;

class D25 extends Day
{
    // addition table
    private const MAP = [
        '0' => [
            '0' => ['0'],
            '1' => ['1'],
            '2' => ['2'],
            '-' => ['-'],
            '=' => ['='],
        ],
        '1' => [
            '0' => ['1'],
            '1' => ['2'],
            '2' => ['=', '1'],
            '-' => ['0'],
            '=' => ['-'],
        ],
        '2' => [
            '0' => ['2'],
            '1' => ['=', '1'],
            '2' => ['-', '1'],
            '-' => ['1'],
            '=' => ['0'],
        ],
        '-' => [
            '0' => ['-'],
            '1' => ['0'],
            '2' => ['1'],
            '-' => ['='],
            '=' => ['2', '-'],
        ],
        '=' => [
            '0' => ['='],
            '1' => ['-'],
            '2' => ['0'],
            '-' => ['2', '-'],
            '=' => ['1', '-'],
        ],
    ];

    public function run(): void
    {
        printf("%s\n",
            implode(
                '',
                array_reduce(
                    $this->inputLines(str_split(...)),
                    $this->add(...),
                    ['0']
                )
            ));
    }

    private function add(array $item1, array $item2): array
    {
        $result = [];
        $item1 = array_reverse($item1);
        $item2 = array_reverse($item2);
        $carry = '0';

        for ($i = 0; $i < max(count($item1), count($item2)); $i++) {
            $c1 = $item1[$i] ?? '0';
            $c2 = $item2[$i] ?? '0';

            $sum = self::MAP[$c1][$c2];
            $cSum = self::MAP[$sum[0]][$carry];
            $carry = self::MAP[$cSum[1] ?? '0'][$sum[1] ?? '0'][0];
            $result[] = $cSum[0];
        }

        $result[] = $carry;

        // remove redundant 0's
        while ($result[count($result) - 1] === '0' && count($result) > 1) {
            array_pop($result);
        }

        return array_reverse($result);
    }
}
