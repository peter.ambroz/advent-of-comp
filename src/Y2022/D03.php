<?php

namespace App\Y2022;

use App\Day;

class D03 extends Day
{
    public function run(): void
    {
        $sum = array_sum($this->inputLines($this->processLine(...)));
        printf("%d\n", $sum);
    }

    public function runB(): void
    {
        $buf = [];
        $cnt = 0;
        $badges = 0;

        foreach ($this->inputLines() as $line) {
            $buf[$cnt++] = $line;

            if ($cnt === 3) {
                $cnt = 0;
                $badges += $this->process3Lines($buf);
            }
        }

        printf("%d\n", $badges);
    }

    private function processLine(string $line): int
    {
        $both = array_intersect(...array_map(str_split(...), str_split($line, intdiv(strlen($line), 2))));
        return $this->prio(array_values($both)[0]);
    }

    private function process3Lines(array $lines): int
    {
        $all = array_intersect(...array_map(str_split(...), $lines));
        return $this->prio(array_values($all)[0]);
    }

    private function prio(string $char): int
    {
        $prio = ord($char);

        if ($prio < 91) {
            return $prio - 38;
        }

        return $prio - 96;
    }
}
