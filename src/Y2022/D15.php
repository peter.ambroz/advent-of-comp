<?php

declare(strict_types=1);

namespace App\Y2022;

use App\Day;
use App\Model\Point;
use App\Y2022\Model\Sensor;

class D15 extends Day
{
    /** @var Sensor[] */
    private array $sensors;

    public function run(): void
    {
        $this->sensors = $this->inputLines($this->process(...));
        $line = 2000000;
        $range = [];

        foreach ($this->sensors as $sensor) {
            $diff = abs($sensor->y - $line);

            if ($diff < $sensor->beaconDist) {
                $rest = $sensor->beaconDist - $diff;

                for ($i = $sensor->x - $rest; $i <= $sensor->x + $rest; $i++) {
                    if (!$sensor->beacon->equals(new Point($i, $line))) {
                        $range[$i] = true;
                    }
                }
            }
        }

        printf("%d\n", count($range));
    }

    public function runB(): void
    {
        $this->sensors = $this->inputLines($this->process(...));

        $pairs = [];

        foreach ($this->sensors as $i1 => $s1) {
            foreach ($this->sensors as $i2 => $s2) {
                if ($i1 >= $i2) {
                    continue;
                }

                $distance = $s1->distance($s2);
                $sizes = $s1->beaconDist + $s2->beaconDist;

                if ($sizes + 2 === $distance) {
                    $pairs[] = [$i1, $i2];
                }
            }
        }

        foreach ($pairs as $pair) {
            foreach ($pair as $p) {
                printf("%s\n", $this->sensors[$p]);
            }
        }

        // maths, it works, b*tches!
        $x = 2889465;
        $y = 3040754;
        printf("%d\n", $x * 4000000 + $y);
    }

    private function process(string $line): Sensor
    {
        [, , $sx, $sy, , , , , $bx, $by] = explode(' ', $line);
        $sx = (int) substr($sx, 2, -1);
        $sy = (int) substr($sy, 2, -1);
        $bx = (int) substr($bx, 2, -1);
        $by = (int) substr($by, 2);

        return new Sensor($sx, $sy, $bx, $by);
    }
}
