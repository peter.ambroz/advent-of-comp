<?php
/**
 * !!! Not nice, will be refactored. No-one should look at the code below
 */

namespace App\Y2022;

use App\Day;

class D09 extends Day
{
    private array $pos = [];

    private array $r;

    private int $size;

    public function run(): void
    {
        $this->pos[$this->key([0, 0])] = 1;
        $this->size = 2;
        $this->r = array_fill(0, 2, [0, 0]);
        $this->inputLines($this->move(...));

        printf("%d\n", count($this->pos));
    }

    public function runB(): void
    {
        $this->pos[$this->key([0, 0])] = 1;
        $this->size = 10;
        $this->r = array_fill(0, 10, [0, 0]);
        $this->inputLines($this->move(...));

        printf("%d\n", count($this->pos));
    }

    private function key(array $t): string
    {
        return sprintf("%d;%d", $t[0], $t[1]);
    }

    private function move(string $line): int
    {
        [$dir, $cnt] = explode(' ', $line);
        $cnt = (int) $cnt;

        for ($i = 0; $i < $cnt; $i++) {
            switch ($dir) {
                case 'U':
                    $this->r[0][1]--;
                    break;
                case 'D':
                    $this->r[0][1]++;
                    break;
                case 'L':
                    $this->r[0][0]--;
                    break;
                case 'R':
                    $this->r[0][0]++;
                    break;
            }

            for ($tid = 1; $tid < $this->size; $tid++) {
                $this->follow($tid);
            }

            $this->pos[$this->key($this->r[$this->size-1])] = 1;
        }

        return 0;
    }

    private function follow(int $tid): void
    {
        $ydiff = $this->r[$tid-1][1] - $this->r[$tid][1];
        $xdiff = $this->r[$tid-1][0] - $this->r[$tid][0];
        if ($ydiff === -2) {
            $this->r[$tid][1]--;
            if ($xdiff === -2) {
                $this->r[$tid][0]--;
            } elseif ($xdiff === 2) {
                $this->r[$tid][0]++;
            } else {
                $this->r[$tid][0] = $this->r[$tid - 1][0];
            }
        } elseif ($ydiff === 2) {
            $this->r[$tid][1]++;
            if ($xdiff === -2) {
                $this->r[$tid][0]--;
            } elseif ($xdiff === 2) {
                $this->r[$tid][0]++;
            } else {
                $this->r[$tid][0] = $this->r[$tid - 1][0];
            }
        }

        $ydiff = $this->r[$tid-1][1] - $this->r[$tid][1];
        $xdiff = $this->r[$tid-1][0] - $this->r[$tid][0];
        if ($xdiff === -2) {
            $this->r[$tid][0]--;
            if ($ydiff === -2) {
                $this->r[$tid][1]--;
            } elseif ($ydiff === 2) {
                $this->r[$tid][1]++;
            } else {
                $this->r[$tid][1] = $this->r[$tid - 1][1];
            }
        } elseif ($xdiff === 2) {
            $this->r[$tid][0]++;
            if ($ydiff === -2) {
                $this->r[$tid][1]--;
            } elseif ($ydiff === 2) {
                $this->r[$tid][1]++;
            } else {
                $this->r[$tid][1] = $this->r[$tid - 1][1];
            }
        }
    }
}
