<?php

namespace App\Y2022;

use App\Day;

class D04 extends Day
{
    public function run(): void
    {
        $sum = array_sum($this->inputLines($this->processLine(...)));

        printf("%d\n", $sum);
    }

    public function runB(): void
    {
        $sum = array_sum($this->inputLines($this->processLine2(...)));

        printf("%d\n", $sum);
    }

    private function processLine(string $line): int // as bool, but sums better
    {
        [$l, $r] = array_map(fn ($x) => explode('-', $x), explode(',', $line));
        if (($l[0] <= $r[0] && $l[1] >= $r[1])
            || ($l[0] >= $r[0] && $l[1] <= $r[1])) {

            return 1;
        }

        return 0;
    }

    private function processLine2(string $line): int // as bool, but sums better
    {
        [$l, $r] = array_map(fn ($x) => explode('-', $x), explode(',', $line));

        if (($l[0] < $r[0] && $l[1] < $r[0])
            || ($l[0] > $r[1] && $l[1] > $r[1])) {

            return 0;
        }

        return 1;
    }
}
