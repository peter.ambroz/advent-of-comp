<?php

namespace App\Y2022;

use App\Day;
use App\Model\Point;

class D14 extends Day
{
    private const XMIN = 330;
    private const XMAX = 680;
    private const YMAX = 166;

    private const SPACE = 0;
    private const ROCK = 1;
    private const SAND = 2;

    /** @var int[][] */
    private array $data;

    /** @var Point[] */
    private array $dirs;

    private Point $start;

    public function __construct()
    {
        $this->dirs = [
            new Point(0, 1),
            new Point(-1, 1),
            new Point(1, 1),
        ];

        $this->data = array_fill(0, self::YMAX + 3, array_fill(0, self::XMAX - self::XMIN + 3, self::SPACE));
        $this->start = new Point(501 - self::XMIN, 0);
    }

    public function run(): void
    {
        $this->doRun();
    }

    public function runB(): void
    {
        $this->process(sprintf('%d,%d -> %d,%d', self::XMIN - 1, self::YMAX + 2, self::XMAX + 1, self::YMAX + 2));
        $this->doRun();
    }

    private function doRun(): void
    {
        $this->inputLines($this->process(...));

        $sand = 0;

        while ($this->traceSand($this->start)) {
            $sand++;
        }

        printf("%d\n", $sand);
    }

    private function process(string $line): void
    {
        $path = array_map(function ($point) {
            [$x, $y] = explode(',', $point);
            return new Point(((int) $x) - self::XMIN + 1, (int) $y);
        }, explode(' -> ', $line));

        $ppt = $path[0];

        foreach ($path as $i => $pt) {
            if ($i === 0) {
                continue;
            }

            $diff = $pt->sub($ppt)->simplify();
            $k = $ppt;

            do {
                $this->data[$k->y][$k->x] = self::ROCK;
                $k = $k->add($diff);
            } while (!$k->equals($pt));

            $this->data[$k->y][$k->x] = self::ROCK;
            $ppt = $pt;
        }
    }

    private function traceSand(Point $sand): bool
    {
        if ($this->data[$sand->y][$sand->x] !== self::SPACE) {
            return false;
        }

        $newSand = new Point(0, 0);

        while (true) {
            $settled = true;

            foreach ($this->dirs as $tryDir) {
                $newSand = $sand->add($tryDir);

                if ($this->data[$newSand->y][$newSand->x] === self::SPACE) {
                    $settled = false;
                    break;
                }
            }

            if ($settled) {
                $this->data[$sand->y][$sand->x] = self::SAND;
                return true;
            }

            if ($newSand->y >= self::YMAX + 2) {
                return false;
            }

            $sand = $newSand;
        }
    }
}
