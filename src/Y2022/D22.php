<?php

namespace App\Y2022;

use App\Day;

/**
 * Doesn't work for first star anymore as I rewrote the code to work for the second star. Whoopsie!
 */
class D22 extends Day
{
    private const RIGHT = 0;
    private const DOWN = 1;
    private const LEFT = 2;
    private const UP = 3;
    private const ROT = 10;

    private array $data;

    public function run(): void
    {
        $data = $this->inputLines(str_split(...));
        $instr = $this->tokenize(array_pop($data));
        array_pop($data);
        $this->data = $data;

        [$vBounds, $hBounds] = $this->findBounds();

        $rv = $this->follow($instr, $vBounds, $hBounds);
        printf("%d\n", ($rv[1] + 1) * 1000 + ($rv[0] + 1) * 4 + $rv[2]);
    }

    public function runB(): void
    {
        $data = $this->inputLines(str_split(...));
        $instr = $this->tokenize(array_pop($data));
        array_pop($data);
        $this->data = $data;

        [$vBounds, $hBounds] = $this->findBBounds();

        $rv = $this->follow($instr, $vBounds, $hBounds);
        printf("%d\n", ($rv[1] + 1) * 1000 + ($rv[0] + 1) * 4 + $rv[2]);
    }

    private function follow(array $instr, array $vBounds, array $hBounds): array
    {
        $rot = [
            self::RIGHT => [1, 0],
            self::DOWN => [0, 1],
            self::LEFT => [-1, 0],
            self::UP => [0, -1],
        ];

        // find start
        //$x = $hBounds[0][self::RIGHT];
        //$y = $vBounds[$x][self::DOWN];
        $x = 50;
        $y = 0;
        $r = 0;
        printf("START: %d, %d\n", $x, $y);

        foreach ($instr as $do) {
            if ($do === 'L') {
                $r = ($r + 3) % 4;
            } elseif ($do === 'R') {
                $r = ($r + 1) % 4;
            } else {
                printf("Walk %d dir %d: ", $do, $r);
                for ($i = 0; $i < $do; $i++) {
                    $nx = $x + $rot[$r][0];
                    $ny = $y + $rot[$r][1];
                    $nrr = 0;
                    $ch = $this->data[$ny][$nx] ?? ' ';

                    if ($ch === ' ') {
                        if ($r % 2) {
                            $nx = $vBounds[$x][$r][0];
                            $ny = $vBounds[$x][$r][1];
                            $nrr = $vBounds[$x][$r][2];
                        } else {
                            $nx = $hBounds[$y][$r][0];
                            $ny = $hBounds[$y][$r][1];
                            $nrr = $hBounds[$y][$r][2];
                        }

                        if (!isset($this->data[$ny][$nx])) {
                            throw new \Exception('wrap failed!');
                        }
                        $ch = $this->data[$ny][$nx];
                    }

                    if ($ch === '#') {
                        printf("Hit wall!");
                        break;
                    }

                    $x = $nx;
                    $y = $ny;
                    $r = ($r + $nrr) % 4;
                }
                printf(" Now at %d, %d\n", $x, $y);
            }
        }

        return [$x, $y, $r];
    }

    private function tokenize(array $instr): array
    {
        $n = 0;
        $rv = [];

        for ($i = 0; $i < count($instr); $i++) {
            $ch = $instr[$i];
            if ($ch === 'L' || $ch === 'R') {
                $rv[] = $n;
                $rv[] = $ch;
                $n = 0;
            } else {
                $n = $n * 10 + (int) $ch;
            }
        }

        $rv[] = $n;

        return $rv;
    }

    private function findBounds(): array
    {
        $hBounds = array_merge(
            array_fill(0, 50, [self::RIGHT => 50, self::LEFT => 149]),
            array_fill(0, 50, [self::RIGHT => 50, self::LEFT => 99]),
            array_fill(0, 50, [self::RIGHT => 0, self::LEFT => 99]),
            array_fill(0, 50, [self::RIGHT => 0, self::LEFT => 49]),
        );
        $vBounds = array_merge(
            array_fill(0, 50, [self::DOWN => 100, self::UP => 199]),
            array_fill(0, 50, [self::DOWN => 0, self::UP => 149]),
            array_fill(0, 50, [self::DOWN => 0, self::UP => 49]),
        );

        return [$vBounds, $hBounds];
    }

    private function findBBounds(): array
    {
        $hBounds = [];
        $vBounds = [];

        for ($i = 0; $i < 50; $i++) {
            $hBounds[$i] = [
                self::RIGHT => [99, 149 - $i, 2], // C
                self::LEFT => [0, 149 - $i, 2], // G
            ];
        }
        for ($i = 0; $i < 50; $i++) {
            $hBounds[$i + 50] = [
                self::RIGHT => [100 + $i, 49, 3], // D
                self::LEFT => [$i, 100, 3], // F
            ];
        }
        for ($i = 0; $i < 50; $i++) {
            $hBounds[$i + 100] = [
                self::RIGHT => [149, 49 - $i, 2], // C
                self::LEFT => [50, 49 - $i, 2], // G
            ];
        }
        for ($i = 0; $i < 50; $i++) {
            $hBounds[$i + 150] = [
                self::RIGHT => [50 + $i, 149, 3], // E
                self::LEFT => [50 + $i, 0, 3], // A
            ];
        }
// ----------------
        for ($i = 0; $i < 50; $i++) {
            $vBounds[$i] = [
                self::DOWN => [100 + $i, 0, 0], // B
                self::UP => [50, 50 + $i, 1], // F
            ];
        }
        for ($i = 0; $i < 50; $i++) {
            $vBounds[$i + 50] = [
                self::DOWN => [49, 150 + $i, 1], // E
                self::UP => [0, 150 + $i, 1], // A
            ];
        }
        for ($i = 0; $i < 50; $i++) {
            $vBounds[$i + 100] = [
                self::DOWN => [99, 50 + $i, 1], // D
                self::UP => [$i, 199, 0], // B
            ];
        }

        return [$vBounds, $hBounds];
    }
}
