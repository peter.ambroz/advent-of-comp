<?php

namespace App\Y2022;

use App\Day;

class D06 extends Day
{
    public function run(): void
    {
        printf("%d\n", $this->find(4));
    }

    public function runB(): void
    {
        printf("%d\n", $this->find(14));
    }

    private function find(int $size): int
    {
        $data = str_split($this->inputLines()[0]);
        $len = count($data);

        for ($i = 0; $i < $len - ($size - 1); $i++) {
            if (count(array_unique(array_slice($data, $i, $size))) === $size) {
                return $i+$size;
            }
        }

        return 0;
    }
}
