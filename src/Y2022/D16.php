<?php

declare(strict_types=1);

namespace App\Y2022;

use App\Day;

class D16 extends Day
{
    /** @var array<string, int> */
    private array $flows = [];

    /** @var array<string, int> */
    private array $bits = [];

    /** @var array<string, array<string, int>> */
    private array $dist;

    private array $result;

    public function run(): void
    {
        $this->runB();
    }

    public function runB(): void
    {
        $lines = $this->inputLines($this->process(...));

        $dist = [];
        foreach ($lines as $lineX) {
            $xid = $lineX['id'];
            $distLine = [];

            foreach ($lines as $lineY) {
                $yid = $lineY['id'];
                $distLine[$yid] = in_array($yid, $lineX['next']) ? 1 : PHP_INT_MAX;
            }
            $dist[$xid] = $distLine;
        }
        // floyd-warshall closure on distances
        foreach (array_keys($dist) as $k) {
            foreach (array_keys($dist) as $i) {
                foreach (array_keys($dist) as $j) {
                    $dist[$i][$j] = min($dist[$i][$j], $dist[$i][$k] + $dist[$k][$j]);
                }
            }
        }
        $this->dist = $dist;

        $this->result = [];
        $this->search('AA', 30, 0, 0);
        $partA = max($this->result);

        $this->result = [];
        $this->search('AA', 26, 0, 0);
        $partB = -1;

        foreach ($this->result as $s1 => $v1) {
            foreach ($this->result as $s2 => $v2) {
                if ($s1 & $s2) {
                    continue;
                }

                if ($v1 + $v2 > $partB) {
                    $partB = $v1 + $v2;
                }
            }
        }

        printf("A: %d\nB: %d\n", $partA, $partB);
    }

    private function process(string $line): array
    {
        [, $id, , , $rate, , , , , $valves] = explode(' ', $line, 10);

        $rate = (int) substr($rate, 5, -1);

        if ($rate > 0) {
            $exp = count($this->flows);
            $this->flows[$id] = $rate;
            $this->bits[$id] = 1 << $exp;
        }

        return [
            'id' => $id,
            'next' => explode(', ', $valves),
        ];
    }

    private function search($node, $ttl, $valveState, $score): void
    {
        $this->result[$valveState] = max($this->result[$valveState] ?? 0, $score);

        foreach ($this->flows as $n => $rate) {
            $remainingTtl = $ttl - $this->dist[$node][$n] - 1;

            if (($this->bits[$n] & $valveState) || ($remainingTtl <= 0)) {
                continue;
            }
            $this->search($n, $remainingTtl, $valveState | $this->bits[$n], $score + $remainingTtl * $rate);
        }
    }
}
