<?php

declare(strict_types=1);

namespace App\Y2022\Model;

use Closure;

class Monkey
{
    private int $cnt = 0;

    /**
     * @param int[] $items
     * @param int[] $next
     */
    public function __construct(
        private array $items,
        private readonly Closure $op,
        private readonly int $divisor,
        private readonly array $next,
    ) {
    }

    public function process(bool $div3): array
    {
        $out = [];

        foreach ($this->items as $item) {
            $this->cnt++;
            $item = ($this->op)($item);
            if ($div3) {
                $item = intdiv($item, 3);
            }

            if ($item % $this->divisor) {
                $next = $this->next[0];
            } else {
                $next = $this->next[1];
            }

            if (!isset($out[$next])) {
                $out[$next] = [];
            }

            $out[$next][] = $item;
        }

        $this->items = [];

        return $out;
    }

    /**
     * @param int[] $items
     */
    public function enqueue(array $items): void
    {
        foreach ($items as $item) {
            $this->items[] = $item % 9699690;
        }
    }

    public function getCount(): int
    {
        return $this->cnt;
    }
}
