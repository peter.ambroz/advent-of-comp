<?php

declare(strict_types=1);

namespace App\Y2022\Model;

class Tree
{
    private bool $visible = false;

    public function __construct(private readonly int $height)
    {
    }

    public function get(): int
    {
        return $this->height;
    }

    public function setVisible(): void
    {
        $this->visible = true;
    }

    public function isVisible(): bool
    {
        return $this->visible;
    }
}
