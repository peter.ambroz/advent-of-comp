<?php

declare(strict_types=1);

namespace App\Y2022\Model;

use App\Model\Point;

class TimePoint extends Point
{
    public function __construct(int $x, int $y, public readonly int $time)
    {
        parent::__construct($x, $y);
    }

    public function isPoint(Point $point): bool
    {
        return $this->equals($point);
    }

    public function __toString(): string
    {
        return sprintf('%s;%d', parent::__toString(), $this->time);
    }
}
