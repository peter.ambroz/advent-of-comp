<?php

declare(strict_types=1);

namespace App\Y2022\Model;

class Inode
{
    public const DIR = 'd';
    public const FILE = 'f';

    /** @var self[] */
    private array $children = [];

    public function __construct(
        private readonly ?self $parent,
        private readonly string $name,
        private readonly string $type,
        private ?int $size = null
    ) {
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function getOrCreateSubdir(string $name): ?self
    {
        foreach ($this->getSubdirectories() as $child) {
            if ($child->name === $name) {
                return $child;
            }
        }

        return $this->addChild(new self($this, $name, self::DIR));
    }

    /**
     * @return self[]
     */
    public function getSubdirectories(): array
    {
        return array_filter($this->children, fn ($x) => $x->isDir());
    }

    public function addChild(self $child): self
    {
        $this->children[] = $child;

        return $this;
    }

    public function getSize(): int
    {
        if ($this->size === null) {
            $size = 0;

            foreach ($this->children as $child) {
                 $size += $child->getSize();
            }

            $this->size = $size;
        }

        return $this->size;
    }

    public function isDir(): bool
    {
        return $this->type === self::DIR;
    }
}
