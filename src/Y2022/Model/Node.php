<?php

declare(strict_types=1);

namespace App\Y2022\Model;

class Node
{
    /** @var self[] */
    private array $next = [];

    public function __construct(
        public readonly string $id,
        public readonly int $value,
    ) {
    }

    public function addNext(self $node): self
    {
        $this->next[] = $node;

        return $this;
    }

    public function getNext(): iterable
    {
        return $this->next;
    }

    public function __toString(): string
    {
        return sprintf(
            'Id: %s, value: %d, next: %s',
            $this->id,
            $this->value,
            implode(',', array_map(fn ($x) => $x->id, $this->next))
        );
    }
}
