<?php

declare(strict_types=1);

namespace App\Y2022\Model;

use App\Model\Point;

class Blizzard
{
    private const UP = '^';
    private const DOWN = 'v';
    private const LEFT = '<';
    private const RIGHT = '>';

    private readonly Point $position;
    private readonly Point $direction;

    public function __construct(
        int $x,
        int $y,
        string $dir,
        private readonly int $sizeX,
        private readonly int $sizeY,
    ) {
        $this->position = new Point($x, $y);

        $this->direction = match ($dir) {
            self::UP => new Point(0, -1),
            self::DOWN => new Point(0, 1),
            self::LEFT => new Point(-1, 0),
            self::RIGHT => new Point(1, 0),
        };
    }

    public function getPosition(int $time): TimePoint
    {
        $newX = ($this->position->x + $this->direction->x * $time) % $this->sizeX;
        if ($newX < 0) {
            $newX += $this->sizeX;
        }

        $newY = ($this->position->y + $this->direction->y * $time) % $this->sizeY;
        if ($newY < 0) {
            $newY += $this->sizeY;
        }

        return new TimePoint($newX, $newY, $time);
    }
}
