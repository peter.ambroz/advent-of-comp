<?php

declare(strict_types=1);

namespace App\Y2022\Model;

class ListItem
{
    public self $prev;
    public self $next;

    public function __construct(public readonly int $value)
    {
    }

    public function move(int $diff): void
    {
        if ($diff <= 0) {
            return;
        }

        $this->prev->next = $this->next;
        $this->next->prev = $this->prev;

        $newPrev = $this->prev;
        for ($i = 0; $i < $diff; $i++) {
            $newPrev = $newPrev->next;
        }

        $newNext = $newPrev->next;

        $newPrev->next = $this;
        $newNext->prev = $this;
        $this->next = $newNext;
        $this->prev = $newPrev;
    }
}
