<?php

declare(strict_types=1);

namespace App\Y2022\Model;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use Traversable;

class Forest implements IteratorAggregate, Countable
{
    public const DIR_RIGHT = 0;
    public const DIR_UP = 1;
    public const DIR_LEFT = 2;
    public const DIR_DOWN = 3;

    private const DIR = [
        self::DIR_RIGHT => [1, 0],
        self::DIR_UP => [0, -1],
        self::DIR_LEFT => [-1, 0],
        self::DIR_DOWN => [0, 1],
    ];

    /** @var Tree[][] */
    private array $trees;

    /** @var $heights string[][] */
    public function __construct(array $heights)
    {
        foreach ($heights as $row) {
            $treeRow = [];

            foreach ($row as $height) {
                $treeRow[] = new Tree((int) $height);
            }

            $this->trees[] = $treeRow;
        }
    }

    public function rotate90(): void
    {
        $tmp = array_fill(0, count($this->trees[0]), []);

        foreach ($this->trees as $y => $row) {
            $rsize = count($row);

            foreach ($row as $x => $tree) {
                $tmp[$x][$y] = $row[$rsize - $x - 1];
            }
        }

        $this->trees = $tmp;
    }

    public function setVisible(int $x, int $y): void
    {
        $this->trees[$y][$x]->setVisible();
    }

    public function visibleCount(): int
    {
        return array_sum(
            array_map(
                fn ($row) => array_sum(
                    array_map(fn (Tree $x) => $x->isVisible() ? 1 : 0, $row)
                ),
                $this->trees
            )
        );
    }

    public function getRay(int $x, int $y, int $dir): iterable
    {
        $a = $x + self::DIR[$dir][0];
        $b = $y + self::DIR[$dir][1];

        while (isset($this->trees[$b][$a])) {
            yield $this->trees[$b][$a]->get();
            $a += self::DIR[$dir][0];
            $b += self::DIR[$dir][1];
        }
    }

    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->trees);
    }

    public function count(): int
    {
        return count($this->trees);
    }
}
