<?php

declare(strict_types=1);

namespace App\Y2022\Model;

use App\Model\Point;

class Sensor extends Point
{
    public readonly int $beaconDist;

    public readonly Point $beacon;

    public function __construct(
        int $x,
        int $y,
        int $beaconX,
        int $beaconY,
    ) {
        parent::__construct($x, $y);

        $this->beacon = new Point($beaconX, $beaconY);
        $this->beaconDist = $this->distance($this->beacon);
    }

    public function __toString(): string
    {
        return sprintf("X=%d, Y=%d, BX=%d, BY=%d, DIST=%d",
            $this->x,
            $this->y,
            $this->beacon->x,
            $this->beacon->y,
            $this->beaconDist
        );
    }
}
