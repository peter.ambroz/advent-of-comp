<?php

declare(strict_types=1);

namespace App\Y2022;

use App\Day;
use App\Model\Point;
use App\Y2022\Model\Blizzard;
use App\Y2022\Model\TimePoint;
use SplQueue;

class D24 extends Day
{
    private const TIME_MAX = 600;

    private int $sizeX;
    private int $sizeY;

    /** @var array<string, bool> */
    private array $data;

    public function run(): void
    {
        $this->precalculate($this->parse());
        $time = $this->bfs(
            new TimePoint(0, -1, 0),
            new Point($this->sizeX - 1, $this->sizeY)
        );

        printf("%d\n", $time);
    }

    public function runB(): void
    {
        $this->precalculate($this->parse());
        $time = $this->bfs(
            new TimePoint(0, -1, 0),
            new Point($this->sizeX - 1, $this->sizeY)
        );

        $time = $this->bfs(
            new TimePoint($this->sizeX - 1, $this->sizeY, $time),
            new Point(0, -1),
        );

        $time = $this->bfs(
            new TimePoint(0, -1, $time),
            new Point($this->sizeX - 1, $this->sizeY)
        );

        printf("%d\n", $time);
    }

    /**
     * @return Blizzard[]
     */
    private function parse(): array
    {
        $data = $this->inputLines(str_split(...));
        $this->sizeY = count($data) - 2;
        $this->sizeX = count($data[0]) - 2;
        $blizzards = [];

        foreach ($data as $y => $line) {
            if ($line[2] === '#') {
                continue;
            }

            foreach ($line as $x => $char) {
                if ($char === '#' || $char === '.') {
                    continue;
                }

                $blizzards[] = new Blizzard($x - 1, $y - 1, $char, $this->sizeX, $this->sizeY);
            }
        }

        return $blizzards;
    }

    /**
     * @param Blizzard[] $blizzards
     */
    private function precalculate(array $blizzards): void
    {
        foreach ($blizzards as $blizzard) {
            for ($time = 0; $time < self::TIME_MAX; $time++) {
                $pos = $blizzard->getPosition($time);
                $this->data[(string) $pos] = true;
            }
        }
    }

    private function bfs(TimePoint $root, Point $goal): ?int
    {
        $explored = [
            (string) $root => true
        ];
        $q = new SplQueue();
        $q->enqueue($root);

        while (!$q->isEmpty()) {
            /** @var TimePoint $v */
            $v = $q->dequeue();
            if ($v->isPoint($goal)) {
                return $v->time;
            }

            foreach ($this->adjacent($v) as $w) {
                if (!isset($explored[(string) $w])) {
                    $explored[(string) $w] = true;
                    $q->enqueue($w);
                }
            }
        }

        return null;
    }

    /**
     * @return TimePoint[]
     */
    private function adjacent(TimePoint $v): array
    {
        $ret = [];

        foreach ([[1, 0],[0, 1],[-1, 0],[0, -1],[0, 0]] as $vec) {
            $x = $v->x + $vec[0];
            $y = $v->y + $vec[1];
            $t = $v->time + 1;

            if ($x < 0 || $y < 0 || $x >= $this->sizeX || $y >= $this->sizeY) {
                continue;
            }

            $tp1 = new TimePoint($x, $y, $t);
            $tp2 = new TimePoint($x, $y, $t % self::TIME_MAX);
            if (!isset($this->data[(string) $tp2])) {
                $ret[] = $tp1;
            }
        }

        // last step to start
        if ($v->x === 0 && $v->y === 0) {
            $ret[] = new TimePoint(0, -1, $v->time + 1);
        }

        // last step to goal
        if ($v->x === $this->sizeX - 1 && $v->y === $this->sizeY - 1) {
            $ret[] = new TimePoint($v->x, $v->y + 1, $v->time + 1);
        }

        // keep waiting at the goal
        if ($v->x === $this->sizeX - 1 && $v->y === $this->sizeY) {
            $ret[] = new TimePoint($v->x, $v->y, $v->time + 1);
        }

        // keep waiting at the start
        if ($v->x === 0 && $v->y === -1) {
            $ret[] = new TimePoint(0, -1, $v->time + 1);
        }

        return $ret;
    }
}
