<?php

namespace App\Y2022;

use App\Day;

/**
 * Meh solution :-/ .. Runs in ~10 minutes. I couldn't care less.
 */
class D19 extends Day
{
    private const ORE = 'ore';
    private const CLAY = 'clay';
    private const OBSIDIAN = 'obsidian';
    private const GEODE = 'geode';

    public function run(): void
    {
        $sum = 0;

        foreach ($this->inputLines($this->process(...)) as $blueprint) {
            $geodes = $this->bestScoreForBlueprint($blueprint);
            $sum += $geodes * $blueprint['id'];
            printf("%d: %d\n", $blueprint['id'], $geodes);
        }

        printf("%d\n", $sum);
    }

    public function runB(): void
    {
        $mul = 1;

        foreach ($this->inputLines($this->process(...)) as $blueprint) {
            if ($blueprint['id'] === 4) {
                break;
            }
            $geodes = $this->bestScoreForBlueprint($blueprint, 30);
            $mul *= $geodes;
            printf("%d: %d\n", $blueprint['id'], $geodes);
        }

        printf("%d\n", $mul);
    }

    private function process(string $line): array
    {
        [, $id, , , , , $oreOre, , , , , , $clayOre, , , , , , $obsidianOre , , , $obsidianClay, , , , , , $geodeOre, , , $geodeObsidian, ] = explode(' ', $line);

        return [
            'id' => (int) substr($id, 0, -1),
            self::ORE => [
                self::ORE => (int) $oreOre
            ],
            self::CLAY => [
                self::ORE => (int) $clayOre
            ],
            self::OBSIDIAN => [
                self::ORE => (int) $obsidianOre,
                self::CLAY => (int) $obsidianClay,
            ],
            self::GEODE => [
                self::ORE => (int) $geodeOre,
                self::OBSIDIAN => (int) $geodeObsidian,
            ],
        ];
    }

    private function bestScoreForBlueprint(
        array $blueprint,
        int $ttl = 22,
        array $robots = [self::GEODE => 0, self::OBSIDIAN => 0, self::CLAY => 0, self::ORE => 1],
        array $stash = [self::ORE => 2, self::CLAY => 0, self::OBSIDIAN => 0, self::GEODE => 0],
    ): int
    {
        if ($ttl === 1) {
            return $stash[self::GEODE] + $robots[self::GEODE];
        }

        $score = -1;

        foreach ($robots as $robotId => $robotCount) {
            // don't buy more ore when noone needs that much
            if ($robotId === self::ORE) {
                if ($robotCount >= $blueprint[self::CLAY][self::ORE]
                    && $robotCount >= $blueprint[self::OBSIDIAN][self::ORE]
                    && $robotCount >= $blueprint[self::GEODE][self::ORE]
                ) {
                    continue;
                }
            }

            // don't buy more clay
            if ($robotId === self::CLAY) {
                if ($robotCount >= $blueprint[self::OBSIDIAN][self::CLAY]) {
                    continue;
                }
            }

            $newStash = $stash;
            $enoughForBuild = true;

            foreach ($blueprint[$robotId] as $material => $requirement) {
                if ($newStash[$material] >= $requirement) {
                    $newStash[$material] -= $requirement;
                } else {
                    $enoughForBuild = false;
                    break;
                }
            }

            if ($enoughForBuild) {
                foreach ($robots as $robotId2 => $robotCount2) {
                    $newStash[$robotId2] += $robotCount2;
                }

                $newRobots = $robots;
                $newRobots[$robotId]++;
                $found = $this->bestScoreForBlueprint($blueprint, $ttl - 1, $newRobots, $newStash);

                // bought a geode / obsidian robot? look no further!
                if ($robotId === self::GEODE || $robotId === self::OBSIDIAN) {
                    return $found;
                }

                if ($found > $score) {
                    $score = $found;
                }
            }
        }

        foreach ($robots as $robotId => $robotCount) {
            $stash[$robotId] += $robotCount;
        }

        $found = $this->bestScoreForBlueprint($blueprint, $ttl - 1, $robots, $stash);

        if ($found > $score) {
            $score = $found;
        }

        return $score;
    }
}
