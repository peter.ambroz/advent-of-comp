<?php

namespace App\Y2022;

use App\Day;

class D17 extends Day
{
    private const PIECES = 5;
    private const SHAFT = 7;

    private const LEFT = '<';
    private const RIGHT = '>';

    /** @var int[][][]  */
    private array $pieces = [
        [
            [1,1,1,1],
        ],
        [
            [0,1,0],
            [1,1,1],
            [0,1,0],
        ],
        [
            [1,1,1],
            [0,0,1],
            [0,0,1],
        ],
        [
            [1],
            [1],
            [1],
            [1],
        ],
        [
            [1,1],
            [1,1],
        ],
    ];

    /** @var string[] */
    private readonly array $moves;

    private readonly int $moveCount;

    private int $nextPiece;

    private int $nextMove;

    /** @var int[][]  */
    private array $well;

    public function __construct()
    {
        $this->moves = $this->inputLines(str_split(...))[0];
        $this->moveCount = count($this->moves);
        $this->reset();
    }

    public function run(): void
    {
        printf("%d\n", $this->findHeight(2022));
    }

    public function runB(): void
    {
        $repetition = $this->findRepetition();

        $this->reset();
        $pattern = $this->findPattern($repetition);

        $this->reset();
        $baseRounds = 1000000000000 - $pattern['round_remainder'];
        $restRounds = $baseRounds % $pattern['round_modulo'] + $pattern['round_remainder'];
        $restHeight = $this->findHeight($restRounds);

        $height = intdiv($baseRounds, $pattern['round_modulo']) * $pattern['height_modulo'] + $restHeight;

        printf("%d\n", $height);
    }

    private function reset(): void
    {
        $this->nextMove = 0;
        $this->nextPiece = 0;
        $this->well = [];
    }

    private function getMove(): string
    {
        $move = $this->moves[$this->nextMove];
        $this->nextMove = ($this->nextMove + 1) % $this->moveCount;

        return $move;
    }

    /**
     * @return int[][]
     */
    private function getPiece(): array
    {
        $piece = $this->pieces[$this->nextPiece];
        $this->nextPiece = ($this->nextPiece + 1) % self::PIECES;

        return $piece;
    }

    private function placeNext(): void
    {
        $x = 2;
        $y = count($this->well) + 3;
        $piece = $this->getPiece();

        do {
            // move
            $newX = match ($this->getMove()) {
                self::LEFT => $x - 1,
                self::RIGHT => $x + 1,
            };

            if (!$this->hasCollision($piece, $newX, $y)) {
                $x = $newX;
            }

            // drop
            $newY = $y - 1;
            if ($this->hasCollision($piece, $x, $newY)) {
                $this->place($piece, $x, $y);
                break;
            }
            $y = $newY;

        } while (true);
    }

    /**
     * @param int[][] $piece
     */
    private function hasCollision(array $piece, int $x, int $y): bool
    {
        $xmax = self::SHAFT - count($piece[0]);

        foreach ($piece as $j => $pLine) {
            foreach ($pLine as $i => $value) {
                $absY = $y + $j;
                $absX = $x + $i;

                if ($value === 1) {
                    if ($x < 0 || $y < 0 || $x > $xmax || ($this->well[$absY][$absX] ?? 0) === 1) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * @param int[][] $piece
     */
    private function place(array $piece, $x, $y): void
    {
        foreach ($piece as $j => $pLine) {
            foreach ($pLine as $i => $value) {
                if ($value === 1) {
                    $absY = $y + $j;
                    $absX = $x + $i;

                    if (!isset($this->well[$absY])) {
                        $this->well[$absY] = array_fill(0, self::SHAFT, 0);
                    }
                    $this->well[$absY][$absX] = 1;
                }
            }
        }
    }

    /**
     * @return array<string, int>
     */
    private function findRepetition(): array
    {
        $mp = [];

        while (true) {
            if (isset($mp[$this->nextMove.';'.$this->nextPiece])) {
                return [
                    'moves' => $this->nextMove,
                    'pieces' => $this->nextPiece,
                ];
            }
            $mp[$this->nextMove.';'.$this->nextPiece] = true;
            $this->placeNext();
        }
    }

    /**
     * @param array<string, int> $repetition
     * @return array<string, int>
     */
    private function findPattern(array $repetition): array
    {
        $mp = [];
        $rounds = 0;

        while (count($mp) < 3) {
            if ($this->nextMove === $repetition['moves'] && $this->nextPiece === $repetition['pieces']) {
                $mp[] = [$rounds, count($this->well)];
            }
            $this->placeNext();
            $rounds++;
        }

        return [
            'round_remainder' => $mp[1][0],
            'round_modulo' => $mp[2][0] - $mp[1][0],
            'height_remainder' => $mp[1][1],
            'height_modulo' => $mp[2][1] - $mp[1][1],
        ];
    }

    private function findHeight(int $rounds): int
    {
        for ($i = 0; $i < $rounds; $i++) {
            $this->placeNext();
        }

        return count($this->well);
    }
}
