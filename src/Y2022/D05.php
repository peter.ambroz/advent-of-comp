<?php

declare(strict_types=1);

namespace App\Y2022;

use App\Day;

class D05 extends Day
{
    private array $crates;

    public function doRun(bool $reverse): void
    {
        $this->crates = $this->initialParse($this->inputLines());
        $this->inputLines(fn ($x) => $this->move($x, $reverse));

        foreach ($this->crates as $i => $crate) {
            echo array_pop($this->crates[$i]);
        }

        echo "\n";
    }

    public function run(): void
    {
        $this->doRun(false);
    }

    public function runB(): void
    {
        $this->doRun(true);
    }

    private function move(string $text, bool $reverse): int
    {
        if (!str_starts_with($text, 'move')) {
            return 0;
        }

        [, $cnt, , $from, , $to] = explode(' ', $text);
        $cnt = (int) $cnt;
        $from--;
        $to--;

        $tmp = [];

        for ($i = 0; $i < $cnt; $i++) {
            $x = array_pop($this->crates[$from]);
            $tmp[] = $x;
        }

        if ($reverse) {
            $tmp = array_reverse($tmp);
        }

        $this->crates[$to] = [...$this->crates[$to], ...$tmp];

        return 0;
    }

    private function initialParse(array $data): array
    {
        $out = [];
        $j = 0;

        foreach ($data as $line) {
            if (!strlen($line)) {
                break;
            }

            $i = 1;
            $out[$j] = [];

            while ($i < strlen($line)) {
                if (is_numeric($line[$i])) {
                    continue 2;
                }

                $out[$j][] = $line[$i];
                $i += 4;
            }

            $j++;
        }

        $out2 = [];
        foreach (array_reverse($out) as $j => $line) {
            foreach ($line as $i => $char) {
                if ($j === 0) {
                    $out2[$i] = [];
                }

                if ($char !== ' ') {
                    $out2[$i][$j] = $char;
                }
            }
        }

        return $out2;
    }
}
