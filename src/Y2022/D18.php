<?php

declare(strict_types=1);

namespace App\Y2022;

use App\Day;

class D18 extends Day
{
    private const NONE = 0;
    private const ROCK = 1;
    private const FLOOD = 2;

    private array $space;

    public function run(): void
    {
        $this->doRun(false);
    }

    public function runB(): void
    {
        $this->doRun(true);
    }

    public function doRun(bool $flood): void
    {
        $cubes = [];
        $surface = 0;
        $this->space = array_fill(0, 21,
            array_fill(0, 21,
                array_fill(0, 20, $flood ? self::NONE : self::FLOOD)
            )
        );

        foreach ($this->inputLines() as $line) {
            [$x, $y, $z] = explode(',', $line);
            $x = (int) $x;
            $y = (int) $y;
            $z = (int) $z;

            $cubes[$line] = [$x, $y, $z];
            $this->space[$x][$y][$z] = self::ROCK;
        }

        if ($flood) {
            $this->flood(0, 0, 0);
        }

        foreach ($cubes as $coords) {
            [$x, $y, $z] = $coords;

            foreach ([[1,0,0], [-1,0,0], [0,1,0], [0,-1,0], [0,0,1], [0,0,-1]] as $vec) {
                $xn = $x + $vec[0];
                $yn = $y + $vec[1];
                $zn = $z + $vec[2];

                if (($this->space[$xn][$yn][$zn] ?? self::FLOOD) === self::FLOOD) {
                    $surface++;
                }
            }
        }

        printf("%d\n", $surface);
    }

    private function flood(int $x, int $y, int $z): void
    {
        $this->space[$x][$y][$z] = self::FLOOD;

        foreach ([[1,0,0], [-1,0,0], [0,1,0], [0,-1,0], [0,0,1], [0,0,-1]] as $vec) {
            $xn = $x + $vec[0];
            $yn = $y + $vec[1];
            $zn = $z + $vec[2];

            if (($this->space[$xn][$yn][$zn] ?? self::ROCK) === self::NONE) {
                $this->flood($xn, $yn, $zn);
            }
        }
    }
}
