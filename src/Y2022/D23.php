<?php

declare(strict_types=1);

namespace App\Y2022;

use App\Day;
use App\Model\Point;

class D23 extends Day
{
    private const NORTH = 0;
    private const SOUTH = 1;
    private const WEST = 2;
    private const EAST = 3;
    private const STAY = 10;

    /** @var array<string, int> */
    private array $data = [];

    /** @var Point[] */
    private array $elf = [];

    private array $bounds = [
        self::NORTH => PHP_INT_MAX,
        self::SOUTH => PHP_INT_MIN,
        self::WEST => PHP_INT_MAX,
        self::EAST => PHP_INT_MIN,
    ];

    /** @var int[] */
    private readonly array $dirs;

    /** @var Point[][] */
    private readonly array $vectors;

    public function __construct()
    {
        $this->dirs = [self::NORTH, self::SOUTH, self::WEST, self::EAST];
        $this->vectors = [
            self::NORTH => [new Point(-1, -1), new Point(0, -1), new Point(1, -1)],
            self::SOUTH => [new Point(-1, 1), new Point(0, 1), new Point(1, 1)],
            self::WEST => [new Point(-1, -1), new Point(-1, 0), new Point(-1, 1)],
            self::EAST => [new Point(1, -1), new Point(1, 0), new Point(1, 1)],
        ];

        foreach ($this->inputLines(str_split(...)) as $j => $line) {
            foreach ($line as $i => $ch) {
                if ($ch === '#') {
                    $this->data[$i . ';' . $j] = 1;
                    $this->elf[] = new Point($i, $j);
                }
            }
        }
    }

    public function run(): void
    {
        $elfCount = count($this->elf);

        for ($i = 0; $i < 10; $i++) {
            $decisions = $this->decide($i);
            $this->move($decisions);
        }

        foreach ($this->elf as $elf) {
            $this->updateBounds($elf);
        }

        $height = $this->bounds[self::SOUTH] - $this->bounds[self::NORTH] + 1;
        $width = $this->bounds[self::EAST] - $this->bounds[self::WEST] + 1;

        printf("%d\n", $height * $width - $elfCount);
    }

    public function runB(): void
    {
        $decisions = [0];
        $i = 0;

        while (count($decisions) > 0) {
            $decisions = $this->decide($i);
            $this->move($decisions);
            $i++;
        }

        printf("%d\n", $i);
    }

    /**
     * @return int[]
     */
    private function decide(int $round): array
    {
        $decisions = [];

        foreach ($this->elf as $eid => $elf) {
            if (!$this->isAlone($elf)) {
                for ($i = 0; $i < 4; $i++) {
                    $dir = $this->dirs[($round + $i) % 4];
                    if ($this->checkDir($dir, $elf)) {
                        $newPos = $elf->add($this->vectors[$dir][1]);
                        $idx = (string)$newPos;

                        if (isset($decisions[$idx])) {
                            $decisions[$idx] = [0, self::STAY];
                        } else {
                            $decisions[$idx] = [$eid, $dir];
                        }
                        break;
                    }
                }
            }
        }

        return $decisions;
    }

    /**
     * @param int[] $decisions
     */
    private function move(array $decisions): void
    {
        foreach ($decisions as [$eid, $dir]) {
            if ($dir === self::STAY) {
                continue;
            }

            unset($this->data[(string) $this->elf[$eid]]);
            $this->elf[$eid] = $this->elf[$eid]->add($this->vectors[$dir][1]);
            $this->data[(string) $this->elf[$eid]] = 1;
        }
    }

    private function checkDir(int $direction, Point $position): bool
    {
        foreach ($this->vectors[$direction] as $vector) {
            if (isset($this->data[(string) $position->add($vector)])) {
                return false;
            }
        }

        return true;
    }

    private function isAlone(Point $position): bool
    {
        foreach ($this->vectors as $dirVectors) {
            foreach ($dirVectors as $vector) {
                if (isset($this->data[(string) $position->add($vector)])) {
                    return false;
                }
            }
        }

        return true;
    }

    private function updateBounds(Point $elf): void
    {
        if ($elf->y < $this->bounds[self::NORTH]) {
            $this->bounds[self::NORTH] = $elf->y;
        }

        if ($elf->y > $this->bounds[self::SOUTH]) {
            $this->bounds[self::SOUTH] = $elf->y;
        }

        if ($elf->x < $this->bounds[self::WEST]) {
            $this->bounds[self::WEST] = $elf->x;
        }

        if ($elf->x > $this->bounds[self::EAST]) {
            $this->bounds[self::EAST] = $elf->x;
        }
    }
}
