<?php

namespace App\Y2022;

use App\Day;

class D02 extends Day
{
    public function run(): void
    {
        $this->doRun([
            'A' => ['X' => 4, 'Y' => 8, 'Z' => 3],
            'B' => ['X' => 1, 'Y' => 5, 'Z' => 9],
            'C' => ['X' => 7, 'Y' => 2, 'Z' => 6],
        ]);
    }

    public function runB(): void
    {
        $this->doRun([
            'A' => ['X' => 3, 'Y' => 4, 'Z' => 8],
            'B' => ['X' => 1, 'Y' => 5, 'Z' => 9],
            'C' => ['X' => 2, 'Y' => 6, 'Z' => 7],
        ]);
    }

    private function doRun($mat): void
    {
        $score = 0;

        foreach ($this->inputLines(fn ($line) => explode(' ', $line)) as $pair) {
            $score += $mat[$pair[0]][$pair[1]];
        }

        printf("%s\n", $score);
    }
}
