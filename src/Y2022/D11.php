<?php

/* Dear @JozefKatruska, I refuse to write a parser for this. Have a nice day. */

declare(strict_types=1);

namespace App\Y2022;

use App\Day;
use App\Y2022\Model\Monkey;

class D11 extends Day
{
    /** @var Monkey[] */
    private array $mon;

    public function init(): void
    {
        $this->mon = [
            new Monkey(
                [66, 71, 94],
                fn($x) => $x * 5,
                3,
                [4, 7],
            ),
            new Monkey(
                [70],
                fn($x) => $x + 6,
                17,
                [0, 3],
            ),
            new Monkey(
                [62, 68, 56, 65, 94, 78],
                fn($x) => $x + 5,
                2,
                [1, 3],
            ),
            new Monkey(
                [89, 94, 94, 67],
                fn($x) => $x + 2,
                19,
                [0, 7],
            ),
            new Monkey(
                [71, 61, 73, 65, 98, 98, 63],
                fn($x) => $x * 7,
                11,
                [6, 5],
            ),
            new Monkey(
                [55, 62, 68, 61, 60],
                fn($x) => $x + 7,
                5,
                [1, 2],
            ),
            new Monkey(
                [93, 91, 69, 64, 72, 89, 50, 71],
                fn($x) => $x + 1,
                13,
                [2, 5],
            ),
            new Monkey(
                [76, 50],
                fn($x) => $x * $x,
                7,
                [6, 4],
            ),
        ];
    }

    public function run(): void
    {
        $this->doRun(20, true);
    }

    public function runB(): void
    {
        $this->doRun(10000, false);
    }

    public function doRun(int $rounds, bool $div3): void
    {
        $this->init();

        $proc = array_fill(0, 8, 0);

        for ($i = 0; $i < $rounds; $i++) {
            foreach ($this->mon as $id => $m) {
                $q = $m->process($div3);

                foreach ($q as $destM => $items) {
                    $this->mon[$destM]->enqueue($items);
                }

                $proc[$id] = $m->getCount();
            }
        }

        rsort($proc);

        printf("%d\n", $proc[0] * $proc[1]);
    }
}
