<?php

declare(strict_types=1);

namespace App\Y2022;

use App\Day;

/**
 * Second part was a hand approximation. Not an universal solution.
 */
class D21 extends Day
{
    private array $data;

    public function run(): void
    {
        $this->inputLines($this->process(...));
        printf("%d\n", $this->calc('root'));
    }

    public function runB(): void
    {
        $this->inputLines($this->process(...));
        $this->data['root'][1] = '==';
        for ($i = 3330805250000; $i < 20000000000000; $i++) {
            $this->data['humn'] = $i;
            if ($this->calc('root')) {
                printf("GOOD %d; %d === %d\n", $i, $this->calc($this->data['root'][0]), $this->calc($this->data['root'][2]));
                break;
            } else {
                printf("BAD %d; %d !== %d\n", $i, $this->calc($this->data['root'][0]), $this->calc($this->data['root'][2]));
            }
        }
    }

    private function process(string $line): void
    {
        [$label, $part] = explode(' ', $line, 2);
        $label = substr($label, 0, -1);
        if (is_numeric($part)) {
            $this->data[$label] = (int) $part;
        } else {
            $this->data[$label] = explode(' ', $part);
        }
    }

    private function calc(string $key): int
    {
        $z = $this->data[$key];
        if (!is_array($z)) {
            return $z;
        }

        return match ($z[1]) {
            '+' => $this->calc($z[0]) + $this->calc($z[2]),
            '-' => $this->calc($z[0]) - $this->calc($z[2]),
            '*' => $this->calc($z[0]) * $this->calc($z[2]),
            '/' => intdiv($this->calc($z[0]), $this->calc($z[2])),
            '==' => $this->calc($z[0]) === $this->calc($z[2]) ? 1 : 0,
        };
    }
}
