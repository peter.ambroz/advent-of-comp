<?php

namespace App\Y2022;

use App\Day;

class D01 extends Day
{
    public function run(): void
    {
        $elf = $this->loadAndSort();

        printf("%d\n", $elf[0]);
    }

    public function runB(): void
    {
        $elf = $this->loadAndSort();

        printf("%d\n", $elf[0] + $elf[1] + $elf[2]);
    }

    private function loadAndSort(): array
    {
        $elf = [];
        $elfId = 0;

        foreach ($this->inputLines() as $line) {
            $line = trim($line);

            if (!strlen($line)) {
                $elfId++;

                continue;
            }

            $elf[$elfId] = ($elf[$elfId] ?? 0) + (int) $line;
        }

        rsort($elf);

        return $elf;
    }
}
