<?php

namespace App\Y2022;

use App\Day;
use App\Model\Point;

class D12 extends Day
{
    /** @var int[][] */
    private array $map;

    /** @var ?int[]  */
    private array $dij = [];

    /** @var Point[] */
    private array $start;

    private Point $end;

    /** @var Point[] */
    private readonly array $directions;

    public function __construct()
    {
        $this->directions = [
            new Point(1, 0),
            new Point(0, -1),
            new Point(-1, 0),
            new Point(0, 1),
        ];
    }

    public function run(): void
    {
        $map = $this->inputLines(str_split(...));
        $this->map = $this->init($map, false);
        $this->dijkstra($this->start);

        printf("%d\n", $this->getBestDistance($this->end));
    }

    public function runB(): void
    {
        $map = $this->inputLines(str_split(...));
        $this->map = $this->init($map, true);
        $this->dijkstra($this->start);

        printf("%d\n", $this->getBestDistance($this->end));
    }

    /**
     * @param string[][] $map
     * @return int[][]
     */
    private function init(array $map, bool $markLowest): array
    {
        $newMap = [];

        foreach ($map as $y => $row) {
            $newRow = [];

            foreach ($row as $x => $char) {
                if ($char === 'S') {
                    $startPoint = new Point($x, $y);
                    $this->start[] = $startPoint;
                    $this->setBestDistance($startPoint, 0);
                    $newRow[] = 0;
                    continue;
                }

                if ($char === 'E') {
                    $this->end = new Point($x, $y);
                    $newRow[] = 25;
                    continue;
                }

                $char = ord($char) - 97;
                $newRow[] = $char;

                if ($markLowest) {
                    if ($char === 0) {
                        $startPoint = new Point($x, $y);
                        $this->start[] = $startPoint;
                        $this->setBestDistance($startPoint, 0);
                    }
                }
            }

            $newMap[] = $newRow;
        }

        return $newMap;
    }

    /**
     * @param Point[] $points
     */
    private function dijkstra(array $points): void
    {
        foreach ($points as $point) {
            $this->dijkstra($this->neighbors($point));
        }
    }

    /**
     * @return Point[]
     */
    private function neighbors(Point $point): array
    {
        $neightborList = [];
        $current = $this->getHeight($point);
        $currentLength = $this->getBestDistance($point);

        foreach ($this->directions as $delta) {
            $neighborPoint = $point->add($delta);
            $n = $this->getHeight($neighborPoint);

            if ($n !== null
                && ($n - $current <= 1)
                && $this->getBestDistance($neighborPoint) > ($currentLength + 1))
            {
                $neightborList[] = $neighborPoint;
                $this->setBestDistance($neighborPoint, $currentLength + 1);
            }
        }

        return $neightborList;
    }

    private function getHeight(Point $point): ?int
    {
        return $this->map[$point->y][$point->x] ?? null;
    }

    private function getBestDistance(Point $point): int
    {
        return $this->dij[(string) $point] ?? 90000;
    }

    private function setBestDistance(Point $point, int $distance): void
    {
        $this->dij[(string) $point] = $distance;
    }
}
