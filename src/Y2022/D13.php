<?php

declare(strict_types=1);

namespace App\Y2022;

use App\Day;

class D13 extends Day
{
    public function run(): void
    {
        $sum = 0;
        $pair = [];

        foreach ($this->inputLines() as $i => $line) {
            if (strlen($line) > 0) {
                $pair[] = json_decode($line, true);
            } else {
                if ($this->cmp($pair[0], $pair[1]) < 0) {
                    $sum += intdiv($i + 1, 3);
                }

                $pair = [];
            }
        }

        printf("%d\n", $sum);
    }

    public function runB(): void
    {
        $pairs = array_filter($this->inputLines(fn ($x) => json_decode($x, true)));
        $pairs[] = [[6]];
        $pairs[] = [[2]];

        usort($pairs, $this->cmp(...));
        $a = array_search([[2]], $pairs) + 2;
        $b = array_search([[6]], $pairs) + 2;

        printf("%d\n", $a * $b);
    }

    public function cmp(array $p0, array $p1): int
    {
        foreach ($p0 as $i => $item) {
            if (!isset($p1[$i])) {
                return 1;
            }

            $item2 = $p1[$i];

            if (!is_array($item) && !is_array($item2)) {
                if ($item > $item2) {
                    return 1;
                } elseif ($item < $item2) {
                    return -1;
                }
            } else {
                if (!is_array($item)) {
                    $item = [$item];
                } elseif (!is_array($item2)) {
                    $item2 = [$item2];
                }

                if (($ret = $this->cmp($item, $item2)) !== 0) {
                    return $ret;
                }
            }
        }

        if (count($p0) < count($p1)) {
            return -1;
        }

        return 0;
    }
}
