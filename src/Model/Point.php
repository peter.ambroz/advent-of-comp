<?php

declare(strict_types=1);

namespace App\Model;

use DivisionByZeroError;
use Exception;

readonly class Point
{
    public function __construct(
        public int $x,
        public int $y,
    ) {
    }

    public static function fromString(string $p): self
    {
        [$x, $y] = array_map(fn($a)=>(int)$a, explode(';', $p));
        return new self($x, $y);
    }

    public function add(self $point): self
    {
        return new self($this->x + $point->x, $this->y + $point->y);
    }

    public function sub(self $point): self
    {
        return new self($this->x - $point->x, $this->y - $point->y);
    }

    public function mul(int $magnitude): self
    {
        return new self($this->x * $magnitude, $this->y * $magnitude);
    }

    public function distance(self $point): int
    {
        $diff = $this->sub($point);

        return abs($diff->x) + abs($diff->y);
    }

    /**
     * @return self[]
     */
    public function neighbors(): array
    {
        return [
            new self($this->x, $this->y - 1),
            new self($this->x + 1, $this->y),
            new self($this->x, $this->y + 1),
            new self($this->x - 1, $this->y),
        ];
    }

    /**
     * @throws Exception
     */
    public function simplify(): self
    {
        if ($this->x === 0 && $this->y === 0) {
            throw new DivisionByZeroError();
        }

        if ($this->x === 0) {
            return new self(0, intdiv($this->y, abs($this->y)));
        }

        if ($this->y === 0) {
            return new self(intdiv($this->x, abs($this->x)), 0);
        }

        throw new Exception('Not yet implemented for non-right angle vectors');
    }

    public function equals(self $point): bool
    {
        return $this->x === $point->x && $this->y === $point->y;
    }

    public function appearsBefore(self $point): bool
    {
        return ($this->y < $point->y) || ($this->y === $point->y && $this->x < $point->x);
    }

    public function __toString(): string
    {
        return sprintf('%d;%d', $this->x, $this->y);
    }
}
