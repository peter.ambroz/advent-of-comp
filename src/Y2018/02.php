<?php
function analyze(string $l): array {
  $x = [0, 0];
  $y = [];
  
  foreach(str_split($l) as $c) {
    if (!isset($y[$c])) {
      $y[$c] = 1;
    } else {
      $y[$c]++;
    }
  }
  
  foreach($y as $cnt) {
    if ($cnt === 2) $x[0] = 1;
    if ($cnt === 3) $x[1] = 1;
  }
  
  return $x;
}


$t2 = 0;
$t3 = 0;

$ls = [];
foreach(explode("\n", file_get_contents('02.txt')) as $l) {
  $l = trim($l);
  if (!strlen($l)) continue;
  
  $ls[] = $l;
  
  [$two, $three] = analyze($l);
  $t2 += $two;
  $t3 += $three;
  
  
}

echo $t2 * $t3 . "\n";

foreach ($ls as $i1=>$l1) {
    foreach ($ls as $i2=>$l2) {
        if ($l1 == $l2) continue;
        if (levenshtein($l1, $l2, 2, 1, 2) == 1) {
            printf("%s\n%s\n\n", $l1, $l2);
            break;
        }
    }
}
?>
