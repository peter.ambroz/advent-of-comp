<?php

declare(strict_types=1);

namespace App\Y2018;

use App\Day;
use App\Y2018\Model\Army;
use App\Y2018\Model\AttackType;
use App\Y2018\Model\UnitGroup;

class D24 extends Day
{
    /** @var Army[] */
    private array $army;

    public function run(): void
    {
        $this->doRun(0);
    }

    public function runB(): void
    {
        $this->doRun(59);
    }

    private function doRun(int $boost): void
    {
        $this->parse();
        $this->army[0]->boostAttack($boost);

        while (true) {
            $selections = $this->select();
            $this->attack($selections);

            foreach ($this->army as $i => $army) {
                if ($army->isDefeated()) {
                    $opponent = $this->army[1 - $i];
                    printf(
                        "%s wins with %d units\n",
                        $opponent->name,
                        $opponent->getSize(),
                    );
                    break 2;
                }
            }
        }
    }

    /**
     * @return UnitGroup[][]
     */
    private function select(): array
    {
        $selections = [];
        foreach ($this->army as $i => $army) {
            $opponent = $this->army[1 - $i];
            $selections = array_merge($selections, $army->select($opponent));
        }

        return $selections;
    }

    /**
     * @param UnitGroup[][] $selections
     */
    private function attack(array $selections): void
    {
        usort($selections, fn (array $a, array $b) => $b[0]->initiative <=> $a[0]->initiative);

        foreach ($selections as $selection) {
            $selection[1]->attackBy($selection[0]);
        }
    }

    private function parse(): void
    {
        $dest = 0;
        $armies = [[], []];
        foreach ($this->inputLines(self::parseLine(...)) as $ug) {
            if ($ug === null) {
                $dest = 1;
                continue;
            }

            $armies[$dest][] = $ug;
        }

        $this->army = [
            new Army('Immune system', $armies[0]),
            new Army('Infection', $armies[1]),
        ];
    }

    private static function parseLine(string $line): ?UnitGroup
    {
        if (!strlen($line)) {
            return null;
        }

        $lmr = strtr($line, ['points with' => 'points () with']);
        [$l, $mr] = explode(' (', $lmr);
        [$m, $r] = explode(') ', $mr);

        [$units, , , , $hp,] = explode(' ', $l);
        [, , , , , $attack, $attackType, , , , $initiative] = explode(' ', $r);
        $immuneWeak = ['immune' => [], 'weak' => []];
        foreach (explode('; ', $m) as $midpart) {
            if ($midpart === '') {
                continue;
            }
            [$what, $attacks] = explode(' to ', $midpart);
            $immuneWeak[$what] = array_map(fn ($x) => AttackType::from($x), explode(', ', $attacks));
        }

        return new UnitGroup(
            (int) $units,
            (int) $hp,
            $immuneWeak['immune'],
            $immuneWeak['weak'],
            (int) $attack,
            AttackType::from($attackType),
            (int) $initiative,
        );
    }
}
