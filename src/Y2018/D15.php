<?php
declare(strict_types=1);

namespace App\Y2018;

use App\Day;
use App\Model\Point;
use App\Y2018\Model\Bandits;
use App\Y2018\Model\Enemy;
use App\Y2018\Model\EnemyType;

class D15 extends Day
{
    private const HP = 200;
    private const STRENGTH = 3;

    /** @var string[][] */
    private array $map;
    private Bandits $bandits;
    private int $round;
    private array $c;
    private array $hits;

    private function loadMap(int $elfPower): void
    {
        $this->round = 0;
        $enemies = [];

        foreach ($this->inputLines() as $y => $line) {
            $data = str_split($line);
            $this->map[] = $data;

            foreach ($data as $x => $char) {
                if (in_array($char, ['E', 'G'])) {
                    $enemies[] = new Enemy(
                        EnemyType::from($char),
                        $char === 'E' ? $elfPower : self::STRENGTH,
                        self::HP,
                        new Point($x, $y),
                    );
                }
            }
        }

        $this->bandits = new Bandits($enemies);
    }

    private function findBestTarget(Enemy $e): ?Point
    {
        $dest = $e->type->opponent()->value;
        $this->pathFinder($dest, $e->getLoc());
        $minP = null;
        $minD = null;
        foreach ($this->hits as $k => $v) {
            if ($minD === null || $minD > $v) {
                $minD = $v;
                $minP = Point::fromString($k);
            } elseif ($minD === $v) {
                $p = Point::fromString($k);
                if ($p->appearsBefore($minP)) {
                    $minP = $p;
                }
            }
        }
        return $minP;
    }

    private function pathFinder(string $dest, Point $p, int $dist = 0): void
    {
        if ($dist === 0) {
            $this->c = [];
            $this->hits = [];
        }
        $vDist = $this->c[(string) $p] ?? null;
        if ($vDist !== null && $vDist <= $dist) {
            return;
        }
        $this->c[(string) $p] = $dist;
        foreach ($p->neighbors() as $n) {
            $next = $this->get($n);
            if ($next === $dest) {
                $this->hits[(string) $p] = $dist;
            } elseif ($next === '.') {
                $this->pathFinder($dest, $n, $dist + 1);
            }
        }
    }

    private function pathFinder2(Point $dest, Point $p, int $dist = 0): void
    {
        if ($dist === 0) {
            $this->c = [];
        }
        $vDist = $this->c[(string) $p] ?? null;
        if ($vDist !== null && $vDist <= $dist) {
            return;
        }
        $this->c[(string) $p] = $dist;
        foreach ($p->neighbors() as $n) {
            $next = $this->get($n);
            if ($n->equals($dest)) {
                $this->hits[(string) $p] = $dist;
            } elseif ($next === '.') {
                $this->pathFinder2($dest, $n, $dist + 1);
            }
        }
    }

    private function moveTowards(Enemy $e, Point $t): void
    {
        if ($e->getLoc()->equals($t)) {
            return;
        }
        $this->pathFinder2($e->getLoc(), $t);
        $min = null;
        $minN = null;
        foreach ($this->hits as $k => $v) {
            if ($min === null || $min > $v) {
                $min = $v;
                $minN = Point::fromString($k);
            } elseif ($min === $v) {
                $p = Point::fromString($k);
                if ($p->appearsBefore($minN)) {
                    $minN = $p;
                }
            }
        }

        if ($minN !== null) {
            $this->set($e->getLoc(), '.');
            $e->move($minN);
            $this->set($minN, $e->type->value);
        }
    }

    private function get(Point $p): string
    {
        return $this->map[$p->y][$p->x] ?? '#';
    }

    private function set(Point $p, string $val): void
    {
        if (isset($this->map[$p->y][$p->x])) {
            $this->map[$p->y][$p->x] = $val;
        }
    }

    public function run(): void
    {
        $this->doRun();
    }

    public function runB(): void
    {
        $this->doRun(10);
    }

    private function doRun(int $elfPower = self::STRENGTH): void
    {
        $this->loadMap($elfPower);
        $protectElves = $elfPower > self::STRENGTH;

        while (true) {
            foreach ($this->bandits->sorted() as $enemy) {
                if ($enemy->isDead()) {
                    continue;
                }
                if (!count($this->bandits->aliveOpponentsTo($enemy))) {
                    break 2;
                }
                $target = $this->findBestTarget($enemy);
                if ($target === null) {
                    continue;
                }
                $this->moveTowards($enemy, $target);
                $defender = $this->bandits->attack($enemy);
                if ($defender !== null && $defender->isDead()) {
                    if ($protectElves && $defender->type === EnemyType::ELF) {
                        printf("Elf died!\n");
                        return;
                    }
                    $this->set($defender->getLoc(), '.');
                }
            }

            $this->round++;
            $hpSum = $this->bandits->totalHp();
            printf("R %d HP %d\n", $this->round, $hpSum);
        }

        $hpSum = $this->bandits->totalHp();

        printf("%d (%d rounds, %d hp)\n", $this->round * $hpSum, $this->round, $hpSum);
    }
}
