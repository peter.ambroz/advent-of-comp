<?php

declare(strict_types=1);

namespace App\Y2018;

use App\Day;

class D19 extends Day
{
    public function run(): void
    {
/*
        $o = new Opcode();
        $o->setIp(3);

        $mem = $this->inputLines(fn ($x) => explode(' ', $x));
        $ip = 0;

        while (isset($mem[$ip])) {
            $instr = $mem[$ip];
            $ip = $o->exec($instr[0], (int)$instr[1], (int)$instr[2], (int)$instr[3]);
        }

        printf("%d\n", $o->getReg()[0]);
*/

        $r2 = 1018;
        $r1 = 1;
        $r0 = 0;
        do {
            $r4 = 1;
            do {
                $r5 = $r1 * $r4;
                if ($r2 === $r5) {
                    $r0 += $r1;
                }
                $r4++;
            } while ($r4 <= $r2);

            $r1++;
        } while ($r1 <= $r2);

        printf("%d\n", $r0);
    }

    public function runB(): void
    {
/*
        $o = new Opcode();
        $o->setIp(3);
        $o->setReg([1, 0, 0, 0, 0, 0]);

        $mem = $this->inputLines(fn ($x) => explode(' ', $x));
        $ip = 0;

        while (isset($mem[$ip])) {
            $instr = $mem[$ip];
            $ip = $o->exec($instr[0], (int)$instr[1], (int)$instr[2], (int)$instr[3]);
        }

        printf("%d\n", $o->getReg()[0]);
*/
        $r2 = 10551418;
        $r1 = 1;
        $r0 = 0;
        $r6 = 0;
        do {
            $r4 = 1;
            do {
                $r5 = $r1 * $r4;
                if ($r2 === $r5) {
                    $r0 += $r1;
                }
                $r4++;
            } while ($r4 <= $r2);

            $r1++;

            $r6++;
            if ($r6 === 10) {
                $r6 = 0;
                printf("%d\n", $r1);
            }
        } while ($r1 <= $r2);

        printf("%d\n", $r0);
    }
}
