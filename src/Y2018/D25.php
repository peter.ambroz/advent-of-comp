<?php
declare(strict_types=1);

namespace App\Y2018;

use App\Day;

class D25 extends Day
{
    private array $con = [];

    private function dist(array $a, array $b): int
    {
        return abs($a[0] - $b[0]) + abs($a[1] - $b[1]) + abs($a[2] - $b[2]) + abs($a[3] - $b[3]);
    }

    public function process(string $line): int
    {
        $ad = explode(',', $line);

        $cids = [];
        foreach ($this->con as $cid => $constellation) {
            foreach ($constellation as $star) {
                if ($this->dist($star, $ad) <= 3) {
                   $cids[] = $cid;
                   break;
                }
            }
        }

        switch (count($cids)) {
            case 0:
                $this->con[] = [$ad];
                //printf("Empty conlist\n");
                break;
            case 1:
                $this->con[$cids[0]][] = $ad;
                //printf("Star into 1 con\n");
                break;
            default:
                $bx = count($this->con);
                $cons = [$ad];
                foreach ($cids as $cid) {
                    $cons = array_merge($cons, $this->con[$cid]);
                    unset($this->con[$cid]);
                }
                $this->con[] = $cons;
                $ax = count($this->con);
                printf("Merger: %d %d %d\n", count($cids), $bx, $ax);
                break;
        }

        return 1;
    }

    public function run(): void
    {
        $this->inputLines([$this, 'process']);
        printf("%d\n", count($this->con));
    }
}