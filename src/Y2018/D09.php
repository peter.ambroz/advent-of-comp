<?php
declare(strict_types=1);

namespace App\Y2018;

use App\Day;

class D09 extends Day
{
    private const PLAYERS = 478;

    public function run(): void
    {
        $this->doRun(71240);
    }

    public function runB(): void
    {
        $this->doRun(7124000);
    }

    public function doRun(int $marbles): void
    {
        $score = array_fill(0, self::PLAYERS, 0);
        $table = new CircularList();
        $table->push(0);

        $player = 1;
        for ($marble = 1; $marble <= $marbles; $marble++) {
            if ($marble % 23) {
                $table->rotate(1);
                $table->push($marble);
            } else {
                $table->rotate(-7);
                $score[$player] += $marble + $table->pop();
                $table->rotate(1);
            }

            $player++;
            if ($player === self::PLAYERS) {
                $player = 0;
            }
        }

        $maxs = 0;
        foreach ($score as $sc) {
            if ($sc > $maxs) {
                $maxs = $sc;
            }
        }

        printf("Max %d\n", $maxs);
    }
}
