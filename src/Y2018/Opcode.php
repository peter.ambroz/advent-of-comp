<?php

declare(strict_types=1);

namespace App\Y2018;

class Opcode
{
    /** @var int[] */
    private array $reg;

    private int $ip;

    public function __construct()
    {
        $this->reset();
    }

    public function exec(string $instr, int $a, int $b, int $c): int
    {
        $this->reg[$c] = match ($instr) {
            'addr' => $this->reg[$a] + $this->reg[$b],
            'addi' => $this->reg[$a] + $b,
            'mulr' => $this->reg[$a] * $this->reg[$b],
            'muli' => $this->reg[$a] * $b,
            'banr' => $this->reg[$a] & $this->reg[$b],
            'bani' => $this->reg[$a] & $b,
            'borr' => $this->reg[$a] | $this->reg[$b],
            'bori' => $this->reg[$a] | $b,
            'setr' => $this->reg[$a],
            'seti' => $a,
            'gtir' => ($a > $this->reg[$b]) ? 1 : 0,
            'gtri' => ($this->reg[$a] > $b) ? 1 : 0,
            'gtrr' => ($this->reg[$a] > $this->reg[$b]) ? 1 : 0,
            'eqir' => ($a === $this->reg[$b]) ? 1 : 0,
            'eqri' => ($this->reg[$a] === $b) ? 1 : 0,
            'eqrr' => ($this->reg[$a] === $this->reg[$b]) ? 1 : 0,
        };

        return ++$this->reg[$this->ip];
    }

    /**
     * @param int[] $reg
     */
    public function setReg(array $reg): void
    {
        $this->reg = $reg;
    }

    /**
     * @return int[]
     */
    public function getReg(): array
    {
        return $this->reg;
    }

    public function reset(): void
    {
        $this->reg = [0, 0, 0, 0, 0, 0];
        $this->ip = 5;
    }

    public function setIp(int $ip): void
    {
        $this->ip = $ip;
    }
}
