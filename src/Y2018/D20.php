<?php

declare(strict_types=1);

namespace App\Y2018;

use App\Day;
use App\Model\Point;
use ArrayIterator;
use Iterator;

class D20 extends Day
{
    private array $map = [];

    public function run(): void
    {
        $this->doRun();
        printf("%d\n", max($this->map));
    }

    public function runB(): void
    {
        $this->doRun();
        printf("%d\n", count(array_filter($this->map, fn ($x) => $x >= 1000)));
    }

    private function doRun(): void
    {
        $start = new Point(20, 20);
        $this->map[(string) $start] = 0;
        $this->evaluate(
            new ArrayIterator(
                str_split(substr($this->inputLines()[0], 1, -1))
            ),
            $start,
        );
    }

    private function evaluate(Iterator $re, Point $p, int $d = 0): void
    {
        $np = $p;
        $nd = $d;
        while ($re->valid()) {
            $c = $re->current();
            switch ($c) {
                case 'N':
                case 'E':
                case 'S':
                case 'W':
                    $n = array_combine(['N', 'E', 'S', 'W'], $np->neighbors());
                    $np = $n[$c];
                    if (isset($this->map[(string) $np])) {
                        $nd = $this->map[(string) $np];
                    } else {
                        $this->map[(string) $np] = ++$nd;
                    }

                    break;
                case '(':
                    $re->next();
                    $this->evaluate($re, $np, $nd);
                    break;
                case ')':
                    return;
                case '|':
                    $np = $p;
                    $nd = $d;
                    break;
            }

            $re->next();
        }
    }
}
