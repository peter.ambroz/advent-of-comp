<?php

declare(strict_types=1);

namespace App\Y2018;

use SplDoublyLinkedList;

class CircularList extends SplDoublyLinkedList
{
    public function rotate(int $num): void
    {
        if ($num === 0) {
            return;
        }

        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $this->push($this->shift());
            }
        } else {
            for ($i = $num; $i < 0; $i++) {
                $this->unshift($this->pop());
            }
        }
    }
}
