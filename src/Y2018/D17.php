<?php

declare(strict_types=1);

namespace App\Y2018;

use App\Day;

class D17 extends Day
{
//  X: 432..621
    private const YMIN = 3;
    private const YMAX = 1971;
// private const YMIN = 1;
// private const YMAX = 13;
    private const SAND = 0;
    private const WALL = 1;
    private const WATER = 2;
    private const FLOAT = 3;

    /**
     * @var int[][]
     */
    private array $w;

    private int $wc;

    private function init(): void
    {
        $this->wc = 0;
        $this->w = array_fill(0, self::YMAX + 1, []);

        foreach ($this->inputLines(function ($x) {
            [$l, $r] = explode(', ', $x);
            [$lc, $lval] = explode('=', $l);
            [, $rval] = explode('=', $r);
            [$rmin, $rmax] = explode('..', $rval);

            return [
                'coord' => $lc,
                'value' => (int) $lval,
                'min' => (int) $rmin,
                'max' => (int) $rmax
            ];
        }) as $line) {
            $value = $line['value'];
            if ($line['coord'] === 'x') {
                for ($y = $line['min']; $y <= $line['max']; $y++) {
                    $this->w[$y][$value] = self::WALL;
                }
            } else {
                for ($x = $line['min']; $x <= $line['max']; $x++) {
                    $this->w[$value][$x] = self::WALL;
                }
            }
        }
    }

    private function follow(int $x, int $y): void
    {
        while (!isset($this->w[$y][$x]) && $y <= self::YMAX) {
            $this->w[$y++][$x] = self::FLOAT;
            if ($y >= self::YMIN) {
                $this->wc++;
            }
        }

        if (($y > self::YMAX) || ($this->w[$y][$x] === self::FLOAT)) {
            return;
        }

        // got wall or water underneath
        while (true) {
            $y--;
            $left = $this->explore($x, $y, -1);
            $right = $this->explore($x, $y, 1);
            if ($left === -1 || $right === -1) {
                break;
            }

            for ($i = $x - $left; $i <= $x + $right; $i++) {
                $this->w[$y][$i] = self::WATER;
            }
        }
    }

    /**
     * @return int steps required to hit the wall
     */
    private function explore(int $x, int $y, int $dx): int
    {
        $steps = 0;
        while (true) {
            $x += $dx;
            $here = $this->w[$y][$x] ?? self::SAND;
            $down = $this->w[$y+1][$x] ?? self::SAND;

            switch ($here) {
                case self::FLOAT:
                    $steps++;
                    break;
                case self::WALL:
                    return $steps;
                case self::WATER:
//                    $this->visual();
//                    printf("water shouldn't be here %d:%d\n", $x, $y);
//                    exit;
                    return -1;
                case self::SAND:
                    $this->w[$y][$x] = self::FLOAT;
                    if ($y >= self::YMIN) {
                        $this->wc++;
                    }
                    $steps++;
                    break;
            }

            switch ($down) {
                case self::FLOAT:
                    break;
                case self::SAND:
                    $this->follow($x, $y+1);
                    return -1;
            }
        }
    }

    private function visual(): void
    {
        for ($y = 1; $y <= self::YMAX; $y++) {
            printf("%4d: ", $y);
            for ($x = 431; $x <= 622; $x++) {
                if (!isset($this->w[$y][$x])) {
                    echo '.';
                } elseif ($this->w[$y][$x] === self::WATER) {
                    echo '~';
                } elseif ($this->w[$y][$x] === self::WALL) {
                    echo '#';
                } elseif ($this->w[$y][$x] === self::FLOAT) {
                    echo '|';
                } else {
                    echo '?';
                }
            }
            echo "\n";
        }
    }

    public function run(): void
    {
        $this->init();
        $this->follow(500, self::YMIN);
        $cnt = 39 - 28;
        $this->visual();
        foreach ($this->w as $row) {
            foreach ($row as $cell) {
                if ($cell === self::WATER || $cell === self::FLOAT) {
                    $cnt++;
                }
            }
        }
        printf("water = %d\n", $cnt);
    }

    public function runB(): void
    {
        $this->init();
        $this->follow(500, self::YMIN);
        $cnt = 0;
        $this->visual();
        foreach ($this->w as $row) {
            foreach ($row as $cell) {
                if ($cell === self::WATER) {
                    $cnt++;
                }
            }
        }
        printf("water = %d\n", $cnt);
    }
}
