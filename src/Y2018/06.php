<?php
$map = array_fill(0, 400, array_fill(0, 400, -1));
$ptsize = array_fill(0, 50, 0);

$points = array_map(function(string $line): array {
    return array_map(function(string $pt): int {
        return (int)$pt;
    }, explode(', ', $line));
}, explode("\n", file_get_contents('06.txt')));

unset($points[50]);

$minx = 999;
$maxx = 0;
$miny = 999;
$maxy = 0;

foreach ($points as $id => $point) {
    $map[$point[0]][$point[1]] = $id;
    if ($point[0] > $maxx) $maxx = $point[0];
    if ($point[0] < $minx) $minx = $point[0];
    if ($point[1] > $maxy) $maxy = $point[1];
    if ($point[1] < $miny) $miny = $point[1];
}

printf("%d %d %d %d\n", $minx, $maxx, $miny, $maxy);

foreach ($map as $mx => $cols) {
    foreach ($cols as $my => &$cell) {
        $shortestPt = -1;
        $shortestDist = 999;
        foreach ($points as $id => $point) {
            $dist = abs($point[0] - $mx) + abs($point[1] - $my);
            if ($dist < $shortestDist) {
                $shortestDist = $dist;
                $shortestPt = $id;
            } elseif ($dist === $shortestDist) {
                $shortestDist = $dist;
                $shortestPt = -1;
            }
        }
        $cell = $shortestPt;
        if ($shortestPt !== -1) {
            $ptsize[$shortestPt]++;
        }
    }
    unset($cell);
}

$ptsizeNew = array_fill(0, 50, 0);

foreach ($map as $mx => $cols) {
    if ($mx === 0 || $mx === 399) continue;
    foreach ($cols as $my => &$cell) {
        if ($my === 0 || $my === 399) continue;
    
        $shortestPt = -1;
        $shortestDist = 999;
        foreach ($points as $id => $point) {
            $dist = abs($point[0] - $mx) + abs($point[1] - $my);
            if ($dist < $shortestDist) {
                $shortestDist = $dist;
                $shortestPt = $id;
            } elseif ($dist === $shortestDist) {
                $shortestDist = $dist;
                $shortestPt = -1;
            }
        }
        $cell = $shortestPt;
        if ($shortestPt !== -1) {
            $ptsizeNew[$shortestPt]++;
        }
    }
    unset($cell);
}


$maxSize = 0;
$considered = 0;
for ($i = 0; $i < 50; $i++) {
    if ($ptsize[$i] === $ptsizeNew[$i]) {
        $considered++;
        if ($maxSize < $ptsize[$i]) {
            $maxSize = $ptsize[$i];
        }
    }
}

printf("%d %d\n", $maxSize, $considered);

$closerThan10k = 0;
foreach ($map as $mx => $cols) {
    foreach ($cols as $my => &$cell) {
        $totalDist = 0;
        foreach ($points as $id => $point) {
            $totalDist += abs($point[0] - $mx) + abs($point[1] - $my);
        }
        if ($totalDist < 10000) {
            $closerThan10k++;
        }
    }
    unset($cell);
}

printf("%d\n", $closerThan10k);
