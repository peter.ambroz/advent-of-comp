<?php
$p = [];

function rst()
{
global $p;
$p = [];
foreach (explode("\n", file_get_contents('10.txt')) as $line) {
  $line = trim($line);
  if (empty($line)) {
    continue;
  }
  
  [,$coordPart, $veloPart] = explode('<', $line);
  [$x, $coordRest] = explode(',', $coordPart);
  [$y,] = explode('>', $coordRest);
  
  [$xd, $veloRest] = explode(',', $veloPart);
  [$yd,] = explode('>', $veloRest);
  
  $p[] = [(int)trim($x), (int)trim($y), (int)trim($xd), (int)trim($yd)];
}
}

function step(int $n = 1): array
{
  global $p;
  
  $r = [99999,99999,-99999,-99999];
  foreach ($p as &$l) {
    $l[0] += $l[2]*$n;
    $l[1] += $l[3]*$n;
    
    if ($l[0] < $r[0]) $r[0] = $l[0];
    if ($l[0] > $r[2]) $r[2] = $l[0];
    if ($l[1] < $r[1]) $r[1] = $l[1];
    if ($l[1] > $r[3]) $r[3] = $l[1];
  }
  unset($l);
  
  return $r;
}

rst();
/*
$s = 0;
$bmin = 9999999;
while(true) {
  $bound = step();
  $s++;
  $b = [$bound[2] - $bound[0]+1, $bound[3] - $bound[1]+1];
  printf("%d: %d x %d\n", $s, $b[0], $b[1]);
  if ($bmin > ($b[0] + $b[1])) {
    $bmin = $b[0] + $b[1];
  } else {
    break;
  }
}
*/

$bound = step(10105);

$pm = [];
foreach ($p as $l) {
  if (!isset($pm[$l[1]])) $pm[$l[1]] = [];
  $pm[$l[1]][$l[0]] = '*';
}

for ($j = $bound[1]; $j<=$bound[3]; $j++) {
  for ($i = $bound[0]; $i <=$bound[2]; $i++) {
    echo $pm[$j][$i] ?? ' ';
  }
  echo "\n";
}
