<?php

declare(strict_types=1);

namespace App\Y2018;

use App\Day;

class D21 extends Day
{
    public function run(): void
    {
        $r4 = 0; // seti 0 8 4
        while (true) {
            $r3 = $r4 | 65536; // bori 4 65536 3
            $r4 = 14464005; // seti 14464005 5 4
            while (true) {
                $r2 = $r3 & 255; // bani 3 255 2
                $r4 = $r4 + $r2; // addr 4 2 4
                $r4 = $r4 & 16777215; // bani 4 16777215 4
                $r4 = $r4 * 65899; // muli 4 65899 4
                $r4 = $r4 & 16777215; // bani 4 16777215 4
                if ($r3 >= 256) {
                    $r2 = 0; // seti 0 3 2
                    while (true) {
                        $r1 = $r2 + 1; // addi 2 1 1
                        $r1 = $r1 * 256; // muli 1 256 1
                        $r1 = ($r1 > $r3) ? 1 : 0; // gtrr 1 3 1

                        if ($r1 === 0) {
                            $r2 = $r2 + 1; // addi 2 1 2
                        } else {
                            break;
                        }
                    }

                    $r3 = $r2; // setr 2 2 3
                } else {
                    break;
                }
            }

            printf("r0: %d\n", $r4);
            return;
        }
    }

    public function runB(): void
    {
        $cnt = 0;
        $keys = [];
        $last = 0;
        $r4 = 0; // seti 0 8 4
i5:
        $r3 = $r4 | 65536; // bit 16 set
        $r4 = 14464005; // seti 14464005 5 4
i7:
        $r2 = $r3 & 255; // keep only 8 lowest bits
        $r4 = $r4 + $r2; // addr 4 2 4
        $r4 = $r4 & 16777215; // keep only 24 lowest bits
        $r4 = $r4 * 65899; // muli 4 65899 4
        $r4 = $r4 & 16777215; // keep only 24 lowest bits
        if (256 > $r3) goto i27; // gtir 256 3 2
        $r2 = 0; // seti 0 3 2
i17:
        $r1 = $r2 + 1; // addi 2 1 1
        $r1 = $r1 * 256; // muli 1 256 1
        if ($r1 > $r3) goto i25;

        $r2 = $r2 + 1; // addi 2 1 2
        goto i17; // seti 17 9 5
i25:
        $r3 = $r2; // setr 2 2 3
        goto i7; // seti 7 3 5
i27:
        if (!isset($keys[$r4])) {
            $last = $r4;
        }
        $keys[$r4] = true;
        $cnt++;
        if ($cnt % 10000 === 0) {
            printf("r0: %d\n", $last);
        }
        //$r2 = ($r4 === $r0) ? 1 : 0; // eqrr 4 0 2
        //if ($r2 === 1) goto end;
        goto i5; // seti 5 9 5

        // 2525543 is too low
    }
}
