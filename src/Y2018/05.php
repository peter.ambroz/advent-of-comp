<?php
$q = file_get_contents('05.txt');

$rules = [
'aA', 'Aa', 'bB', 'Bb', 'cC', 'Cc', 'dD', 'Dd',
'eE', 'Ee', 'fF', 'Ff', 'gG', 'Gg', 'hH', 'Hh',
'iI', 'Ii', 'jJ', 'Jj', 'kK', 'Kk', 'lL', 'Ll',
'mM', 'Mm', 'nN', 'Nn', 'oO', 'Oo', 'pP', 'Pp',
'qQ', 'Qq', 'rR', 'Rr', 'sS', 'Ss', 'tT', 'Tt',
'uU', 'Uu', 'vV', 'Vv', 'wW', 'Ww', 'xX', 'Xx',
'yY', 'Yy', 'zZ', 'Zz',
];

$min = 50000;

foreach ($rules as $rem) {
    $s = str_replace(str_split($rem), '', $q);
    $oldlen = -1;
    $newlen = strlen($s);

    while ($oldlen != $newlen) {
        $oldlen = $newlen;
        $s = str_replace($rules, '', $s);
        $newlen = strlen($s);
    }

    
    if ($newlen < $min) $min = $newlen;
}

echo $min."\n";
