<?php
$fwdeps = ['O' => []];
$revdeps = ['L' => [], 'N' => [], 'R' => [], 'T' => []];

foreach (explode("\n", file_get_contents('07.txt')) as $line) {
    if (!strlen(trim($line))) continue;
    $before = $line[5];
    $after = $line[36];
    if (!isset($fwdeps[$before])) $fwdeps[$before] = [];
    $fwdeps[$before][] = $after;
    if (!isset($revdeps[$after])) $revdeps[$after] = [];
    $revdeps[$after][] = $before;
}

ksort($revdeps);
ksort($fwdeps);

$ready = [
'A' => 0,
'B' => 0,
'C' => 0,
'D' => 0,
'E' => 0,
'F' => 0,
'G' => 0,
'H' => 0,
'I' => 0,
'J' => 0,
'K' => 0,
'L' => 1,
'M' => 0,
'N' => 1,
'O' => 0,
'P' => 0,
'Q' => 0,
'R' => 1,
'S' => 0,
'T' => 1,
'U' => 0,
'V' => 0,
'W' => 0,
'X' => 0,
'Y' => 0,
'Z' => 0,
];

$ready2 = $ready;

//var_dump($fwdeps);
//var_dump($revdeps);


$order = '';
while (strlen($order) < 26) {
foreach ($ready as $key => $val) {
    if ($val === 1) {
        $order .= $key;
        $ready[$key] = 2;
        foreach ($ready as $letter => $dummy) {
            $sat = true;
            foreach ($revdeps[$letter] as $rev) {
                if ($ready[$rev] !== 2) {
                    $sat = false;
                    break;
                }
            }
            if ($sat) {
                if (!$ready[$letter]) {
                    $ready[$letter] = 1;
                    break;
                }
            }
        }
        break;
    }
}
}

echo $order."\n";

$sec = -1;
$workers = array_fill(0, 5, ['sec' => 0, 'ch' => '']);
$order = '';

while (strlen($order) < 26) {
$sec++;
foreach ($workers as $wid => &$w) {
    if ($w['sec'] <= $sec && $w['ch'] !== '') {
        $order .= $w['ch'];
        $ready2[$w['ch']] = 3;
        printf("T%04d W %d done with %s\n", $sec, $wid, $w['ch']);
        $w['ch'] = '';
    }
}
unset($w);

foreach ($ready2 as $letter => $dummy) {
    $sat = true;
    foreach ($revdeps[$letter] as $rev) {
        if ($ready2[$rev] !== 3) {
            $sat = false;
            break;
        }
    }
    if ($sat) {
        if (!$ready2[$letter]) {
            $ready2[$letter] = 1;
        }
    }
}


foreach ($ready2 as $key => $val) {
    if ($val === 1) {
        $took = false;
        foreach ($workers as $wid => $w) {
            if ($w['ch'] === '') {
                $ready2[$key] = 2;
                $workers[$wid] = ['sec' => ord($key) - 4 + $sec, 'ch' => $key];
                printf("T%04d W %d takes %s until %d\n", $sec, $wid, $key, $workers[$wid]['sec']);
                $took = true;
                break;
            }
        }
        if (!$took) {
            break;
        }
    }
}

}

printf("Worked for %d seconds\n", $sec); 
