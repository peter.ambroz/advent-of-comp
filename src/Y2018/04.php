<?php
$log = [];
foreach(explode("\n", file_get_contents('04.txt')) as $l) {
    $l = trim($l);
    if (!strlen($l)) continue;
    
    preg_match('/^\[1518-(?<m>\d+)-(?<d>\d+) (?<h>\d+):(?<i>\d+)\] (?:Guard #(?<gid>\d+) begins shift|wakes (?<up>up)|falls (?<down>asleep))$/', $l, $m);
    
    $key = $m['m'].$m['d'].$m['h'].$m['i'];
    if (!empty($m['gid'])) {
        $log[$key] = (int)$m['gid'];
    } elseif (!empty($m['up'])) {
        $log[$key] = 'up';
    } elseif (!empty($m['down'])) {
        $log[$key] = 'down';
    } else {
        throw new Exception('wtf? '.$l);
    }
}
ksort($log);
$a = [];
$last = -1;
foreach($log as $k => $v) {
    switch($v) {
        case 'down':
            $a[$last]['ss'] = (int)substr($k, 6, 2);
            break;
        case 'up':
            $start = $a[$last]['ss'];
            $stop = (int)substr($k, 6, 2);
            $a[$last]['s'] += $stop - $start;
            for ($i = $start; $i < $stop; $i++) {
                $a[$last]['sm'][$i]++;
            }
            break;
        default:
            $last = $v;
            if (!isset($a[$v])) $a[$v] = ['s' => 0, 'ss' => -1, 'sm' => array_fill(0, 60, 0)];
    }
}

$gid = -1;
$max = 0;
foreach ($a as $g => $v) {
    if ($v['s'] > $max) {
        $max = $v['s'];
        $gid = $g;
    }
}

$min = -1;
$max = 0;
foreach ($a[$gid]['sm'] as $m => $c) {
    if ($c > $max) {
        $max = $c;
        $min = $m;
    }
}

printf("g %d m %d = %d\n", $gid, $min, $gid * $min);

$min = -1;
$max = 0;
foreach ($a as $g => $v) {
    foreach ($v['sm'] as $m => $c) {
        if ($c > $max) {
            $max = $c;
            $min = $m;
            $gid = $g;
        }
    }
}

printf("g %d m %d = %d\n", $gid, $min, $gid * $min);
