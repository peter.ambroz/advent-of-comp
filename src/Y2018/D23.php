<?php

declare(strict_types=1);

namespace App\Y2018;

use App\Day;
use App\Y2018\Model\Bot;
use App\Y2018\Model\Point3D;

class D23 extends Day
{
    /** @var Bot[] */
    private array $bots;
    private Bot $min;
    private Bot $max;

    public function run(): void
    {
        $this->parse();
        $maxBot = new Bot(new Point3D(0, 0, 0), 0);

        foreach ($this->bots as $bot) {
            if ($bot->radius > $maxBot->radius) {
                $maxBot = $bot;
            }
        }

        $cnt = 0;
        foreach ($this->bots as $bot) {
            if ($maxBot->sees($bot->position)) {
                $cnt++;
            }
        }

        printf("in range: %d\n", $cnt);
    }

    public function runB(): void
    {
        $this->parse();
        $maxX = max(array_map(fn (Bot $b) => $b->position->x, $this->bots));
        $maxY = max(array_map(fn (Bot $b) => $b->position->y, $this->bots));
        $maxZ = max(array_map(fn (Bot $b) => $b->position->z, $this->bots));
        $minX = min(array_map(fn (Bot $b) => $b->position->x, $this->bots));
        $minY = min(array_map(fn (Bot $b) => $b->position->y, $this->bots));
        $minZ = min(array_map(fn (Bot $b) => $b->position->z, $this->bots));

        $maxR = max(array_map(fn (Bot $b) => $b->radius, $this->bots));
        $minR = min(array_map(fn (Bot $b) => $b->radius, $this->bots));

        $this->min = new Bot(new Point3D($minX, $minY, $minZ), $minR);
        $this->max = new Bot(new Point3D($maxX, $maxY, $maxZ), $maxR);

        $scale = 100000000;
        $min = $this->min->position;
        $max = $this->max->position;
        while ($scale >= 1) {
            $b = $this->findBestPlace($min, $max, $scale);
            printf("%d: %s\n", $scale, $b);
            $scale = intdiv($scale, 10);
            $min = new Point3D($b->x - 7*$scale, $b->y - 7*$scale, $b->z - 7*$scale);
            $max = new Point3D($b->x + 7*$scale, $b->y + 7*$scale, $b->z + 7*$scale);
        }

        printf("%d\n", $b->abs());
    }

    private function findBestPlace(Point3D $min, Point3D $max, int $scale): Point3D
    {
        $bestP = null;
        $bestC = 0;
        for ($z = $min->z; $z <= $max->z; $z+=$scale) {
            for ($y = $min->y; $y <= $max->y; $y+=$scale) {
                for ($x = $min->x; $x <= $max->x; $x+=$scale) {
                    $p = new Point3D($x, $y, $z);
                    $c = 0;
                    foreach ($this->bots as $bot) {
                        if ($bot->sees($p)) {
                            $c++;
                        }
                    }

                    if ($c > $bestC || ($c === $bestC && ($bestP === null || $p->abs() < $bestP->abs()))) {
                        $bestP = $p;
                        $bestC = $c;
                    }
                }
            }
        }

        return $bestP;
    }

    private function parse(): void
    {
        $this->bots = $this->inputLines(
            function (string $line): Bot {
                [, $rest] = explode('<', $line);
                [$point, $radius] = explode('>, r=', $rest);
                [$x, $y, $z] = explode(',', $point);

                return new Bot(new Point3D((int) $x, (int) $y, (int) $z), (int) $radius);
            },
        );
    }
}
