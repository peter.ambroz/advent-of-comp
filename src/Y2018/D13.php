<?php

declare(strict_types=1);

namespace App\Y2018;

use App\Day;
use App\Y2018\Model\Car;

class D13 extends Day
{
    /** @var string[] */
    private array $map;
    /** @var Car[] */
    private array $cars;

    private function readInput(): void
    {
        $carCount = 0;
        $y = 0;
        foreach (explode("\n", file_get_contents($this->inputFileName())) as $line) {
            $lineData = [];
            $x = 0;
            foreach (str_split($line) as $char) {
                switch ($char) {
                    case ' ':
                    case '/':
                    case '\\':
                    case '-':
                    case '|':
                    case '+':
                        $lineData[] = $char;
                        break;
                    case '>':
                    case '<':
                        $lineData[] = '-';
                        $carCount++;
                        $this->cars[] = new Car($carCount, $x, $y, $char);
                        break;
                    case '^':
                    case 'v':
                        $lineData[] = '|';
                        $carCount++;
                        $this->cars[] = new Car($carCount, $x, $y, $char);
                        break;
                }
                $x++;
            }
            $this->map[] = $lineData;
            $y++;
        }
    }

    private function hasCollided(Car $car): bool
    {
        foreach ($this->cars as $car2) {
            if ($car->sameAs($car2)) {
                continue;
            }

            if ($car->collideWith($car2)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return Car[]
     */
    private function tick(bool $continue = false): array
    {
        $collisions = [];
        $sortedCars = $this->sortCars();
        foreach ($sortedCars as $car) {
            if ($this->hasCollided($car)) {
                continue;
            }

            $car->ride($this->map[$car->y][$car->x]);
            if ($this->hasCollided($car)) {
                if (!$continue) {
                    $collisions[] = $car;
                    return $collisions;
                }
            }
        }

        foreach ($sortedCars as $car) {
            if ($this->hasCollided($car)) {
                $collisions[] = $car;
            }
        }

        return $collisions;
    }

    /**
     * @return Car[]
     */
    private function sortCars(): array
    {
        $cars = $this->cars;

        usort($cars, function (Car $c1, Car $c2): int {
            if ($c1->y > $c2->y) {
                return 1;
            } elseif ($c1->y < $c2->y) {
                return -1;
            } elseif ($c1->x > $c2->x) {
                return 1;
            } elseif ($c1->x < $c2->x) {
                return -1;
            } else {
                return 0;
            }
        });

        return $cars;
    }

    public function run(): void
    {
        $this->readInput();

        $ticks = 0;
        do {
            $collidedCars = $this->tick();
            $ticks++;
        } while (empty($collidedCars));

        printf("Collision on tick no.%d @ %d,%d\n", $ticks, $collidedCars[0]->x, $collidedCars[0]->y);
    }

    public function runB(): void
    {
        $this->readInput();
        $ticks = 0;
        do {
            $collidedCars = $this->tick(true);
            foreach ($collidedCars as $car) {
                for ($i = count($this->cars) - 1; $i >= 0; $i--) {
                    if ($car->sameAs($this->cars[$i])) {
                        array_splice($this->cars, $i, 1);
                    }
                }
            }
            $ticks++;
        } while (count($this->cars) > 1);

        printf("Last car in tick no.%d @ %d,%d dir %s\n", $ticks, $this->cars[0]->x, $this->cars[0]->y, $this->cars[0]->dir);
    }
}
