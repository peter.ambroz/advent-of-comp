<?php
declare(strict_types=1);

namespace App\Y2018;

use App\Day;

class D14 extends Day
{
    private string $r = '37';
    private int $e0 = 0;
    private int $e1 = 1;
    private int $len = 2;

    private function step(): string
    {
        $r0 = (int) $this->r[$this->e0];
        $r1 = (int) $this->r[$this->e1];

        $sm = $r0 + $r1;
        $this->r .= $sm;
        $this->len += ($sm >= 10) ? 2 : 1;

        $this->e0 = ($this->e0 + $r0 + 1) % $this->len;
        $this->e1 = ($this->e1 + $r1 + 1) % $this->len;

        return (string) $sm;
    }

    public function run(): void
    {
        $lookFor = 939601 + 10;
        do {
            $this->step();
        } while ($this->len < $lookFor);

        printf("last 10: %s\n", substr($this->r, -10, 10));
    }

    public function runB(): void
    {
        $lookFor = ['9', '3', '9', '6', '0', '1'];
        $ptr = 0;
        $buf = '';
        while (true) {
            $buf .= $this->step();
            $bufc = strlen($buf);

            while ($ptr < min($bufc, 6)) {
                if ($lookFor[$ptr] === $buf[$ptr]) {
                    $ptr++;
                } else {
                    $buf = substr($buf, 1);
                    $bufc--;
                    $ptr = 0;
                }
            }
            if ($ptr === 6) {
                $recs = $this->len - strlen($buf);
                break;
            }
        }

        printf("Recipes so far: %s\n", $recs);
    }
}
