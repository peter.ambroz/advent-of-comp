<?php
$a = [];
$b = array_fill(0,1268, 1);
$b[0] = 0;
foreach(explode("\n", file_get_contents('03.txt')) as $l) {
    $l = trim($l);
    if (!strlen($l)) continue;
   
    preg_match('/^#(?<id>\d+) @ (?<x>\d+),(?<y>\d+): (?<w>\d+)x(?<h>\d+)$/', $l, $m);
    for ($j = $m['y']; $j < $m['y'] + $m['h']; $j++) {
        for ($i = $m['x']; $i < $m['x'] + $m['w']; $i++) {
            $id = (int)$m['id'];
            $idx = $j.':'.$i;
            if (!isset($a[$idx])) $a[$idx] = [$id];
            else {
                $a[$idx][] = $id;
                foreach ($a[$idx] as $oid) {
                    $b[$oid] = 0;
                }
            }
        }
    }
}

$cnt = 0;
foreach($a as $i) {
    if (count($i) > 1) $cnt++;
}

echo $cnt."\n";

foreach($b as $q=>$c) {
    if ($c != 0) echo $q."\n";
}
