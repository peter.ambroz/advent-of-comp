<?php

declare(strict_types=1);

namespace App\Y2018\Model;

final readonly class Bandits
{
    /**
     * @param Enemy[] $group
     */
    public function __construct(public array $group)
    {
    }

    public function totalHp(): int
    {
        return array_sum(array_map(fn (Enemy $e) => $e->getHp(), $this->group));
    }

    /**
     * @return Enemy[]
     */
    public function aliveOpponentsTo(Enemy $e): array
    {
        return array_filter($this->group, fn (Enemy $x) => !$x->isDead() && $x->type === $e->type->opponent());
    }

    /**
     * @return array<string, Enemy>
     */
    public function sorted(): array
    {
        $bandits = array_filter($this->group, fn (Enemy $e) => !$e->isDead());
        usort($bandits, function (Enemy $a, Enemy $b) {
            $ap = $a->getLoc();
            $bp = $b->getLoc();
            if ($ap->y === $bp->y) {
                return $ap->x <=> $bp->x;
            } else {
                return $ap->y <=> $bp->y;
            }
        });

        return array_reduce($bandits, function (array $c, Enemy $e) {
            $c[(string) $e->getLoc()] = $e;
            return $c;
        }, []);
    }

    public function attack(Enemy $e): ?Enemy
    {
        $defender = null;
        foreach ($this->aliveOpponentsTo($e) as $opponent) {
            if ($e->getLoc()->distance($opponent->getLoc()) !== 1) {
                continue;
            }
            if ($defender === null || $defender->getHp() > $opponent->getHp()) {
                $defender = $opponent;
            } elseif ($defender->getHp() === $opponent->getHp() && $opponent->getLoc()->appearsBefore($defender->getLoc())) {
                $defender = $opponent;
            }
        }

        $defender?->attackBy($e);
        return $defender;
    }
}
