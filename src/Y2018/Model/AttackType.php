<?php

declare(strict_types=1);

namespace App\Y2018\Model;

enum AttackType: string
{
    case SLASHING = 'slashing';
    case RADIATION = 'radiation';
    case FIRE = 'fire';
    case BLUDGEONING = 'bludgeoning';
    case COLD = 'cold';
}
