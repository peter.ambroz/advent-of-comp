<?php

declare(strict_types=1);

namespace App\Y2018\Model;

class Car
{
    private const X_LEFT = 0;
    private const X_STRAIGHT = 1;
    private const X_RIGHT = 2;

    private const D_UP = '^';
    private const D_DOWN = 'v';
    private const D_LEFT = '<';
    private const D_RIGHT = '>';

    private const R_FORWARD = '/';
    private const R_BACK = '\\';
    private const R_CROSS = '+';
    private const R_VERTICAL = '|';
    private const R_HORIZONTAL = '-';
    private const R_EMPTY = ' ';

    public int $id;
    public int $x;
    public int $y;
    public string $dir;

    private int $dx = 0;
    private int $dy = 0;

    private int $nextCross = self::X_LEFT;

    private array $rotationMap = [
        self::X_LEFT => [
            self::D_UP => self::D_LEFT,
            self::D_RIGHT => self::D_UP,
            self::D_DOWN => self::D_RIGHT,
            self::D_LEFT => self::D_DOWN,
        ],
        self::X_STRAIGHT => [
            self::D_UP => self::D_UP,
            self::D_RIGHT => self::D_RIGHT,
            self::D_DOWN => self::D_DOWN,
            self::D_LEFT => self::D_LEFT,
        ],
        self::X_RIGHT => [
            self::D_UP => self::D_RIGHT,
            self::D_RIGHT => self::D_DOWN,
            self::D_DOWN => self::D_LEFT,
            self::D_LEFT => self::D_UP,
        ],
    ];

    private array $mirrorMap = [
        self::R_FORWARD => [
            self::D_UP => self::D_RIGHT,
            self::D_RIGHT => self::D_UP,
            self::D_DOWN => self::D_LEFT,
            self::D_LEFT => self::D_DOWN,
        ],
        self::R_BACK => [
            self::D_UP => self::D_LEFT,
            self::D_RIGHT => self::D_DOWN,
            self::D_DOWN => self::D_RIGHT,
            self::D_LEFT => self::D_UP,
        ],
    ];

    public function __construct(int $id, int $x, int $y, string $dir)
    {
        $this->id = $id;
        $this->x = $x;
        $this->y = $y;
        $this->setDir($dir);
    }

    private function setDir(string $dir): void
    {
        switch ($dir) {
            case self::D_UP:
                $this->dx = 0;
                $this->dy = -1;
                $this->dir = self::D_UP;
                break;
            case self::D_RIGHT:
                $this->dx = 1;
                $this->dy = 0;
                $this->dir = self::D_RIGHT;
                break;
            case self::D_DOWN:
                $this->dx = 0;
                $this->dy = 1;
                $this->dir = self::D_DOWN;
                break;
            case self::D_LEFT:
                $this->dx = -1;
                $this->dy = 0;
                $this->dir = self::D_LEFT;
                break;
            default:
                throw new \InvalidArgumentException('Wrong dir given: ' . $dir);
        }
    }

    public function ride(string $road): void
    {
        switch ($road) {
            case self::R_VERTICAL:
            case self::R_HORIZONTAL:
                break;
            case self::R_CROSS:
                $this->setDir($this->rotationMap[$this->nextCross][$this->dir]);
                $this->nextCross = ($this->nextCross + 1) % 3;
                break;
            case self::R_FORWARD:
            case self::R_BACK:
                $this->setDir($this->mirrorMap[$road][$this->dir]);
                break;
            case self::R_EMPTY:
            default:
                throw new \InvalidArgumentException('Bad road');
        }

        $this->x += $this->dx;
        $this->y += $this->dy;
    }

    public function collideWith(Car $car): bool
    {
        return ($this->x === $car->x && $this->y === $car->y);
    }

    public function sameAs(Car $car): bool
    {
        return ($this->id === $car->id);
    }
}
