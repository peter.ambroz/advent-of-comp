<?php

declare(strict_types=1);

namespace App\Y2018\Model;

final class UnitGroup
{
    public function __construct(
        private int $size,
        public readonly int $hp,
        /** @var AttackType[] */
        public readonly array $immune,
        /** @var AttackType[] */
        public readonly array $weak,
        private int $attack,
        public readonly AttackType $attackType,
        public readonly int $initiative,
    ) {
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function getPower(): int
    {
        return $this->size * $this->attack;
    }

    public function boostAttack(int $boost): void
    {
        $this->attack += $boost;
    }

    public function considerDamageBy(self $attacker): int
    {
        $damage = $attacker->getPower();
        if (in_array($attacker->attackType, $this->immune)) {
            $damage = 0;
        } elseif (in_array($attacker->attackType, $this->weak)) {
            $damage *= 2;
        }

        return $damage;
    }

    public function attackBy(self $attacker): bool
    {
        $damage = $this->considerDamageBy($attacker);

        $kills = intdiv($damage, $this->hp);
        $this->size = max($this->size - $kills, 0);

        return $this->size === 0;
    }
}
