<?php

declare(strict_types=1);

namespace App\Y2018\Model;

final readonly class Army
{
    public function __construct(
        public string $name,
        /** @var UnitGroup[] */
        public array $groups,
    ) {
    }

    public function getSize(): int
    {
        return array_sum(array_map(fn (UnitGroup $g) => $g->getSize(), $this->groups));
    }

    public function isDefeated(): bool
    {
        return $this->getSize() === 0;
    }

    public function boostAttack(int $boost): void
    {
        foreach ($this->groups as $group) {
            $group->boostAttack($boost);
        }
    }

    /**
     * @return UnitGroup[][]
     */
    public function select(self $otherArmy): array
    {
        $groups = array_filter($this->groups, fn (UnitGroup $x) => $x->getSize() > 0);
        $defendGroups = array_filter($otherArmy->groups, fn (UnitGroup $x) => $x->getSize() > 0);

        usort($groups, self::groupSort(...));
        usort($defendGroups, self::groupSort(...));

        $selection = [];

        foreach ($groups as $attacker) {
            $maxDamage = 0;
            $defenderId = null;
            foreach ($defendGroups as $di => $defender) {
                $damage = $defender->considerDamageBy($attacker);
                if ($damage > $maxDamage) {
                    $maxDamage = $damage;
                    $defenderId = $di;
                }
            }

            if ($defenderId !== null) {
                $selection[] = [$attacker, $defendGroups[$defenderId]];
                unset($defendGroups[$defenderId]);
            }
        }

        return $selection;
    }

    private static function groupSort(UnitGroup $a, UnitGroup $b): int
    {
        $apower = $a->getPower();
        $bpower = $b->getPower();
        if ($apower === $bpower) {
            return $b->initiative <=> $a->initiative;
        } else {
            return $bpower <=> $apower;
        }
    }
}
