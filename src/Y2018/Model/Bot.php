<?php

declare(strict_types=1);

namespace App\Y2018\Model;

readonly class Bot
{
    public function __construct(public Point3D $position, public int $radius)
    {
    }

    public function sees(Point3D $otherBot): bool
    {
        return $this->position->distance($otherBot) <= $this->radius;
    }

    public function __toString(): string
    {
        return sprintf("%s;R%d", $this->position, $this->radius);
    }
}
