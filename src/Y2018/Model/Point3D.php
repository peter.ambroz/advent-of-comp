<?php

declare(strict_types=1);

namespace App\Y2018\Model;

readonly class Point3D
{
    public function __construct(public int $x, public int $y, public int $z)
    {
    }

    public function sub(self $point): self
    {
        return new self($this->x - $point->x, $this->y - $point->y, $this->z - $point->z);
    }

    public function add(self $point): self
    {
        return new self($this->x + $point->x, $this->y + $point->y, $this->z + $point->z);
    }

    public function distance(self $point): int
    {
        $diff = $this->sub($point);

        return abs($diff->x) + abs($diff->y) + abs($diff->z);
    }

    public function abs(): int
    {
        return abs($this->x) + abs($this->y) + abs($this->z);
    }

    public function __toString(): string
    {
        return sprintf('%d;%d;%d', $this->x, $this->y, $this->z);
    }
}
