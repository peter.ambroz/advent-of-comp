<?php

declare(strict_types=1);

namespace App\Y2018\Model;

enum EnemyType: string
{
    case ELF = 'E';
    case GOBLIN = 'G';

    public function opponent(): self
    {
        return match ($this) {
            self::ELF => self::GOBLIN,
            self::GOBLIN => self::ELF,
        };
    }
}
