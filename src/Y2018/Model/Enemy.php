<?php
declare(strict_types=1);

namespace App\Y2018\Model;

use App\Model\Point;

final class Enemy
{
    public function __construct(
        public readonly EnemyType $type,
        public readonly int $strength,
        private int $hp,
        private Point $loc,
    ) {
    }

    public function attackBy(Enemy $e): void
    {
        $this->hp = max($this->hp - $e->strength, 0);
    }

    public function move(Point $newLoc): void
    {
        $this->loc = $newLoc;
    }

    public function getLoc(): Point
    {
        return $this->loc;
    }

    public function isDead(): bool
    {
        return $this->hp < 1;
    }

    public function getHp(): int
    {
        return $this->hp;
    }

    public function __toString(): string
    {
        return sprintf("%s H%d L%s", $this->type->value, $this->hp, $this->loc);
    }
}
