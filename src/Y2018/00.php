<?php

abstract class LineHandler
{
    public function handle(string $line): void;
}

class AOC
{
    private $filename;

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }
    
    public function handleLines(LineHandler $lineHandler)
    {
        foreach(explode("\n", file_get_contents($this->filename . '.txt')) as $l) {
            $lineHandler->handle($l);
        }
    }
}

?>
