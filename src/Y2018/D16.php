<?php

declare(strict_types=1);

namespace App\Y2018;

use App\Day;

class D16 extends Day
{
    private Opcode $o;

    private const OPCODES = [
        'addr',
        'addi',
        'mulr',
        'muli',
        'banr',
        'bani',
        'borr',
        'bori',
        'setr',
        'seti',
        'gtir',
        'gtri',
        'gtrr',
        'eqir',
        'eqri',
        'eqrr',
    ];

    private const OPMAP = [
        0 => 'bani',
        1 => 'addr',
        2 => 'mulr',
        3 => 'addi',
        4 => 'gtri',
        5 => 'banr',
        6 => 'borr',
        7 => 'eqri',
        8 => 'seti',
        9 => 'eqrr',
        10 => 'bori',
        11 => 'setr',
        12 => 'eqir',
        13 => 'muli',
        14 => 'gtrr',
        15 => 'gtir',
    ];

    public function run(): void
    {
        $this->o = new Opcode();

        $before = [0, 0, 0, 0];
        $code = [0, 0, 0, 0];
        $matched3 = 0;
        $possible = array_fill(0, 16, []);

        foreach ($this->inputLines() as $lineNo => $line) {
            if ($lineNo >= 3189) {
                break;
            }

            if (!strlen($line)) {
                continue;
            }

            if (str_starts_with($line, 'Before')) {
                $before = json_decode(substr($line, 8));
                if ($before === null) {
                    printf("null @%d:%s\n", $lineNo, $line);
                }
                continue;
            }

            if (str_starts_with($line, 'After')) {
                $after = json_decode(substr($line, 8));
                $matched = $this->tryOut($before, $after, $code);

                $possible[$code[0]] = array_unique(array_merge($possible[$code[0]], $matched));

                if (count($matched) >= 3) {
                    $matched3++;
                }

                continue;
            }

            $code = array_map(fn ($x) => (int) $x, explode(' ', $line));
        }

        printf("%d\n", $matched3);

        while (count($possible)) {
            foreach ($possible as $opcode => $opValues) {
                if (count($opValues) === 1) {
                    $opValues = array_values($opValues);
                    printf("%d => '%s',\n", $opcode, $opValues[0]);
                    foreach ($possible as $opcode2 => &$opValues2) {
                        if ($opcode === $opcode2) {
                            continue;
                        }

                        $opValues2 = array_diff($opValues2, [$opValues[0]]);
                    }
                    unset($opValues2);
                    unset($possible[$opcode]);
                    break;
                }
            }
        }
    }

    public function runB(): void
    {
        $o = new Opcode();

        foreach ($this->inputLines() as $lineNo => $line) {
            if ($lineNo < 3190) {
                continue;
            }

            $code = array_map(fn ($x) => (int) $x, explode(' ', $line));
            $o->exec(self::OPMAP[$code[0]], $code[1], $code[2], $code[3]);
        }

        $reg = $o->getReg();
        printf("%d, %d, %d, %d\n", $reg[0], $reg[1], $reg[2], $reg[3]);
    }

    private function tryOut(array $before, array $after, array $code): array
    {
        $matched = [];

        foreach (self::OPCODES as $opcode) {
            $this->o->setReg($before);
            $this->o->exec($opcode, $code[1], $code[2], $code[3]);
            if ($this->o->getReg() === $after) {
                $matched[] = $opcode;
            }
        }

        return $matched;
    }
}
