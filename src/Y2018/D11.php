<?php
declare(strict_types=1);

namespace App\Y2018;

use App\Day;

class D11 extends Day
{
    private const S = 4842;

    /** @var int[] */
    private array $fuel;

    private function level(int $x, int $y): int
    {
        $r = $x + 10;
        $p = ($r * $y + self::S) * $r;
        $px = substr((string)$p, -3, 1);
        return (int)$px - 5;
    }

    private function sum(int $x, int $y, int $w = 3): int
    {
        $s = 0;
        for ($j = 0; $j < $w; $j++) {
            for ($i = 0; $i < $w; $i++) {
                $s += $this->fuel[$y + $j][$x + $i];
            }
        }
        return $s;
    }

    public function run(): void
    {
        for ($y = 0; $y <= 300; $y++) {
            $line = [];
            for ($x = 0; $x <= 300; $x++) {
                $line[$x] = $this->level($x, $y);
            }
            $this->fuel[$y] = $line;
        }

        $max = -100;
        $maxx = 0;
        $maxy = 0;
        $maxw = 0;
        for ($w = 2; $w <= 300; $w++) {
            echo $w."\n";
            for ($y = 1; $y <= 301-$w; $y++) {
                for ($x = 1; $x <= 301-$w; $x++) {
                    $s = $this->sum($x, $y, $w);
                    if ($s > $max) {
                        $max = $s;
                        $maxx = $x;
                        $maxy = $y;
                        $maxw = $w;
                    }
                }
            }
        }

        printf("Max %d @ %d,%d,%d\n", $max, $maxx, $maxy, $maxw);
    }

    public function runB(): void
    {
        parent::runB();
    }
}
