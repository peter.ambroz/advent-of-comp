<?php
function step()
{
  global $state;
  global $rulez;
  global $min;
  global $max;
  $newstate = $state;
  
  for ($i = $min + 2; $i <= $max - 2; $i++) {
    $val = $state[$i-2]*16 + $state[$i-1]*8 + $state[$i]*4 + $state[$i+1]*2 + $state[$i+2];
    $newstate[$i] = $rulez[$val];
  }
  
  $state = $newstate;
}

function summ()
{
  global $state;
  $s = 0;
  
  foreach ($state as $k => $v) {
    if ($v) $s+=$k;
  }
  
  return $s;
}

function prn()
{
  global $state;
  foreach($state as $v) {
    echo $v;
  }
  echo "\n";
}

$init = '##..##....#.#.####........##.#.#####.##..#.#..#.#...##.#####.###.##...#....##....#..###.#...#.#.#.#';
$len = strlen($init);
$steps = $argv[1] ?? 0;
$state = [];
$min = -3;
$max = $len + $steps + 1;
$rulez = [
0 => 0,
1 => 0,
2 => 1,
3 => 0,
4 => 1,
5 => 1,
6 => 0,
7 => 1,
8 => 1,
9 => 0,
10 => 1,
11 => 0,
12 => 1,
13 => 1,
14 => 1,
15 => 0,
16 => 0,
17 => 0,
18 => 1,
19 => 0,
20 => 1,
21 => 0,
22 => 0,
23 => 0,
24 => 1,
25 => 0,
26 => 0,
27 => 0,
28 => 1,
29 => 1,
30 => 0,
31 => 1,
];

for ($i = $min; $i <= $max; $i++) {
  if (($i < 0) || ($i >= $len)) {
    $state[$i] = 0;
  } else {
    $state[$i] = (($init[$i] == '#') ? 1 : 0);
  }
}

for ($s = 0; $s < $steps; $s++) {
  step();
}
echo summ();
echo "\n";
prn();

