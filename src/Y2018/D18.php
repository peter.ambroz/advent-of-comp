<?php

declare(strict_types=1);

namespace App\Y2018;

use App\Day;

class D18 extends Day
{
    private array $land;
    private array $outputs = [];

    private function doRun(int $minutes): void
    {
        $this->land = $this->inputLines(fn ($l) => str_split($l));
        for ($m = 0; $m < $minutes; $m++) {
            $this->land = $this->step();
            $this->output();
        }
    }

    public function run(): void
    {
        $this->doRun(10);
        printf("%d\n", $this->outputs[count($this->outputs) - 1]);
    }

    public function runB(): void
    {
        $this->doRun(600);

        $istart = count($this->outputs) - 1;
        $check = $this->outputs[$istart];
        $i = $istart - 1;
        while ($check !== $this->outputs[$i]) {
            $i--;
        }
        $mod = $istart - $i + 2;

        printf("%d\n", $this->outputs[(1000000000 % $mod) + 559]);
    }

    private function step(): array
    {
        $newland = [];

        foreach ($this->land as $y => $line) {
            $newland[$y] = [];

            foreach ($line as $x => $acre) {
                $adj = $this->adjacent($x, $y);
                $newAcre = $acre;

                if ($acre === '.' && $adj['|'] >= 3) {
                    $newAcre = '|';
                } elseif ($acre === '|' && $adj['#'] >= 3) {
                    $newAcre = '#';
                } elseif ($acre === '#' && ($adj['#'] === 0 || $adj['|'] === 0)) {
                    $newAcre = '.';
                }

                $newland[$y][$x] = $newAcre;
            }
        }

        return $newland;
    }

    private function output(): void
    {
        $wood = 0;
        $lumber = 0;

        foreach ($this->land as $line) {
            foreach ($line as $acre) {
                if ($acre === '|') {
                    $wood++;
                } elseif ($acre === '#') {
                    $lumber++;
                }
            }
        }

        $this->outputs[] = $wood * $lumber;
    }

    private function adjacent(int $x, int $y): array
    {
        $data = [
            '.' => 0,
            '|' => 0,
            '#' => 0,
        ];

        if (!isset($this->land[$y][$x])) {
            return $data;
        }

        for ($j = $y - 1; $j <= $y + 1; $j++) {
            for ($i = $x - 1; $i <= $x + 1; $i++) {
                if ($j === $y && $i === $x) {
                    continue;
                }

                if (!isset($this->land[$j][$i])) {
                    continue;
                }

                $acre = $this->land[$j][$i];
                $data[$acre]++;
            }
        }

        return $data;
    }
}
