<?php

declare(strict_types=1);

namespace App\Y2018;

use App\Day;
use App\Model\Point;

class D22 extends Day
{
    private const DEPTH = 9171;
    private const TX = 7;
    private const TY = 721;
    private const GX_MUL = 16807;
    private const GY_MUL = 48271;
    private const DEPTH_MOD = 20183;
    private const ROCKY = 0;
    private const WET = 1;
    private const NARROW = 2;
    private const NO_GEAR = 0;
    private const CLIMB = 1;
    private const TORCH = 2;

    private const GEAR = [
        self::ROCKY => [
            self::ROCKY => null,
            self::WET => self::CLIMB,
            self::NARROW => self::TORCH,
        ],
        self::WET => [
            self::ROCKY => self::CLIMB,
            self::WET => null,
            self::NARROW => self::NO_GEAR,
        ],
        self::NARROW => [
            self::ROCKY => self::TORCH,
            self::WET => self::NO_GEAR,
            self::NARROW => null,
        ],
    ];

    private array $ycache = [];
    private int $xcache = 0;

    private array $map = [];
    private array $dist = [];

    public function run(): void
    {
        $this->populate(self::TX, self::TY);
        $sum = 0;
        foreach ($this->map as $type) {
            $sum += $type;
        }

        printf("risk %d\n", $sum);
    }

    public function runB(): void
    {
        $this->populate(self::TX * 3, self::TY * 3);

        $start = new Point(0, 0);
        $end = new Point(self::TX, self::TY);
        $this->find($start);

        printf("time %d\n", $this->dist[(string) $end]);
    }

    private function find(Point $p, int $d = 0, int $e = self::TORCH): void
    {
        if ($d >= 1100) {
            return;
        }
        if ($p->x === self::TX && $p->y === self::TY) {
            if ($e !== self::TORCH) {
                $d += 7;
            }
            $this->dist[(string) $p] = $d;

            return;
        }

        $this->dist[(string) $p] = $d;
        $cur = $this->map[(string) $p];
        foreach ($p->neighbors() as $n) {
//            foreach (self::GEAR[$cur] as $nextType => $nextGear) {
                if ($n->x < 0 || $n->y < 0 || $n->x >= self::TX * 3 || $n->y >= self::TY * 3) {
                    continue;
                }

                $new = $this->map[(string)$n];
                $nd = $this->dist[(string)$n] ?? null;
                $try = $d + 1;
                $equip = self::GEAR[$cur][$new] ?? $e;
                if ($equip !== $e) {
                    $try += 7;
                }
                if ($nd !== null && $nd <= $try) {
                    continue;
                }

                $this->find($n, $try, $equip);
  //          }
        }
    }

    private function populate(int $xMax, int $yMax): void
    {
        for ($x = 0; $x <= $xMax; $x++) {
            for ($y = 0; $y <= $yMax; $y++) {
                $p = new Point($x, $y);
                $this->map[(string) $p] = $this->geoIndex($p) % 3;
            }
        }
    }

    private function geoIndex(Point $p): int
    {
        if ($p->x === 0 && $p->y === 0) {
            return self::DEPTH;
        }

        if ($p->x === self::TX && $p->y === self::TY) {
            return $this->ycache[$p->y] = $this->xcache = self::DEPTH;
        }

        if ($p->x === 0) {
            $ret = ($p->y * self::GY_MUL + self::DEPTH) % self::DEPTH_MOD;
            $this->ycache[$p->y] = $ret;

            return $ret;
        }

        if ($p->y === 0) {
            $ret = ($p->x * self::GX_MUL + self::DEPTH) % self::DEPTH_MOD;
            $this->ycache[0] = $this->xcache = $ret;

            return $ret;
        }

        $ret = ($this->ycache[$p->y] * $this->xcache + self::DEPTH) % self::DEPTH_MOD;
        $this->ycache[$p->y] = $this->xcache = $ret;

        return $ret;
    }
}
