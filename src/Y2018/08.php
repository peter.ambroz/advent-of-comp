<?php
function addNode(array $data): array
{
    if (count($data) < 3) {
        throw new Exception("pici " . implode(',', $data));
    }
    
    $childCount = $data[0];
    $mdCount = $data[1];
    
    $data = array_slice($data, 2);
    
    $node = [
        'ch' => [],
        'm' => [],
        'size' => $mdCount + 2,
    ];
    
    if ($childCount > 0) {
        for ($i = 0; $i < $childCount; $i++) {
            $subNode = addNode($data);
            $node['ch'][] = $subNode;
            $node['size'] += $subNode['size'];
            $data = array_slice($data, $subNode['size']);
        }
    }

    $node['m'] = array_slice($data, 0, $mdCount);
//    var_dump($node);
    return $node;
}

function sumMeta(array $tree): int
{
    $sum = array_sum($tree['m']);
    foreach ($tree['ch'] as $subnode) {
        $sum += sumMeta($subnode);
    }
    return $sum;
}

function value(array $tree): int
{
    if (!count($tree['ch'])) return array_sum($tree['m']);
    $sum = 0;
    foreach ($tree['m'] as $meta) {
        $idx = $meta-1;
        if (isset($tree['ch'][$idx])) {
            $sum += value($tree['ch'][$idx]);
        }
    }
    return $sum;
}

$data = array_map(function($x) {return (int)$x;}, explode(" ", trim(file_get_contents('08.txt'))));
//$data = [2, 3, 0, 3, 10, 11, 12, 1, 1, 0, 1, 99, 2, 1, 1, 2];
$tree = addNode($data);

echo sumMeta($tree) . "\n";

echo value($tree) . "\n";
