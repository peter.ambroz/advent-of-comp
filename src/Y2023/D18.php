<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;
use App\Model\Point;

class D18 extends Day
{
    private const DIRS = ['U' => 0, 'R' => 1, 'D' => 2, 'L' => 3];

    /** @var Point[] */
    private array $map = [];

    public function run(): void
    {
        $p = new Point(0, 0);
        foreach ($this->inputLines(function ($l) {
            [$dir, $num, ] = explode(' ', $l);
            return [self::DIRS[$dir], (int) $num];
        }) as [$dir, $num]) {
            for ($i = 0; $i < $num; $i++) {
                $p = $p->neighbors()[$dir];
                $this->map[(string) $p] = true;
            }
        }

        $this->fill(new Point (4, 1));
        printf("%d\n", count($this->map));
    }

    public function runB(): void
    {
        $y = 0;
        $s = 0;
        $l = 0;
        foreach ($this->inputLines(function ($line) {
            [, , $data] = explode(' ', $line);
            $num = hexdec(substr($data, 2, 5));
            $dir = (int) $data[7];
            return [$dir, $num];
        }) as [$dir, $num]) {
            $l += $num;
            switch ($dir) {
                case 0: // R
                    $s += $num * $y;
                    break;
                case 1: // D
                    $y -= $num;
                    break;
                case 2: // L
                    $s -= $num * $y;
                    break;
                case 3: // U
                    $y += $num;
                    break;
            }
        }
        printf("%d\n", $s + intdiv($l, 2) + 1);
    }

    private function fill(Point $p): void
    {
        if (isset($this->map[(string) $p])) {
            return;
        }

        $this->map[(string) $p] = true;
        foreach ($p->neighbors() as $n) {
            $this->fill($n);
        }
    }
}
