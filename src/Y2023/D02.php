<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;

class D02 extends Day
{
    public function run(): void
    {
        $goodIds = 0;
        $lim = ['red' => 12, 'green' => 13, 'blue' => 14];
        foreach ($this->inputLines() as $line) {
            [$idpart, $cubepart] = explode(': ', $line);
            [, $id] = explode(' ', $idpart);
            $shows = explode('; ', $cubepart);
            foreach ($shows as $show) {
                $dice = explode(', ', $show);
                foreach ($dice as $die) {
                    [$cnt, $color] = explode(' ', $die);

                    if ($lim[$color] < (int) $cnt) {
                        continue 3;
                    }
                }
            }

            $goodIds += (int) $id;
        }

        printf("%d\n", $goodIds);
    }

    public function runB(): void
    {
        $gameSets = 0;
        foreach ($this->inputLines() as $line) {
            $min = ['red' => 0, 'green' => 0, 'blue' => 0];
            [, $cubepart] = explode(': ', $line);
            $shows = explode('; ', $cubepart);
            foreach ($shows as $show) {
                $dice = explode(', ', $show);
                foreach ($dice as $die) {
                    [$cnt, $color] = explode(' ', $die);
                    $cnt = (int) $cnt;

                    if ($min[$color] < $cnt) {
                        $min[$color] = $cnt;
                    }
                }
            }

            $gameSets += $min['red'] * $min['green'] * $min['blue'];
        }

        printf("%d\n", $gameSets);
    }
}
