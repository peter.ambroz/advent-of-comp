<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;

class D04 extends Day
{
    private array $wins;
    private array $reps;
    private function doRun(): void
    {
        $this->wins = $this->inputLines(function ($line) {
            [$l, $r] = explode(' | ', $line);
            $win = explode(' ', $l);
            $all = explode(' ', $r);

            array_shift($win);
            array_shift($win);

            return count(array_intersect($win, $all));
        });

        $this->reps = array_fill(0, count($this->wins), 1);
    }

    public function run(): void
    {
        $this->doRun();

        $sum = 0;
        foreach ($this->wins as $win) {
            if ($win === 0) {
                continue;
            }

            $sum += 2 ** ($win - 1);
        }

        printf("won: %d\n", $sum);
    }

    public function runB(): void
    {
        $this->doRun();

        foreach ($this->wins as $id => $win) {
            for ($i = $id + 1; $i <= $id + $win; $i++) {
                $this->reps[$i] += $this->reps[$id];
            }
        }

        printf("cards: %d\n", array_sum($this->reps));
    }
}
