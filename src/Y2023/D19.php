<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;

use App\Y2023\Model\Block;
use App\Y2023\Model\Part19;
use App\Y2023\Model\Range;
use App\Y2023\Model\Rule;
use App\Y2023\Model\RuleSet;
use SplQueue;

class D19 extends Day
{
    /** @var array<string, RuleSet> */
    private array $rule = [];

    /** @var array<int, Part19> */
    private array $part = [];

    public function run(): void
    {
        $this->parse();

        $sum = 0;
        foreach ($this->part as $p) {
            $nxt = 'in';
            while (true) {
                $nxt = $this->rule[$nxt]->match($p);
                if ($nxt === 'A') {
                    $sum += $p->sum();
                    break;
                }
                if ($nxt === 'R') {
                    break;
                }
            }
        }

        printf("%d\n", $sum);
    }

    public function runB(): void
    {
        $this->parse();

        $start = new Block(
            new Range(1, 4000),
            new Range(1, 4000),
            new Range(1, 4000),
            new Range(1, 4000),
        );

        $sum = 0;
        $q = new SplQueue();
        $q->enqueue(['in', $start]);
        while (!$q->isEmpty()) {
            /** @var Block $b */
            [$nxt, $b] = $q->dequeue();
            $blocks = $this->rule[$nxt]->split($b);
            foreach ($blocks as [$target, $nb]) {
                if ($target === 'A') {
                    $sum += $nb->volume();
                } elseif ($target !== 'R') {
                    $q->enqueue([$target, $nb]);
                }
            }
        }

        printf("%d\n", $sum);
    }

    private function parse(): void
    {
        $part = false;
        foreach ($this->inputLines() as $line) {
            if (!strlen($line)) {
                $part = true;
                continue;
            }
            if (!$part) {
                [$ruleName, $ruleRest] = explode('{', $line);
                $rules = explode(',', substr($ruleRest, 0, -1));
                $this->rule[$ruleName] = new RuleSet(array_map(fn ($x) => Rule::fromString($x), $rules));
            } else {
                $this->part[] = Part19::fromString($line);
            }
        }
    }
}
