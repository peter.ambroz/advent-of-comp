<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;

class D13 extends Day
{
    private const POWS = [0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65536,131072,262144];

    public function run(): void
    {
        $this->parse();
    }

    public function runB(): void
    {
        $this->parse(true);
    }

    private function parse(bool $withSmudge = false): void
    {
        $pts = 0;
        $image = [];
        $imageH = [];
        $imageV = [];
        foreach ($this->inputLines() as $line) {
            if (strlen($line) > 0) {
                $line = strtr($line, ['#' => '1', '.' => 0]);
                $image[] = str_split($line);
                $imageH[] = bindec('1' . $line);
            } else {
                for ($i = 0; $i < count($image[0]); $i++) {
                    $imageV[] = bindec('1' . implode('', array_column($image, $i)));
                }

                $pts += self::process($imageH, $withSmudge) * 100 + self::process($imageV, $withSmudge);
                $image = [];
                $imageH = [];
                $imageV = [];
            }
        }

        printf("%d\n", $pts);
    }

    private static function process(array $image, bool $withSmudge): int
    {
        $ic = count($image);
        for ($m = 0; $m < $ic - 1; $m++) {
            $i = 1;
            $found = true;
            $smudge = !$withSmudge;
            while (($m - $i + 1) >= 0 && ($m + $i) < $ic) {
                $diff = $image[$m - $i + 1] ^ $image[$m + $i];
                if ($smudge && $diff !== 0) {
                    $found = false;
                    break;
                }
                if (!in_array($diff, self::POWS)) {
                    $found = false;
                    break;
                }
                if (!$smudge && $diff !== 0) {
                    $smudge = true;
                }
                $i++;
            }
            if ($found && $smudge) {
                return $m + 1;
            }
        }
        return 0;
    }
}
