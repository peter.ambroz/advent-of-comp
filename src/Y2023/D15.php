<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;

class D15 extends Day
{
    public function run(): void
    {
        $data = $this->inputLines(fn($l) => explode(',', $l))[0];
        printf("%d\n", array_sum(array_map(fn ($x) => self::hash($x), $data)));
    }

    public function runB(): void
    {
        $box = array_fill(0, 256, []);
        $data = $this->inputLines(fn($l) => explode(',', $l))[0];

        foreach ($data as $lens) {
            if (str_ends_with($lens, '-')) {
                $label = substr($lens, 0, -1);
                $f = 0;
            } else {
                [$label, $f] = explode('=', $lens);
                $f = (int) $f;
            }
            $h = self::hash($label);
            if ($f === 0) {
                unset($box[$h][$label]);
            } else {
                $box[$h][$label] = $f;
            }
        }

        $sum = 0;
        foreach ($box as $i => $b) {
            foreach (array_values($b) as $pos => $f) {
                $sum += ($i + 1) * ($pos + 1) * $f;
            }
        }
        printf("%d\n", $sum);
    }

    private static function hash(string $t): int
    {
        return array_reduce(str_split($t), fn ($s, $e) => (($s + ord($e)) * 17) % 256, 0);
    }
}
