<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;
use App\Model\Point;
use SplQueue;

class D21 extends Day
{
    private array $map;
    private array $path;
    private Point $size;
    public int $cnt;

    public function run(): void
    {
        $start = $this->parse();
        $this->find($start, 64);
        printf("%d\n", $this->cnt);
    }

    /**
     * Calculate for lower sizes, then extrapolate using quadratic formula
     */
    public function runB(): void
    {
        $start = $this->parse();
        $steps = 26501365;
        $x = ($steps - $start->x) / $this->size->x;

        $res = [];
        foreach ([2, 4] as $i) {
            $this->find($start, $this->size->x * $i + $start->x);
            $res[$i] = $this->cnt;
        }
        [$even, $odd] = $this->fullField();

        $diffs = [];
        foreach ($res as $i => $cnt) {
            $diffs[] = $cnt - (($i ** 2) * $odd + (($i - 1) ** 2) * $even);
        }
        $diff = $diffs[1] - $diffs[0];
        $sum =
            ($x ** 2) * $odd + (($x - 1) ** 2) * $even // points inside all of the full squares (quadratic)
            + intdiv($x - 2, 2) * $diff // points on the outer layers (linear)
            + $diffs[0]; // points on 4 tips (constant)

        printf("%d\n", $sum);
    }

    private function parse(): ?Point
    {
        $this->map = $this->inputLines(str_split(...));
        $this->size = new Point(count($this->map[0]), count($this->map));
        foreach ($this->map as $y => $row) {
            foreach ($row as $x => $char) {
                if ($char === 'S') {
                    $this->map[$y][$x] = '.';
                    return new Point($x, $y);
                }
            }
        }

        return null;
    }

    private function find(Point $start, int $maxSteps): void
    {
        $this->path = [];
        $this->cnt = 0;
        $q = new SplQueue();
        $q->enqueue([$start, 0]);
        $rem = $maxSteps % 2;

        while (!$q->isEmpty()) {
            [$p, $steps] = $q->dequeue();
            $ch = $this->path[(string)$p] ?? null;
            if ($ch !== null && $ch <= $steps) {
                continue;
            }
            $this->path[(string)$p] = $steps;
            if ($steps % 2 === $rem && $ch === null) {
                $this->cnt++;
            }
            if ($steps === $maxSteps) {
                continue;
            }
            foreach ($p->neighbors() as $n) {
                if ($this->g($n) === '.') {
                    $q->enqueue([$n, $steps + 1]);
                }
            }
        }
    }

    /**
     * @return int[]
     */
    private function fullField(): array
    {
        $even = 0;
        $odd = 0;
        for ($y = 0; $y < $this->size->y; $y++) {
            for ($x = $this->size->x; $x < 2 * $this->size->x; $x++) {
                $p = new Point($x, $y);
                if (isset($this->path[(string)$p])) {
                    if (($x + $y) % 2 === 0) {
                        $even++;
                    } else {
                        $odd++;
                    }
                }
            }
        }

        return [$even, $odd];
    }

    private function g(Point $p): string
    {
        $x = $p->x % $this->size->x;
        $y = $p->y % $this->size->y;
        if ($x < 0) {
            $x += $this->size->x;
        }
        if ($y < 0) {
            $y += $this->size->y;
        }
        return $this->map[$y][$x];
    }
}
