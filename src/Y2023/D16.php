<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;
use App\Model\Point;

class D16 extends Day
{
    private const N = 0;
    private const E = 1;
    private const S = 2;
    private const W = 3;

    /** @var string[] */
    private array $map;
    /** @var array<string, int> */
    private array $light;

    public function run(): void
    {
        $this->map = $this->inputLines();
        $this->light(new Point(0, 0), self::E);
        printf("%d\n", count($this->light));
    }

    public function runB(): void
    {
        $this->map = $this->inputLines();
        $start = [
            'NS' => [self::N => count($this->map) - 1, self::S => 0],
            'EW' => [self::E => 0, self::W => strlen($this->map[0]) - 1],
        ];
        $max = 0;
        foreach ($start['NS'] as $d => $y) {
            for ($x = 0; $x < strlen($this->map[0]); $x++) {
                $this->light = [];
                $this->light(new Point($x, $y), $d);
                if (count($this->light) > $max) {
                    $max = count($this->light);
                }
            }
        }
        foreach ($start['EW'] as $d => $x) {
            for ($y = 0; $y < count($this->map); $y++) {
                $this->light = [];
                $this->light(new Point($x, $y), $d);
                if (count($this->light) > $max) {
                    $max = count($this->light);
                }
            }
        }

        printf("%d\n", $max);
    }

    private function light(Point $p, int $d): void
    {
        $mirrors = [
            '/' => [self::N => self::E, self::E => self::N, self::S => self::W, self::W => self::S],
            '\\' => [self::N => self::W, self::E => self::S, self::S => self::E, self::W => self::N],
        ];
        $splitters = [
            '|' => [
                self::N => [self::N],
                self::E => [self::N, self::S],
                self::S => [self::S],
                self::W => [self::N, self::S],
            ],
            '-' => [
                self::N => [self::E, self::W],
                self::E => [self::E],
                self::S => [self::E, self::W],
                self::W => [self::W],
            ],
        ];
        while (true) {
            $cur = $this->get($p);
            if ($cur === null) {
                break;
            }
            if (($this->light[(string) $p] ?? -1) === $d) {
                break;
            }
            $this->light[(string) $p] = $d;
            $n = $p->neighbors();
            switch ($cur) {
                case '.':
                    $p = $n[$d];
                    break;
                case '/':
                case '\\':
                    $d = $mirrors[$cur][$d];
                    $p = $n[$d];
                    break;
                case '|':
                case '-':
                    $dirs = $splitters[$cur][$d];
                    if (count($dirs) === 1) {
                        $d = $dirs[0];
                        $p = $n[$d];
                    } else {
                        foreach ($dirs as $dir) {
                            $this->light($n[$dir], $dir);
                        }
                        break 2;
                    }
                    break;
            }
        }
    }

    private function get(Point $p): ?string
    {
        if ($p->x < 0) {
            return null;
        }
        return $this->map[$p->y][$p->x] ?? null;
    }
}
