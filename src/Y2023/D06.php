<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;

class D06 extends Day
{
    public function run(): void
    {
        $races = [
            [53, 275],
            [71, 1181],
            [78, 1215],
            [80, 1524],
        ];

        $mul = 1;
        foreach ($races as $race) {
            $mul *= self::results($race);
        }

        printf("%d\n", $mul);
    }

    public function runB(): void
    {
        $race = [53717880, 275118112151524];

        printf("%d\n", self::results($race));
    }

    private static function results(array $race): int
    {
        $res = 0;
        for ($i = 1; $i < $race[0]; $i++) {
            $time = $race[0] - $i;
            $dist = $time * $i;

            if ($dist > $race[1]) {
                $res++;
            }
        }

        return $res;
    }
}
