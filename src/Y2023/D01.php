<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;

class D01 extends Day
{
    public function run(): void
    {
        $sum = 0;
        foreach ($this->inputLines(fn ($line) => preg_replace("/[^0-9]/", "", $line)) as $line) {
            if (!strlen($line)) {
                continue;
            }
            $sum += ((int) $line[0]) * 10;
            $sum += (int) $line[strlen($line) - 1];
        }
        printf("%d\n", $sum);
    }

    public function runB(): void
    {
        $sum = 0;
        foreach ($this->inputLines(
            function ($line) {
                $line = strtr($line, [
                    'oneight' => 'oneeight',
                    'twone' => 'twoone',
                    'threeight' => 'threeeight',
                    'fiveight' => 'fiveeight',
                    'sevenine' => 'sevennine',
                    'eightwo' => 'eighttwo',
                    'eighthree' => 'eightthree',
                    'nineight' => 'nineeight',
                ]);
                $line = strtr($line, [
                    'one' => '1',
                    'two' => '2',
                    'three' => '3',
                    'four' => '4',
                    'five' => '5',
                    'six' => '6',
                    'seven' => '7',
                    'eight' => '8',
                    'nine' => '9',
                ]);
                return preg_replace("/[^0-9]/", "", $line);
            },
        ) as $line) {
            if (!strlen($line)) {
                continue;
            }
            $sum += ((int) $line[0]) * 10;
            $sum += (int) $line[strlen($line) - 1];
        }
        printf("%d\n", $sum);
    }
}
