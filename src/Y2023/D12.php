<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;

class D12 extends Day
{
    private array $c = [];

    public function run(): void
    {
        printf("%d\n", array_sum(
            $this->inputLines(function ($l) {
                [$line, $b] = explode(' ', $l);
                $hints = array_map(fn($x) => (int)$x, explode(',', $b));
                return $this->patterns($line, $hints);
            }),
        ));
    }

    public function runB(): void
    {
        printf("%d\n", array_sum(
            $this->inputLines(function ($l) {
                [$line, $b] = explode(' ', $l);
                $line = substr(str_repeat($line . '?', 5), 0, -1);
                $hints = array_merge(
                    ...array_fill(0, 5, array_map(fn($x)=>(int)$x, explode(',', $b)))
                );
                return $this->patterns($line, $hints);
            }),
        ));
    }

    private function patterns(
        string $line,
        array $hints,
    ): int {
        if (!count($hints)) {
            return str_contains($line, '#') ? 0 : 1;
        }
        $key = sprintf('%s;%s', $line, implode(',', $hints));
        if (isset($this->c[$key])) {
            return $this->c[$key];
        }
        $limit = strlen($line) - array_sum($hints) - count($hints) + 1;
        $hint = $hints[0];
        $restHints = array_slice($hints, 1);
        $results = 0;
        for ($i = 0; $i <= $limit; $i++) {
            if (($i > 0) && ($line[$i - 1] === '#')) {
                break;
            }
            if (str_contains(substr($line, $i, $hint), '.') || (($line[$i + $hint] ?? '?') === '#')) {
                continue;
            }
            $results += $this->patterns(substr($line, $i + $hint + 1), $restHints);
        }
        return $this->c[$key] = $results;
    }
}
