<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;
use App\Model\Point;
use Exception;
use SplPriorityQueue;
use SplQueue;

class D23 extends Day
{
    private array $map;
    /** @var Point[] */
    private array $jun;
    private array $conn;

    /**
     * Simple BFS works for this part
     */
    public function run(): void
    {
        $this->map = $this->inputLines(str_split(...));
        $start = new Point(1, 0);
        $end = new Point(count($this->map[0]) - 2, count($this->map) - 1);
        $l = $this->longest($start, $end);
        printf("%d\n", $l);
    }

    /**
     * Find all the junctions and precompute path lengths between them
     * This gives us a simplified graph to run BFS
     */
    public function runB(): void
    {
        $this->map = $this->inputLines(str_split(...));
        $start = new Point(1, 0);
        $end = new Point(count($this->map[0]) - 2, count($this->map) - 1);
        $this->junctions($start, $end);
        $this->paths();
        $l = $this->longestB($start, $end);
        printf("%d\n", $l);
    }

    private function junctions(Point $start, Point $end): void
    {
        $startNext = new Point($start->x, $start->y + 1);
        $endPrev = new Point($end->x, $end->y - 1);
        $this->jun = [(string) $start => [$startNext]];
        foreach ($this->map as $y => $row) {
            foreach ($row as $x => $c) {
                if ($c === '.') {
                    $p = new Point($x, $y);
                    $can = [];
                    foreach ($p->neighbors() as $n) {
                        if ($this->g($n) !== '#') {
                            $can[] = $n;
                        }
                    }
                    if (count($can) > 2) {
                        $this->jun[(string) $p] = $can;
                    }
                }
            }
        }
        $this->jun[(string) $end] = [$endPrev];
    }

    private function paths(): void
    {
        $this->conn = [];
        /** @var Point[] $candidates */
        foreach ($this->jun as $ph => $candidates) {
            $start = Point::fromString($ph);
            $this->conn[$ph] = [];
            foreach ($candidates as $p) {
                [$end, $len] = $this->toNextJunction($start, $p);
                $this->conn[$ph][(string) $end] = $len;
            }
        }
    }

    private function toNextJunction(Point $start, Point $dir): array
    {
        $path = [(string) $start => true, (string) $dir => true];
        $q = new SplQueue();
        $q->enqueue([$dir, 1]);

        while (!$q->isEmpty()) {
            /** @var Point $p */
            [$p, $l] = $q->dequeue();
            if (isset($this->jun[(string) $p])) {
                return [$p, $l];
            }
            foreach ($p->neighbors() as $n) {
                if (isset($path[(string) $n]) || $this->g($n) === '#') {
                    continue;
                }
                $path[(string) $n] = true;
                $q->enqueue([$n, $l + 1]);
            }
        }

        throw new Exception('Unreachable');
    }

    private function longestB(Point $start, Point $end): int
    {
        $maxl = 0;
        $q = new SplPriorityQueue();
        $q->insert([$start, 0, [(string) $start => true]], 0);

        while (!$q->isEmpty()) {
            /** @var Point $p */
            [$p, $l, $path] = $q->extract();
            if ($end->equals($p)) {
                if ($l > $maxl) {
                    $maxl = $l;
                    printf("%s\n", $l);
                }
                continue;
            }
            foreach ($this->conn[(string) $p] as $destH => $len) {
                if (isset($path[$destH])) {
                    continue;
                }
                $npath = $path;
                $npath[$destH] = true;
                $q->insert([Point::fromString($destH), $l + $len, $npath], $l + $len);
            }
        }

        return $maxl;
    }

    private function longest(Point $start, Point $end): int
    {
        $maxl = 0;
        $q = new SplQueue();
        $q->enqueue([$start, $start, 0]);

        while (!$q->isEmpty()) {
            /**
             * @var Point $prev
             * @var Point $p
             * @var int $l
             */
            [$prev, $p, $l] = $q->dequeue();
            if ($p->equals($end)) {
                if ($l > $maxl) {
                    $maxl = $l;
                }
                continue;
            }
            foreach ($p->neighbors() as $n) {
                if ($n->equals($prev)) {
                    continue;
                }
                $c = $this->g($n);
                /** @var Point $np */
                [$np, $nl] = self::nxt($n, $c);
                if ($nl === 0) {
                    continue;
                }
                if ($p->equals($np)) { // P === NP ??!?!
                    continue;
                }
                $q->enqueue([$p, $np, $l + $nl]);
            }
        }

        return $maxl;
    }

    private function g(Point $p): string
    {
        return $this->map[$p->y][$p->x] ?? '#';
    }

    private static function nxt(Point $p, string $c): array
    {
        return match ($c) {
            '^' => [new Point($p->x, $p->y - 1), 2],
            '>' => [new Point($p->x + 1, $p->y), 2],
            'v' => [new Point($p->x, $p->y + 1), 2],
            '<' => [new Point($p->x - 1, $p->y), 2],
            '.' => [$p, 1],
            '#' => [$p, 0],
        };
    }
}
