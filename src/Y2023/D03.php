<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;
use App\Model\Point;
use App\Y2023\Model\Part;

class D03 extends Day
{
    private array $data;
    private array $parts = [];
    private int $total;

    public function run(): void
    {
        $this->doRun();

        printf("parts: %d\n", $this->total);
    }

    public function runB(): void
    {
        $this->doRun();

        $total = 0;
        foreach ($this->parts as $gear) {
            if (count($gear) === 2) {
                $total += $gear[0] * $gear[1];
            }
        }

        printf("gears: %d\n", $total);
    }

    private function findPart(int $x1, int $x2, int $y): ?Part
    {
        for ($j = $y - 1; $j <= $y + 1; $j++) {
            if (!isset($this->data[$j])) {
                continue;
            }

            $l = $this->data[$j];

            for ($i = $x1 - 1; $i <= $x2 + 1; $i++) {
                $ch = ord($l[$i] ?? '.');

                if ($ch === 46) { // .
                    continue;
                }

                if ($ch < 48 || $ch >= 58) { // 0-9
                    return new Part(new Point($i, $j), $ch);
                }
            }
        }

        return null;
    }

    private function doRun(): void
    {
        $this->data = $this->inputLines();
        $this->total = 0;

        foreach ($this->data as $j => $line) {
            $last = 0;
            $x1 = -1;
            for ($i = 0; $i <= strlen($line); $i++) {
                $nv = ord($line[$i] ?? '.');
                if ($nv >= 48 && $nv < 58) {
                    $last *= 10;
                    $last += ($nv - 48);
                    if ($x1 === -1) {
                        $x1 = $i;
                    }
                } else {
                    $part = $this->findPart($x1, $i - 1, $j);
                    if ($last > 0 && $part !== null) {
                        $this->total += $last;

                        if ($part->type === 42) { // *
                            $this->registerPart($part, $last);
                        }
                    }
                    $last = 0;
                    $x1 = -1;
                }
            }
        }
    }

    private function registerPart(Part $part, int $num): void
    {
        $loc = (string) $part->location;

        if (!isset($this->parts[$loc])) {
            $this->parts[$loc] = [];
        }

        $this->parts[$loc][] = $num;
    }
}
