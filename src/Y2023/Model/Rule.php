<?php

declare(strict_types=1);

namespace App\Y2023\Model;

final readonly class Rule
{
    public const LT = 0;
    public const GT = 1;
    public const FORWARD = 2;

    private function __construct(
        public int $operation,
        public string $target,
        private ?string $attribute = null,
        private ?int $value = null,
    ) {
    }

    public static function fromString(string $rule): self
    {
        if (!str_contains($rule, '<') && !str_contains($rule, '>')) {
            return new self(self::FORWARD, $rule);
        }

        [$r, $nxt] = explode(':', $rule);
        if (str_contains($r, '<')) {
            [$k, $v] = explode('<', $r);
            return new self(self::LT, $nxt, $k, (int) $v);
        }
        if (str_contains($r, '>')) {
            [$k, $v] = explode('>', $r);
            return new self(self::GT, $nxt, $k, (int) $v);
        }

        return new self(self::FORWARD, 'R');
    }

    public function match(Part19 $part): ?string
    {
        $a = $this->attribute;
        return match ($this->operation) {
            self::FORWARD => $this->target,
            self::LT => $part->$a < $this->value ? $this->target : null,
            self::GT => $part->$a > $this->value ? $this->target : null,
        };
    }

    /**
     * Apply a comparison rule: Produce one block respecting the rule and another with the rest
     * @return array<int, Block>
     */
    public function split(Block $b): array
    {
        if ($this->operation === self::FORWARD) {
            return [$b];
        }

        $blocks = [];
        $attr = $this->attribute;
        /** @var Range $r */
        $r = $b->$attr;
        switch ($this->operation) {
            case self::LT:
                $blocks[] = $b->limit(
                    $attr,
                    Range::makeAbsolute($r->start, min($r->end, $this->value - 1)),
                );

                $blocks[] = $b->limit(
                    $attr,
                    Range::makeAbsolute(max($r->start, $this->value), $r->end),
                );
                break;
            case self::GT:
                $blocks[] = $b->limit(
                    $attr,
                    Range::makeAbsolute(max($r->start, $this->value + 1), $r->end),
                );

                $blocks[] = $b->limit(
                    $attr,
                    Range::makeAbsolute($r->start, min($r->end, $this->value)),
                );
                break;
        }

        return $blocks;
    }
}
