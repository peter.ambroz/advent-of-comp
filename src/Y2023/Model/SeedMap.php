<?php

declare(strict_types=1);

namespace App\Y2023\Model;

use LogicException;

final readonly class SeedMap
{
    private int $diff;

    public Range $range;

    public function __construct(
        int $destStart,
        int $srcStart,
        int $length,
    ) {
        $this->range = new Range($srcStart, $length);
        $this->diff = $destStart - $srcStart;
    }

    public function map(int $src): int
    {
        if ($this->range->has($src)) {
            return $src + $this->diff;
        }

        throw new LogicException('Unable to map out-of-range value');
    }
}
