<?php

declare(strict_types=1);

namespace App\Y2023\Model;

final readonly class RuleSet
{
    /**
     * @param Rule[] $rules
     */
    public function __construct(private array $rules)
    {
    }

    public function match(Part19 $part): string
    {
        foreach ($this->rules as $r) {
            $nxt = $r->match($part);
            if ($nxt !== null) {
                return $nxt;
            }
        }

        return 'unreachable';
    }

    /**
     * @return array<string, Block>
     */
    public function split(Block $b): array
    {
        $res = [];
        $nb = $b;
        foreach ($this->rules as $r) {
            $blocks = $r->split($nb);
            $res[] = [$r->target, $blocks[0]];
            $nb = array_pop($blocks);
        }

        return $res;
    }
}
