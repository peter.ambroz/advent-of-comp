<?php

declare(strict_types=1);

namespace App\Y2023\Model;

final readonly class Part19
{
    public function __construct(
        public int $x,
        public int $m,
        public int $a,
        public int $s,
    ) {
    }

    public static function fromString(string $line): self
    {
        $attrs = explode(',', substr($line, 1, -1));
        $attrs = array_reduce($attrs, function ($c, $i) {
            [$n, $v] = explode('=', $i);
            $c[$n] = (int)$v;
            return $c;
        }, []);

        return new self($attrs['x'], $attrs['m'], $attrs['a'], $attrs['s']);
    }

    public function sum(): int
    {
        return $this->x + $this->m + $this->a + $this->s;
    }
}
