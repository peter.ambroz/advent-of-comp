<?php

declare(strict_types=1);

namespace App\Y2023\Model;

final class Vertex
{
    /** @var array<int, Edge> */
    private array $edges = [];

    public function __construct(public readonly string $name)
    {
    }

    /**
     * @return Edge[]
     */
    public function edgesExcept(?Edge $edge = null): array
    {
        return array_filter($this->edges, fn (Edge $e) => $e->id !== $edge?->id);
    }

    /**
     * @return iterable<self>
     */
    public function neighbors(): iterable
    {
        foreach ($this->edges as $e) {
            yield $e->opposite($this);
        }
    }

    public function addEdge(Edge $edge): void
    {
        $this->edges[] = $edge;
    }

    public function removeEdge(Edge $edge): void
    {
        $this->edges = $this->edgesExcept($edge);
    }

    public function getSize(): int
    {
        return intdiv(strlen($this->name), 3);
    }

    public function __toString(): string
    {
        return sprintf("%s %d (%d)", $this->name, count($this->edges), $this->getSize());
    }
}
