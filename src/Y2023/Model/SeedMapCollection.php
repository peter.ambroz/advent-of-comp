<?php

declare(strict_types=1);

namespace App\Y2023\Model;

final class SeedMapCollection
{
    /** @var SeedMap[] */
    private array $maps = [];

    private bool $sorted = true;

    public function __construct(public string $name)
    {
    }

    public function add(SeedMap $map): void
    {
        $this->maps[] = $map;
        $this->sorted = false;
    }

    public function map(int $src): int
    {
        foreach ($this->maps as $map) {
            if ($map->range->has($src)) {
                return $map->map($src);
            }
        }

        return $src;
    }

    /**
     * @return Range[]
     */
    public function mapRange(Range $range): array
    {
        $this->sortAndFill();

        $ranges = [];
        foreach ($this->maps as $map) {
            if ($range->start < $map->range->start) {
                $start = $map->map($map->range->start);
            } elseif ($map->range->has($range->start)) {
                $start = $map->map($range->start);
            } else {
                continue;
            }

            if ($range->end > $map->range->end) {
                $end = $map->map($map->range->end);
            } elseif ($map->range->has($range->end)) {
                $end = $map->map($range->end);
            } else {
                continue;
            }

            $ranges[] = Range::makeAbsolute($start, $end);
        }

        return $ranges;
    }

    private function sortAndFill(): void
    {
        if ($this->sorted || !count($this->maps)) {
            return;
        }

        usort($this->maps, fn (SeedMap $a, SeedMap $b) => $a->range->start <=> $b->range->start);

        $filling = [];

        if ($this->maps[0]->range->start > 0) {
            $filling[] = new SeedMap(0, 0, $this->maps[0]->range->start);
        }

        for ($i = 0; $i < count($this->maps) - 1; $i++) {
            $curMap = $this->maps[$i];
            $nextMap = $this->maps[$i + 1];

            if ($nextMap->range->start - $curMap->range->end > 1) {
                $filling[] = new SeedMap(
                    $curMap->range->end + 1,
                    $curMap->range->end + 1,
                    $nextMap->range->start - $curMap->range->end - 1,
                );
            }
        }

        $lastMap = $this->maps[count($this->maps) - 1];
        $filling[] = new SeedMap(
            $lastMap->range->end + 1,
            $lastMap->range->end + 1,
            PHP_INT_MAX - $lastMap->range->end - 1,
        );

        $this->maps = array_merge($this->maps, $filling);
        usort($this->maps, fn (SeedMap $a, SeedMap $b) => $a->range->start <=> $b->range->start);
        $this->sorted = true;
    }
}
