<?php

declare(strict_types=1);

namespace App\Y2023\Model;

use SplQueue;

final class Graph
{
    /** @var array<string, Vertex> */
    private array $vertices = [];

    /** @var array<int, Edge> */
    private array $edges = [];

    public function addVertex(string $label): Vertex
    {
        if (isset($this->vertices[$label])) {
            return $this->vertices[$label];
        }
        return $this->vertices[$label] = new Vertex($label);
    }

    public function removeVertex(string $label): void
    {
        unset($this->vertices[$label]);
    }

    public function addEdge(string $label1, string $label2): void
    {
        $v1 = $this->vertices[$label1] ?? $this->addVertex($label1);
        $v2 = $this->vertices[$label2] ?? $this->addVertex($label2);
        $this->edges[] = $e = new Edge($v1, $v2);
        $v1->addEdge($e);
        $v2->addEdge($e);
    }

    public function removeEdge(Edge $edge): void
    {
        $this->edges = array_filter($this->edges, fn (Edge $e) => $e->id !== $edge->id);
        $edge->v1->removeEdge($edge);
        $edge->v2->removeEdge($edge);
    }

    public function karger(): void
    {
        while (count($this->edges) > 10) {
            $eid = mt_rand(0, count($this->edges) - 1);
            $e = array_values($this->edges)[$eid];
            $this->collapse($e);
        }
    }

    private function collapse(Edge $e): void
    {
        $v1 = $e->v1;
        $v2 = $e->v2;

        $nv = $this->addVertex($v1->name . $v2->name);
        $this->removeVertex($v1->name);
        $this->removeVertex($v2->name);

        $this->removeEdge($e);
        $vEdges = array_merge($v1->edgesExcept($e), $v2->edgesExcept($e));

        foreach ($vEdges as $vEdge) {
            if ($vEdge->id === $e->id) {
                continue;
            }
            $neighbor = $vEdge->opposite($v1) ?? $vEdge->opposite($v2);
            $this->removeEdge($vEdge);
            $this->addEdge($nv->name, $neighbor->name);
        }
    }

    /**
     * @return Edge[]
     */
    public function getEdges(): array
    {
        return $this->edges;
    }

    public function componentSize(string $rootLabel): int
    {
        $seen = [];
        $q = new SplQueue();
        $q->enqueue($rootLabel);
        while (!$q->isEmpty()) {
            $l = $q->dequeue();
            $seen[$l] = true;
            foreach ($this->vertices[$l]->neighbors() as $n) {
                if (!isset($seen[$n->name])) {
                    $q->enqueue($n->name);
                }
            }
        }

        return count($seen);
    }
}
