<?php

declare(strict_types=1);

namespace App\Y2023\Model;

final class Module
{
    private const FLIP = '%';
    private const CONJ = '&';
    private const BEGIN = 'b';

    /** @var array<string, bool> */
    private array $back = [];

    private bool $state = false;

    public function __construct(
        private readonly string $type,
        public readonly string $label,
        public readonly array $next,
    ) {
    }

    public function addBack(string $back, bool $value = false): void
    {
        $this->back[$back] = $value;
    }

    public function signal(string $from, bool $level): ?bool
    {
        $ret = null;
        switch ($this->type) {
            case self::BEGIN:
                $ret = $level;
                break;
            case self::CONJ:
                $this->back[$from] = $level;
                $ret = $this->state = !array_reduce($this->back, fn ($c, $e) => $c && $e, true);
                break;
            case self::FLIP:
                if ($level === false) {
                    $ret = $this->state = !$this->state;
                }
                break;
        }

        return $ret;
    }
}
