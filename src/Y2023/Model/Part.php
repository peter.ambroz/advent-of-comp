<?php

declare(strict_types=1);

namespace App\Y2023\Model;

use App\Model\Point;

readonly final class Part
{
    public function __construct(
        public Point $location,
        public int $type,
    ) {
    }
}
