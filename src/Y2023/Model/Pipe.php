<?php

declare(strict_types=1);

namespace App\Y2023\Model;

use Exception;

final readonly class Pipe
{
    public const N = 0;
    public const E = 1;
    public const S = 2;
    public const W = 3;

    // [from, to, [sides1], [sides2]]
    private const MAP = [
        '-' => [self::W, self::E, [self::N], [self::S]],
        '|' => [self::N, self::S, [self::E], [self::W]],
        '7' => [self::W, self::S, [self::N, self::E], []],
        'J' => [self::N, self::W, [self::S, self::E], []],
        'L' => [self::E, self::N, [self::S, self::W], []],
        'F' => [self::S, self::E, [self::N, self::W], []],
    ];

    /**
     * @return int[]
     * @throws Exception
     */
    public static function directions(string $segment): array
    {
        if (!isset(self::MAP[$segment])) {
            throw new Exception('Wrong segment');
        }

        return array_slice(self::MAP[$segment], 0, 2);
    }

    /**
     * @return int[][]
     * @throws Exception
     */
    public static function sides(string $segment): array
    {
        if (!isset(self::MAP[$segment])) {
            throw new Exception('Wrong segment');
        }

        return array_slice(self::MAP[$segment], 2, 2);
    }

    /**
     * @throws Exception
     */
    public static function nextDirection(string $segment, int $previousDirection): int
    {
        $dirs = self::directions($segment);
        $enterDirection = ($previousDirection + 2) % 4;
        if ($dirs[0] === $enterDirection) {
            return $dirs[1];
        } elseif ($dirs[1] === $enterDirection) {
            return $dirs[0];
        } else {
            throw new Exception('Can not enter segment');
        }
    }

    /**
     * @return int[]
     * @throws Exception
     */
    public static function leftSide(string $segment, int $previousDirection): array
    {
        $dirs = self::directions($segment);
        $enterDirection = $previousDirection;
        $sides = self::sides($segment);
        if ($dirs[0] === $enterDirection) {
            return $sides[1];
        } elseif ($dirs[1] === $enterDirection) {
            return $sides[0];
        } else {
            throw new Exception('Can not enter segment');
        }
    }
}
