<?php

declare(strict_types=1);

namespace App\Y2023\Model;

use Exception;

final class Hand
{
    public const HIGH_CARD = 0;
    public const ONE_PAIR = 1;
    public const TWO_PAIR = 2;
    public const THREE = 3;
    public const FULL_HOUSE = 4;
    public const FOUR = 5;
    public const FIVE = 6;

    public readonly string $cards;

    public int $type;

    public function __construct(
        string $cards,
        public readonly int $win,
        bool $jokers,
    ) {
        if ($jokers) {
            $this->cards = strtr($cards, ['A' => 'Z', 'K' => 'Y', 'Q' => 'X', 'T' => 'V', 'J' => '0']);
        } else {
            $this->cards = strtr($cards, ['A' => 'Z', 'K' => 'Y', 'Q' => 'X', 'J' => 'W', 'T' => 'V']);
        }
        $this->type = $jokers ? self::evaluateJokers($cards) : self::evaluate($cards);
    }

    private static function evaluate(string $cards): int
    {
        $map = [];
        foreach (str_split($cards) as $card) {
            $map[$card] = ($map[$card] ?? 0) + 1;
        }

        rsort($map);
        $map = array_values($map);

        if ($map === [5]) {
            return self::FIVE;
        }

        if ($map === [4, 1]) {
            return self::FOUR;
        }

        if ($map === [3, 2]) {
            return self::FULL_HOUSE;
        }

        if ($map === [3, 1, 1]) {
            return self::THREE;
        }

        if ($map === [2, 2, 1]) {
            return self::TWO_PAIR;
        }

        if ($map === [2, 1, 1, 1]) {
            return self::ONE_PAIR;
        }

        return self::HIGH_CARD;
    }

    private static function evaluateJokers(string $cards): int
    {
        $map = [];
        $j = 0;
        foreach (str_split($cards) as $card) {
            if ($card !== 'J') {
                $map[$card] = ($map[$card] ?? 0) + 1;
            }
        }

        rsort($map);
        $map = array_values($map);
        $mapc = count($map);

        if ($mapc <= 1) { // [], 1, 2, 3, 4, 5
            return self::FIVE;
        }

        if ($mapc === 2 && $map[1] === 1) { // 41, 31, 21, 11
            return self::FOUR;
        }

        if ($mapc === 2 && $map[1] === 2) { // 32, 22
            return self::FULL_HOUSE;
        }

        if ($mapc === 3 && $map[1] === 1) { // 111, 211, 311
            return self::THREE;
        }

        if ($mapc === 3) { // 221
            return self::TWO_PAIR;
        }

        if ($mapc === 4) { // 1111, 2111
            return self::ONE_PAIR;
        }

        return self::HIGH_CARD;
    }
}
