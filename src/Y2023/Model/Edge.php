<?php

declare(strict_types=1);

namespace App\Y2023\Model;

final class Edge
{
    public readonly string $id;

    public function __construct(
        public readonly Vertex $v1,
        public readonly Vertex $v2,
    ) {
        if (strcmp($v1->name, $v2->name) < 0) {
            $this->id = $v1->name . ';' . $v2->name;
        } else {
            $this->id = $v2->name . ';' . $v1->name;
        }
    }

    public function opposite(Vertex $v): ?Vertex
    {
        if ($v->name === $this->v1->name) {
            return $this->v2;
        }

        if ($v->name === $this->v2->name) {
            return $this->v1;
        }

        return null;
    }

    public function __toString(): string
    {
        return sprintf("%s", $this->id);
    }
}
