<?php

declare(strict_types=1);

namespace App\Y2023\Model;

final readonly class Range
{
    public int $end;

    public static function makeAbsolute(int $start, int $end): self
    {
        return new self($start, $end - $start + 1);
    }

    public function __construct(public int $start, private int $length)
    {
        $this->end = $this->start + $this->length - 1;
    }

    public function has(int|float $item): bool
    {
        return ($item >= $this->start && $item <= $this->end);
    }

    public function isEmpty(): bool
    {
        return $this->length <= 0;
    }

    public function size(): int
    {
        return max(0, $this->length);
    }
}
