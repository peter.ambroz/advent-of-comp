<?php

declare(strict_types=1);

namespace App\Y2023\Model;

final readonly class Block
{
    public function __construct(
        public Range $x,
        public Range $m,
        public Range $a,
        public Range $s,
    ) {
    }

    public function limit(string $attr, Range $newRange): self
    {
        return new self(
            $attr === 'x' ? $newRange : $this->x,
            $attr === 'm' ? $newRange : $this->m,
            $attr === 'a' ? $newRange : $this->a,
            $attr === 's' ? $newRange : $this->s,
        );
    }

    public function volume(): int
    {
        return $this->x->size() * $this->m->size() * $this->a->size() * $this->s->size();
    }

    public function isEmpty(): bool
    {
        return $this->x->isEmpty() || $this->m->isEmpty() || $this->a->isEmpty() || $this->s->isEmpty();
    }
}
