<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;
use App\Y2023\Model\Graph;

class D25 extends Day
{
    public function run(): void
    {
        $fw = fopen('/tmp/D25.dot', 'w');
        fprintf($fw, "graph {\n");
        foreach ($this->inputLines() as $l) {
            [$from, $to] = explode(': ', $l);
            $to = explode(' ', $to);
            fprintf($fw, "%s -- {%s};\n", $from, implode(', ', $to));
        }
        fprintf($fw, "}\n");
        fclose($fw);

        printf("Convert the .dot file to SVG\n");
        printf("dot -Tsvg /tmp/D25.dot -o /tmp/D25.svg\n");
        printf("See the resulting /tmp/D25.svg file, remove the 3 edges and re-run part B\n");

        /* Karger algorithm implementation too slow
        $i = 1;
        while (true) {
            printf("%d\n", $i++);
            $g = $this->parse();
            $g->karger();
            if (count($g->getEdges()) === 3) {
                break;
            }
        }

        foreach ($g->getEdges() as $e) {
            printf("%s\n", $e);
            printf("%s %s\n", $e->v1, $e->v2);
        }
        */
    }

    public function runB(): void
    {
        $g = $this->parse();
        $c1 = $g->componentSize('pxf');
        $c2 = $g->componentSize('cbz');

        printf("%d*%d = %d\n", $c1, $c2, $c1 * $c2);
    }

    private function parse(): Graph
    {
        $g = new Graph();
        foreach ($this->inputLines(function ($l) {
            [$from, $to] = explode(': ', $l);
            return [$from, explode(' ', $to)];
        }) as [$from, $to]) {
            foreach ($to as $item) {
                $g->addEdge($from, $item);
            }
        }

        return $g;
    }
}
