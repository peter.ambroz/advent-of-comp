<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;
use App\Model\Point;
use App\Y2023\Model\Range;

class D24 extends Day
{
    public array $data;

    public function run(): void
    {
        $data = $this->inputLines(function ($l) {
            [$pos, $vel] = explode(' @ ', $l);
            [$x, $y, $z] = array_map(fn($x)=>(int)$x, explode(', ', $pos));
            [$xv, $yv, $zv] = array_map(fn($x)=>(int)$x, explode(', ', $vel));
            return [new Point($x, $y), new Point($xv, $yv)];
        });

        $r = Range::makeAbsolute(200000000000000, 400000000000000);

        $cnt = 0;
        foreach ($data as $i => [$pos1, $vel1]) {
            foreach ($data as $j => [$pos2, $vel2]) {
                if ($i >= $j) {
                    continue;
                }

                $cnt += self::check($pos1, $vel1, $pos2, $vel2, $r, $r) ? 1 : 0;
            }
        }

        printf("%d\n", $cnt);
    }

    public function runB(): void
    {
        $this->data = $this->inputLines(function ($l) {
            [$pos, $vel] = explode(' @ ', $l);
            [$x, $y, $z] = array_map(fn($x)=>(int)$x, explode(', ', $pos));
            [$xv, $yv, $zv] = array_map(fn($x)=>(int)$x, explode(', ', $vel));
            return ['x' => $x, 'y' => $y, 'z' => $z, 'xv' => $xv, 'yv' => $yv, 'zv' => $zv];
        });

        $yvs = [];
        foreach ($this->data as $i => ['yv' => $yv]) {
            $yvs[$yv] = $i;
        }

        // try various XY velocity combinations.
        foreach (range(-200, 200) as $xv) {
            foreach ($yvs as $yv => $j) {
                $xyz = $this->tryVelocities($xv, $yv, $j);
                if ($xyz !== null) {
                    printf("%d (%d + %d + %d)\n", array_sum($xyz), $xyz[0], $xyz[1], $xyz[2]);
                }
            }
        }
    }

    private function tryVelocities(int $xv, int $yv, int $id): ?array
    {
        $y = $this->data[$id]['y'];

        // find $x
        while (true) {
            $s1 = $this->data[mt_rand(0, count($this->data) - 1)];
            $ydif = $y - $s1['y'];
            $yvdif = $s1['yv'] - $yv;
            if ($yvdif === 0) {
                continue;
            }
            if ($ydif % $yvdif !== 0) {
                return null;
            }
            $t1 = intdiv($ydif, $yvdif);
            $x = ($s1['xv'] - $xv) * $t1 + $s1['x'];
            break;
        }

        // verify against random data points
        $eq = [];
        $verify = 10;
        while ($verify > 0) {
            $s = $this->data[mt_rand(0, count($this->data) - 1)];
            $yvdif = $s['yv'] - $yv;
            $xvdif = $s['xv'] - $xv;

            $ydif = $y - $s['y'];
            $xdif = $x - $s['x'];

            if ($yvdif === 0 || $xvdif === 0) {
                continue;
            }
            if ($ydif % $yvdif !== 0 || $xdif % $xvdif !== 0) {
                return null;
            }
            $t = intdiv($ydif, $yvdif);
            if ($t !== intdiv($xdif, $xvdif)) {
                return null;
            }
            $eq[] = [$t, $s];
            $verify--;
        }

        // find $z
        [$t1, $s1] = $eq[0];
        [$t2, $s2] = $eq[1];
        if ($t1 === $t2) {
            [$t2, $s2] = $eq[2];
        }
        $zv = (($s1['z'] + $s1['zv'] * $t1) - ($s2['z'] + $s2['zv'] * $t2)) / ($t1 - $t2);
        $z = ($s1['zv'] - $zv) * $t1 + $s1['z'];

        return [$x, $y, $z];
    }

    private static function check(Point $pos1, Point $vel1, Point $pos2, Point $vel2, Range $xr, Range $yr): bool
    {
        $a1 = $vel1->y / $vel1->x;
        $a2 = $vel2->y / $vel2->x;
        $b1 = $pos1->y - $a1 * $pos1->x;
        $b2 = $pos2->y - $a2 * $pos2->x;

        $adiff = $a1 - $a2;
        if ($adiff !== 0) {
            $x = ($b2 - $b1) / $adiff;
            $y = $a1 * $x + $b1;
        } else {
            return false;
        }

        $t1 = ($x - $pos1->x) / $vel1->x;
        $t2 = ($x - $pos2->x) / $vel2->x;

        return $xr->has($x) && $yr->has($y) && $t1 > 0 && $t2 > 0;
    }
}
