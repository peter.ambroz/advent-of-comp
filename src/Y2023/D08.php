<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;
use ArrayIterator;
use InfiniteIterator;
use Iterator;

class D08 extends Day
{
    private array $data = [];
    private array $starts = [];
    private array $ends = [];

    private function parse(): Iterator
    {
        $lr = [];
        foreach ($this->inputLines() as $i => $line) {
            if ($i === 0) {
                $lr = array_map(fn ($x) => (int) $x, str_split(strtr($line, ['L' => '0', 'R' => '1'])));
            } elseif ($i > 1) {
                [$a, $b, $c] = explode(' ', strtr($line, ['= (' => '', ',' => '', ')' => '']));
                $this->data[$a] = [$b, $c];
                if ($a[2] === 'A') {
                    $this->starts[] = $a;
                }
                if ($a[2] === 'Z') {
                    $this->ends[] = $a;
                }
            }
        }

        return new InfiniteIterator(new ArrayIterator($lr));
    }

    public function run(): void
    {
        $i = $this->parse();
        $pos = 'AAA';
        $st = 0;
        $i->rewind();
        while (true) {
            $dir = $i->current();
            $pos = $this->data[$pos][$dir];
            $st++;
            $i->next();
            if ($pos === 'ZZZ') {
                break;
            }
        }

        printf("%d\n", $st);
    }

    public function runB(): void
    {
        $i = $this->parse();
        $steps = [];

        foreach ($this->starts as $start) {
            $pos = $start;
            $st = 0;
            $i->rewind();
            while (true) {
                $dir = $i->current();
                $pos = $this->data[$pos][$dir];
                $st++;
                $i->next();
                if (in_array($pos, $this->ends)) {
                    printf("Reached %s from %s in %d\n", $pos, $start, $st);
                    $steps[] = $st;
                    break;
                }
            }
        }

        $lcm = array_reduce($steps, gmp_lcm(...), 1);

        printf("%s\n", gmp_strval($lcm));
    }
}
