<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;

/**
 * I do not like this code. Please don't look at it
 */
class D14 extends Day
{
    private array $data;
    private int $maxx;
    private int $maxy;

    public function run(): void
    {
        $sum = 0;
        $data = $this->inputLines(fn ($l) => str_split($l));
        foreach ($data[0] as $k => $dummy) {
            $sum += self::calcColumn(array_column($data, $k));
        }
        printf("%d\n", $sum);
    }

    public function runB(): void
    {
        $this->data = $this->inputLines(fn ($l) => str_split($l));
        $this->maxx = count($this->data[0]);
        $this->maxy = count($this->data);
        // good enough in this particular case. probably not universal :(
        for ($i = 0; $i < 1000; $i++) {
            $this->spin();
        }
        printf("%d\n", $this->load());
    }

    private static function calcColumn(array $col): int
    {
        $max = count($col);
        $cur = $max;
        $sum = 0;
        foreach ($col as $i => $item) {
            if ($item === 'O') {
                $sum += $cur--;
            } elseif ($item === '#') {
                $cur = $max - $i - 1;
            }
        }
        return $sum;
    }

    private function spin(): void
    {
        // N
        for ($i = 0; $i < $this->maxx; $i++) {
            $dest = 0;
            for ($j = 0; $j < $this->maxy; $j++) {
                $item = $this->data[$j][$i];
                if ($item === 'O') {
                    $this->data[$j][$i] = '.';
                    $this->data[$dest][$i] = 'O';
                    $dest++;
                } elseif ($item === '#') {
                    $dest = $j + 1;
                }
            }
        }
        // W
        for ($j = 0; $j < $this->maxy; $j++) {
            $dest = 0;
            for ($i = 0; $i < $this->maxx; $i++) {
                $item = $this->data[$j][$i];
                if ($item === 'O') {
                    $this->data[$j][$i] = '.';
                    $this->data[$j][$dest] = 'O';
                    $dest++;
                } elseif ($item === '#') {
                    $dest = $i + 1;
                }
            }
        }
        // S
        for ($i = 0; $i < $this->maxx; $i++) {
            $dest = $this->maxy - 1;
            for ($j = $this->maxy - 1; $j >= 0; $j--) {
                $item = $this->data[$j][$i];
                if ($item === 'O') {
                    $this->data[$j][$i] = '.';
                    $this->data[$dest][$i] = 'O';
                    $dest--;
                } elseif ($item === '#') {
                    $dest = $j - 1;
                }
            }
        }
        // E
        for ($j = 0; $j < $this->maxy; $j++) {
            $dest = $this->maxx - 1;
            for ($i = $this->maxx - 1; $i >= 0; $i--) {
                $item = $this->data[$j][$i];
                if ($item === 'O') {
                    $this->data[$j][$i] = '.';
                    $this->data[$j][$dest] = 'O';
                    $dest--;
                } elseif ($item === '#') {
                    $dest = $i - 1;
                }
            }
        }
    }

    private function load(): int
    {
        $sum = 0;
        foreach ($this->data as $j => $row) {
            foreach ($row as $item) {
                if ($item === 'O') {
                    $sum += $this->maxy - $j;
                }
            }
        }
        return $sum;
    }
}
