<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;
use App\Model\Point;
use SplPriorityQueue;
use SplQueue;

class D17 extends Day
{
    private array $map;

    public function run(): void
    {
        $this->doRun(1, 3);
    }

    public function runB(): void
    {
        $this->doRun(4, 10);
    }

    private function doRun(int $min, int $max): void
    {
        $this->map = $this->inputLines(fn ($l) => array_map(fn($x) => (int) $x, str_split($l)));
        $d = $this->find(
            new Point(0, 0),
            (string) (new Point(count($this->map[0])-1, count($this->map)-1)),
            $min,
            $max,
        );
        printf("%d\n", $d);
    }

    private function find(Point $start, string $end, int $min, int $max): int
    {
        $s = [];
        $q = new SplPriorityQueue();
        $n = $start->neighbors();
        $q->insert([(string)$n[1], $this->g($n[1]), 1, 1], 100000);
        $q->insert([(string)$n[2], $this->g($n[2]), 1, 2], 100000);

        while (!$q->isEmpty()) {
            [$p, $d, $dirCnt, $dir] = $q->extract();

            if ($end === $p) {
                break;
            }
            $skey = $p.':'.$dir.':'.$dirCnt;
            if (isset($s[$skey])) {
                continue;
            }
            $s[$skey] = true;
            foreach (Point::fromString($p)->neighbors() as $newDir => $n) {
                if (abs($newDir - $dir) === 2) {
                    continue;
                }
                if ($newDir === $dir && $dirCnt === $max) {
                    continue;
                }
                if ($newDir !== $dir && $dirCnt < $min) {
                    continue;
                }
                $val = $this->g($n);
                if ($val === null) {
                    continue;
                }
                $ns = (string) $n;
                $q->insert([
                    $ns,
                    $d + $val,
                    $newDir === $dir ? $dirCnt + 1 : 1,
                    $newDir,
                ], 10000 - ($d + $val));
            }
        }

        return $d;
    }

    private function g(Point $p): ?int
    {
        return $this->map[$p->y][$p->x] ?? null;
    }
}
