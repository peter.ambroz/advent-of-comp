<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;
use App\Y2023\Model\Hand;

class D07 extends Day
{
    public function run(): void
    {
        $this->doRun(false);
    }

    public function runB(): void
    {
        $this->doRun(true);
    }

    private function doRun(bool $jokers): void
    {
        $ha = [];
        foreach ($this->inputLines(fn ($l) => explode(' ', $l)) as $hand) {
            $ha[] = new Hand($hand[0], (int) $hand[1], $jokers);
        }

        usort($ha, function (Hand $a, Hand $b) {
            $tc = $a->type <=> $b->type;
            if ($tc !== 0) {
                return $tc;
            }

            return strcmp($a->cards, $b->cards);
        });

        $sum = 0;
        foreach ($ha as $i => $hand) {
            $sum += $hand->win * ($i + 1);
        }
        printf("%d\n", $sum);
    }
}
