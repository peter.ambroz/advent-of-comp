<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;
use SplQueue;

class D22 extends Day
{
    private array $data;
    private array $supp;

    public function run(): void
    {
        $this->parseAndSettle();

        $cnt = 0;
        foreach ($this->data as $i => $brick) {
            $ok = true;
            foreach (array_keys($this->supp[$i][1]) as $dep) {
                if (count($this->supp[$dep][0]) === 1) {
                    $ok = false;
                    break;
                }
            }
            if ($ok) {
                $cnt++;
            }
        }

        printf("%d\n", $cnt);
    }

    public function runB(): void
    {
        $this->parseAndSettle();

        $falls = 0;
        foreach ($this->data as $i => $brick) {
            $falls += $this->fall($i);
        }

        printf("%d\n", $falls);
    }

    private function fall(int $i): int
    {
        $f = -1;
        $supp = $this->supp;
        $q = new SplQueue();
        $q->enqueue($i);

        while (!$q->isEmpty()) {
            $n = $q->dequeue();
            if (!isset($supp[$n])) {
                continue;
            }
            foreach (array_keys($supp[$n][1]) as $item) {
                unset($supp[$item][0][$n]);
            }
            unset($item);
            unset($supp[$n]);
            $f++;

            foreach ($supp as $c => $item) {
                if (!count($item[0])) {
                    $q->enqueue($c);
                }
            }
        }

        return $f;
    }

    private function parseAndSettle(): void
    {
        $well = array_fill(0, 10, array_fill(0, 10, [0, -1]));
        $data = $this->inputLines(function ($line) {
            [$p1, $p2] = explode('~', $line);
            $p1 = array_map(fn($x)=>(int)$x, explode(',', $p1));
            $p2 = array_map(fn($x)=>(int)$x, explode(',', $p2));
            return [$p1, $p2];
        });
        $supp = array_fill(0, count($data), [[], []]);
        usort($data, fn ($a, $b) => $a[0][2] <=> $b[0][2]);
        foreach ($data as $i => $brick) {
            $max = -1;
            $floor = [];
            for ($x = $brick[0][0]; $x <= $brick[1][0]; $x++) {
                for ($y = $brick[0][1]; $y <= $brick[1][1]; $y++) {
                    $w = $well[$y][$x];
                    if ($max === $w[0]) {
                        $floor[$w[1]] = true;
                    }
                    if ($max < $w[0]) {
                        $max = $w[0];
                        $floor = [$w[1] => true];
                    }
                }
            }

            for ($x = $brick[0][0]; $x <= $brick[1][0]; $x++) {
                for ($y = $brick[0][1]; $y <= $brick[1][1]; $y++) {
                    $top = $brick[1][2] - ($brick[0][2] - $max - 1);
                    $well[$y][$x] = [$top, $i];
                }
            }

            $supp[$i][0] = $floor;
            foreach (array_keys($floor) as $x) {
                if ($x === -1) {
                    continue;
                }
                $supp[$x][1][$i] = true;
            }
        }

        $this->data = $data;
        $this->supp = $supp;
    }
}
