<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;
use App\Y2023\Model\Module;
use SplQueue;

class D20 extends Day
{
    private const BEGIN = 'roadcaster'; // initial b is parsed as type
    private const END = 'rx';

    /** @var array<string, Module> */
    private array $mod;

    public function run(): void
    {
        $this->parse();
        [$sigcount, ] = $this->doRun(1000);

        printf("%d\n", $sigcount[0] * $sigcount[1]);
    }

    public function runB(): void
    {
        $mul = 1;
        for ($k = 0; $k < 4; $k++) {
            $this->parse($k);
            [, $trigger] = $this->doRun(10000);

            $mul *= $trigger[1] - $trigger[0];
        }

        printf("%d\n", $mul);
    }

    private function parse(?int $limit = null): void
    {
        $this->mod = [];
        $back = [];
        foreach ($this->inputLines(function ($x) use ($limit) {
            [$name, $rest] = explode(' -> ', $x);
            $type = $name[0];
            $label = substr($name, 1);
            $next = explode(', ', $rest);
            if ($label === self::BEGIN && $limit !== null) {
                $next = [$next[$limit]];
            }
            return new Module($type, $label, $next);
        }) as $m) {
            assert($m instanceof Module);
            $ml = $m->label;
            $this->mod[$ml] = $m;
            foreach ($m->next as $n) {
                if (!isset($back[$n])) {
                    $back[$n] = [];
                }
                $back[$n][] = $ml;
            }
        }

        $lastAnd = $back[self::END][0];

        foreach ($back as $n => $bm) {
            if (!isset($this->mod[$n])) {
                continue;
            }
            $initValue = false;
            if ($n === $lastAnd && $limit !== null) {
                $initValue = true;
            }
            foreach ($bm as $ml) {
                $this->mod[$n]->addBack($ml, $initValue);
            }
        }
    }

    /**
     * @return array<array<int>>
     */
    public function doRun(int $reps): array
    {
        $sigcount = [0, 0];
        $trigger = [];
        $log = new SplQueue();

        for ($i = 0; $i < $reps; $i++) {
            $log->enqueue([self::BEGIN, false, '']);
            $sigcount[0]++;
            while (!$log->isEmpty()) {
                [$to, $sig, $from] = $log->dequeue();
                if ($to === self::END) {
                    if (!$sig) {
                        $trigger[] = $i;
                    }
                    continue;
                }
                $ret = $this->mod[$to]->signal($from, $sig);
                if ($ret !== null) {
                    foreach ($this->mod[$to]->next as $nxt) {
                        $log->enqueue([$nxt, $ret, $to]);
                        $sigcount[(int)$ret]++;
                    }
                }
            }
        }

        return [$sigcount, $trigger];
    }
}
