<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;
use App\Y2023\Model\Range;
use App\Y2023\Model\SeedMap;
use App\Y2023\Model\SeedMapCollection;

class D05 extends Day
{
    /** @var int[] */
    private array $seeds = [];

    /** @var array<SeedMapCollection> */
    private array $maps = [];

    public function run(): void
    {
        $this->parse();

        $bestLoc = null;
        foreach ($this->seeds as $seed) {
            foreach ($this->maps as $mapCollection) {
                $seed = $mapCollection->map($seed);
            }

            if ($bestLoc === null || $seed < $bestLoc) {
                $bestLoc = $seed;
            }
        }

        printf("best location: %d\n", $bestLoc);
    }

    public function runB(): void
    {
        $this->parse();

        $ranges = array_map(fn ($x) => new Range($x[0], $x[1]), array_chunk($this->seeds, 2));
        $newRanges = [];
        foreach ($this->maps as $mapCollection) {
            foreach ($ranges as $range) {
                $newRanges = array_merge($newRanges, $mapCollection->mapRange($range));
            }

            $ranges = $newRanges;
            $newRanges = [];
        }

        $min = null;
        foreach ($ranges as $range) {
            if ($min === null || $range->start < $min) {
                $min = $range->start;
            }
        }

        printf("best location: %d\n", $min);
    }

    private function parse(): void
    {
        $mapPtr = -1;
        foreach($this->inputLines() as $i => $line) {
            if ($i === 0) {
                $this->seeds = array_map(fn ($x) => (int) $x, explode(' ', $line));
                continue;
            }

            $data = explode(' ', $line);
            if (count($data) < 2) {
                continue;
            }

            if (count($data) < 3) {
                $this->maps[++$mapPtr] = new SeedMapCollection($data[0]);
                continue;
            }

            $this->maps[$mapPtr]->add(new SeedMap((int)$data[0], (int)$data[1], (int)$data[2]));
        }
    }
}
