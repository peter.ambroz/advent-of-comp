<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;
use App\Model\Point;

class D11 extends Day
{
    /** @var Point[] */
    private array $map = [];

    public function run(): void
    {
        $this->doRun(1);
    }

    public function runB(): void
    {
        $this->doRun(999999);
    }

    private function doRun(int $expansion): void
    {
        $this->parse($expansion);

        $dist = 0;
        foreach ($this->map as $i1 => $p1) {
            foreach ($this->map as $i2 => $p2) {
                if ($i1 >= $i2) {
                    continue;
                }

                $dist += $p1->distance($p2);
            }
        }

        printf("%d\n", $dist);
    }

    private function parse(int $expansion): void
    {
        $map = [];
        $dupX = [];
        $addY = [];
        $addX = [];
        $lastY = 0;
        foreach ($this->inputLines() as $y => $line) {
            $chars = array_map(fn ($x) => (int) $x, str_split(strtr($line, ['#' => '1', '.' => '0'])));
            if (array_sum($chars) === 0) {
                $lastY += $expansion;
            }
            $addY[$y] = $lastY;

            if ($y === 0) {
                $dupX = array_fill(0, count($chars), true);
            }
            foreach ($chars as $x => $char) {
                if ($char === 1) {
                    $map[] = new Point($x, $y);
                    $dupX[$x] = false;
                }
            }
        }

        $lastX = 0;
        foreach ($dupX as $x => $val) {
            if ($val) {
                $lastX += $expansion;
            }
            $addX[$x] = $lastX;
        }

        foreach ($map as $p) {
            $this->map[] = new Point($p->x + $addX[$p->x], $p->y + $addY[$p->y]);
        }
    }
}
