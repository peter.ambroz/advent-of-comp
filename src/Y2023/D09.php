<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;

class D09 extends Day
{
    public function run(): void
    {
        $sum = array_sum(array_map(fn ($x) => $x[1], $this->inputLines($this->process(...))));
        printf("%d\n", $sum);
    }

    public function runB(): void
    {
        $sum = array_sum(array_map(fn ($x) => $x[0], $this->inputLines($this->process(...))));
        printf("%d\n", $sum);
    }

    private function process(string $line): array
    {
        $data = [array_map(fn($x)=>(int)$x, explode(' ', $line))];
        $dc = 1;
        while (array_sum($data[$dc - 1]) !== 0) {
            $diffs = [];
            for ($i = 0; $i < count($data[$dc - 1]) - 1; $i++) {
                $diffs[] = $data[$dc - 1][$i + 1] - $data[$dc - 1][$i];
            }
            $data[] = $diffs;
            $dc++;
        }

        $data[$dc - 1][] = 0;

        $last = 0;
        $first = 0;
        for ($i = $dc - 2; $i >= 0; $i--) {
            $data[$i][] = $last = $data[$i][count($data[$i]) - 1] + $data[$i + 1][count($data[$i + 1]) - 1];
        }

        for ($i = $dc - 2; $i >= 0; $i--) {
            $first = $data[$i][0] - $data[$i + 1][0];
            array_unshift($data[$i], $first);
        }

        return [$first, $last];
    }
}
