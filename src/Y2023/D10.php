<?php

declare(strict_types=1);

namespace App\Y2023;

use App\Day;
use App\Model\Point;
use App\Y2023\Model\Pipe;
use Exception;

class D10 extends Day
{
    /** @var string[] */
    private array $map;
    /** @var array<string, bool> */
    private array $loop;

    /**
     * @throws Exception
     */
    public function run(): void
    {
        $this->parse();
        $this->followPipe(new Point(40, 51));

        printf("%d\n", intdiv(count($this->loop), 2));
    }

    /**
     * @throws Exception
     */
    public function runB(): void
    {
        $this->parse();
        $this->followPipe(new Point(40, 51));
        $outside = $this->fillOutside(new Point(0, 0));
        $outside += $this->markOutsideAlongPipe(new Point(57, 1));
        $pipe = count($this->loop);
        $total = count($this->map) * strlen($this->map[0]);

        printf("%d\n", $total - $outside - $pipe);
    }

    private function parse(): void
    {
        $this->map = [''];
        foreach ($this->inputLines() as $line) {
            $this->map[] = '.' . $line . '.';
        }
        $this->map[0] = str_repeat('.', strlen($this->map[1]));
        $this->map[] = str_repeat('.', strlen($this->map[1]));
    }

    /**
     * @throws Exception
     */
    private function followPipe(Point $start): void
    {
        $p = $start;
        $dir = Pipe::directions($this->get($p))[1];
        do {
            $p = $p->neighbors()[$dir];
            $dir = Pipe::nextDirection($this->get($p), $dir);
            $this->loop[(string) $p] = true;
        } while (!$start->equals($p));
    }

    /**
     * @throws Exception
     */
    private function markOutsideAlongPipe(Point $start): int
    {
        $outCount = 0;
        $dir = Pipe::E;
        $p = $start;
        do {
            $cur = $this->get($p);
            $sides = array_map(fn ($x) => $p->neighbors()[$x], Pipe::leftSide($cur, $dir));
            foreach ($sides as $side) {
                $outCount += $this->fillOutside($side);
            }

            $p = $p->neighbors()[$dir];
            $dir = Pipe::nextDirection($this->get($p), $dir);
        } while (!$start->equals($p));

        return $outCount;
    }

    private function get(Point $p): ?string
    {
        if ($p->x < 0) {
            return null;
        }
        return $this->map[$p->y][$p->x] ?? null;
    }

    private function set(Point $p): void
    {
        $this->map[$p->y][$p->x] = 'O';
    }

    private function isMainPipe(Point $p): bool
    {
        return $this->loop[(string) $p] ?? false;
    }

    private function fillOutside(Point $seed): int
    {
        if ($this->isMainPipe($seed)) {
            return 0;
        }

        $cur = $this->get($seed);
        if ($cur === null || $cur === 'O') {
            return 0;
        }

        $this->set($seed);
        $filled = 1;

        foreach ($seed->neighbors() as $n) {
            $filled += $this->fillOutside($n);
        }

        return $filled;
    }
}
