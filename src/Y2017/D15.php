<?php

namespace App\Y2017;

use App\Day;
use Iterator;

class D15 extends Day
{
    private const ASEED = 591;
    private const BSEED = 393;
    private const AMUL = 16807;
    private const BMUL = 48271;

    public function run(): void
    {
        $ag = $this->generator(self::ASEED, self::AMUL, 0, 40000000);
        $bg = $this->generator(self::BSEED, self::BMUL, 0, 40000000);

        $jud = 0;

        while ($ag->valid() && $bg->valid()) {
            $av = $ag->current();
            $bv = $bg->current();

            if (($av & 0xffff) === ($bv & 0xffff)) {
                $jud++;
            }

            $ag->next();
            $bg->next();
        }

        printf("%d\n", $jud);
    }

    public function runB(): void
    {
        $ag = $this->generator(self::ASEED, self::AMUL, 3, 4000000000);
        $bg = $this->generator(self::BSEED, self::BMUL, 7, 4000000000);

        $jud = 0;

        for ($i = 0; $i < 5000000; $i++) {
            $av = $ag->current();
            $bv = $bg->current();

            if (($av & 0xffff) === ($bv & 0xffff)) {
                $jud++;
            }

            $ag->next();
            $bg->next();
        }

        printf("%d\n", $jud);
    }

    private function generator(int $seed, int $multiplier, int $mask, int $iterations): Iterator
    {
        for ($i = 0; $i < $iterations; $i++) {
            $seed = ($seed * $multiplier) % 2147483647;
            if (($seed & $mask) === 0) {
                yield $seed;
            }
        }
    }
}