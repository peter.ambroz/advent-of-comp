<?php

namespace App\Y2017;

use App\Day;

class D12 extends Day
{
    public function run(): void
    {
        $group = [];
        for ($i = 0; $i < 2000; $i++) {
            $group[$i] = $i;
        }

        $this->inputLines(function ($line) use (&$group) {
            [$id, $parts] = explode(' <-> ', $line);

            $nums = array_map(fn ($num) => (int) $num, explode(', ', $parts));
            $nums[] = (int) $id;

            $lowGid = 10000;
            $gids = [];
            foreach ($nums as $num) {
                $gid = $group[$num];
                $gids[$gid] = $gid;
                if ($gid < $lowGid) {
                    $lowGid = $gid;
                }
            }

            foreach ($group as $n => &$g) {
                if (in_array($g, $gids)) {
                    $g = $lowGid;
                }
            }
            unset($g);
        });

        var_dump($group);
        $zgid = $group[0];
        $zcnt = 0;

        $uniq = [];
        $ugid = 0;

        foreach ($group as $num => $gid) {
            if ($gid === $zgid) {
                $zcnt++;
            }

            if (!isset($uniq[$gid])) {
                $uniq[$gid] = 1;
                $ugid++;
            }
        }

        printf("%d\n", $zcnt);

        printf("%d\n", $ugid);
    }
}