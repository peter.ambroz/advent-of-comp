<?php
declare(strict_types=1);

namespace App\Y2017;

use App\Day;

class D02 extends Day
{
    public function run(): void
    {
        $sheet = $this->inputLines(function (string $line): array {
            return array_map(function (string $data): int {
                return (int)$data;
            }, explode(' ', $line));
        });

        $sum = 0;
        foreach ($sheet as $line) {
            $min = 999999;
            $max = -1;
            foreach ($line as $num) {
                if ($num > $max) {
                    $max = $num;
                }
                if ($num < $min) {
                    $min = $num;
                }
            }
            $sum += $max - $min;
        }

        printf("%d\n", $sum);
    }

    public function runB(): void
    {
        $sheet = $this->inputLines(function (string $line): array {
            return array_map(function (string $data): int {
                return (int)$data;
            }, explode(' ', $line));
        });

        $sum = 0;
        foreach ($sheet as $line) {
            foreach ($line as $i1 => $num) {
                foreach ($line as $i2 => $num2) {
                    if ($i2 !== $i1 && $num !== 0) {
                        if ($num2 % $num === 0) {
                            $sum += $num2 / $num;
                        }
                    }
                }
            }
        }

        printf("%d\n", $sum);
    }

}