<?php

namespace App\Y2017;

use App\Day;

class D16 extends Day
{
    private string $prg;
    private int $ptr = 0;
    private int $mod;
    private array $moves;

    public function run(): void
    {
        $this->init();
        $this->moves = $this->inputLines(fn ($line) => explode(',', $line))[0];

        foreach ($this->moves as $move) {
            $this->move($move);
        }

        $this->normalize();

        printf("%s\n", $this->prg);
    }

    public function runB(): void
    {
        $this->init();
        $this->moves = $this->inputLines(fn ($line) => explode(',', $line))[0];

        $perm1 = $this->findPermutation(['s', 'x']);
        $this->init();
        $perm2 = $this->findPermutation(['p']);
        $this->init();
        $prg = strtr($this->prg, $perm1);
        printf("%s\n", strtr($prg, $perm2));
    }

    private function move(string $move, array $restrict = ['s', 'x', 'p']): void
    {
        $type = substr($move, 0, 1);
        $rest = substr($move, 1);

        switch ($type) {
            case 's':
                if (!in_array('s', $restrict)) {
                    break;
                }
                $this->rotateRight((int) $rest);

                break;
            case 'x':
                if (!in_array('x', $restrict)) {
                    break;
                }
                $this->normalize();

                [$pos1, $pos2] = array_map(fn ($p) => (int) $p, explode('/', $rest, 2));
                [$prg1, $prg2] = array_map(fn ($p) => $this->prg[$p], [$pos1, $pos2]);

                $this->prg[$pos1] = $prg2;
                $this->prg[$pos2] = $prg1;

                break;
            case 'p':
                if (!in_array('p', $restrict)) {
                    break;
                }
                [$prg1, $prg2] = explode('/', $rest, 2);
                $this->prg = strtr($this->prg, [$prg1 => $prg2, $prg2 => $prg1]);

                break;
        }
    }

    private function init(): void
    {
        $this->prg = 'abcdefghijklmnop';
        $this->mod = strlen($this->prg);
    }

    private function rotateRight(int $steps): void
    {
        $this->ptr -= $steps;
        while ($this->ptr < 0) {
            $this->ptr += $this->mod;
        }
    }

    private function normalize(): void
    {
        $this->prg = substr($this->prg, $this->ptr) . substr($this->prg, 0, $this->ptr);
        $this->ptr = 0;
    }

    private function findPermutation(array $restrict): array
    {
        $start = $this->prg;
        foreach ($this->moves as $move) {
            $this->move($move, $restrict);
        }
        $this->normalize();
        $stop = $this->prg;
        $x = $start;

        for ($exp = 0; $exp < 3; $exp++) {
            $perm = array_combine(str_split($start), str_split($stop));

            for ($i = 0; $i < 1000; $i++) {
                $x = strtr($x, $perm);
            }
            $stop = $x;
        }

        return array_combine(str_split($start), str_split($stop));
    }
}
