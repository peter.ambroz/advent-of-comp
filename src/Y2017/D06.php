<?php
declare(strict_types=1);

namespace App\Y2017;

use App\Day;

class D06 extends Day
{
    private const MEM = [11, 11, 13, 7, 0, 15, 5, 5, 4, 4, 1, 1, 7, 1, 15, 11];

    private array $state = [];

    private array $cur = [];

    private function highestIdx(): int
    {
        $max = -9999;
        $maxi = -1;
        foreach ($this->cur as $id => $num) {
            if ($num > $max) {
                $max = $num;
                $maxi = $id;
            }
        }
        return $maxi;
    }

    private function redistribute(): void
    {
        $top = $this->highestIdx();
        $topAmt = $this->cur[$top];
        $this->cur[$top] = 0;
        for ($i = 1; $i <= $topAmt; $i++) {
            $this->cur[($top + $i) % 16]++;
        }
    }

    private function exists(): bool
    {
        foreach ($this->state as $s) {
            if ($this->cur === $s) {
                return true;
            }
        }
        return false;
    }

    private function existsB(): bool
    {
        if ($this->cur === $this->state[0]) {
            return true;
        }
        return false;
    }

    public function run(): void
    {
        $this->cur = self::MEM;
        $iter = 0;
        do {
            $this->state[] = $this->cur;
            $this->redistribute();
            $iter++;
        } while (!$this->exists());

        printf("%d %s\n", $iter, implode(', ', $this->cur));
    }

    public function runB(): void
    {
        $this->cur = [1, 0, 14, 14, 12, 12, 10, 10, 8, 8, 6, 6, 4, 3, 2, 1];

        $iter = 0;
        do {
            $this->state[] = $this->cur;
            $this->redistribute();
            $iter++;
        } while (!$this->existsB());

        printf("%d\n", $iter);
    }
}