<?php

declare(strict_types=1);

namespace App\Y2017;

use App\Day;

class D17 extends Day
{
    private const LOOP = 382;

    private int $cur = 0;
    private array $data = [0];

    public function run(): void
    {
        for ($i = 1; $i <= 2017; $i++) {
            $this->cur = ($this->cur + self::LOOP) % count($this->data);

            array_splice($this->data, ++$this->cur, 0, $i);
        }

        $loc = ($this->cur + 1) % count($this->data);
        printf("%d\n", $this->data[$loc]);
    }

    public function runB(): void
    {
        $size = 1;
        $last = 0;
        for ($i = 1; $i <= 50000000; $i++) {
            $this->cur = ($this->cur + self::LOOP) % $size;

            if ($this->cur === 0) {
                $last = $i;
            }

            $this->cur++;
            $size++;
        }

        printf("%d\n", $last);
    }
}
