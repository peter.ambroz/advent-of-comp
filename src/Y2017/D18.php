<?php

declare(strict_types=1);

namespace App\Y2017;

use App\Day;

class D18 extends Day
{
    private int $sound = 0;

    public function run(): void
    {
        $data = $this->inputLines();
        $vm = new Vm($data, 0, true);
        $vm->run();

        $outbox = $vm->getOutbox();
        $last = array_pop($outbox);
        printf("%d\n", $last);
    }

    public function runB(): void
    {
        $data = $this->inputLines();
        $vm = [
            new Vm($data, 0),
            new Vm($data, 1),
        ];
        $avm = 0;

        while (!$vm[0]->isStuck() || !$vm[1]->isStuck()) {
            $other = 1 - $avm;
            $vm[$avm]->addInbox($vm[$other]->getOutbox());
            $vm[$avm]->run();

            $avm = $other;
        }

        printf("%d\n", $vm[1]->getSendCounter());
    }
}
