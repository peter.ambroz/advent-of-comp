<?php

declare(strict_types=1);

namespace App\Y2017;

use App\Day;
use Exception;

class D23 extends Day
{
    private Coprocessor $copr;

    public function run(): void
    {
        $this->copr = new Coprocessor($this->inputLines(function (string $line) {
            return explode(' ', $line, 3);
        }));

        while (true) {
            try {
                $this->copr->run();
            } catch (Exception $e) {
                printf("Coprocessor: %s\n", $e->getMessage());
                break;
            }
        }

        printf("muls: %d\n", $this->copr->muls);
        printf("h: %d\n", $this->copr->getReg('h'));
    }

    public function runB(): void
    {
        $this->copr = new Coprocessor($this->inputLines(function (string $line) {
            return explode(' ', $line, 3);
        }, '_optimum'));
        $this->copr->setReg('a', 1);
/*
        while (true) {
            try {
                $this->copr->run();
            } catch (Exception $e) {
                printf("Coprocessor: %s\n", $e->getMessage());
                break;
            }
        }

        printf("h: %d\n", $this->copr->getReg('h'));
*/
        printf("h: %d\n", $this->bcode());
    }

    private function bcode(): int
    {
        $h = 0;
        $b = 108400;
        $c = 125400;

        while (true) {
            $f = true;
            $d = 2;

            do {
                if ($b % $d === 0) {
                    $f = false;
                }

                $d++;
            } while ($d !== $b);

            if (!$f) {
                $h++;
            }

            if ($b === $c) {
                return $h;
            }

            $b += 17;
        }
    }
}
