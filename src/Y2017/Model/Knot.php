<?php

namespace App\Y2017\Model;

class Knot
{
    private const SALT = [17, 31, 73, 47, 23];
    private const ROUNDS = 64;

    private int $skip;

    private int $current;

    /** @var int[] */
    private array $data;

    /** @var int[] */
    private array $input;

    public function hash(string $input, bool $binary = false): string
    {
        $this->init();

        $this->input = array_merge(
            unpack('C*', $input),
            self::SALT
        );

        for ($i = 0; $i < self::ROUNDS; $i++) {
            $this->round();
        }

        $result = pack('C*', ...$this->compact());

        if ($binary) {
            return $result;
        }

        return bin2hex($result);
    }

    private function round(): void
    {
        $current = $this->current;
        $skip = $this->skip;

        foreach ($this->input as $input) {
            $data = $this->data;

            for ($i = 0; $i < $input; $i++) {
                $im = ($current + $i) % 256;
                $ism = ($current + ($input - $i - 1)) % 256;

                $this->data[$im] = $data[$ism];
            }

            $current = ($current + $input + $skip) % 256;
            $skip++;
        }

        $this->current = $current;
        $this->skip = $skip % 256;
    }

    /**
     * @return int[]
     */
    private function compact(): array
    {
        $xo = 0;
        $output = [];
        foreach ($this->data as $i => $byte) {
            $xo ^= $byte;
            if ($i % 16 === 15) {
                $output[] = $xo;
                $xo = 0;
            }
        }

        return $output;
    }

    private function init(): void
    {
        $this->skip = 0;
        $this->current = 0;
        $this->data = range(0, 255);
    }
}