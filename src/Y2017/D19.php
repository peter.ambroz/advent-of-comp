<?php

declare(strict_types=1);

namespace App\Y2017;

use App\Day;

class D19 extends Day
{
    /** @var string[][] */
    private array $maze;

    /** @var int[] */
    private array $dir = [0, 1]; // down

    private array $pos;

    private string $letters = '';

    private int $steps = 0;

    public function run(): void
    {
        $this->maze = $this->inputLines(str_split(...));
        $this->pos = [$this->findStart(), 0];

        do {
            $ret = $this->follow();
        } while ($ret);

        printf("%s\nsteps: %d\n", $this->letters, $this->steps);
    }

    public function runB(): void
    {
        $this->run();
    }

    /**
     * @throws \Exception
     */
    private function findStart(): int
    {
        foreach ($this->maze[0] as $y => $char) {
            if ($char !== ' ') {
                return $y;
            }
        }

        throw new \Exception('No starting point');
    }

    private function follow(): bool
    {
        $char = $this->maze[$this->pos[1]][$this->pos[0]];

        switch ($char) {
            case ' ': return false;
            case '|':
            case '-':
                break;

            case '+':
                if (!$this->findNext()) {
                    return false;
                }
                break;

            default: // letter
                $this->letters .= $char;
        }

        $this->steps++;

        $this->pos[0] += $this->dir[0];
        $this->pos[1] += $this->dir[1];

        return true;
    }

    private function findNext(): bool
    {
        if ($this->dir[0] === 0) {
            $possible = [[1, 0], [-1, 0]];
        } else {
            $possible = [[0, 1], [0, -1]];
        }

        foreach ($possible as $dir) {
            $char = $this->maze[$this->pos[1] + $dir[1]][$this->pos[0] + $dir[0]] ?? ' ';
            if ($char !== ' ') {
                $this->dir = $dir;
                return true;
            }
        }

        return false;
    }
}