<?php

declare(strict_types=1);

namespace App\Y2017;

use App\Day;

class D24 extends Day
{
    public function run(): void
    {
        $ports = $this->inputLines(function ($line) {
            [$s1, $s2] = explode('/', $line);

            return new Port((int) $s1, (int) $s2);
        });

        printf("best bridge: %d\n", $this->bestBridge($ports));
    }

    public function runB(): void
    {
        $ports = $this->inputLines(function ($line) {
            [$s1, $s2] = explode('/', $line);

            return new Port((int) $s1, (int) $s2);
        });

        $br = $this->longestBridge($ports);

        printf("longest bridge (length, strength): %d, %d\n", $br[0], $br[1]);
    }

    /**
     * @param Port[] $bridge
     * @return Port[]
     */
    private function bestBridge(array $freePorts, array $bridge = [], int $freePins = 0, int $currentSize = 0): int
    {
        $best = $currentSize;

        foreach ($this->findAllPorts($freePins, $freePorts) as $i => $port) {
            $newBridge = $bridge;
            $newBridge[] = $port;
            $newFreePorts = $freePorts;
            unset($newFreePorts[$i]);

            $size = $this->bestBridge(
                $newFreePorts,
                $newBridge,
                $port->freeSide($freePins),
                $currentSize + $port->getSize(),
            );

            if ($size > $best) {
                $best = $size;
            }
        }

        return $best;
    }

    private function longestBridge(
        array $freePorts,
        array $bridge = [],
        int $freePins = 0,
        int $currentLength = 0,
        int $currentStrength = 0,
    ): array {
        $bestLength = $currentLength;
        $bestStrength = $currentStrength;

        foreach ($this->findAllPorts($freePins, $freePorts) as $i => $port) {
            $newBridge = $bridge;
            $newBridge[] = $port;
            $newFreePorts = $freePorts;
            unset($newFreePorts[$i]);

            [$l, $s] = $this->longestBridge(
                $newFreePorts,
                $newBridge,
                $port->freeSide($freePins),
                $currentLength + 1,
                $currentStrength + $port->getSize(),
            );

            if ($l === $bestLength) {
                if ($s > $bestStrength) {
                    $bestStrength = $s;
                }
            } elseif ($l > $bestLength) {
                $bestLength = $l;
                $bestStrength = $s;
            }
        }

        return [$bestLength, $bestStrength];
    }

    /**
     * @return Port[]
     */
    private static function findAllPorts(int $pinCount, array $ports): array
    {
        $found = [];
        foreach ($ports as $i => $port) {
            if ($port->hasSide($pinCount)) {
                $found[$i] = $port;
            }
        }
        return $found;
    }
}
