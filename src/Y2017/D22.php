<?php

declare(strict_types=1);

namespace App\Y2017;

use App\Day;
use App\Model\Point;

class D22 extends Day
{
    private array $map = [];
    private Point $pos;
    private int $dir;
    private array $dirs;

    private const CLEAN = 0;
    private const INFECTED = 1;
    private const WEAKENED = 2;
    private const FLAGGED = 3;

    public function __construct()
    {
        $this->dirs = [
            new Point(0, -1),
            new Point(1, 0),
            new Point(0, 1),
            new Point(-1, 0),
        ];
    }

    public function run(): void
    {
        $this->initMap();
        $infected = 0;
        for ($i = 0; $i < 10000; $i++) {
            $infected += $this->go();
        }

        printf("infected: %d\n", $infected);
    }

    public function runB(): void
    {
        $this->initMap();
        $infected = 0;
        for ($i = 0; $i < 10000000; $i++) {
            $infected += $this->goB();
        }

        printf("infected: %d\n", $infected);
    }

    private function initMap(): void
    {
        $xsize = 0;
        $ysize = 0;

        foreach ($this->inputLines(str_split(...)) as $y => $line) {
            $ysize = $y;
            foreach ($line as $x => $char) {
                $xsize = $x;
                if ($char === '#') {
                    $this->map[(string) new Point($x, $y)] = self::INFECTED;
                }
            }
        }

        $this->pos = new Point(intdiv($xsize, 2), intdiv($ysize, 2));
        $this->dir = 0;
    }

    private function go(): int
    {
        $key = (string) $this->pos;
        $infected = $this->map[$key] ?? self::CLEAN;
        if ($infected) {
            $this->dir += 1;
            $this->map[$key] = self::CLEAN;
            $result = 0;
        } else {
            $this->dir += 3;
            $this->map[$key] = self::INFECTED;
            $result = 1;
        }
        $this->dir %= 4;
        $this->pos = $this->pos->add($this->dirs[$this->dir]);

        return $result;
    }

    private function goB(): int
    {
        $key = (string) $this->pos;
        $state = $this->map[$key] ?? self::CLEAN;

        $result = 0;
        switch ($state) {
            case self::CLEAN:
                $this->dir += 3;
                $this->map[$key] = self::WEAKENED;
                break;
            case self::WEAKENED:
                $this->map[$key] = self::INFECTED;
                $result = 1;
                break;
            case self::INFECTED:
                $this->dir += 1;
                $this->map[$key] = self::FLAGGED;
                break;
            case self::FLAGGED:
                $this->dir += 2;
                $this->map[$key] = self::CLEAN;
                break;
        }

        $this->dir %= 4;
        $this->pos = $this->pos->add($this->dirs[$this->dir]);

        return $result;
    }
}
