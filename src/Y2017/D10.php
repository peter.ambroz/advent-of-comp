<?php
declare(strict_types=1);

namespace App\Y2017;

use App\Day;
use App\Y2017\Model\Knot;

class D10 extends Day
{
    /** @var int[] */
    private array $data;

    private int $cur;

    private function process(int $len, int $skip): void
    {
        $nd = $this->data;
        for ($i = 0; $i < $len; $i++) {
            $im = ($this->cur + $i) % 256;
            $ism = ($this->cur + ($len - $i - 1)) % 256;

            $nd[$im] = $this->data[$ism];
        }

        $this->data = $nd;
        $this->cur = ($this->cur + $len + $skip) % 256;
    }

    public function run(): void
    {
        $input = [14, 58, 0, 116, 179, 16, 1, 104, 2, 254, 167, 86, 255, 55, 122, 244];
        $this->data = range(0, 255);
        $this->cur = 0;

        foreach ($input as $skip => $len) {
            $this->process($len, $skip);
        }

        printf("%d\n", $this->data[0] * $this->data[1]);
    }

    public function runB(): void
    {
        $input = '14,58,0,116,179,16,1,104,2,254,167,86,255,55,122,244';
        $knot = new Knot();

        printf("%s\n", $knot->hash($input));
    }
}