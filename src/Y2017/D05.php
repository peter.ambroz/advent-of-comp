<?php
declare(strict_types=1);

namespace App\Y2017;

use App\Day;

class D05 extends Day
{
    public function run(): void
    {
        $instr = $this->inputLines(function (string $x): int { return (int)$x; });
        $instrL = count($instr);
        $ip = 0;
        $c = 0;
        while ($ip >= 0 && $ip < $instrL) {
            $ip += $instr[$ip]++;
            $c++;
        }

        printf("%d\n", $c);
    }

    public function runB(): void
    {
        $instr = $this->inputLines(function (string $x): int { return (int)$x; });
        $instrL = count($instr);
        $ip = 0;
        $c = 0;
        while ($ip >= 0 && $ip < $instrL) {
            $in = $instr[$ip];
            if ($instr[$ip] >= 3) {
                $instr[$ip]--;
            } else {
                $instr[$ip]++;
            }
            $ip += $in;

            $c++;
        }

        printf("%d\n", $c);
    }
}