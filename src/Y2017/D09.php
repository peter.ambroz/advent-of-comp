<?php
declare(strict_types=1);

namespace App\Y2017;

use App\Day;

class D09 extends Day
{
    private int $ncg = 0;

    public function parse(string $line): int
    {
        $l = 0;
        $s = 0;
        $g = false;
        $ig = false;
        $len = strlen($line);

        for ($i = 0; $i < $len; $i++) {
            if ($ig) {
                $ig = false;
                continue;
            }

            $ch = $line[$i];

            if ($g && !in_array($ch, ['>', '!'])) {
                $this->ncg++;
                continue;
            }

            switch ($ch) {
                case '{':
                    $l++;
                    break;
                case '}':
                    if ($l <= 0) {
                        throw new \Exception('Unmatched } @ ' . $i);
                    }
                    $s += $l--;
                    break;
                case '<':
                    $g = true;
                    break;
                case '>':
                    if (!$g) {
                        throw new \Exception('Unmatched > @ ' . $i);
                    }
                    $g = false;
                    break;
                case '!':
                    $ig = true;
                    break;
                default:
                    break;
            }
        }

        return $s;
    }

    public function run(): void
    {
        try {
            printf("%d\n", array_sum($this->inputLines([$this, 'parse'])));
        } catch (\Exception $e) {
            printf("%s\n", $e->getMessage());
        }
    }

    public function runB(): void
    {
        try {
            $this->inputLines([$this, 'parse']);
        } catch (\Exception $e) {
            printf("%s\n", $e->getMessage());
        }

        printf("%d\n", $this->ncg);
    }
}