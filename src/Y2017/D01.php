<?php

declare(strict_types=1);

namespace App\Y2017;

use App\Day;

class D01 extends Day
{
    public function run(): void
    {
        $sum = 0;
        $num = str_split(file_get_contents($this->inputFileName()));
        foreach ($num as $k => $v) {
            if (isset($num[$k+1])) {
                if ($num[$k+1] === $v) {
                    $sum += (int)$v;
                }
            }
        }

        printf("%d\n", $sum);
    }

    public function runB(): void
    {
        $sum = 0;
        $num = str_split(file_get_contents($this->inputFileName()));
        $cnt = count($num);
        $cnth = $cnt / 2;
        foreach ($num as $k => $v) {
            if ($num[($k+$cnth) % $cnt] === $v) {
                $sum += (int)$v;
            }
        }

        printf("%d\n", $sum);
    }
}