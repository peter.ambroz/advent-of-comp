<?php

namespace App\Y2017;

use App\Day;

class D11 extends Day
{
    private int $y;
    private int $z;

    private function dist(int $y, int $z): int
    {
        $az = abs($z);
        $ay = abs($y);
        if ($z !== 0 && $y !== 0) {
            if (($z > 0 && $y < 0) || ($z < 0 && $y > 0)) {
                return max($ay, $az);
            } else {
                return $ay + $az;
            }
        } elseif ($z === 0) {
            return $ay;
        } else {
            return $az;
        }
    }

    private function process(): int
    {
        $z = 0;
        $y = 0;
        $max = 0;
        $cmds = $this->inputLines(fn ($line) => explode(',', $line));
        foreach ($cmds[0] as $cmd) {
            switch ($cmd) {
                case 'n':
                    $y++;
                    break;
                case 'ne':
                    $z++;
                    break;
                case 'se':
                    $y--;
                    $z++;
                    break;
                case 's':
                    $y--;
                    break;
                case 'sw':
                    $z--;
                    break;
                case 'nw':
                    $y++;
                    $z--;
                    break;
            }

            $dist = $this->dist($y, $z);
            if ($dist > $max) {
                $max = $dist;
            }
        }
        $this->y = $y;
        $this->z = $z;

        return $max;
    }

    public function run(): void
    {
        $this->process();
        printf("%d\n", $this->dist($this->y, $this->z));
    }

    public function runB(): void
    {
        $max = $this->process();
        printf("%d\n", $max);
    }
}
