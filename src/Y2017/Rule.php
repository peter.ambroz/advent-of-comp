<?php

declare(strict_types=1);

namespace App\Y2017;

readonly class Rule
{
    public const SIZE_3 = 0;
    public const SIZE_2 = 1;

    private function __construct(
        public int $size,
        /** @var int[] */
        public array $inputs,
        public int $output,
    ) {
    }

    public static function fromString(string $rule): self
    {
        [$l, $r] = explode(' => ', $rule);
        $lparts = array_map(str_split(...), explode('/', $l));
        $rparts = strtr($r, ['/' => '', '#' => '1', '.' => '0']);

        $inputs = [];
        for ($i = 0; $i < 4; $i++) {
            $inputs[] = self::pixels2int($lparts);
            $lparts = self::rot90($lparts);
        }
        $lparts = array_reverse($lparts);
        for ($i = 0; $i < 4; $i++) {
            $inputs[] = self::pixels2int($lparts);
            $lparts = self::rot90($lparts);
        }

        return new self(
            count($lparts) === 2 ? self::SIZE_2 : self::SIZE_3,
            $inputs,
            bindec($rparts),
        );
    }

    private static function rot90(array $data): array
    {
        $newData = array_fill(0, count($data), array_fill(0, count($data), 0));

        foreach ($data as $y => $row) {
            foreach ($row as $x => $pixel) {
                $newData[$x][$y] = $pixel;
            }
        }

        return array_reverse($newData);
    }

    private static function pixels2int(array $pixels): int
    {
        return bindec(strtr(implode('', array_merge(...$pixels)), ['.' => '0', '#' => '1']));
    }
}
