<?php

declare(strict_types=1);

namespace App\Y2017;

use App\Y2018\Model\Point3D;

class Particle
{
    public function __construct(
        private Point3D $position,
        private Point3D $velocity,
        public readonly Point3D $acceleration,
    ) {
    }

    public function tick(): void
    {
        $this->velocity = $this->velocity->add($this->acceleration);
        $this->position = $this->position->add($this->velocity);
    }

    public function __toString(): string
    {
        return (string) $this->position;
    }
}
