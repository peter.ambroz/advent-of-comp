<?php
declare(strict_types=1);

namespace App\Y2017;

use App\Day;

class D25 extends Day
{
    private string $s;
    private int $p;
    private array $ts = [];
    private array $t = [];

    public function process(string $line): int
    {
        if (substr($line, 0, 2) === '//') {
            return 0;
        }

        [$state, $value, $write, $move, $nextState] = explode(',', $line);
        $value = (int) $value;
        $write = (int) $write;
        $move = (int) $move;

        if (!isset($this->ts[$state])) {
            $this->ts[$state] = [];
        }
        $this->ts[$state][$value] = [$write, $move, $nextState];

        return 1;
    }

    private function step(): void
    {
        $instr = $this->ts[$this->s][$this->t[$this->p] ?? 0];
        if ($instr[0] === 0) {
            unset($this->t[$this->p]);
        } else {
            $this->t[$this->p] = 1;
        }

        $this->s = $instr[2];
        $this->p += $instr[1];
    }

    public function run(): void
    {
        $this->inputLines([$this, 'process']);
        $steps = 12459852;
        $this->s = 'A';
        $this->p = 0;
        for ($i = 0; $i < $steps; $i++) {
            $this->step();
        }
        printf("%d\n", count($this->t));
    }
}