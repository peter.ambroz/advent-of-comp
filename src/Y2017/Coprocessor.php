<?php

declare(strict_types=1);

namespace App\Y2017;

use Exception;

class Coprocessor
{
    private array $reg;
    private int $ip;
    public int $muls;

    public function __construct(private readonly array $mem)
    {
        $this->reg = [
            'a' => 0,
            'b' => 0,
            'c' => 0,
            'd' => 0,
            'e' => 0,
            'f' => 0,
            'g' => 0,
            'h' => 0,
            '1' => 1,
        ];

        $this->ip = 0;
        $this->muls = 0;
    }

    /**
     * @throws Exception
     */
    public function run(): void
    {
        $instr = $this->mem[$this->ip] ?? null;
        if ($instr === null) {
            throw new Exception('HALTED!');
        }

        if (!is_numeric($instr[2])) {
            $src = $this->reg[$instr[2]];
        } else {
            $src = (int) $instr[2];
        }

        switch ($instr[0]) {
            case 'set':
                $this->reg[$instr[1]] = $src;
                break;
            case 'sub':
                $this->reg[$instr[1]] -= $src;
                break;
            case 'mul':
                $this->reg[$instr[1]] *= $src;
                $this->muls++;
                break;
            case 'jnz':
                if ($this->reg[$instr[1]] !== 0) {
                    $this->ip += ($src - 1);
                }
                break;
        }

        $this->ip++;
    }

    public function setReg(string $name, int $value): void
    {
        $this->reg[$name] = $value;
    }

    public function getReg(string $name): int
    {
        return $this->reg[$name];
    }
}
