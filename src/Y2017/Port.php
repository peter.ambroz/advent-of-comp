<?php

declare(strict_types=1);

namespace App\Y2017;

use LogicException;

readonly final class Port
{
    public function __construct(public int $side1, public int $side2)
    {
    }

    public function hasSide(int $side): bool
    {
        return $this->side1 === $side || $this->side2 === $side;
    }

    /**
     * @throws LogicException
     */
    public function freeSide(int $occupiedSide): int
    {
        if ($occupiedSide === $this->side1) {
            return $this->side2;
        } elseif ($occupiedSide === $this->side2) {
            return $this->side1;
        } else {
            throw new LogicException('No such side');
        }
    }

    public function getSize(): int
    {
        return $this->side1 + $this->side2;
    }
}
