<?php

namespace App\Y2017;

use App\Day;

class D13 extends Day
{
    public function run(): void
    {
        $fw = 0;

        $this->inputLines(function ($line) use (&$fw) {
            $data = explode(': ', $line);
            $id = $data[0] * 1;
            $range = ($data[1] * 2) - 2;

            if ($id % $range === 0) {
                $fw += $id * $data[1];
            }
        });

        printf("%d\n", $fw);
    }

    public function runB(): void
    {
        $fw = [];

        $this->inputLines(function ($line) use (&$fw) {
            $data = explode(': ', $line);
            $id = $data[0] * 1;
            $range = ($data[1] * 2) - 2;
            $fw[$id] = $range;
        });

        for ($i = 0; $i < 10000000; $i++) {
            foreach ($fw as $id => $range) {
                if (($id + $i) % $range === 0) {
                    continue 2;
                }
            }

            printf("%d\n", $i);
            break;
        }

    }
}