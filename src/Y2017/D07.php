<?php
declare(strict_types=1);

namespace App\Y2017;

use App\Day;

class D07 extends Day
{
    private array $fwd = [];
    private array $bk = [];


    public function load(string $line): bool
    {
        $parts = explode(' ', $line);

        $prog = array_shift($parts);
        $w = (int)array_shift($parts);

        $this->fwd[$prog] = [
            'w' => $w,
            'ch' => $parts,
        ];

        if (!isset($this->bk[$prog])) {
            $this->bk[$prog] = [];
        }

        foreach ($parts as $ch) {
            if (!isset($this->bk[$ch])) {
                $this->bk[$ch] = [];
            }
            $this->bk[$ch][] = $prog;
        }

        return true;
    }

    private function weight(string $root): int
    {
        $w = $this->fwd[$root]['w'];
        foreach ($this->fwd[$root]['ch'] as $child) {
            $w += $this->weight($child);
        }
        return $w;
    }

    public function run(): void
    {
        $this->inputLines([$this, 'load']);
        foreach ($this->bk as $x => $val) {
            if (empty($val)) {
                printf("%s\n", $x);
            }
        }
    }

    public function runB(): void
    {
        $root = 'aapssr';

        if (isset($GLOBALS['argv'][4])) {
            $root = $GLOBALS['argv'][4];
        }
        $this->inputLines([$this, 'load']);
        foreach ($this->fwd[$root]['ch'] as $child) {
            printf("%s: %d\n", $child, $this->weight($child));
        }
    }
}