<?php

declare(strict_types=1);

namespace App\Y2017;

class Vm
{
    public const OK = 0;
    public const HALTED = 1;

    /** @var array<string, int> */
    private array $reg;

    private int $ip = 0;

    /** @var int[] */
    private array $outbox = [];

    /** @var int[] */
    private array $inbox = [];

    private int $status = self::OK;

    private int $sendCounter = 0;

    public function __construct(private readonly array $data, int $pid = 0, private readonly bool $partA = false)
    {
        $this->reg['p'] = $pid;
    }

    public function run(): void
    {
        do {
            $ret = $this->execute($this->data[$this->ip] ?? '');
        } while ($ret === self::OK);

        $this->status = $ret;
    }

    /**
     * @param int[] $inbox
     */
    public function addInbox(array $inbox): void
    {
        $this->inbox = [...$this->inbox, ...$inbox];
    }

    /**
     * @return int[]
     */
    public function getOutbox(): array
    {
        $outbox = $this->outbox;
        $this->outbox = [];

        return $outbox;
    }

    public function getSendCounter(): int
    {
        return $this->sendCounter;
    }

    public function isStuck(): bool
    {
        return $this->status === self::HALTED && count($this->outbox) === 0;
    }

    private function execute(string $line): int
    {
        if (!strlen($line)) {
            return self::HALTED;
        }

        [$instr, $rest] = explode(' ', $line, 2);

        switch ($instr) {
            case 'snd':
                if (is_numeric($rest)) {
                    $this->outbox[] = (int)$rest;
                } else {
                    $this->outbox[] = $this->reg[$rest];
                }

                $this->sendCounter++;
                break;

            case 'set':
                [$dest, $src] = explode(' ', $rest);
                if (is_numeric($src)) {
                    $this->reg[$dest] = (int) $src;
                } else {
                    $this->reg[$dest] = $this->reg[$src];
                }

                break;

            case 'add':
                [$dest, $src] = explode(' ', $rest);
                if (is_numeric($src)) {
                    $this->reg[$dest] += (int) $src;
                } else {
                    $this->reg[$dest] += $this->reg[$src];
                }
                break;

            case 'mul':
                [$dest, $src] = explode(' ', $rest);
                if (is_numeric($src)) {
                    $this->reg[$dest] *= (int) $src;
                } else {
                    $this->reg[$dest] *= $this->reg[$src];
                }

                break;

            case 'mod':
                [$dest, $src] = explode(' ', $rest);
                if (is_numeric($src)) {
                    $this->reg[$dest] %= (int) $src;
                } else {
                    $this->reg[$dest] %= $this->reg[$src];
                }

                break;

            case 'jgz':
                [$dest, $src] = explode(' ', $rest);
                if (!is_numeric($dest)) {
                    $dest = $this->reg[$dest];
                } else {
                    $dest = (int) $dest;
                }

                if ($dest <= 0) {
                    break;
                }

                if (is_numeric($src)) {
                    $this->ip += (int) $src;
                } else {
                    $this->ip += $this->reg[$src];
                }

                return self::OK;

            case 'rcv':
                if (!$this->partA || $this->reg[$rest] !== 0) {
                    if (count($this->inbox) === 0) {
                        return self::HALTED;
                    }

                    $this->reg[$rest] = array_shift($this->inbox);
                }

                break;
        }

        $this->ip++;

        return self::OK;
    }
}
