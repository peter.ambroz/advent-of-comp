<?php

declare(strict_types=1);

namespace App\Y2017;

use App\Day;
use App\Y2018\Model\Point3D;

class D20 extends Day
{
    /** @var Particle[] */
    private array $particles;

    public function run(): void
    {
        $this->parse();
        $minKey = 0;
        $minAccel = PHP_INT_MAX;

        foreach ($this->particles as $key => $particle) {
            $mag = $particle->acceleration->distance(new Point3D(0, 0, 0));
            if ($mag < $minAccel) {
                $minAccel = $mag;
                $minKey = $key;
            }
        }

        printf("Part 1: %d\n", $minKey);
    }

    public function runB(): void
    {
        $this->parse();

        while (true) {
            $collisions = [];
            foreach ($this->particles as $key => $particle) {
                $collidesWith = $collisions[(string) $particle] ?? null;
                if ($collidesWith !== null) {
                    unset($this->particles[$key]);
                    unset($this->particles[$collidesWith]);
                    printf("%d\n", count($this->particles));
                } else {
                    $collisions[(string) $particle] = $key;
                }
            }

            foreach ($this->particles as $particle) {
                $particle->tick();
            }
        }
    }

    private function parse(): void
    {
        $this->particles = $this->inputLines(function (string $line): Particle {
            [, $p, $v, $a] = explode('<', $line);
            [$p2, ] = explode('>', $p);
            [$v2, ] = explode('>', $v);
            [$a2, ] = explode('>', $a);

            [$px, $py, $pz] = explode(',', $p2);
            [$vx, $vy, $vz] = explode(',', $v2);
            [$ax, $ay, $az] = explode(',', $a2);

            return new Particle(
                new Point3D((int) $px, (int) $py, (int) $pz),
                new Point3D((int) $vx, (int) $vy, (int) $vz),
                new Point3D((int) $ax, (int) $ay, (int) $az),
            );
        });
    }
}
