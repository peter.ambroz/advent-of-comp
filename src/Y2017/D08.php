<?php
declare(strict_types=1);

namespace App\Y2017;

use App\Day;

class D08 extends Day
{
    private array $v = [];
    private int $max = -9999999;

    private function read(string $reg): int
    {
        return $this->v[$reg] ?? 0;
    }

    private function inc(string $reg, int $val): void
    {
        if (!isset($this->v[$reg])) {
            $this->v[$reg] = 0;
        }
        $this->v[$reg] += $val;
    }

    private function dec(string $reg, int $val): void
    {
        if (!isset($this->v[$reg])) {
            $this->v[$reg] = 0;
        }
        $this->v[$reg] -= $val;
    }

    public function process(string $line): int
    {
        [$reg, $op, $val, , $creg, $cond, $cval] = explode(' ', $line);

        $cdata = $this->read($creg);
        if (eval(sprintf("return (%d %s %d);", $cdata, $cond, (int)$cval))) {
            $this->$op($reg, (int)$val);
            $newval = $this->read($reg);
            if ($newval > $this->max) {
                $this->max = $newval;
            }
        }

        return 1;
    }

    public function run(): void
    {
        $this->inputLines([$this, 'process']);
        $m = -999999;
        foreach ($this->v as $v) {
            if ($v > $m) {
                $m = $v;
            }
        }
        printf("%d %d\n", $m, $this->max);
    }
}