<?php

namespace App\Y2017;

use App\Day;
use App\Model\Point;
use App\Y2017\Model\Knot;

class D14 extends Day
{
    private const KEY = 'hwlqcszp';
    private array $map;

    public function run(): void
    {
        $this->doRun();

        $sum = 0;
        foreach ($this->map as $row) {
            $sum += array_sum($row);
        }

        printf("%d\n", $sum);
    }

    private function doRun(): void
    {
        $knot = new Knot();

        foreach (range(0, 127) as $i) {
            $input = sprintf('%s-%d', self::KEY, $i);
            $this->map[$i] = str_split(
                implode('', array_map(
                    fn($x) => sprintf('%08s', decbin($x)),
                    unpack('C*', $knot->hash($input, true))
                ))
            );
        }
    }

    public function runB(): void
    {
        $this->doRun();

        $color = 2;
        foreach (array_keys($this->map) as $j) {
            foreach (array_keys($this->map[$j]) as $i) {
                if ($this->map[$j][$i] == 1) {
                    $this->colorize(new Point($i, $j), $color);
                    $color++;
                }
            }
        }

        printf("colors: %d\n", $color - 2);
    }

    private function colorize(Point $p, int $color): void
    {
        $this->map[$p->y][$p->x] = $color;

        foreach ($p->neighbors() as $n) {
            if ($n->x < 0 || $n->x > 127 || $n->y < 0 || $n->y > 127) {
                continue;
            }

            $nc = $this->map[$n->y][$n->x];

            if ($nc == 0 || $nc === $color) {
                continue;
            }

            $this->colorize($n, $color);
        }
    }
}