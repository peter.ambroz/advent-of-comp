<?php

declare(strict_types=1);

namespace App\Y2017;

use App\Day;

class D21 extends Day
{
    /** @var int[][] */
    private array $rules; // 0 => 3, 1 => 2

    /** @var int[][] */
    private array $data = [[143]];

    public function run(): void
    {
        $this->parseRules($this->inputLines());

        for ($i = 0; $i < 5; $i++) {
            $this->round($i);
        }

        printf("pixels: %d\n", $this->countPixels());
    }

    public function runB(): void
    {
        $this->parseRules($this->inputLines());

        for ($i = 0; $i < 18; $i++) {
            $this->round($i);
        }

        printf("pixels: %d\n", $this->countPixels());
    }

    private function parseRules(array $rules): void
    {
        $this->rules = [
            array_fill(0, 512, 0),
            array_fill(0, 16, 0),
        ];

        foreach ($rules as $rule) {
            $rule = Rule::fromString($rule);
            foreach ($rule->inputs as $input) {
                $this->rules[$rule->size][$input] = $rule->output;
            }
        }
    }

    private function round(int $roundNo): void
    {
        $type = $roundNo % 3;
        $rules = $this->rules[$type === 2 ? 1 : $type];

        if ($type === 2) {
            $preprocess = [];
            foreach ($this->data as $y => $row) {
                if ($y % 2 === 1) {
                    continue;
                }

                $row0 = [];
                $row1 = [];
                $row2 = [];
                foreach ($row as $x => $value) {
                    if ($x % 2 === 1) {
                        continue;
                    }
                    $split = $this->split6to4($x, $y);
                    $row0[] = $split[0]; $row0[] = $split[1]; $row0[] = $split[2];
                    $row1[] = $split[3]; $row1[] = $split[4]; $row1[] = $split[5];
                    $row2[] = $split[6]; $row2[] = $split[7]; $row2[] = $split[8];
                }

                $preprocess[] = $row0;
                $preprocess[] = $row1;
                $preprocess[] = $row2;
            }
        } else {
            $preprocess = $this->data;
        }

        $newData = array_map(fn ($row) => array_map(fn ($value) => $rules[$value], $row), $preprocess);

        if ($type === 0) { // 3
            $this->data = [];

            foreach ($newData as $row) {
                $row0 = [];
                $row1 = [];

                foreach ($row as $value) {
                    $split = self::split4to2($value);
                    $row0[] = $split[0]; $row0[] = $split[1];
                    $row1[] = $split[2]; $row1[] = $split[3];
                }

                $this->data[] = $row0;
                $this->data[] = $row1;
            }
        } else { // 2
            $this->data = $newData;
        }
    }

    private static function split4to2(int $num): array
    {
        $data = str_split(sprintf('%016b', $num));
        return [
            bindec(implode('', [$data[0], $data[1], $data[4], $data[5]])),
            bindec(implode('', [$data[2], $data[3], $data[6], $data[7]])),
            bindec(implode('', [$data[8], $data[9], $data[12], $data[13]])),
            bindec(implode('', [$data[10], $data[11], $data[14], $data[15]])),
        ];
    }

    private function split6to4(int $x, int $y): array
    {
        $a = str_split(sprintf('%09b', $this->data[$y][$x]));
        $b = str_split(sprintf('%09b', $this->data[$y][$x + 1]));
        $c = str_split(sprintf('%09b', $this->data[$y + 1][$x]));
        $d = str_split(sprintf('%09b', $this->data[$y + 1][$x + 1]));
        return [
            bindec(implode('', [$a[0], $a[1], $a[3], $a[4]])),
            bindec(implode('', [$a[2], $b[0], $a[5], $b[3]])),
            bindec(implode('', [$b[1], $b[2], $b[4], $b[5]])),

            bindec(implode('', [$a[6], $a[7], $c[0], $c[1]])),
            bindec(implode('', [$a[8], $b[6], $c[2], $d[0]])),
            bindec(implode('', [$b[7], $b[8], $d[1], $d[2]])),

            bindec(implode('', [$c[3], $c[4], $c[6], $c[7]])),
            bindec(implode('', [$c[5], $d[3], $c[8], $d[6]])),
            bindec(implode('', [$d[4], $d[5], $d[7], $d[8]])),
        ];
    }

    private function countPixels(): int
    {
        return array_sum(
            array_map(
                fn ($row) => array_sum(
                    array_map(
                        fn ($pixel) => array_sum(self::int2pixels($pixel)),
                        $row
                    ),
                ),
                $this->data
            ),
        );
    }

    /**
     * @return int[]
     */
    private static function int2pixels(int $num, int $size = 16): array
    {
        return array_map(fn ($c) => (int) $c, str_split(sprintf(sprintf('%%0%db', $size), $num)));
    }

    private function visualize(int $round): void
    {
        $parts = ($round % 2 === 1) ? 2 : 3;
        foreach ($this->data as $row) {
            for ($i = 0; $i < $parts; $i++) {
                foreach ($row as $value) {
                    $px = self::int2pixels($value, $parts*$parts);
                    for ($j = 0; $j < $parts; $j++) {
                        echo $px[$parts * $i + $j];
                    }
                }
                echo "\n";
            }
        }
        echo "\n";
    }
}
