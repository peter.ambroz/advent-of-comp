<?php
declare(strict_types=1);

namespace App\Y2017;

use App\Day;

class D04 extends Day
{
    public function valid(string $phrase): int
    {
        $w = explode(" ", $phrase);
        foreach ($w as $i1 => $w1) {
            foreach ($w as $i2 => $w2) {
                if ($i2 > $i1 && $w2 === $w1) {
                    return 0;
                }
            }
        }
        return 1;
    }

    public function run(): void
    {
        printf("%d\n", array_sum($this->inputLines([$this, 'valid'])));
    }

    public function validB(string $phrase): int
    {
        $w = explode(" ", $phrase);
        foreach ($w as $i1 => $w1) {
            foreach ($w as $i2 => $w2) {
                if ($i2 > $i1) {
                    if (strlen($w1) === strlen($w2)) {
                        $aw1 = str_split($w1);
                        $aw2 = str_split($w2);
                        sort($aw1);
                        sort($aw2);
                        if ($aw1 === $aw2) return 0;
                    }
                }
            }
        }
        return 1;
    }

    public function runB(): void
    {
        printf("%d\n", array_sum($this->inputLines([$this, 'validB'])));
    }

}