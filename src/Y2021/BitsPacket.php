<?php

declare(strict_types=1);

namespace App\Y2021;

abstract class BitsPacket
{
    public const TYPE_ADD = 0;
    public const TYPE_MUL = 1;
    public const TYPE_MIN = 2;
    public const TYPE_MAX = 3;
    public const TYPE_VALUE = 4;
    public const TYPE_GT = 5;
    public const TYPE_LT = 6;
    public const TYPE_EQ = 7;

    public function __construct(protected int $version, protected int $type)
    {
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function getType(): int
    {
        return $this->type;
    }

    /** @return BitsPacket[] */
    public abstract function getPackets(): array;

    public abstract function getValue(): int;

    public function __toString(): string
    {
        return sprintf("Type %d, Ver %d, Subpackets %d", $this->type, $this->version, count($this->getPackets()));
    }
}
