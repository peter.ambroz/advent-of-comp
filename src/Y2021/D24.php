<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D24 extends Day
{
    /*                     0   1   2   3   4   5   6    7    8    9  10  11  12   13 */
    private const DIVS = [ 1,  1,  1,  1, 26,  1,  1,  26,  26,  26, 26,  1, 26,  26];
    private const ADDX = [10, 10, 12, 11,  0, 15, 13, -12, -15, -15, -4, 10, -5, -12];
    private const ADDY = [12, 10,  8,  4,  3, 10,  6,  13,   8,   1,  7,  6,  9,   9];

    /*
    i
    0 1-9
    1 1-3
    2 8-9
    3 1-5
    4 5-9 (3)
    5 6-9
    6 7-9
    7 1-3 (6)
    8 1-4 (5)
    9 1-2 (2)
    10 7-9 (1)
    11 1-8
    12 2-9 (11)
    13 1-9 (0)
     */
    public function run(): void
    {
        $m = $this->_argv[0] ?? '00000000000000';
        if (strlen($m) !== 14) {
            printf("Wrong input\n");
            return;
        }
        $z = 0;

        for ($i = 0; $i < 14; $i++) {
            $w = (int) $m[$i];
            $x = ((($z % 26) + self::ADDX[$i]) === $w) ? 0 : 1;
            $z = (intdiv($z, self::DIVS[$i]) * ((25 * $x) + 1)) + (($w + self::ADDY[$i]) * $x);
        }

        printf("%s\n", ($z === 0) ? 'VALID' : 'NOPE');

        printf("93959993429899\n");
    }

    public function runB(): void
    {
        printf("11815671117121\n");
    }
}
