<?php

declare(strict_types=1);

namespace App\Y2021;

class SnailDuo extends Snail
{
    public function __construct(public Snail $l, public Snail $r)
    {
        $this->parent = null;
    }

    public function isLeaf(): bool
    {
        return false;
    }
}
