<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D23 extends Day
{
    private const OUT = 0;
    private const IN = 1;
    private const DONE = 2;

    private const DIST = [
        'A' => [3, 2, 2, 4, 6, 8, 9],
        'B' => [5, 4, 2, 2, 4, 6, 7],
        'C' => [7, 6, 4, 2, 2, 4, 5],
        'D' => [9, 8, 6, 4, 2, 2, 3],
    ];

    private int $length = 9999999;
    private int $depth;
    private array $bestSeq;

    public function runB(): void
    {
        $this->depth = 4;
        $s = [
            0 => '',
            1 => '',
            2 => '',
            3 => '',
            4 => '',
            5 => '',
            6 => '',
            'A0' => '1D',
            'B0' => '2D',
            'C0' => '1C',
            'D0' => '2B',
            'A1' => '3D',
            'B1' => '3C',
            'C1' => '3B',
            'D1' => '3A',
            'A2' => '4D',
            'B2' => '4B',
            'C2' => '4A',
            'D2' => '4C',
            'A3' => '1B',
            'B3' => '1A',
            'C3' => '2A',
            'D3' => '2C',
        ];

        $this->move([
            'A' => [0, self::OUT],
            'B' => [0, self::OUT],
            'C' => [0, self::OUT],
            'D' => [0, self::OUT],
        ], $s, [
            'A' => [0, 0, 0, 0, 0, 0, 0],
            'B' => [0, 0, 0, 0, 0, 0, 0],
            'C' => [0, 0, 0, 0, 0, 0, 0],
            'D' => [0, 0, 0, 0, 0, 0, 0],
        ]);

        printf("%d\n", $this->length);
        printf("%s\n", implode('; ', $this->bestSeq));
    }

    public function run(): void
    {
        $this->depth = 2;
        $s = [
            0 => '',
            1 => '',
            2 => '',
            3 => '',
            4 => '',
            5 => '',
            6 => '',
            'A0' => '1D',
            'B0' => '2D',
            'C0' => '1C',
            'D0' => '2B',
            'A1' => '1B',
            'B1' => '1A',
            'C1' => '2A',
            'D1' => '2C',
        ];

        $this->move([
            'A' => [0, self::OUT],
            'B' => [0, self::OUT],
            'C' => [0, self::OUT],
            'D' => [0, self::OUT],
        ], $s, [
            'A' => [0, 0, 0, 0, 0, 0, 0],
            'B' => [0, 0, 0, 0, 0, 0, 0],
            'C' => [0, 0, 0, 0, 0, 0, 0],
            'D' => [0, 0, 0, 0, 0, 0, 0],
        ]);

        printf("%d\n", $this->length);
        printf("%s\n", implode('; ', $this->bestSeq));
    }

    private function move(array $colState, array $pawnState, array $paths, int $price = 0, array $sequence = []): void
    {
        if ($price > $this->length) {
            return;
        }

        $allDone = true;

        foreach ($colState as $col => $state) {
            if ($state[1] === self::OUT) {
                $allDone = false;

                foreach (array_filter($paths[$col], fn ($x) => $x === 0) as $corrId => $dumb) {
                    $newColumnState = $colState;
                    $newPawnState = $pawnState;

                    if ($state[0] === $this->depth - 1) {
                        $newColumnState[$col] = [$this->depth - 1, self::IN];
                    } else {
                        $newColumnState[$col] = [$state[0] + 1, self::OUT];
                    }

                    $pawn = $pawnState[$col . $state[0]];
                    assert($pawn !== '');
                    $newPawnState[$corrId] = $pawn;
                    $newPawnState[$col . $state[0]] = '';

                    $newSequence = array_merge($sequence, [$col . $state[0] . '->' . $corrId]);

                    $newPaths = $this->updatePaths($paths, $corrId);
                    $addPrice = $this->price($pawn[1], $col, $state[0], $corrId);

                    $this->move($newColumnState, $newPawnState, $newPaths, $price + $addPrice, $newSequence);
                }
            } elseif ($state[1] === self::IN) {
                $allDone = false;

                foreach (array_filter($paths[$col], fn ($x) => $x === 1) as $corrId => $dumb) {
                    $pawn = $pawnState[$corrId];
                    if ($pawn === '' || $pawn[1] !== $col) {
                        continue;
                    }
                    $newColumnState = $colState;
                    $newPawnState = $pawnState;

                    $newPawnState[$col . $state[0]] = $pawn;
                    $newPawnState[$corrId] = '';

                    if ($state[0] > 0) {
                        $newColumnState[$col] = [$state[0] - 1, self::IN];
                    } else {
                        $newColumnState[$col] = [0, self::DONE];
                    }
                    $newSequence = array_merge($sequence, [$col . $state[0] . '<-' . $corrId]);

                    $newPaths = $this->updatePaths($paths, $corrId, -1);
                    $addPrice = $this->price($pawn[1], $col, $state[0], $corrId);
                    $this->move($newColumnState, $newPawnState, $newPaths, $price + $addPrice, $newSequence);
                }
            }
        }

        if ($allDone) {
            if ($price < $this->length) {
                $this->length = $price;
                $this->bestSeq = $sequence;
                printf("%d\n", $price);
            }
        }
    }

    private function price(string $pcol, string $col, int $colId, int $corrId): int
    {
        $basePrice = match ($pcol) {
            'A' => 1,
            'B' => 10,
            'C' => 100,
            'D' => 1000,
        };

        $dist = self::DIST[$col][$corrId] + $colId;

        return $basePrice * $dist;
    }

    private function updatePaths(array $currentPaths, int $corrId, int $dir = 1): array
    {
        $newPaths = $currentPaths;
        switch ($corrId) {
            case 0:
                foreach ($newPaths as &$path) {
                    $path[0] += $dir;
                }
                unset($path);
                break;
            case 1:
                foreach ($newPaths as &$path) {
                    $path[0] += $dir;
                    $path[1] += $dir;
                }
                unset($path);
                break;

            case 2:
                for ($i = 2; $i < 7; $i++) {
                    $newPaths['A'][$i] += $dir;
                }
                for ($i = 0; $i <= 2; $i++) {
                    $newPaths['B'][$i] += $dir;
                    $newPaths['C'][$i] += $dir;
                    $newPaths['D'][$i] += $dir;
                }
                break;

            case 3:
                for ($i = 3; $i < 7; $i++) {
                    $newPaths['A'][$i] += $dir;
                    $newPaths['B'][$i] += $dir;
                }
                for ($i = 0; $i <= 3; $i++) {
                    $newPaths['C'][$i] += $dir;
                    $newPaths['D'][$i] += $dir;
                }
                break;

            case 4:
                for ($i = 4; $i < 7; $i++) {
                    $newPaths['A'][$i] += $dir;
                    $newPaths['B'][$i] += $dir;
                    $newPaths['C'][$i] += $dir;
                }
                for ($i = 0; $i <= 4; $i++) {
                    $newPaths['D'][$i] += $dir;
                }
                break;

            case 5:
                foreach ($newPaths as &$path) {
                    $path[5] += $dir;
                    $path[6] += $dir;
                }
                unset($path);
                break;
            case 6:
                foreach ($newPaths as &$path) {
                    $path[6] += $dir;
                }
                unset($path);
                break;

        }

        return $newPaths;
    }
}

/*
 * D -> 15 000
 * C -> 10 00
 * B -> 12 0 (14)
 * A -> 13   (17)
 * 16133 (too low) - 16157 - 16159 (too high) 16175 (too high)
 *
 * D -> 39 000
 * C -> 28 00
 * B -> 30 0
 * A -> 39
 *
 * 42139 (min) - 43139 (too low) - 43490 (too high)
 */