<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D20 extends Day
{
    private const RULE = '11100110000100001100010101000111111010101011101111000001011000111000000111000000010101000110110001011111001010101110011110010100100100011101111001011101000101010100010000011111001011010101010001111010100001100111111001110111100000010010011111011010101010111100011101111001010100110001001001001100110011100100111110110001010000011010011010001111010001001101010000100100001101010111010010111011101111110110111111000100110011010111001011100100101111111101110101010100110101011101011101010111000100101100001101101100';

    private int $lit;

    public function run(): void
    {
        $data = $this->inputLines(fn ($x) => array_map(fn ($y) => ($y === '#') ? 1 : 0, str_split($x)));
        $this->apply($data);

        printf("%d\n", $this->lit);
    }

    public function runB(): void
    {
        $data = $this->inputLines(fn ($x) => array_map(fn ($y) => ($y === '#') ? 1 : 0, str_split($x)));
        for ($i = 0; $i < 25; $i++) {
            $data = $this->apply($data);
        }

        printf("%d\n", $this->lit);
    }

    private function apply(array $d): array
    {
        $size = count($d);
        $nd = [];
        $ndd = [];

        for ($j = -1; $j <= $size; $j++) {
            $nd[$j+1] = [];
            for ($i = -1; $i <= $size; $i++) {
                $idx =
                    ($d[$j-1][$i-1] ?? 0) * 256
                    + ($d[$j-1][$i] ?? 0) * 128
                    + ($d[$j-1][$i+1] ?? 0) * 64
                    + ($d[$j][$i-1] ?? 0) * 32
                    + ($d[$j][$i] ?? 0) * 16
                    + ($d[$j][$i+1] ?? 0) * 8
                    + ($d[$j+1][$i-1] ?? 0) * 4
                    + ($d[$j+1][$i] ?? 0) * 2
                    + ($d[$j+1][$i+1] ?? 0);
                $nd[$j+1][$i+1] = (int) self::RULE[$idx];
            }
        }

        $lit = 0;

        for ($j = -1; $j <= $size+2; $j++) {
            $ndd[$j+1] = [];
            for ($i = -1; $i <= $size+2; $i++) {
                $idx =
                    ($nd[$j-1][$i-1] ?? 1) * 256
                    + ($nd[$j-1][$i] ?? 1) * 128
                    + ($nd[$j-1][$i+1] ?? 1) * 64
                    + ($nd[$j][$i-1] ?? 1) * 32
                    + ($nd[$j][$i] ?? 1) * 16
                    + ($nd[$j][$i+1] ?? 1) * 8
                    + ($nd[$j+1][$i-1] ?? 1) * 4
                    + ($nd[$j+1][$i] ?? 1) * 2
                    + ($nd[$j+1][$i+1] ?? 1);
                $val = (int) self::RULE[$idx];
                $ndd[$j+1][$i+1] = $val;
                if ($val === 1) {
                    $lit++;
                }
            }
        }

        $this->lit = $lit;
        return $ndd;
    }
}
