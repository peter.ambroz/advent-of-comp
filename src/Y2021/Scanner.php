<?php

declare(strict_types=1);

namespace App\Y2021;

class Scanner
{
    /** @var Beacon[] */
    private array $beacons;

    /** @var int[][] */
    private array $rotation;

    /** @var int[] */
    private array $translation;

    public function __construct(private int $id, private bool $aligned = false)
    {
        $this->beacons = [];
        $this->rotation = [[1, 0, 0], [0, 1, 0], [0, 0, 1]];
        $this->translation = [0, 0, 0];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function addBeacon(Beacon $beacon): Scanner
    {
        $this->beacons[] = $beacon;
        return $this;
    }

    /**
     * @return Beacon[]
     */
    public function getBeacons(): array
    {
        return $this->beacons;
    }

    /**
     * @param int[][] $rotation
     */
    public function setRotation(array $rotation): void
    {
        $this->rotation = $rotation;
        foreach ($this->beacons as $beacon) {
            $beacon->setRotation($rotation);
        }
    }

    /**
     * @return int[][]
     */
    public function getRotation(): array
    {
        return $this->rotation;
    }

    /**
     * @param int[] $translation
     */
    public function setTranslation(array $translation): void
    {
        $this->translation = $translation;
        foreach ($this->beacons as $beacon) {
            $beacon->setTranslation($translation);
        }
    }

    /**
     * @return int[]
     */
    public function getTranslation(): array
    {
        return $this->translation;
    }

    public function alignWith(Scanner $scanner): bool
    {
        if ($this->aligned) {
            throw new \Exception('Already aligned');
        }
        foreach (D19::$matrices as $matrix) {
            $this->setRotation($matrix);

            foreach ($this->beacons as $myId => $myBeacon) {
                $this->setTranslation([0, 0, 0]);
                $myCoords = $myBeacon->getCoords();
                foreach ($scanner->beacons as $theirId => $theirBeacon) {
                    $this->setTranslation(D19::vectorDiff($theirBeacon->getCoords(), $myCoords));
                    $matched = array_intersect(
                        array_map(fn ($x) => implode(',', $x->getCoords()), $this->getBeacons()),
                        array_map(fn ($x) => implode(',', $x->getCoords()), $scanner->getBeacons())
                    );
                    if (count($matched) >= 12) {
                        $this->aligned = true;
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function isAligned(): bool
    {
        return $this->aligned;
    }
}
