<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D02 extends Day
{
    public function run(): void
    {
        $x = 0;
        $y = 0;
        foreach ($this->inputLines() as $line) {
            [$cmd, $num] = explode(' ', $line);
            $num = (int) $num;
            switch ($cmd) {
                case 'forward':
                    $x += $num;
                    break;
                case 'up':
                    $y -= $num;
                    break;
                case 'down':
                    $y += $num;
                    break;
            }
        }

        printf("fw: %d, dn: %d, tot: %d\n", $x, $y, $x*$y);
    }

    public function runB(): void
    {
        $x = 0;
        $y = 0;
        $aim = 0;
        foreach ($this->inputLines() as $line) {
            [$cmd, $num] = explode(' ', $line);
            $num = (int) $num;
            switch ($cmd) {
                case 'forward':
                    $x += $num;
                    $y += $aim * $num;
                    break;
                case 'up':
                    $aim -= $num;
                    break;
                case 'down':
                    $aim += $num;
                    break;
            }
        }

        printf("fw: %d, dn: %d, aim: %d, tot: %d\n", $x, $y, $aim, $x*$y);
    }
}
