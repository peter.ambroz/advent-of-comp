<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D11 extends Day
{
    private array $octo;
    private array $octoflash;
    private int $flash;

    public function run(): void
    {
        $this->octo = $this->inputLines(fn ($x) => array_map(fn ($y) => (int) $y, str_split($x)));
        $this->octoflash = array_fill(0, 10, array_fill(0, 10, false));

        $oldflash = 0;
        $this->flash = 0;
        $first100 = 0;

        for ($r = 0; $r < 1000000; $r++) {
            $this->octoflash = array_fill(0, 10, array_fill(0, 10, false));

            for ($y = 0; $y < 10; $y++) {
                for ($x = 0; $x < 10; $x++) {
                    $this->inc($x, $y);
                }
            }

            if ($this->flash - $oldflash === 100) {
                $first100 = $r + 1;
                printf("all in %d\n", $first100);
                break;
            }

            $oldflash = $this->flash;

            if ($r === 99) {
                printf("r100: %d\n", $this->flash);
            }
        }
    }

    private function inc(int $x, int $y): void
    {
        if ($x < 0 || $x > 9 || $y < 0 || $y > 9) {
            return;
        }

        if ($this->octoflash[$y][$x]) {
            return;
        }

        if (++$this->octo[$y][$x] > 9) {
            $this->flash++;
            $this->octo[$y][$x] = 0;
            $this->octoflash[$y][$x] = true;

            $this->inc($x + 1, $y);
            $this->inc($x + 1, $y - 1);
            $this->inc($x, $y - 1);
            $this->inc($x - 1, $y - 1);
            $this->inc($x - 1, $y);
            $this->inc($x - 1, $y + 1);
            $this->inc($x, $y + 1);
            $this->inc($x + 1, $y + 1);
        }
    }

    private function prn(): void
    {
        foreach ($this->octo as $line) {
            printf("%s\n", implode('', $line));
        }
        printf("\n");
    }
}
