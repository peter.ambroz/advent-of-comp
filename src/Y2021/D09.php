<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D09 extends Day
{
    private array $data;

    public function run(): void
    {
        $data = $this->inputLines(fn ($x) => array_map(fn ($y) => (int) $y, str_split($x)));

        $danger = 0;

        foreach ($data as $y => $line) {
            foreach ($line as $x => $height) {
                if (
                    (($data[$y-1][$x] ?? 99) > $height)
                    && (($data[$y+1][$x] ?? 99) > $height)
                    && (($line[$x-1] ?? 99) > $height)
                    && (($line[$x+1] ?? 99) > $height)
                ) {
                    $danger += $height + 1;
                }
            }
        }

        printf("%d\n", $danger);
    }

    public function runB(): void
    {
        $this->data = $this->inputLines(fn ($x) => array_map(fn ($y) => $y === '9' ? '9' : ' ', str_split($x)));

        $sizes = [];
        foreach (array_keys($this->data) as $y) {
            foreach (array_keys($this->data[$y]) as $x) {
                $size = $this->fill($x, $y);
                if ($size > 0) {
                    $sizes[] = $size;
                }
            }
        }

        foreach ($this->data as $line) {
            printf("%s\n", implode('', $line));
        }

        rsort($sizes);

        printf("%d\n", $sizes[0] * $sizes[1] * $sizes[2]);
    }

    private function fill(int $x, int $y): int
    {
        if (!isset($this->data[$y][$x])) {
            return 0;
        }

        if ($this->data[$y][$x] !== ' ') {
            return 0;
        }

        $this->data[$y][$x] = 'X';

        return
            1 + $this->fill($x + 1, $y)
            + $this->fill($x, $y - 1)
            + $this->fill($x - 1, $y)
            + $this->fill($x, $y + 1);
    }
}
