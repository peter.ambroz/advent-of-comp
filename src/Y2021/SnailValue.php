<?php

declare(strict_types=1);

namespace App\Y2021;

class SnailValue extends Snail
{
    public function __construct(public int $value)
    {
        $this->parent = null;
    }

    public function isLeaf(): bool
    {
        return true;
    }
}
