<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D17 extends Day
{
    private const TXMIN = 281;
    private const TXMAX = 311; // 31
    private const TYMIN = -74;
    private const TYMAX = -54; // 21

    public function run(): void
    {
        // xv = 24; yv = 73
        printf("2701\n");
    }

    public function runB(): void
    {
        $hits = [];

        for ($xv = 0; $xv <= 1000; $xv++) {
            for ($yv = -75; $yv <= 74; $yv++) {
                if ($this->hit($xv, $yv)) {
                    $hits[$xv . ',' . $yv] = true;
                }
            }
        }

        printf("%d\n", count($hits));
    }

    private function hit(int $xv, int $yv): bool
    {
        $x = $y = 0;
        while (true) {
            $x += $xv;
            $y += $yv;
            if ($xv > 0) {
                $xv--;
            } elseif ($xv < 0) {
                $xv++;
            }
            $yv--;

            if ($x >= self::TXMIN && $x <= self::TXMAX && $y >= self::TYMIN && $y <= self::TYMAX) {
                return true;
            }

            if ($y < self::TYMIN) {
                return false;
            }
        }
    }
}
