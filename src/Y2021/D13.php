<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D13 extends Day
{
    private array $map;

    public function run(): void
    {
        $this->map = [];
        $folds = 0;

        foreach ($this->inputLines() as $line) {
            if (!strlen($line)) {
                continue;
            }

            if (!str_starts_with($line, 'fold')) {
                $values = array_map(fn ($x) => (int) $x, explode(',', $line));
                $this->map[$line] = $values;
            } else {
                $folds++;
                [, , $part] = explode(' ', $line);
                [$dir, $val] = explode('=', $part);
                $this->fold($dir, (int) $val);
                printf("after %d: %d\n", $folds, count($this->map));
            }
        }

        $expand = [];
        foreach ($this->map as $v) {
            if (!isset($expand[$v[1]])) {
                $expand[$v[1]] = [];
            }
            $expand[$v[1]][$v[0]] = '*';
        }

        for ($y = 0; $y <= max(array_keys($expand)); $y++) {
            for ($x = 0; $x <= max(array_keys($expand[$y])); $x++) {
                if (isset($expand[$y][$x])) {
                    echo '*';
                } else {
                    echo ' ';
                }
            }
            echo "\n";
        }
    }

    private function fold(string $axis, int $value): void
    {
        if ($axis === 'x') {
            foreach ($this->map as $key => $pt) {
                $x = $pt[0];
                if ($x > $value) {
                    $y = $pt[1];
                    $x = 2 * $value - $x;
                    $this->map[sprintf("%d,%d", $x, $y)] = [$x, $y];
                    unset($this->map[$key]);
                }
            }
        } else {
            foreach ($this->map as $key => $pt) {
                $y = $pt[1];
                if ($y > $value) {
                    $x = $pt[0];
                    $y = 2 * $value - $y;
                    $this->map[sprintf("%d,%d", $x, $y)] = [$x, $y];
                    unset($this->map[$key]);
                }
            }
        }
    }
}
