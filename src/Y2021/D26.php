<?php

namespace App\Y2021;

use App\Day;

class D26 extends Day
{
    public function run(): void
    {
        $lines = explode("\n", file_get_contents('../../input/Y2021/D26.txt'));

        foreach ($lines as $line) {
            [$name, $ageBeer] = explode(' ', $line);
            [$age, $beer] = explode(',', $ageBeer);

            echo "$name aged $age likes to drink $beer\n";
        }
    }
}