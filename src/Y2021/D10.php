<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D10 extends Day
{
    private const SCORE = [
        ')' => 3,
        ']' => 57,
        '}' => 1197,
        '>' => 25137,
    ];

    private const SCORE_B = [
        '(' => 1,
        '[' => 2,
        '{' => 3,
        '<' => 4,
    ];

    private const MAP = [
        ')' => '(',
        ']' => '[',
        '}' => '{',
        '>' => '<',
    ];

    public function run(): void
    {
        $scores = $this->inputLines(fn ($x) => $this->calculate($x, 'a'));

        printf("%d\n", array_sum($scores));
    }

    public function runB(): void
    {
        $data = array_filter($this->inputLines(), fn ($x) => $this->calculate($x, 'a') === 0);
        $data2 = array_map(fn ($x) => $this->calculate($x, 'b'), $data);
        sort($data2);

        printf("%d\n", $data2[intdiv(count($data2), 2)]);
    }

    public function calculate(string $line, string $alg): int
    {
        $stack = [];
        foreach (str_split($line) as $char) {
            switch ($char) {
                case '(':
                case '[':
                case '{':
                case '<':
                    array_push($stack, $char);
                    break;
                case ')':
                case ']':
                case '}':
                case '>':
                    $pop = array_pop($stack);
                    if ($pop !== self::MAP[$char]) {
                        return self::SCORE[$char];
                    }
                    break;
            }
        }

        if ($alg === 'a') {
            return 0;
        } else {
            $score = 0;
            while (($char = array_pop($stack)) !== null) {
                $score = $score * 5 + self::SCORE_B[$char];
            }

            return $score;
        }
    }
}
