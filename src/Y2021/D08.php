<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D08 extends Day
{
    public function run(): void
    {
        $data = $this->inputLines(fn ($x) => explode(' | ', $x));
        $unique = 0;
        foreach ($data as $datum) {
            foreach (explode(' ', $datum[1]) as $digit) {
                switch (strlen($digit)) {
                    case 2:
                    case 3:
                    case 4:
                    case 7:
                        $unique++;
                }
            }
        }

        printf("%d\n", $unique);
    }

    public function runB(): void
    {
        $data = $this->inputLines(function ($x) {
            return array_map(function ($y) {
                return array_map(fn ($z) => str_split($z), explode(' ', $y));
            }, explode(' | ', $x));
        });

        $sum = 0;

        foreach ($data as $line) {
            $sum += $this->decode($line);
        }

        printf("%d\n", $sum);
    }

    private function decode(array $line): int
    {
        $digits = array_fill(0, 10, []);
        $count6 = [];
        $count5 = [];

        foreach ($line[0] as $digit) {
            switch (count($digit)) {
                case 2:
                    $digits[1] = $digit;
                    break;
                case 3:
                    $digits[7] = $digit;
                    break;
                case 4:
                    $digits[4] = $digit;
                    break;
                case 5:
                    $count5[] = $digit;
                    break;
                case 6:
                    $count6[] = $digit;
                    break;
                case 7:
                    $digits[8] = $digit;
                    break;
            }
        }

        foreach ($count6 as $c6) {
            if (count(array_diff($digits[7], $c6)) > 0) {
                $digits[6] = $c6;
            } elseif (count(array_diff($digits[4], $c6)) === 0) {
                $digits[9] = $c6;
            } else {
                $digits[0] = $c6;
            }
        }

        foreach ($count5 as $c5) {
            if (count(array_diff($digits[7], $c5)) === 0) {
                $digits[3] = $c5;
            } elseif (count(array_diff(array_diff($digits[4], $digits[1]), $c5)) === 0) {
                $digits[5] = $c5;
            } else {
                $digits[2] = $c5;
            }
        }

        $found = [0,0,0,0];
        foreach ($digits as $key => $val) {
            foreach ($line[1] as $lk => $lookup) {
                if (!count(array_diff($val, $lookup)) && !count(array_diff($lookup, $val))) {
                    $found[$lk] = $key;
                }
            }
        }

        return $found[0] * 1000 + $found[1] * 100 + $found[2] * 10 + $found[3];
    }
}
