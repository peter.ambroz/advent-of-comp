<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D01 extends Day
{
    public function run(): void
    {
        $prev = null;
        $cnt = 0;

        $data = $this->inputLines();
        for ($i = 289; $i < 300; $i++) {
            if ($data[$i-1] < $data[$i]) {
                $cnt++;
            }
        }
        /*
        foreach ($this->inputLines(fn ($x) => (int) $x) as $line) {
            if ($prev !== null && $line > $prev) {
                $cnt++;
            }
            $prev = $line;
        }
*/
        printf("%d\n", $cnt);
    }

    public function runB(): void
    {
        $cnt = 0;
        $lines = $this->inputLines(fn ($x) => (int) $x);

        for ($i = 3; $i < count($lines); $i++) {
            if ($lines[$i] > $lines[$i-3]) {
                $cnt++;
            }
        }

        printf("%d\n", $cnt);
    }
}
