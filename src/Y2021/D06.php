<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D06 extends Day
{
    /** @var int[] */
    private array $fish;

    public function run(): void
    {
        $this->fish = array_map(fn ($x) => (int) $x, explode(',', $this->inputLines()[0]));

        for ($i = 0; $i < 80; $i++) {
            $this->step();
        }

        printf("%d\n", count($this->fish));
    }

    public function runB(): void
    {
        $allfish = array_map(fn ($x) => (int) $x, explode(',', $this->inputLines()[0]));

        $steps = array_fill(0, 9, 0);
        foreach ($allfish as $fish) {
            $steps[$fish]++;
        }

        for ($i = 0; $i < 256; $i++) {
            $steps = array_merge(
                array_slice($steps, 1, 6),
                [$steps[7] + $steps[0]],
                [$steps[8]],
                [$steps[0]]
            );
        }

        printf("%d\n", array_sum($steps));
    }

    private function step(): void
    {
        $newFish = [];

        foreach ($this->fish as &$fish) {
            if ($fish > 0) {
                $fish--;
            } else {
                $fish = 6;
                $newFish[] = 8;
            }
        }

        $this->fish = array_merge($this->fish, $newFish);
    }
}
