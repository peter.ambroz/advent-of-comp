<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D21 extends Day
{
    //Player 1 starting position: 8
    //Player 2 starting position: 2

    private const PROB = [
        3 => 1,
        4 => 3,
        5 => 6,
        6 => 7,
        7 => 6,
        8 => 3,
        9 => 1,
    ];

    private array $wincount;

    public function run(): void
    {
        $die = 1;
        $cast = 0;
        $p = [7, 1];
        $ps = [0, 0];

        while (true) {
            for ($i = 0; $i < 2; $i++) {
                $d1 = 3 * $die + 3;
                $cast += 3;
                $die += 3;
                if ($die > 100) {
                    $die -= 100;
                    $d1 -= ($die - 1) * 100;
                }
                $p[$i] = ($p[$i] + $d1) % 10;
                $ps[$i] += $p[$i] + 1;
                if ($ps[$i] >= 1000) {
                    $win = $i;
                    break 2;
                }
            }
        }

        printf("%d\n", $ps[1 - $win] * $cast);
    }

    public function runB(): void
    {
        $pos = [7, 1];
        $this->wincount = [0, 0];

        $this->round($pos, 0, [0, 0], 1);

        printf("1: %d\n2: %d\n", $this->wincount[0], $this->wincount[1]);
    }

    private function round(array $pos, int $turn, array $score, int $path): void
    {
        for ($d = 3; $d <= 9; $d++) {
            $npos = $pos;
            $npos[$turn] = $np = ($pos[$turn] + $d) % 10;
            $nscore = $score;
            $nscore[$turn] = $ns = $score[$turn] + $np + 1;
            $npath = $path * self::PROB[$d];
            if ($ns >= 21) {
                $this->wincount[$turn] += $npath;
                continue;
            }
            $this->round($npos, 1 - $turn, $nscore, $npath);
        }
    }
}
/*

9/3, 19/7, 20/12, 22/- (P1)



 */
