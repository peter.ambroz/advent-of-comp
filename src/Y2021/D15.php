<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;
use JMGQ\AStar\AStar;
use JMGQ\AStar\DomainLogicInterface;

class D15 extends Day implements DomainLogicInterface
{
    private array $data;

    private int $size = 100;

    public function run(): void
    {
        $this->data = $this->inputLines(fn ($x) => array_map(fn ($y) => (int) $y, str_split($x)));
        $start = new D15Node(0, 0);
        $goal = new D15Node($this->size - 1, $this->size - 1);

        $aStar = new AStar($this);

        $solution = $aStar->run($start, $goal);
        $cost = 0;
        /** @var D15Node $node */
        foreach ($solution as $node) {
            $cost += $this->getData($node->x, $node->y);
        }

        printf("%d\n", $cost - $this->data[0][0]);
    }

    public function runB(): void
    {
        $this->size = 500;
        $this->run();
    }

    /**
     * @param D15Node $node
     * @return D15Node[]
     */
    public function getAdjacentNodes(mixed $node): iterable
    {
        $adjacentNodes = [
            $this->getData($node->x + 1, $node->y) ? new D15Node($node->x + 1, $node->y) : null,
            $this->getData($node->x - 1, $node->y) ? new D15Node($node->x - 1, $node->y) : null,
            $this->getData($node->x, $node->y + 1) ? new D15Node($node->x, $node->y + 1) : null,
            $this->getData($node->x, $node->y - 1) ? new D15Node($node->x, $node->y - 1) : null,
        ];

        return array_filter($adjacentNodes, fn ($x) => !is_null($x));
    }

    /**
     * @param D15Node $node
     * @param D15Node $adjacent
     * @return float|int
     */
    public function calculateRealCost(mixed $node, mixed $adjacent): float | int
    {
        return $this->getData($adjacent->x, $adjacent->y);
    }

    /**
     * @param D15Node $fromNode
     * @param D15Node $toNode
     */
    public function calculateEstimatedCost(mixed $fromNode, mixed $toNode): float | int
    {
        return abs($fromNode->x - $toNode->x) + abs($fromNode->y - $toNode->y);
    }

    private function getData(int $x, int $y): ?int
    {
        if ($x < 0 || $x >= $this->size || $y < 0 || $y >= $this->size) {
            return null;
        }

        $r = $this->data[$y % 100][$x % 100] + intdiv($x, 100) + intdiv($y, 100);
        if ($r > 9) {
            $r -= 9;
        }
        return $r;
    }
}
