<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D19 extends Day
{
    public static array $matrices;

    public function run(): void
    {
        $scanners = $this->input();
        $this->generateMatrices();

        /** @var Scanner[] $aligned */
        $aligned = [];
        $aligned[] = array_shift($scanners);

        $pairs = [];

        while (count($scanners) > 0) {
            foreach ($scanners as $id => $scanner) {
                foreach ($aligned as $idol) {
                    $pair = $scanner->getId() . ':' . $idol->getId();
                    if (isset($pairs[$pair])) {
                        continue;
                    }
                    $pairs[$pair] = true;
                    printf("Align %d to %d .. ", $scanner->getId(), $idol->getId());
                    $success = $scanner->alignWith($idol);
                    printf("%s\n", $success ? 'OK' : 'FAIL');
                    if ($success) {
                        $aligned[] = $scanner;
                        unset($scanners[$id]);
                        break 2;
                    }
                }
            }
        }

        $beacons = [];
        foreach ($aligned as $scanner) {
            foreach ($scanner->getBeacons() as $beacon) {
                $beacons[implode(',', $beacon->getCoords())] = true;
            }
        }

        printf("B: %d\n", count($beacons));
    }

    public function runB(): void
    {
        $alignment = [
            12 => 0,
            10 => 12,
            13 => 0,
            3 => 13,
            2 => 3,
            4 => 13,
            8 => 3,
            6 => 8,
            1 => 6,
            5 => 1,
            7 => 5,
            9 => 13,
            11 => 8,
            16 => 6,
            19 => 9,
            21 => 9,
            14 => 21,
            22 => 7,
            23 => 4,
            24 => 5,
            20 => 24,
            25 => 20,
            26 => 6,
            27 => 12,
            28 => 6,
            29 => 0,
            15 => 29,
            17 => 15,
            30 => 2,
            18 => 30,
        ];

        $scanners = $this->input();
        $this->generateMatrices();

        printf("Aligning scanners..");
        foreach ($alignment as $from => $to) {
            $scanners[$from]->alignWith($scanners[$to]);
        }
        printf("DONE\n");

        $maxDist = 0;
        foreach ($scanners as $i1 => $s1) {
            foreach ($scanners as $i2 => $s2) {
                if ($i1 >= $i2) {
                    continue;
                }

                $dist = self::vectorDistance($s1->getTranslation(), $s2->getTranslation());
                if ($dist > $maxDist) {
                    $maxDist = $dist;
                }
            }
        }

        printf("D: %d\n", $maxDist);
    }

    /**
     * @return Scanner[]
     */
    private function input(): array
    {
        /** @var Scanner[] $scanners */
        $scanners = [];
        $sid = 0;

        foreach ($this->inputLines() as $line) {
            if (!strlen($line)) {
                continue;
            }

            if (str_starts_with($line, '---')) {
                [, $sid, ] = explode('---', $line);
                $sid = (int) $sid;
                $scanners[$sid] = new Scanner($sid);
                continue;
            }

            $beacon = new Beacon(array_map(fn ($x) => (int) $x, explode(',', $line)));
            $scanners[$sid]->addBeacon($beacon);
        }

        return $scanners;
    }

    private function generateMatrices(): void
    {
        $matrices = array_fill(0, 6, array_fill(0, 4, []));
        $identity = [
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 1],
        ];
        $z = [
            [0, -1, 0],
            [1,  0, 0],
            [0,  0, 1],
        ];
        $y = [
            [0,  0, 1],
            [0,  1, 0],
            [-1, 0, 0],
        ];
        $x = [
            [1, 0,  0],
            [0, 0, -1],
            [0, 1,  0],
        ];

        $matrices[0][0] = $identity;
        $matrices[4][0] = $x;
        $matrices[5][0] = self::matrixProduct($x, self::matrixProduct($x, $x));

        for ($i = 1; $i < 4; $i++) {
            $matrices[$i][0] = self::matrixProduct($matrices[$i-1][0], $y);
        }

        for ($j = 0; $j < 6; $j++) {
            for ($i = 1; $i < 4; $i++) {
                $matrices[$j][$i] = self::matrixProduct($matrices[$j][$i-1], $z);
            }
        }

        self::$matrices = array_merge(...$matrices);
    }

    public static function matrixProduct(array $m1, array $m2): array
    {
        $m = [];
        for ($j = 0; $j < 3; $j++) {
            $l = [];
            for ($i = 0; $i < 3; $i++) {
                $s = 0;
                for ($k = 0; $k < 3; $k++) {
                    $s += $m1[$k][$i] * $m2[$j][$k];
                }
                $l[] = $s;
            }
            $m[] = $l;
        }
        return $m;
    }

    public static function vectorDiff(array $v1, array $v2): array
    {
        return [$v1[0] - $v2[0], $v1[1] - $v2[1], $v1[2] - $v2[2]];
    }

    public static function vectorDistance(array $v1, array $v2): int
    {
        return array_sum(
            array_map(
                fn ($x) => abs($x),
                self::vectorDiff($v1, $v2)
            )
        );
    }
}
