<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D04 extends Day
{
    /** @var int[] */
    private array $numbers;

    private array $boards;

    public function run(): void
    {
        $this->readBoard();

        foreach ($this->numbers as $num) {
            $boardNo = $this->crossOut($num);
            if (count($boardNo)) {
                $sum = array_sum(array_map('array_sum', $this->boards[$boardNo[0]]));
                printf("b:%d, s:%d, n:%d, %d\n", $boardNo[0], $sum, $num, $sum * $num);
                break;
            }
        }
    }

    public function runB(): void
    {
        $this->readBoard();

        foreach ($this->numbers as $num) {
            $boardNo = $this->crossOut($num);
            $remaining = 0;
            $lastBoard = -1;

            foreach ($this->boards as $boardNumber => $board) {
                if (count($board) === 5) {
                    $remaining++;
                    $lastBoard = $boardNumber;
                }
            }

            if ($remaining === 1 && count($boardNo) === 1) {
                $sum = array_sum(array_map('array_sum', $this->boards[$lastBoard]));
                printf("b:%d, s:%d, n:%d, %d\n", $lastBoard, $sum, $num, $sum * $num);
                break;
            }

            foreach ($boardNo as $x) {
                $this->boards[$x] = [];
            }
        }
    }

    private function readBoard(): void
    {
        foreach ($this->inputLines() as $lineNo => $line) {
            if ($lineNo === 0) {
                $this->numbers = array_map(fn ($x) => (int) $x, explode(',', $line));
                continue;
            }

            if (!strlen($line)) {
                continue;
            }

            $boardNo = intdiv($lineNo - 2, 6);
            $startOfBoard = (($lineNo - 2) % 6 === 0);

            if ($startOfBoard) {
                $this->boards[$boardNo] = [];
            }

            $this->boards[$boardNo][] = array_map(fn ($x) => (int) trim($x), str_split($line, 3));
        }
    }

    private function crossOut(int $num): array
    {
        $foundBoard = [];

        foreach ($this->boards as $boardNo => &$board) {
            $foundY = false;
            $foundX = false;

            foreach ($board as $y => $boardLine) {
                $foundX = array_search($num, $boardLine, true);
                if ($foundX !== false) {
                    $foundY = $y;
                    break;
                }
            }

            if ($foundY !== false && $foundX !== false) {
                $board[$foundY][$foundX] = '';

                $foundRow = true;
                $foundCol = true;

                for ($i = 0; $i < 5; $i++) {
                    if ($board[$foundY][$i] !== '') {
                        $foundRow = false;
                    }

                    if ($board[$i][$foundX] !== '') {
                        $foundCol = false;
                    }
                }

                if ($foundRow || $foundCol) {
                    $foundBoard[] = $boardNo;
                }
            }
        }
        unset($board);

        return $foundBoard;
    }
}
