<?php

declare(strict_types=1);

namespace App\Y2021;

class BitsOpPacket extends BitsPacket
{
    public const PAYLOAD_TYPE_BITS = 0;
    public const PAYLOAD_TYPE_PACKETS = 1;

    /** @var BitsPacket[] */
    private array $packets;

    public function __construct(int $version, int $type)
    {
        parent::__construct($version, $type);

        $this->packets = [];
    }

    /**
     * @return BitsPacket[]
     */
    public function getPackets(): array
    {
        return $this->packets;
    }

    public function addPacket(BitsPacket $packet): void
    {
        $this->packets[] = $packet;
    }

    public function getValue(): int
    {
        return 0;
    }
}
