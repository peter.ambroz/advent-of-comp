<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D22 extends Day
{
    private ?int $cnt = null;

    public function run(): void
    {
        $this->cnt = 20;
        $this->runB();
    }

    public function runB(): void
    {
        $data = $this->inputLines([$this, 'parse']);
        /** @var Cuboid[] $space */
        $space = [];

        foreach (array_slice($data, 0, $this->cnt) as $cube) {
            $cuboid = new Cuboid(
                [$cube['xmin'], $cube['ymin'], $cube['zmin']],
                [$cube['xmax'], $cube['ymax'], $cube['zmax']],
                $cube['state'] ? 1 : 0
            );

            $newUnits = [$cuboid];

            foreach ($space as $spaceUnit) {
                $isect = $spaceUnit->intersect($cuboid);
                if ($isect !== null) {
                    $newUnits[] = $isect;
                }
            }

            $space = array_merge($space, $newUnits);
        }

        $on = array_sum(array_map(fn ($x) => $x->volume(), $space));
        printf("%d\n", $on);
    }

    public function parse(string $line): array
    {
        $data = [];
        [$state, $coords] = explode(' ', $line);

        $data['state'] = $state === 'on';
        $cparts = explode(',', $coords);

        foreach ($cparts as $cpart) {
            [$axis, $range] = explode('=', $cpart);
            [$min, $max] = explode('..', $range);

            $data[$axis . 'min'] = (int) $min;
            $data[$axis . 'max'] = (int) $max;
        }

        return $data;
    }
}
