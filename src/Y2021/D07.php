<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D07 extends Day
{
    public function run(): void
    {
        $positions = array_map(fn ($x) => (int) $x, explode(',', $this->inputLines()[0]));
        sort($positions);

        $middle = intdiv(count($positions), 2);
        $meetup = $positions[$middle];

        $fuel = 0;
        foreach ($positions as $pos) {
            $fuel += abs($meetup - $pos);
        }

        printf("m: %d, f: %d\n", $meetup, $fuel);
    }

    public function runB(): void
    {
        $positions = array_map(fn ($x) => (int) $x, explode(',', $this->inputLines()[0]));
        sort($positions);

        $avg = array_sum($positions) / count($positions);

        $diff = [0];

        for ($i = 1; $i < 1900; $i++) {
            $diff[$i] = $diff[$i-1] + $i;
        }

        $minFuel = null;
        $minPos = 0;
        for ($i = 400; $i < 500; $i++) {
            $fuel = 0;

            foreach ($positions as $pos) {
                $fuel += $diff[abs($i - $pos)];
            }

            if ($minFuel === null || $minFuel > $fuel) {
                $minFuel = $fuel;
                $minPos = $i;
            }
        }

        printf("a: %0.2f, p: %d, f: %d\n", $avg, $minPos, $minFuel);
    }
}
