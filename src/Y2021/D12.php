<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D12 extends Day
{
    private array $g;
    private array $p;
    private bool $modeB = false;

    public function run(): void
    {
        $this->g = [];
        $this->p = [];

        foreach ($this->inputLines(fn ($x) => explode('-', $x)) as $pair) {
            $l = $pair[0];
            $r = $pair[1];
            if (!isset($this->g[$l])) {
                $this->g[$l] = [];
            }
            if (!isset($this->g[$r])) {
                $this->g[$r] = [];
            }
            $this->g[$l][] = $r;
            $this->g[$r][] = $l;
        }

        if ($this->modeB) {
            $this->exploreB('1start', []);
        } else {
            $this->exploreA('1start', []);
        }

        printf("%d\n", count($this->p));
    }

    public function runB(): void
    {
        $this->modeB = true;
        $this->run();
    }

    private function exploreA(string $start, array $path): void
    {
        $path[] = $start;
        if ($start === 'end') {
            $this->p[] = $path;
            return;
        }

        foreach ($this->g[$start] as $next) {
            if (($next[0] === '0' || $next[0] === '1') && in_array($next, $path)) {
                continue;
            }

            $this->exploreA($next, $path);
        }
    }

    private function exploreB(string $start, array $path): void
    {
        $path[] = $start;
        if ($start === 'end') {
            $this->p[] = $path;
            return;
        }

        foreach ($this->g[$start] as $next) {
            if ($next[0] === '0') {
                $pc = [];
                $has2 = false;
                $hasN = false;
                foreach ($path as $comp) {
                    if ($comp[0] === '0') {
                        $pc[$comp] = ($pc[$comp] ?? 0) + 1;
                        if ($pc[$comp] === 2) {
                            $has2 = true;
                        }
                        if ($comp === $next) {
                            $hasN = true;
                        }
                    }
                }
                if ($has2 && $hasN) {
                    continue;
                }
            }

            if ($next[0] === '1' && in_array($next, $path)) {
                continue;
            }

            $this->exploreB($next, $path);
        }
    }
}
