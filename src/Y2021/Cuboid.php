<?php

declare(strict_types=1);

namespace App\Y2021;

class Cuboid
{
    /**
     * @param int[] $min
     * @param int[] $max
     */
    public function __construct(private array $min, private array $max, private int $sign)
    {
    }

    public function volume(): int
    {
        return $this->sign * array_product(array_map(fn ($min, $max) => $max - $min + 1, $this->min, $this->max));
    }

    public function intersect(Cuboid $cuboid): ?Cuboid
    {
        if ($this->sign === 0) {
            return null;
        }

        $parts = array_filter(
            array_map(function ($amin, $amax, $bmin, $bmax) {
                $low = max($amin, $bmin);
                $high = min($amax, $bmax);
                if ($low <= $high) {
                    return [$low, $high];
                } else {
                    return null;
                }
            }, $this->min, $this->max, $cuboid->min, $cuboid->max),
            fn ($x) => $x !== null
        );

        if (count($parts) !== 3) {
            return null;
        }

        $min = array_column($parts, 0);
        $max = array_column($parts, 1);

        return new Cuboid($min, $max, $this->sign * -1);
    }
}
