<?php

declare(strict_types=1);

namespace App\Y2021;

class Beacon
{
    /** @var int[][] */
    private array $rotation;

    /** @var int[] */
    private array $translation;

    /** @var int[] */
    private array $cache;

    private bool $dirty;

    /**
     * @param int[] $coords
     */
    public function __construct(private array $coords)
    {
        $this->rotation = [[1, 0, 0], [0, 1, 0], [0, 0, 1]];
        $this->translation = [0, 0, 0];
        $this->dirty = false;
        $this->cache = $this->coords;
    }

    /**
     * @param int[][] $rotation
     */
    public function setRotation(array $rotation): void
    {
        $this->rotation = $rotation;
        $this->dirty = true;
    }

    /**
     * @param int[] $translation
     */
    public function setTranslation(array $translation): void
    {
        $this->translation = $translation;
        $this->dirty = true;
    }

    public function getCoords(): array
    {
        if ($this->dirty) {
            $this->transform();
        }

        return $this->cache;
    }

    public function diff(Beacon $beacon, bool $normalize = false): array
    {
        return array_map(fn ($x, $y) => $x - $y, $this->getCoords(), $beacon->getCoords());
    }

    private function transform(): void
    {
        $this->cache = array_map(
            fn (array $line, int $move) => array_sum(
                array_map(
                    fn (int $x, int $y) => $x * $y,
                    $this->coords,
                    $line
                )
            ) + $move,
            $this->rotation,
            $this->translation
        );

        $this->dirty = false;
    }
}
