<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D18 extends Day
{
    public function run(): void
    {
        $data = $this->inputLines(fn ($x) => $this->toSnail(json_decode($x, true)));
        $root = array_shift($data);
        foreach ($data as $snail) {
            $root = $this->add($root, $snail);
            $this->reduce($root);
        }

        printf("%d\n", $this->magnitude($root));
    }

    public function runB(): void
    {
        $data = $this->inputLines(fn ($x) => json_decode($x, true));
        $max = 0;
        foreach ($data as $id1 => $snail1) {
            foreach ($data as $id2 => $snail2) {
                if ($id1 === $id2) {
                    continue;
                }

                $sum = $this->add($this->toSnail($snail1), $this->toSnail($snail2));
                $this->reduce($sum);

                $mag = $this->magnitude($sum);
                if ($mag > $max) {
                    $max = $mag;
                }
            }
        }

        printf("%d\n", $max);
    }

    private function reduce(Snail $top): void
    {
        do {
            $action = false;

            while (($duo = $this->findExplode($top)) !== null) {
                $this->doExplode($duo);
                $action = true;
            }

            if (($val = $this->findSplit($top)) !== null) {
                $this->doSplit($val);
                $action = true;
            }
        } while ($action);
    }

    private function add(Snail $a1, Snail $a2): SnailDuo
    {
        $d = new SnailDuo($a1, $a2);
        $a1->setParent($d);
        $a2->setParent($d);
        return $d;
    }

    private function toSnail(array|int $input): Snail
    {
        if (!is_array($input)) {
            return new SnailValue($input);
        }

        $l = $this->toSnail($input[0]);
        $r = $this->toSnail($input[1]);

        $d = new SnailDuo($l, $r);
        $l->setParent($d);
        $r->setParent($d);
        return $d;
    }

    private function findExplode(Snail $top, int $lev = 0): ?SnailDuo
    {
        if (!$top instanceof SnailDuo) {
            return null;
        }

        if ($lev === 4) {
            return $top;
        }

        return
            $this->findExplode($top->l, $lev + 1)
            ?? $this->findExplode($top->r, $lev + 1)
            ?? null;
    }

    private function findSplit(Snail $top): ?SnailValue
    {
        if ($top instanceof SnailValue) {
            if ($top->value >= 10) {
                return $top;
            } else {
                return null;
            }
        }

        assert($top instanceof SnailDuo);

        return
            $this->findSplit($top->l)
            ?? $this->findSplit($top->r)
            ?? null;
    }

    private function doExplode(SnailDuo $duo): void
    {
        assert($duo->l instanceof SnailValue && $duo->r instanceof SnailValue);

        $parent = $duo->getParent();
        $replacement = new SnailValue(0);
        $nextLeft = $this->findNextLeft($duo);
        $nextRight = $this->findNextRight($duo);
        if ($nextLeft !== null) {
            $nextLeft->value += $duo->l->value;
        }
        if ($nextRight !== null) {
            $nextRight->value += $duo->r->value;
        }

        $replacement->setParent($parent);
        if ($parent->l === $duo) {
            $parent->l = $replacement;
        } else {
            $parent->r = $replacement;
        }
    }

    private function findNextLeft(SnailDuo $me): ?SnailValue
    {
        $ptr = $me;
        while (true) {
            if ($ptr->getParent() === null) {
                return null;
            }
            if ($ptr->getParent()->r === $ptr) {
                break;
            }
            $ptr = $ptr->getParent();
        }

        $lookR = $ptr->getParent()->l;
        while (true) {
            if ($lookR instanceof SnailValue) {
                return $lookR;
            }
            assert($lookR instanceof SnailDuo);
            $lookR = $lookR->r;
        }
    }

    private function findNextRight(SnailDuo $me): ?SnailValue
    {
        $ptr = $me;
        while (true) {
            if ($ptr->getParent() === null) {
                return null;
            }
            if ($ptr->getParent()->l === $ptr) {
                break;
            }
            $ptr = $ptr->getParent();
        }

        $lookL = $ptr->getParent()->r;
        while (true) {
            if ($lookL instanceof SnailValue) {
                return $lookL;
            }
            assert($lookL instanceof SnailDuo);
            $lookL = $lookL->l;
        }
    }

    private function doSplit(SnailValue $leaf): void
    {
        $lVal = intdiv($leaf->value, 2);
        $rVal = $leaf->value - $lVal;

        $l = new SnailValue($lVal);
        $r = new SnailValue($rVal);

        $parent = $leaf->getParent();
        $replacement = new SnailDuo($l, $r);
        $l->setParent($replacement);
        $r->setParent($replacement);
        $replacement->setParent($parent);

        if ($parent->l === $leaf) {
            $parent->l = $replacement;
        } else {
            $parent->r = $replacement;
        }
    }

    private function magnitude(Snail $top): int
    {
        if ($top instanceof SnailValue) {
            return $top->value;
        }

        assert($top instanceof SnailDuo);

        return 3 * $this->magnitude($top->l) + 2 * $this->magnitude($top->r);
    }
}
