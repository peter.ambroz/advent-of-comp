<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D03 extends Day
{
    public function run(): void
    {
        $bits = array_fill(0, 12, 0);

        foreach ($this->inputLines() as $line) {
            foreach (str_split($line) as $id => $bit) {
                if ($bit === '1') {
                    $bits[$id]++;
                }
            }
        }

        $gamma = 0;
        $eps = 0;
        foreach ($bits as $id => $bitCount) {
            $exp = 11 - $id;
            $value = 2 ** $exp;
            if ($bitCount > 500) {
                $gamma += $value;
            } elseif ($bitCount < 500) {
                $eps += $value;
            }
        }

        printf("g: %d, e: %d, pow: %d\n", $gamma, $eps, $gamma * $eps);
    }

    public function runB(): void
    {
        $data0 = $this->inputLines();
        $data1 = $data0;
        $prefix1 = '';
        $prefix0 = '';

        while (count($data1) > 1) {
            $filtered1 = $this->filter($data1, $prefix1);

            if (count($filtered1['1']) >= count($filtered1['0'])) {
                $prefix1 .= '1';
                $data1 = $filtered1['1'];
            } else {
                $prefix1 .= '0';
                $data1 = $filtered1['0'];
            }
        }

        while (count($data0) > 1) {
            $filtered0 = $this->filter($data0, $prefix0);

            if (count($filtered0['1']) < count($filtered0['0'])) {
                $prefix0 .= '1';
                $data0 = $filtered0['1'];
            } else {
                $prefix0 .= '0';
                $data0 = $filtered0['0'];
            }
        }

        printf("g: %s, e: %s\n", $data1[0], $data0[0]);
    }

    private function filter(array $data, string $prefix): array
    {
        $out = [
           '0' => [],
           '1' => [],
        ];

        foreach ($data as $datum) {
            if (str_starts_with($datum, $prefix . '1')) {
                $out['1'][] = $datum;
            } elseif (str_starts_with($datum, $prefix . '0')) {
                $out['0'][] = $datum;
            }
        }

        return $out;
    }
}
