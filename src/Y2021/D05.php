<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D05 extends Day
{
    private bool $isB = false;

    public function run(): void
    {
        $intersect = [];
        $cnt = 0;

        $data = $this->inputLines(function (string $line): array {
            [$start, $end] = explode(' -> ', $line);
            [$x1, $y1] = array_map(fn ($x) => (int) $x, explode(',', $start));
            [$x2, $y2] = array_map(fn ($x) => (int) $x, explode(',', $end));
            return ['x1' => $x1, 'y1' => $y1, 'x2' => $x2, 'y2' => $y2];
        });

        foreach ($data as $d) {
            $dx = $d['x2'] - $d['x1'];
            $dy = $d['y2'] - $d['y1'];

            if ($dx !== 0 && $dy !== 0 && !$this->isB) {
                continue;
            }

            $length = max(abs($dx), abs($dy)) + 1;
            if ($dx > 0) {
                $dx = 1;
            } elseif ($dx < 0) {
                $dx = -1;
            }

            if ($dy > 0) {
                $dy = 1;
            } elseif ($dy < 0) {
                $dy = -1;
            }

            for ($i = 0; $i < $length; $i++) {
                $key = sprintf("%d,%d", $d['x1'] + $dx * $i, $d['y1'] + $dy * $i);
                $icount = ($intersect[$key] ?? 0) + 1;
                if ($icount == 2) {
                    $cnt++;
                }
                $intersect[$key] = $icount;
            }
        }

        printf("%d\n", $cnt);
    }

    public function runB(): void
    {
        $this->isB = true;
        $this->run();
    }
}
