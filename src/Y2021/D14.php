<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D14 extends Day
{
    private int $steps = 10;

    public function run(): void
    {
        $b = [
            'SF' => 1,
            'BB' => 1,
            'NK' => 1,
            'KO' => 1,
            'HH' => 2,
            'HP' => 1,
            'FO' => 1,
            'FF' => 1,
            'SP' => 1,
            'FV' => 1,
            'FB' => 1,
            'BN' => 1,
            'KK' => 1,
            'OH' => 1,
            'PF' => 2,
            'OF' => 1,
            'FS' => 1,
        ];

        $rules = [];

        foreach ($this->inputLines(fn ($x) => explode(' -> ', $x)) as $rule) {
            $rules[$rule[0]] = $rule[1];
        }

        for ($i = 0; $i < $this->steps; $i++) {
            $bnew = [];
            foreach ($rules as $rule => $ch) {
                $old = $rule;
                $new1 = $rule[0] . $ch;
                $new2 = $ch . $rule[1];

                $countOld = $b[$old] ?? 0;

                $bnew[$old] = ($bnew[$old] ?? 0) - $countOld;
                $bnew[$new1] = ($bnew[$new1] ?? 0) + $countOld;
                $bnew[$new2] = ($bnew[$new2] ?? 0) + $countOld;
            }
            foreach (array_unique(array_merge(array_keys($b), array_keys($bnew))) as $key) {
                $b[$key] = ($b[$key] ?? 0) + ($bnew[$key] ?? 0);
            }
        }

        $freq = [
            'S' => 1,
            'V' => 1,
        ];

        foreach ($b as $bg => $cnt) {
            $freq[$bg[0]] = ($freq[$bg[0]] ?? 0) + $cnt;
            $freq[$bg[1]] = ($freq[$bg[1]] ?? 0) + $cnt;
        }

        printf("%d\n", intdiv($freq['V'] - $freq['O'], 2));
    }

    public function runB(): void
    {
        $this->steps = 40;
        $this->run();
    }
}
