<?php

declare(strict_types=1);

namespace App\Y2021;

use JMGQ\AStar\Node\NodeIdentifierInterface;

class D15Node implements NodeIdentifierInterface
{
    public function __construct(public int $x, public int $y)
    {
    }

    public function getUniqueNodeId(): string
    {
        return $this->x . ',' . $this->y;
    }
}
