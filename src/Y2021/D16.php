<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;
use Exception;

class D16 extends Day
{
    private BitsPacket $topPacket;

    public function run(): void
    {
        $this->parse();
        printf("%d\n", $this->addVersions($this->topPacket));
    }

    public function runB(): void
    {
        $this->parse();
        printf("%d\n", $this->evaluate($this->topPacket));
    }

    private function evaluate(BitsPacket $packet): int
    {
        if ($packet->getType() === BitsPacket::TYPE_VALUE) {
            return $packet->getValue();
        }

        $sub = array_map(fn (BitsPacket $x): int => $this->evaluate($x), $packet->getPackets());

        return match ($packet->getType()) {
            BitsPacket::TYPE_ADD => array_sum($sub),
            BitsPacket::TYPE_MUL => array_reduce($sub, fn(int $x, int $y): int => $x * $y, 1),
            BitsPacket::TYPE_MAX => max($sub),
            BitsPacket::TYPE_MIN => min($sub),
            BitsPacket::TYPE_GT => ($sub[0] > $sub[1]) ? 1 : 0,
            BitsPacket::TYPE_LT => ($sub[0] < $sub[1]) ? 1 : 0,
            BitsPacket::TYPE_EQ => ($sub[0] === $sub[1]) ? 1 : 0,
        };
    }

    private function parse(): void
    {
        $b = new Bits(Bits::hex2binary($this->inputLines()[0]));
        $this->topPacket = $b->decodePacket();
    }

    private function addVersions(BitsPacket $p): int
    {
        $c = $p->getVersion();
        foreach ($p->getPackets() as $subP) {
            $c += $this->addVersions($subP);
        }
        return $c;
    }
/*
    private function printPacket(BitsPacket $p, int $lev = 0): void
    {
        for ($i = 0; $i < 2*$lev; $i++) {
            echo " ";
        }

        printf("%s\n", (string) $p);

        foreach ($p->getPackets() as $subP) {
            $this->printPacket($subP, $lev + 1);
        }
    }
*/
}
