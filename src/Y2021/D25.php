<?php

declare(strict_types=1);

namespace App\Y2021;

use App\Day;

class D25 extends Day
{
    private const XC = 139;
    private const YC = 137;

    public function run(): void
    {
        $data = $this->inputLines(fn ($x) => str_split($x));

        $steps = 0;
        while (true) {
            if ($steps < 3) {
            //    $this->printout($data, $steps);
            }

            $change = false;
            $newData = array_fill(0, self::YC, array_fill(0, self::XC, '.'));

            for ($j = 0; $j < self::YC; $j++) {
                $jprev = $j - 1;
                if ($jprev === -1) {
                    $jprev = self::YC - 1;
                }

                for ($i = 0; $i < self::XC; $i++) {
                    $iprev = $i - 1;
                    $inext = $i + 1;
                    if ($iprev === -1) {
                        $iprev = self::XC - 1;
                    }
                    if ($inext === self::XC) {
                        $inext = 0;
                    }

                    if ($data[$j][$iprev] === '>') {
                        if ($data[$j][$i] === '.') {
                            $newData[$j][$i] = '>';
                            $change = true;
                        } else {
                            $newData[$j][$iprev] = '>';
                        }
                    }

                    if ($data[$jprev][$i] === 'v') {
                        if (($data[$j][$i] === '.' && $data[$j][$iprev] !== '>')
                            || ($data[$j][$i] === '>' && $data[$j][$inext] === '.')
                        ) {
                            $newData[$j][$i] = 'v';
                            $change = true;
                        } else {
                            $newData[$jprev][$i] = 'v';
                        }
                    }
                }
            }

            $steps++;
            if ($steps % 100 === 0) {
                printf("%d\n", $steps);
            }
            $data = $newData;

            if (!$change) {
                break;
            }
        }

        printf("HALT %d\n", $steps);
    }

    private function printout(array $data, int $steps): void
    {
        printf("step %d:\n", $steps);
        foreach ($data as $line) {
            printf("%s\n", implode('', $line));
        }
        printf("\n");
    }
}
