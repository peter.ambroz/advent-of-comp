<?php

declare(strict_types=1);

namespace App\Y2021;

class Bits
{
    private int $nextOffset;

    public function __construct(private string $bits)
    {
        $this->nextOffset = 0;
    }

    public function decodePacket(): ?BitsPacket
    {
        $this->adjustOffset();

        if (strlen($this->bits) < 11) {
            return null;
        }

        $version = bindec($this->sub(0, 3));
        $type = bindec($this->sub(3, 3));

        if ($type === BitsPacket::TYPE_VALUE) {
            $p = new BitsValuePacket($version, $this->decodeValue());
        } else {
            $payloadType = (int) $this->sub(6, 1);
            $payloadLength = match ($payloadType) {
                BitsOpPacket::PAYLOAD_TYPE_BITS => bindec($this->sub(7, 15)),
                BitsOpPacket::PAYLOAD_TYPE_PACKETS => bindec($this->sub(7, 11)),
            };
            $p = new BitsOpPacket($version, $type);

            if ($payloadType === BitsOpPacket::PAYLOAD_TYPE_BITS) {
                $b = new Bits($this->sub(22, $payloadLength));
                while (($dp = $b->decodePacket()) !== null) {
                    $p->addPacket($dp);
                }
            } elseif ($payloadType === BitsOpPacket::PAYLOAD_TYPE_PACKETS) {
                // give it the rest of the bits while not touching them
                $b = new Bits($this->sub(18, null, false));
                for ($i = 0; $i < $payloadLength; $i++) {
                    $p->addPacket($b->decodePacket());
                }
                $b->adjustOffset();
                // get the rest back
                $this->bits = $b->bits;
                $this->nextOffset = 0;
            }
        }
        return $p;
    }

    public static function hex2binary(string $hexData): string
    {
        $bits = '';
        foreach (str_split($hexData) as $nibble) {
            $bits .= sprintf("%04s", base_convert($nibble, 16, 2));
        }
        return $bits;
    }

    private function decodeValue(): int
    {
        $val = '';
        $base = 0;

        do {
            $more = $this->sub($base + 6, 1) === '1';
            $val .= $this->sub($base + 7, 4);
            $base += 5;
        } while ($more);

        return bindec($val);
    }

    private function sub(int $offset, int $length = null, bool $touch = true): string
    {
        if ($touch) {
            $this->nextOffset = max($offset + $length, $this->nextOffset);
        }

        return substr($this->bits, $offset, $length);
    }

    private function adjustOffset(): void
    {
        if ($this->nextOffset > 0) {
            $this->bits = substr($this->bits, $this->nextOffset);
            $this->nextOffset = 0;
        }
    }
}
