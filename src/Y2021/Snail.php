<?php

declare(strict_types=1);

namespace App\Y2021;

abstract class Snail
{
    protected ?SnailDuo $parent;

    public abstract function isLeaf(): bool;

    public function getParent(): ?SnailDuo
    {
        return $this->parent;
    }

    public function setParent(?SnailDuo $parent): void
    {
        $this->parent = $parent;
    }
}
