<?php

declare(strict_types=1);

namespace App\Y2021;

class BitsValuePacket extends BitsPacket
{
    public function __construct(int $version, private int $value)
    {
        parent::__construct($version, BitsPacket::TYPE_VALUE);
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function getPackets(): array
    {
        return [];
    }

    public function __toString(): string
    {
        return sprintf("Type %d, Ver %d, Value %d", $this->type, $this->version, $this->value);
    }
}
