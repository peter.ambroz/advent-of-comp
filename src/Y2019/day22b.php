<?php

class Deck {
    private int $start;
    private int $step;
    private int $size;
    private int $start1;
    private int $step1;

    public function __construct(int $size)
    {
        $this->size = $size;
        $this->start = 0;
        $this->step = 1;
        $this->start1 = 0;
        $this->step1 = 1;
    }

    public function deal(): void
    {
        $this->start = ($this->start - $this->step) % $this->size;
        $this->step *= -1;
    }

    public function cut(int $position): void
    {
        $this->start = $this->get($position);
    }

    public function dealN(int $n): void
    {
        $this->step = (int)bcmod(bcmul($this->step, modInverse($n, $this->size)), $this->size);
    }

    public function get(int $position): int
    {
        $diff = (int)bcmod(bcmul($this->step, $position), $this->size);
        return ($this->start + $diff + $this->size) % $this->size;
    }
    
    public function save(): void
    {
        $this->start1 = $this->start;
        $this->step1 = $this->step;
    }
    
    public function apply(int $times): void
    {
        $this->start = 0;
        $this->step = 1;
        foreach (str_split(decbin($times)) as $bit) {
            $this->start = (int)bcmod(bcmul($this->start, $this->step+1), $this->size);
            $this->step = (int)bcmod(bcmul($this->step, $this->step), $this->size);
            if ($bit == 1) {
                $this->start += (int)bcmod(bcmul($this->start1, $this->step), $this->size);
                $this->step = (int)bcmod(bcmul($this->step, $this->step1), $this->size);
            }
        }
    }
}

function modInverse($a, $m) 
{
    $x = 0; 
    $y = 0; 
    $g = gcdExtended($a, $m, $x, $y); 
    return ($x % $m + $m) % $m; 
} 
  
// function for extended Euclidean Algorithm 
function gcdExtended($a, $b, &$x, &$y) 
{ 
    if ($a == 0) { 
        $x = 0; 
        $y = 1; 
        return $b; 
    } 
  
    $x1; 
    $y1;
    $gcd = gcdExtended($b%$a, $a, $x1, $y1); 
  
    $x = $y1 - (int)($b/$a) * $x1; 
    $y = $x1; 
  
    return $gcd; 
}

$deck = new Deck(119315717514047);
//$deck = new Deck(10007);
$fh = fopen('input22.txt', 'rb');
//for($_i = 0; $_i < 8; $_i++) {
//rewind($fh);
while (!feof($fh)) {
    $line = trim(fgets($fh));
    if (!strlen($line)) {
        continue;
    }

    $cmd = explode(' ', $line);
    if ($cmd[0] === 'cut') {
        $deck->cut($cmd[1]*1);
    } else {
        if ($cmd[1] === 'with') {
            $deck->dealN($cmd[3]*1);
        } else {
            $deck->deal();
        }
    }
}
//}
fclose($fh);
//echo array_search(2019, $d, true)."\n";

echo $deck->get(0)."\n".$deck->get(1)."\n\n";
$deck->save();
$deck->apply(101741582076661);
//var_dump($deck);
echo $deck->get(0)."\n".$deck->get(2019)."\n".$deck->get(2020)."\n\n";
//echo modInverse(1519,10007)."\n";
//echo modInverse(2559,10007)."\n";
/*
3 2 1 0 6 5 4 N=1, d=6
3 6 2 5 1 4 0 N=2, d=3
3 5 0 2 4 6 1 N=3, d=2
3 1 6 4 2 0 5 N=4, d=5
3 0 4 1 5 2 6 N=5, d=4
3 4 5 6 0 1 2 N=6, d=1

5 6 0 1 2 3 4  1
5 2 6 3 0 4 1  4
5 3 1 6 4 2 0  5
5 0 2 4 6 1 3  2
5 1 4 0 3 6 2  3
5 4 3 2 1 0 6  6

5 0 2 4 6 1 3
5 6 0 1 2 3 4
5 1 4 0 3 6 2

0 1 (1)

1519 6678 (5159)

2559 9227 (6668)

4167 313 (6153)

4036 5159 (1123)

8683 8187 (9511)

5784 8712 (2928)

301 5290 (4989)

3293 3540 (247)

101741582076661
 50870791038330 (+1)
 25435395519165
 12717697759582 (+1)
  6358848879791
  3179424439895 (+1)
  1589712219947 (+1)
   794856109973 (+1)
   397428054986 (+1)
   198714027493
    99357013746 (+1)
    49678506873
    24839253436 (+1)
    12419626718
     6209813359
     3104906679 (+1)
     1552453339 (+1)
      776226669 (+1)
      388113334 (+1)
      194056667
       97028333 (+1)
       48514166 (+1)
       24257083
       12128541 (+1)
        6064270 (+1)
        3032135 
        1516067 (+1)
         758033 (+1)
         379016 (+1)
         189508
          94754
          47377
          23688 (+1)
          11844
           5922
           2961
           1480 (+1)
            740
            370
            185
             92 (+1)
             46
             23
             11 (+1)
              5 (+1)
              2 (+1)
              1
              0 (+1)
*/