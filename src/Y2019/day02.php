<?php
function exe() {
  global $data;
  global $ptr;
  switch($data[$ptr]) {
    case 1:
      $data[$data[$ptr+3]] = $data[$data[$ptr+1]] + $data[$data[$ptr+2]];
      $ptr+=4;
      break;
    case 2:
      $data[$data[$ptr+3]] = $data[$data[$ptr+1]] * $data[$data[$ptr+2]];
      $ptr+=4;
      break;
    case 99: 
      $ptr+=1;
      return -1;
    default: die('error '.$data[$ptr]);
  }
  return 1;
}

for ($i = 0; $i<=99; $i++) {
for ($j = 0; $j<=99; $j++) {
printf("%d , %d: ", $i, $j);
$data = explode(',', file_get_contents('input02.txt'));
$data[1] = $i;
$data[2] = $j;
$ptr = 0;
while(1) {
  if (exe() < 0) {
    printf("%d\n", $data[0]);
    if ($data[0] == 19690720) exit(); 
    break;
  }
}
}
}
?>
