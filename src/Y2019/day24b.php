<?php
function dump($life)
{
    foreach($life as $line) {
        echo implode('', $line)."\n";
    }
    echo "\n";
}

function adj($level, $x, $y)
{
    global $overlife;
    
    if ($level < 1) {
        $prevlife = array_fill(0, 5, [0,0,0,0,0]);
    } else {
        $prevlife = $overlife[$level-1];
    }
    $life = $overlife[$level];
    if ($level > 399) {
        $nextlife = array_fill(0, 5, [0,0,0,0,0]);
    } else {
        $nextlife = $overlife[$level+1];
    }
    
    $adj = 0;
    // top, bottom
    switch ($y) {
        case 0:
            $adj += $nextlife[1][2];
            $adj += $life[1][$x];
            break;
        case 1:
            $adj += $life[0][$x];
            if ($x == 2) {
                $adj += array_sum($prevlife[0]);
            } else {
                $adj += $life[2][$x];
            }
            break;
        case 2:
            if ($x != 2) {
                $adj += $life[1][$x];
                $adj += $life[3][$x];
            }
            break;
        case 3:
            if ($x == 2) {
                $adj += array_sum($prevlife[4]);
            } else {
                $adj += $life[2][$x];
            }
            $adj += $life[4][$x];
            break;
        case 4:
            $adj += $life[3][$x];
            $adj += $nextlife[3][2];
            break;
    }
    
    // left, right
    switch ($x) {
        case 0:
            $adj += $nextlife[2][1];
            $adj += $life[$y][1];
            break;
        case 1:
            $adj += $life[$y][0];
            if ($y == 2) {
                $adj += array_sum(array_column($prevlife, 0));
            } else {
                $adj += $life[$y][2];
            }
            break;
        case 2:
            if ($y != 2) {
                $adj += $life[$y][1];
                $adj += $life[$y][3];
            }
            break;
        case 3:
            if ($y == 2) {
                $adj += array_sum(array_column($prevlife, 4));
            } else {
                $adj += $life[$y][2];
            }
            $adj += $life[$y][4];
            break;
        case 4:
            $adj += $life[$y][3];
            $adj += $nextlife[2][3];
            break;
    }
    
    return $adj;
}

function step()
{
    global $overlife;
    $newoverlife = [];
    
    foreach ($overlife as $m => $life) {
        $newlife = [];
        for ($j = 0; $j < 5; $j++) {
            $newrow = [];
            for ($i = 0; $i < 5; $i++) {
                $adj = adj($m, $i, $j);
                $cur = $life[$j][$i];
                
                if ($cur == 1) {
                    if ($adj == 1) {
                        $newrow[] = 1;
                    } else {
                        $newrow[] = 0;
                    }
                } else {
                    if ($adj == 1 || $adj == 2) {
                        $newrow[] = 1;
                    } else {
                        $newrow[] = 0;
                    }
                }
            }
            $newlife[] = $newrow;
        }
        $newoverlife[] = $newlife;
    }
    $overlife = $newoverlife;
}

function bugcnt($overlife)
{
    $cnt = 0;
    foreach ($overlife as $life) {
        foreach ($life as $line) {
            $cnt += array_sum($line);
        }
    }
    return $cnt;
}

$emptylife = array_fill(0, 5, [0, 0, 0, 0, 0]);
$overlife = array_fill(0, 401, $emptylife);
$life = [
  [0, 0, 0, 1, 0],
  [1, 0, 1, 1, 0],
  [1, 0, 0, 1, 1],
  [1, 0, 1, 1, 1],
  [1, 1, 0, 0, 0],
];
/*
$life = [
    [0,0,0,0,1],
    [1,0,0,1,0],
    [1,0,0,1,1],
    [0,0,1,0,0],
    [1,0,0,0,0],
];*/
$overlife[200] = $life;
for ($min = 1; $min <= 200; $min++) {
    step();
}
echo bugcnt($overlife)."\n";