<?php
require('Intcode.php');
$data = explode(',', file_get_contents('input19.intcode.txt'));
/*
echo "\n";

$beam = 0;
$st = 0;
for ($y=0; $y<50; $y++) {
  for ($x=0; $x<50; $x++) {
    $code = new Intcode($data, false);
    $code->setDebug(false);
    
    while (true) {
      try {
        $code->run();
      } catch (Exception $e) {
        $ecode = $e->getCode();
        switch ($ecode) {
          case 4:
            $res = $code->getResult();
            if ($res == 1) $beam++;
            echo $res ? '#' : '.';

            break;
          case 3:
            if ($st == 0) {
              $code->setInput($x);
              $st = 1;
            } else {
              $code->setInput($y);
              $st = 0;
            }
            break;
          default:
            //echo $e->getMessage();
            break 2;
        }
      }
    }
  }
  echo "\n";
}

echo "Beam: $beam\n";
*/

$code = new Intcode($data, false);
$code->setDebug(false);

$line = 950;
$beam = 0;
$st = 0;

$xmin = 0;
$xmax = 0;
$x = 0;
$found = 0;

while (true) {
try {
  $code->run();
} catch (Exception $e) {
  $ecode = $e->getCode();
  switch ($ecode) {
    case 3:
      if ($st == 0) {
        $code->setInput($x);
        $st = 1;
      } else {
        $code->setInput($line);
        $st = 0;
      }
      break;
    case 4:
      $res = $code->getResult();
      switch ($found) {
        case 0:
          if ($res == 1) {
            $found = 1;
            $xmin = $x;
          }
          $x++;
          break;
        case 1:
          if ($res == 0) {
            $found = 0;
            $xmax = $x-1;
            printf("L%03d: W%03d (%03d-%03d)\n", $line, ($xmax - $xmin + 1), $xmin, $xmax);
            $line++;
            if ($line > 1150) break 3;
            $x=$xmin-2;
            $xmax=0;
            $xmin=0;
          } else {
            $x++;
          }
          break;
      }
      break;
    case 99:
      $code->reset();
      break;
  }
}
}