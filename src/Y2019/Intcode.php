<?php
class Intcode
{
  private $rom;

  private $console;
  private $input;
  private $result;
  private $mem;
  private $ip;
  private $base;
  
  private $prefetch;
  
  private $instructionInfo;
  private $execFlags;
  private $halted;
  
  private $params;
  private $execReturn;
  
  private $dbg;
  
  // name, params, immediate-mode params
  private const INSTR = [
    0  => ['err', 0, [], ],
    1  => ['add', 3, [2],],
    2  => ['mul', 3, [2],],
    3  => ['in',  1, [0],],
    4  => ['out', 1, [], ],
    5  => ['jt',  2, [], ],
    6  => ['jf',  2, [], ],
    7  => ['cmplt', 3, [2],],
    8  => ['cmpeq', 3, [2],],
    9  => ['cbase', 1, [], ],
    99 => ['hlt',   0, [], ],
  ];
  
  private const PREFETCH_MAX = 4;
  
  public function __construct(array $data, bool $console = true)
  {
    $this->rom = $data;
    $this->console = $console;
    $this->dbg = true;
    
    $this->reset();
  }
  
  public function reset(): void
  {
    $this->mem = $this->rom;
    $this->result = null;
    $this->input = null;
    $this->halted = false;
    $this->base = 0;
    $this->ip = 0;
  }
  
  public function loadFile(string $file): void
  {
    $this->rom = explode(',', file_get_contents($file));
    $this->reset();
  }
  
  public function run(): void
  {
    if ($this->isHalted()) {
      $this->hlt();
    }
    while(true) {
      $this->fetch();
      $this->decode();
      $this->execute();
      $this->writeback();
    }
  }
  
  public function getResult(): int
  {
    return $this->result;
  }
  
  public function setInput(?int $num): void
  {
    $this->input = $num;
  }
  
  public function isHalted(): bool
  {
    return $this->halted;
  }

  public function setDebug(bool $debug): void
  {
    $this->dbg = $debug;
  }
  
  private function fetch(): void
  {
    $this->prefetch = [];
    for ($i = 0; $i < self::PREFETCH_MAX; $i++) {
      $this->prefetch[] = $this->mem[$this->ip + $i] ?? 0;
    }
    if (!count($this->prefetch)) {
      throw new FatalException(sprintf("Fetch failed, empty memory at IP: %d\n", $this->ip));
    }
  }
  
  private function decode(): void
  {
    if (!count($this->prefetch)) {
      throw new FatalException(sprintf("Decode failed, empty memory at IP: %d\n", $this->ip));
    }
    
    $opcode = ($this->prefetch[0] % 100)*1;
    $this->instructionInfo = self::INSTR[$opcode] ?? self::INSTR[0];
    $this->execFlags = $this->decodeModeFlags($this->prefetch[0], $this->instructionInfo);
  }
  
  private function execute(): void
  {
    $method = $this->instructionInfo[0];
    $this->params = [];
    for ($i = 0; $i < $this->instructionInfo[1]; $i++) {
      switch ($this->execFlags[$i]) {
        case 0:
          $this->params[] = $this->mem[$this->prefetch[$i+1]] ?? 0;
          break;
        case 1:
        case -1:
          $this->params[] = $this->prefetch[$i+1];
          break;
        case 2:
          $this->params[] = $this->mem[$this->prefetch[$i+1] + $this->base] ?? 0;
          break;
        case -2:
          $this->params[] = $this->prefetch[$i+1] + $this->base;
          break;
      }
    }
    $this->execReturn = call_user_func_array([$this, $this->instructionInfo[0]], $this->params);
    if ($this->execReturn === false) {
      throw new FatalException(sprintf("Execute failed, method '%d' not found\n", $this->instructionInfo[0]));
    }
  }
  
  private function writeback(): void
  {
    $ipSet = false;
    foreach ($this->execReturn as $pos => $data) {
      if ($pos === 'ip') {
        $this->ip = $data;
        $ipSet = true;
      } else {
        $this->mem[$pos] = $data;
      }
    }
    if (!$ipSet) {
      $this->ip += $this->instructionInfo[1] + 1;
    }
  }
  
  private function decodeModeFlags($instrOrig, $info): array
  {
    $flags = [];
    $instr = '0'.substr((string)$instrOrig, 0, -2);
    for ($i = 0; $i < $info[1]; $i++) {
      $f = substr($instr, ($i+1)*-1, 1)*1;
      if (in_array($i, $info[2])) {
        switch ($f) {
          case 0:
            $f = -1;
            break;
          case 1:
            throw new FatalException(sprintf("Decode Mode Flags failed Opcode %d @ IP %d\n", $instrOrig, $this->ip));
          case 2:
            $f = -2;
            break;
        }
        
      }
      $flags[] = $f;
    }
    return $flags;
  }
  
  /***************************************/
  private function err()
  {
    throw new FatalException(sprintf("Bad instruction %d at IP: %d\n", $this->mem[$this->ip], $this->ip));
  }
  
  private function add($a, $b, $c)
  {
    return [$c => $a + $b];
  }
  
  private function mul($a, $b, $c)
  {
    return [$c => $a * $b];
  }
  
  private function in($a)
  {
    if ($this->console) {
      printf('IN? ', $a);
      return [$a => trim(fgets(STDIN))*1];
    } else {
      if ($this->input === null) {
        throw new InputRequiredException();
      }
      $input = $this->input*1;
      $this->input = null;
      if ($this->dbg) echo "IN? ".$input."\n";
      return [$a => $input];
    }
  }
  
  private function out($a)
  {
    if (!$this->console && $this->result !== null) {
      $this->result = null;
      return [];
    }
    
    if ($this->console || $this->dbg) printf("OUT: %d\n", $a);
    $this->result = $a;
    if (!$this->console) {
      throw new OutputAvailableException();
    }
    return [];
  }
  
  private function hlt()
  {
    $this->halted = true;
    if ($this->dbg) {
      printf("Halt @ %d\n", $this->ip);
    }
    throw new HaltedException("Halted!\n", 99);
  }
  
  private function jt($a, $b)
  {
    if ($a != 0) {
      if ($this->dbg) {
        printf("JumpT %d -> %d\n", $this->ip, $b);
      }
      return ['ip' => $b];
    }
    return [];
  }
  
  private function jf($a, $b)
  {
    if ($a == 0) {
      if ($this->dbg) {
        printf("JumpF %d -> %d\n", $this->ip, $b);
      }
      return ['ip' => $b];
    }
    return [];
  }
  
  private function cmplt($a, $b, $c)
  {
    if ($a < $b) {
      return [$c => 1];
    }
    return [$c => 0];
  }

  private function cmpeq($a, $b, $c)
  {
    if ($a == $b) {
      return [$c => 1];
    }
    return [$c => 0];
  }
  
  private function cbase($a)
  {
    $this->base += $a;
    if ($this->dbg) {
      printf("Base %d (diff %d)\n", $this->base, $a);
    }
    return [];
  }
}

class InputRequiredException extends Exception
{
    public function __construct()
    {
        parent::__construct("Input required\n", 3);
    }
}

class OutputAvailableException extends Exception
{
    public function __construct()
    {
        parent::__construct("Output available\n", 4);
    }
}

class HaltedException extends Exception
{
    public function __construct()
    {
        parent::__construct("Halted\n", 99);
    }
}


class FatalException extends Exception {}