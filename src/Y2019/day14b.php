<?php
function calc($ins)
{
    global $rec;
    $amt = [];
    foreach ($ins as $in => $need) {
        $provide = $rec[$in]['q'];
        foreach ($rec[$in]['i'] as $out => $req) {
            if (!isset($amt[$out])) $amt[$out] = 0;
            $amt[$out] += (($need*$req) / $provide);
        }
    }
    return $amt;
}

$rec = json_decode(file_get_contents('input14.json'), true);

$ore = 0.0;
$iter = ['FUEL' => 1];
while (count($iter)) {
    $iter = calc($iter);
    if (isset($iter['ORE'])) {
        $ore += $iter['ORE'];
        unset($iter['ORE']);
    }
}
echo $ore."\n";
echo 1000000000000 / $ore . "\n";