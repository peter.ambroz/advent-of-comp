<?php
require('Intcode.php');
$data = explode(',', file_get_contents('input09.txt'));
$code = new Intcode($data);
$code->setDebug(false);
try {
  $code->run();
} catch (Exception $e) {
  echo $e->getMessage();
}
?>