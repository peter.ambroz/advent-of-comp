<?php

function simplify($x, $y): array {
  $sx = $x;
  $sy = $y;
  $c = 1;
  for ($i=20; $i>1; $i--) {
    if (($sx%$i == 0) && ($sy%$i == 0)) {
      $c*=$i;
      $sx = intdiv($sx, $i);
      $sy = intdiv($sy, $i);
    }
  }
  return [$sx, $sy, $c];
}

function visible($x1, $y1, $x2, $y2): int {
  global $map;
  $dx = $x2-$x1;
  $dy = $y2-$y1;
  [$sx, $sy, $c] = simplify($dx, $dy);
  for ($i=1; $i<$c; $i++) {
    $tx = $x1 + $sx*$i;
    $ty = $y1 + $sy*$i;
    if (isset($map[$tx.'_'.$ty])) {
      return 0;
    }
  }
  return 1;
}

$map = [];
$fh = fopen('input10.txt', 'r');
$y = 0;
while(!feof($fh)) {
  $l = trim(fgets($fh));
  if (!strlen($l)) continue;
  $x = 0;
  foreach(str_split($l) as $char) {
    if ($char === '#') {
      $map[$x.'_'.$y] = true;
    }
    $x++;
  }
  $y++;
}
fclose($fh);

$max = 0;
$data = [];
$coords = [-1, -1];
foreach ($map as $id1 => $p1) {
  $vis = 0;
  [$x1, $y1] = explode('_', $id1);
  foreach ($map as $id2 => $p2) {
    if ($id1 === $id2) continue;
    [$x2, $y2] = explode('_', $id2);
    $vis += visible($x1, $y1, $x2, $y2);
  }
  if ($vis > $max) {
    $max = $vis;
    $coords = $x1.' '.$y1;
  }
}

echo $max."\n";
echo $coords."\n";


?>