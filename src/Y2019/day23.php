<?php
require('Intcode.php');

class Que
{
    public $state;
    public $inState;
    private $inPacket;
    private $queue;
    private $id;
    
    public function __construct(int $id)
    {
        $this->queue = [];
        $this->state = 0;
        $this->inState = 0;
        $this->id = $id;
    }
    
    public function getId(): int
    {
        return $this->id;
    }
    
    public function isEmpty(): bool
    {
        return !count($this->queue);
    }
    
    private function push(array $packet)
    {
        if ($this->id == 255) {
            $this->queue = [$packet];
        } else {
            $this->queue[] = $packet;
        }
        printf("Recv for %d: [%d, %d]\n", $this->id, $packet[0], $packet[1]);
    }
    
    private function pop(): ?array
    {
        if (count($this->queue)) {
            $packet = array_shift($this->queue);
            printf("Send to %d: [%d, %d]\n", $this->id, $packet[0], $packet[1]);
            return $packet;
        } else {
            return null;
        }
        $this->state = 0;

    }

    private function peek(): ?array
    {
        if (isset($this->queue[0])) {
            return $this->queue[0];
        } else {
            return null;
        }
    }
    
    public function halfPop(): int
    {
        $packet = $this->peek();
        if ($packet === null) {
            $this->state = 0;
            return -1;
        }
        if ($this->state == 0) {
            $this->state = 1;
            return $packet[0];
        } else {
            $this->state = 0;
            $this->pop();
            return $packet[1];
        }
    }
    
    public function halfPush(int $val): void
    {
        if ($this->inState == 0) {
            $this->inPacket = [];
            $this->inState = 1;
        } elseif ($this->inState == 1) {
            $this->inPacket[0] = $val;
            $this->inState = 2;
        } else {
            $this->inPacket[1] = $val;
            $this->inState = 0;
            $this->push($this->inPacket);
        }
    }
}

function isIdle(array $queue)
{
    for ($i=0; $i<50; $i++) {
        if (!$queue[$i]->isEmpty()) return false;
    }
    return true;
}

$queue = [];
$machine = [];

for ($i=0; $i<50; $i++) {
    $code = new IntCode([], false);
    $code->setDebug(false);
    $code->loadFile('input23.intcode.txt');
    $code->setInput($i);
    $machine[$i] = $code;
    $queue[$i] = new Que($i);
}
$queue[255] = new Que(255);

$i = 0;
while (true) {
    $code = $machine[$i];
    $q = $queue[$i];
    $outQ = null;
    while (true) {
        try {
            $code->run();
        } catch (InputRequiredException $e) {
            $code->setInput($q->halfPop());
            if ($q->state == 0) {
                break;
            }
        } catch (OutputAvailableException $e) {
            $res = $code->getResult();
            if ($outQ === null) {
                $outQ = $queue[$res];
                $outQ->halfPush(0);
            } else {
                $outQ->halfPush($res);
                if ($outQ->inState == 0) {
                    $outQ = null;
                    break;
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            break;
        }
    }
    $i = ($i+1) % 50;
    if ($i == 0 && isIdle($queue)) {
        $q0 = $queue[0];
        $q255 = $queue[255];
        $q0->halfPush(0);
        $q0->halfPush($q255->halfPop());
        $q0->halfPush($q255->halfPop());
    }
}