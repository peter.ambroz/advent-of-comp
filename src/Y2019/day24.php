<?php
function dump($life)
{
    foreach($life as $line) {
        echo implode('', $line)."\n";
    }
    echo "\n";
}

function step($life)
{
    $newlife = [];
    for ($j = 0; $j < 5; $j++) {
        $newrow = [];
        for ($i = 0; $i < 5; $i++) {
            $adj = 0;
            $cur = $life[$j][$i];
            if ($j > 0) $adj += ($life[$j-1][$i] === '#' ? 1 : 0);
            if ($i < 4) $adj += ($life[$j][$i+1] === '#' ? 1 : 0);
            if ($j < 4) $adj += ($life[$j+1][$i] === '#' ? 1 : 0);
            if ($i > 0) $adj += ($life[$j][$i-1] === '#' ? 1 : 0);
            
            if ($cur === '#') {
                if ($adj == 1) {
                    $newrow[] = '#';
                } else {
                    $newrow[] = '.';
                }
            } else {
                if ($adj == 1 || $adj == 2) {
                    $newrow[] = '#';
                } else {
                    $newrow[] = '.';
                }
            }
        }
        $newlife[] = $newrow;
    }
    return $newlife;
}

function val($life)
{
    $val = 0;
    $cnt = 0;
    foreach ($life as $line) {
        foreach($line as $char) {
            if ($char === '#') {
                $val += 2 ** $cnt;
            }
            $cnt++;
        }
    }
    return $val;
}

$vmap = [];
$life = [
  ['.', '.', '.', '#', '.'],
  ['#', '.', '#', '#', '.'],
  ['#', '.', '.', '#', '#'],
  ['#', '.', '#', '#', '#'],
  ['#', '#', '.', '.', '.'],
];
/*
$life = [
  ['.', '.', '.', '.', '#'],
  ['#', '.', '.', '#', '.'],
  ['#', '.', '.', '#', '#'],
  ['.', '.', '#', '.', '.'],
  ['#', '.', '.', '.', '.'],
];
*/
while (true) {
    //dump($life);
    $hash = val($life);
    if (isset($vmap[$hash])) {
        echo $hash."\n";
        exit();
    }
    $vmap[$hash] = true;
    $life = step($life);
}