<?php
require('Intcode.php');
$code = new Intcode(explode(',', file_get_contents('input05.txt')));
try {
  $code->run();
} catch (Exception $e) {
  printf($e->getMessage());
}
?>
