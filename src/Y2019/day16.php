<?php
$num = file_get_contents('input16.txt');
$origSize = 650;
$offset = 369;

$bignum = '';
for ($i=0; $i<814; $i++) {
  $bignum.=$num;
}

$biga = array_map(function($x) {return (int)$x;}, str_split($bignum));

function fft($ar) {
  $base = [0, 1, 0, -1];
  $ret = [];
  $arc = count($ar);
  
  for ($j = 1; $j <= $arc; $j++) {
    $sum = 0;
    $bp = 0;
    $br = 1;
    for ($k = 0; $k < $arc; $k++) {
      if ($br >= $j) {
        $bp = ($bp+1)%4;
        $br = 0;
      }
      switch($bp) {
        case 1: $sum += $ar[$k]; break;
        case 3: $sum -= $ar[$k]; break;
      }
      $br++;
    }
    $ret[] = ((int)abs($sum))%10;
    echo "  $j\n";
  }
  return $ret;
}

function fft2($ar)
{
    $ret = [];
    $arc = count($ar);
  
    $sum = 0;
    for ($j = $arc - 1; $j >= 0; $j--) {
        $sum += $ar[$j];
        $ret[] = $sum%10;
    }
    return array_reverse($ret);
}

for ($i = 0; $i < 100; $i++) {
  //$num = fft($num);
  $biga = fft2($biga);
  echo $i."\n";
}

//echo $offset."\n";
echo substr(implode('', $biga), $offset, 8)."\n";

