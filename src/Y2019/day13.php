<?php
require('Intcode.php');
$data = explode(',', file_get_contents('input13b.txt'));
$code = new Intcode($data, false);
$code->setDebug(false);
$st = 0;
$dt = [0, 0, 0];
$gr = 0;
$cr = 0; $bl = 0;
$score = 0;

$img = imagecreate(38, 22);
imagecolorallocate($img, 0,0,0);
$cx = [];
$cx[0] = imagecolorallocate($img, 64,64,64);
$cx[1] = imagecolorallocate($img, 255,0,0);
$cx[2] = imagecolorallocate($img, 0,255,0);
$cx[3] = imagecolorallocate($img, 0,0,255);
$cx[4] = imagecolorallocate($img, 255,255,0);
$cx[5] = imagecolorallocate($img, 0,255,255);

while(true) {
try {
  $code->run();
} catch (Exception $e) {
  $excode = $e->getCode();
  switch ($excode) {
    case 4:
      $dt[$st] = $code->getResult();
      $st = ($st+1)%3;
      if ($st == 0) {
        if ($dt[0] == -1 && $dt[1] == 0) {
          $score = $dt[2];
          printf("sc: %d\n", $score);
        } else {
          if ($dt[2] == 3) {
            $cr = $dt[0];
          }
          if ($dt[2] == 4) {
            $bl = $dt[0];
          }
          imagesetpixel($img, $dt[0], $dt[1], $cx[$dt[2]]);
          if ($dt[2] == 2) $gr++;
        }
      }
      continue 2;
    case 3:
      imagepng($img, 'day13.png');
      if ($cr > $bl) $code->setInput(-1);
      elseif ($cr < $bl) $code->setInput(1);
      else $code->setInput(0);
      usleep(100000);
      break;
    case 99:
    default:
      echo $e->getMessage();
      break 2;
  }
}
}
//echo $gr."\n";