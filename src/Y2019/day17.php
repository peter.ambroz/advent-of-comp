<?php
require('AsciiCode.php');
$code = new AsciiCode('input17.intcode.txt');

$st = 0;
while (true) {
try {
  $code->run();
} catch (Exception $e) {
  $ecode = $e->getCode();
  switch ($ecode) {
    case 4:
      $res = $code->getResult();
      if ($res > 255) {
        echo $res;
        break;
      }
      echo chr($res);
      if ($st == 0 && $res == 10) {
        $st = 1;
      } elseif ($st == 1 && $res != 10) {
        $st = 0;
      } elseif ($st == 1 && $res == 10) {
        $st = 0;
        usleep(200000);
      }
      break;
    case 3:
      echo "IN? ";
      $code->setAsciiInput(fgets(STDIN));
      break;
    default:
      echo $e->getMessage();
      break 2;
  }
}
}

/*
A: R,12,L,8,R,6,
A: R,12,L,8,R,6,
B: R,12,L,6,R,6,R,8,R,6,
C: L,8,R,8,R,6,R,12,
A: R,12,L,8,R,6,
C: L,8,R,8,R,6,R,12,
A: R,12,L,8,R,6,
B: R,12,L,6,R,6,R,8,R,6,
C: L,8,R,8,R,6,R,12,
B: R,12,L,6,R,6,R,8,R,6
*/

/*
R,12,L,8,R,6,
R,12,L,8,R,6,
R,12,L,6,R,6,
R,8,R,6,L,8,
R,8,R,6,R,12,
R,12,L,8,R,6,
L,8,R,8,R,6,
R,12,R,12,L,8,
R,6,R,12,L,6,
R,6,R,8,R,6,
L,8,R,8,R,6,
R,12,R,12,L,6,
R,6,R,8,R,6

88 + 77  = 165
165 / 20 = 8
*/