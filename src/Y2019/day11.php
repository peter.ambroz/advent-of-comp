<?php
require('Intcode.php');
$data = explode(',', file_get_contents('input11.txt'));
$code = new Intcode($data, false);
$code->setDebug(false);
$data = ["0_0" => 1];
$dir = 0;
$x = 0;
$y = 0;
$state = 0;
$ext = ['L'=>0,'R'=>0,'U'=>0,'D'=>0];
while(true) {
try {
  $code->run();
} catch (Exception $e) {
  $pos = sprintf('%d_%d', $x, $y);
  switch($e->getCode()) {
    case 3:
      if ($state != 0) {
        echo 'Wrong state';
        exit();
      }
      $code->setInput($data[$pos] ?? 0);
      break;
    case 4:
      if ($state == 0) {
        $data[$pos] = $code->getResult();
        $state = 1;
      } else {
        $turn = $code->getResult();
        if ($turn) $dir = ($dir+1)%4;
        else $dir = ($dir+3)%4;
        switch($dir) {
          case 0: $y--; if ($y < $ext['U']) $ext['U'] = $y; break;
          case 1: $x++; if ($x > $ext['R']) $ext['R'] = $x; break;
          case 2: $y++; if ($y > $ext['D']) $ext['D'] = $y; break;
          case 3: $x--; if ($x < $ext['L']) $ext['L'] = $x; break;        
        }
        $state = 0;
      }
      break;
    case 99:
      echo $e->getMessage();
      break 2;
  }
}
}
echo count($data)."\n";

$img = imagecreate(abs($ext['L'])+abs($ext['R'])+1, abs($ext['U'])+abs($ext['D'])+1);
imagecolorallocate($img, 0,0,0);
$cx = [];
$cx[0] = imagecolorallocate($img, 255,0,0);
$cx[1] = imagecolorallocate($img, 0,255,0);
foreach($data as $idx => $id) {
  list($x, $y) = explode('_', $idx);
  imagesetpixel($img, $x*1 + $ext['L']*-1, $y*1+$ext['U']*-1, $cx[$id]);
}
imagepng($img, 'day11.png');
?>