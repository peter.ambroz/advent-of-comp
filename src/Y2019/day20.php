<?php
$map = [];

$fh = fopen('output20.txt', 'r');
while (!feof($fh)) {
  $l = fgets($fh);
  if (!strlen($l)) continue;
  $map[] = str_split($l);
}
fclose($fh);

foreach($map as $y=>$ln) {
  if (!isset($map[$y-1]) || !isset($map[$y+1])) continue;
  foreach($ln as $x=>$c) {
    if (!isset($ln[$x-1]) || !isset($ln[$x+1])) continue;
    $h=0;
    if ($map[$y-1][$x]==='#') $h++;
    if ($map[$y+1][$x]==='#') $h++;
    if ($map[$y][$x-1]==='#') $h++;
    if ($map[$y][$x+1]==='#') $h++;
    if ($h >= 3 && $c === '.') $map[$y][$x] = '#';
  }
  
}


$fh = fopen('output20.txt', 'w');
foreach($map as $ln) {
  fputs($fh, implode('', $ln));
}
fclose($fh);
