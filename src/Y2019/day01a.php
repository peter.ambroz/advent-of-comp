<?php
function fuel($x) {
  return max((int)($x / 3) - 2, 0);
}

function fuel_r($x) {
  $next = fuel($x);
  if ($next <= 0) return 0;

  return $next + fuel_r($next);
}

$fh = fopen('input1.txt', 'r');
$c = 0;
$c2 = 0;
while(!feof($fh)) {
  $l = trim(fgets($fh));
  if (!strlen($l)) continue;

  $c += (int)($l / 3)-2;
  $c2 += fuel_r($l);
}
fclose($fh);
echo $c."\n";
echo $c2;
?>
