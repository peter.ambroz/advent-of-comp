<?php
function mark($id, $x, $y, $steps) {
  global $cr;
  global $cross;
  global $closest;
  global $shortest;
  
  $idx = sprintf('%d_%d', $x, $y);
  if (!isset($cr[$idx])) {
    $cr[$idx] = [$id => $steps];
  } else {
    if (!isset($cr[$idx][$id])) {
      $cr[$idx][$id] = $steps;
    }
    if (count($cr[$idx]) > 1) {
      $cross[$idx] = array_sum($cr[$idx]);
      $dist = abs($x) + abs($y);
      if ($closest > $dist) $closest = $dist;
      if ($shortest > $cross[$idx]) $shortest = $cross[$idx];
    }
  }
}

$fh = fopen('input03.txt', 'r');
$l[0] = explode(',',trim(fgets($fh)));
$l[1] = explode(',',trim(fgets($fh)));
fclose($fh);
$cr = [];
$cross = [];
$ext = ['U' => 0, 'D' => 0, 'L' => 0, 'R' => 0];
$closest = 9999999;
$shortest = $closest;
foreach($l as $id => $line) {
  $x = 0;
  $y = 0;
  $steps = 0;
  foreach($line as $instr) {
    $dir = $instr[0];
    $cnt = substr($instr, 1)*1;
    switch($dir) {
      case 'U':
        for($i=1; $i<=$cnt; $i++) {
          $y--;
          $steps++;
          mark($id, $x, $y, $steps);
        }
        if ($ext['U'] > $y) $ext['U'] = $y;
        break;
      case 'D':
        for($i=1; $i<=$cnt; $i++) {
          $y++;
          $steps++;
          mark($id, $x, $y, $steps);
        }
        if ($ext['D'] < $y) $ext['D'] = $y;
        break;
      case 'L':
        for($i=1; $i<=$cnt; $i++) {
          $x--;
          $steps++;
          mark($id, $x, $y, $steps);
        }
        if ($ext['L'] > $x) $ext['L'] = $x;
        break;
      case 'R':
        for($i=1; $i<=$cnt; $i++) {
          $x++;
          $steps++;
          mark($id, $x, $y, $steps);
        }
        if ($ext['R'] < $x) $ext['R'] = $x;
        break;
    }
  }
}
echo $closest."\n";
echo count($cr)."\n";
echo count($cross)."\n";
echo $shortest."\n";
exit();
$img = imagecreate(abs($ext['L'])+abs($ext['R'])+1, abs($ext['U'])+abs($ext['D'])+1);
imagecolorallocate($img, 0,0,0);
$cx = [];
$cx[0] = imagecolorallocate($img, 255,0,0);
$cx[1] = imagecolorallocate($img, 0,255,0);
foreach($cr as $idx => $colors) {
  foreach($colors as $id => $dum) {
    list($x, $y) = explode('_', $idx);
    imagesetpixel($img, $x*1 + $ext['L']*-1, $y*1+$ext['U']*-1, $cx[$id]);
  }
}
imagepng($img, 'day03.png');
?>
