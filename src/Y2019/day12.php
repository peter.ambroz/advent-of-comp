<?php
function sim() {
  global $moons;
  global $vel;

  foreach($moons as $id1 => $m1) {
    foreach($moons as $id2 => $m2) {
      if ($id1 >= $id2) continue;
      
      foreach (['x', 'y', 'z'] as $axis) {
        if ($m1[$axis] > $m2[$axis]) {
          $vel[$id1][$axis]--;
          $vel[$id2][$axis]++;
        } elseif ($m1[$axis] < $m2[$axis]) {
          $vel[$id1][$axis]++;
          $vel[$id2][$axis]--;
        }
      }
    }
  }
  
  foreach($moons as $id => $moon) {
    foreach(['x', 'y', 'z'] as $axis) {
      $moons[$id][$axis] = $moon[$axis] + $vel[$id][$axis];
    }
  }  
}

$moons = [
  ['x' =>  3, 'y' => 15, 'z' =>  8],
  ['x' =>  5, 'y' => -1, 'z' => -2],
  ['x' =>-10, 'y' =>  8, 'z' =>  2],
  ['x' =>  8, 'y' =>  4, 'z' => -5],
];
$vel = [
  ['x' => 0, 'y' => 0, 'z' => 0],
  ['x' => 0, 'y' => 0, 'z' => 0],
  ['x' => 0, 'y' => 0, 'z' => 0],
  ['x' => 0, 'y' => 0, 'z' => 0],
];

for ($t = 0; $t < 1000; $t++) {
  sim();
}

$e = 0;
foreach($moons as $id => $moon) {
  $kin = 0;
  $pot = 0;
  foreach(['x', 'y', 'z'] as $axis) {
    $kin += (int)abs($vel[$id][$axis]);
    $pot += (int)abs($moon[$axis]);
  }
  $e += $kin*$pot;
}

echo $e."\n";
/*
$moons = [
  ['x' =>  -1, 'y' => 0, 'z' =>  2],
  ['x' =>  2, 'y' => -10, 'z' => -7],
  ['x' =>  4, 'y' =>  -8, 'z' =>  8],
  ['x' =>  3, 'y' =>  5, 'z' => -1],
];
*/
$moons = [
  ['x' =>  3, 'y' => 15, 'z' =>  8],
  ['x' =>  5, 'y' => -1, 'z' => -2],
  ['x' =>-10, 'y' =>  8, 'z' =>  2],
  ['x' =>  8, 'y' =>  4, 'z' => -5],
];
$vel = [
  ['x' => 0, 'y' => 0, 'z' => 0],
  ['x' => 0, 'y' => 0, 'z' => 0],
  ['x' => 0, 'y' => 0, 'z' => 0],
  ['x' => 0, 'y' => 0, 'z' => 0],
];
$snaps = [
    'x' => [array_column($moons, 'x'), array_column($vel, 'x')],
    'y' => [array_column($moons, 'y'), array_column($vel, 'y')],
    'z' => [array_column($moons, 'z'), array_column($vel, 'z')],
];

$steps = 0;
$done = false;
$reps = [
    'x' => 0,
    'y' => 0,
    'z' => 0,
];
while (!$done) {
    sim();
    $steps++;
    $done = true;
    foreach ($snaps as $coord => $init) {
        if ($reps[$coord] != 0) continue;
        $done = false;
        if (array_column($moons, $coord) == $init[0] && array_column($vel, $coord) == $init[1]) {
            $reps[$coord] = $steps;
        }
    }
}

var_dump($reps);
echo gmp_lcm(gmp_lcm($reps['x'], $reps['y']), $reps['z'])."\n";
