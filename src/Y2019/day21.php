<?php
require('AsciiCode.php');
$code = new AsciiCode('input21.intcode.txt');

while (true) {
try {
  $code->run();
} catch (Exception $e) {
  $ecode = $e->getCode();
  switch ($ecode) {
    case 4:
      $res = $code->getResult();
      if ($res < 256) {
        echo chr($res);
      } else {
        echo $res;
      } 
      break;
    case 3:
      echo "IN? ";
      $code->setAsciiInput(fgets(STDIN));
      break;
    default:
      echo $e->getMessage();
      break 2;
  }
}
}

/*
  PART 1:
NOT A J
NOT C T
AND D T
OR T J
NOT B T
AND D T
OR T J

  PART 2:
NOT A J
NOT B T
AND D T
OR T J
NOT C T
AND D T
AND H T
OR T J
*/