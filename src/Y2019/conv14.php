<?php
$fh = fopen('input14c.txt', 'r');
$rec = [];
while(!feof($fh)) {
  $l = trim(fgets($fh));
  if (!strlen($l)) continue;
  [$left, $right] = explode(' => ', $l, 2);
  [$rq, $rn] = explode(' ', $right, 2);
  $rn = trim($rn);
  $leftparts = explode(', ', $left);
  $lp = [];
  foreach ($leftparts as $part) {
    [$lq, $ln] = explode(' ', $part, 2);
    $lp[trim($ln)] = $lq*1;
  }
  $rec[$rn] = ['q' => $rq*1, 'i' => $lp];
}
fclose($fh);
file_put_contents('input14c.json', json_encode($rec));
?>
