<?php
require_once('Intcode.php');

class AsciiCode extends Intcode
{
    private $asciiInput;

    public function __construct(string $fileName)
    {
        parent::__construct([], false);
        $this->setDebug(false);
        $this->loadFile($fileName);
        $this->asciiInput = [];
    }
    
    public function run(): void
    {
        while (true) {
            try {
                parent::run();
            } catch (InputRequiredException $e) {
                if (count($this->asciiInput)) {
                    $this->setInput(ord(array_shift($this->asciiInput)));
                } else {
                    throw $e;
                }
                break;
            }
        }
    }
    
    public function setAsciiInput(string $input): void
    {
        $this->asciiInput = str_split(trim($input) . "\n");
    }
    
    public function getAsciiResult(): string
    {
        return chr($this->getResult());
    }
}