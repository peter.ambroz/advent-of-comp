<?php

function canGoTo($place)
{
  return (
    ($place === '.') ||
    ($place >= 'a' && $place <= 'z') ||
    ($place >= 'A' && $place <= 'Z')
  );
}

function walk($x, $y, $off): array
{
  global $map;
  
  $step = [
    'x' => $x,
    'y' => $y,
    'c' => $map[$y][$x],
    'o' => $off,
    'f' => []
  ];
  $map[$y][$x] = ' ';
  
  if ($y > 0 && canGoTo($map[$y-1][$x])) {
    $step['f'][] = walk($x, $y-1, $off+1);
  }
  if ($x < 80 && canGoTo($map[$y][$x+1])) {
    $step['f'][] = walk($x+1, $y, $off+1);
  }
  if ($y < 80 && canGoTo($map[$y+1][$x])) {
    $step['f'][] = walk($x, $y+1, $off+1);
  }
  if ($x > 0 && canGoTo($map[$y][$x-1])) {
    $step['f'][] = walk($x-1, $y, $off+1);
  }
  
  if (count($step['f'])) {
    $path = array_shift($step['f']);
    array_unshift($path, $step);
    return $path;
  } else {
    return [$step];
  }
}

function trace($steps, $start)
{
  $forks = [];
  echo str_repeat('.', $start);
  foreach ($steps as $step) {
    echo ($step['c'] === '.' ? '-' : $step['c']);
    if (count($step['f'])) {
      $forks[] = $step;
    }
  }
  echo "\n";
  while (count($forks)) {
    $step = array_pop($forks);
    foreach ($step['f'] as $id => $s) {
      trace($s, $step['o']);
    }
  }
}

function path($a, $b, $stack)
{
  global $map;
  
  if ($a[0] == $b[0] && $a[1] == $b[1]) {
    return 0;
  }
  
  $len = 999999999;
  $x = $a[0];
  $y = $a[1];
  $try = [-2, -2, -2, -2];
  
  $a = [$x, $y-1];
  $st = $stack;
  if ($map[$y-1][$x] !== '#' && !in_array($a, $stack)) {
    $st[] = $a;
    $try[0] = path($a, $b, $st)+1;
  }
  $a = [$x+1, $y];
  $st = $stack;
  if ($map[$y][$x+1] !== '#' && !in_array($a, $stack)) {
    $st[] = $a;
    $try[1] = path($a, $b, $st)+1;
  }
  $a = [$x, $y+1];
  $st = $stack;
  if ($map[$y+1][$x] !== '#' && !in_array($a, $stack)) {
    $st[] = $a;
    $try[2] = path($a, $b, $st)+1;
  }
  $a = [$x-1, $y];
  $st = $stack;
  if ($map[$y][$x-1] !== '#' && !in_array($a, $stack)) {
    $st[] = $a;
    $try[3] = path($a, $b, $st)+1;
  }
  
  $found = false;
  foreach ($try as $t) {
    if ($t < 0) continue;
    $found = true;
    if ($t < $len) $len = $t;
  }
  
  return $found ? $len : -2;
}

$map = [];

$fh = fopen('input18.txt', 'r');
while (!feof($fh)) {
  $l = trim(fgets($fh));
  if (!strlen($l)) continue;
  $map[] = str_split($l);
}
fclose($fh);

$abc = ['@' => [40,40]];
foreach($map as $y=>$ln) {
  foreach($ln as $x=>$c) {
    if ($c >= 'a' && $c <= 'z') $abc[$c] = [$x, $y];
    if ($c >= '0' && $c <= '9') $abc[$c] = [$x, $y];
  }
}
ksort($abc);

//$steps = walk(39, 41, 0);
//trace($steps, 0);

$scenario = [
  ['1', 'c'],
  ['2', 'r', 's'],
  ['3', 'b', 'j', 'i'],
  ['4', 'l'],
  ['c', 'o', 'f'],
  ['s', 'q', 'a'],
  ['i', 'g', 'k', 'h'],
  ['l', 'z', 'v'],
  ['h', 'e'],
  ['v', 'u']
];

$cnt = 0;
foreach ($scenario as $scen) {
for ($i=0; $i<count($scen)-1; $i++) {
  echo $scen[$i].$scen[$i+1].": ";
  $from = $abc[$scen[$i]];
  $to = $abc[$scen[$i+1]];
  $len = path($from, $to, []);
  echo $len."\n";
  $cnt+=$len;
}
}
echo $cnt."\n";