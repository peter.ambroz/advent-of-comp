<?php
function trans($in)
{
    switch($in) {
        case 'n':
            return "north\n";
        case 's':
            return "south\n";
        case 'e':
            return "east\n";
        case 'w':
            return "west\n";
        default:
            return $in."\n";
    }
}

require('AsciiCode.php');
$code = new AsciiCode('input25.intcode.txt');
while (true) {
    try {
        $code->run();
    } catch (InputRequiredException $e) {
        echo "IN? ";
        $in = trim(fgets(STDIN));
        $code->setAsciiInput(trans($in));
    } catch (OutputAvailableException $e) {
        $res = $code->getResult();
        if ($res < 256) {
            echo chr($res);
        } else {
            echo $res;
        } 
    } catch (Exception $e) {
        echo $e->getMessage();
        break;
    }
}