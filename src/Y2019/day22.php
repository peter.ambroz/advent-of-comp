<?php

class Deck {
    /** @var int[] */
    private array $cards;
    private int $start;
    private bool $dir;
    private int $size;

    public function __construct(int $size)
    {
        $this->size = $size;
        $this->cards = [];
        for ($i = 0; $i < $size; $i++) {
            $this->cards[$i] = $i;
        }

        $this->start = 0;
        $this->dir = true;
    }

    public function deal(): void
    {
        $this->dir = !$this->dir;
    }

    public function cut(int $position): void
    {
        if ($this->dir) {
            $this->start = ($this->start + $position + $this->size) % $this->size;
        } else {
            $this->start = ($this->start - $position + $this->size) % $this->size;
        }
    }

    public function dealN(int $n): void
    {
        $this->align();
        $deck = $this->cards;
        $pos = 0;
        foreach ($this->cards as $card) {
            $deck[$pos] = $card;
            $pos = ($pos + $n) % $this->size;
        }
        $this->cards = $deck;
    }

    private function align(): void
    {
        $deck = [];
        if ($this->dir) {
            for ($i = 0; $i < $this->size; $i++) {
                $pos = ($i + $this->start + $this->size) % $this->size;
                $deck[] = $this->cards[$pos];
            }
        } else {
            for ($i = $this->size-1; $i >= 0; $i--) {
                $pos = ($i + $this->start + $this->size) % $this->size;
                $deck[] = $this->cards[$pos];
            }
        }

        $this->cards = $deck;
        $this->start = 0;
        $this->dir = true;
    }

    /**
     * @return int[]
     */
    public function get(): array
    {
        $this->align();
        return $this->cards;
    }
}

$deck = new Deck(10007);
$fh = fopen('input22.txt', 'rb');
for($_i = 0; $_i < 1; $_i++) {
rewind($fh);
while (!feof($fh)) {
    $line = trim(fgets($fh));
    if (!strlen($line)) {
        continue;
    }

    $cmd = explode(' ', $line);
    if ($cmd[0] === 'cut') {
        $deck->cut($cmd[1]*1);
    } else {
        if ($cmd[1] === 'with') {
            $deck->dealN($cmd[3]*1);
        } else {
            $deck->deal();
        }
    }
}
}
fclose($fh);
$d = $deck->get();
//echo array_search(2019, $d, true)."\n";

echo $d[2020]."\n".$d[5412]."\n";
echo $d[2020]."\n".$d[6678]."\n";
/*
3 2 1 0 6 5 4 N=1, d=6
3 6 2 5 1 4 0 N=2, d=3
3 5 0 2 4 6 1 N=3, d=2
3 1 6 4 2 0 5 N=4, d=5
3 0 4 1 5 2 6 N=5, d=4
3 4 5 6 0 1 2 N=6, d=1

5 6 0 1 2 3 4  1
5 2 6 3 0 4 1  4
5 3 1 6 4 2 0  5
5 0 2 4 6 1 3  2
5 1 4 0 3 6 2  3
5 4 3 2 1 0 6  6

5 0 2 4 6 1 3
5 6 0 1 2 3 4
5 1 4 0 3 6 2

1519 6678 (5159)

2559 9227 (6668)

4167 313 (6153)

101741582076661
 50870791038330 (+1)
 25435395519165
 12717697759582 (+1)
  6358848879791
  3179424439895 (+1)
  1589712219947 (+1)
   794856109973 (+1)
   397428054986 (+1)
   198714027493
    99357013746 (+1)
    49678506873
    24839253436 (+1)
    12419626718
     6209813359
     3104906679 (+1)
     1552453339 (+1)
      776226669 (+1)
      388113334 (+1)
      194056667
       97028333 (+1)
       48514166 (+1)
       24257083
       12128541 (+1)
        6064270 (+1)
        3032135 
        1516067 (+1)
         758033 (+1)
         379016 (+1)
         189508
          94754
          47377
          23688 (+1)
          11844
           5922
           2961
           1480 (+1)
            740
            370
            185
             92 (+1)
             46
             23
             11 (+1)
              5 (+1)
              2 (+1)
              1
              0 (+1)
*/