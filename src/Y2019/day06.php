<?php
function findRoot($x)
{
  return count(roots($x));
}

function roots($x)
{
  global $tree;
  $roots = [];
  while(isset($tree[$x])) {
    $roots[] = $x = $tree[$x];
  }
  return $roots;
}

$tree = [];

$fh = fopen('input06.txt', 'r');
while (!feof($fh)) {
  $line = trim(fgets($fh));
  if (!strlen($line)) continue;
  list($left, $right) = explode(')', $line, 2);
  $tree[$right] = $left;
}

$total = 0;
foreach ($tree as $right => $left) {
  $total += findRoot($left) + 1;
}
echo $total."\n";


$san = roots('SAN');
$you = roots('YOU');
echo implode('-',$san)."\n";
echo implode('-',$you)."\n";
$sanp = count($san)-1;
$youp = count($you)-1;
while($san[$sanp] === $you[$youp]) {
  $sanp--;
  $youp--;
}

echo ($sanp + $youp + 2)."\n";
?>
