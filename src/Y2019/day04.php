<?php
function validate($x) {
  $x = (string)$x;
  if (strlen($x) != 6) return false;
  $pair = 1;
  $pair2 = false;
  for ($i=0; $i<5; $i++) {
    if ($x[$i] > $x[$i+1]) return false;
    if ($x[$i] == $x[$i+1]) {
      $pair++;
    } else {
      if ($pair == 2) {
        $pair2 = true;
      }
      $pair=1;
    }
  }
  return $pair2 || ($pair == 2);
}

$valid = 0;
for ($i=124075; $i<=580769; $i++) {
  if (validate($i)) $valid++;
}
echo $valid."\n";
?>