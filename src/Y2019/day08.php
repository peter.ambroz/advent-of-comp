<?php
$img = file_get_contents('input08.txt');
$len = strlen($img);
$part = 0;
$min0 = 200;
$min0partResult = 0;
while($len > ($part*150)) {
  $partImg = substr($img, $part*150, 150);
  $cnts = [0, 0, 0];
  for ($i=0; $i<150; $i++) {
    switch($partImg[$i]) {
      case '0':
        $cnts[0]++;
        break;
      case '1':
        $cnts[1]++;
        break;
      case '2':
        $cnts[2]++;
        break;
    }
  }
  if ($min0 > $cnts[0]) {
    $min0 = $cnts[0];
    $min0partResult = $cnts[1] * $cnts[2];
  }
  $part++;
}

printf("%d\n", $min0partResult);

$i = 0;
$partImg = array_fill(0, 150, 2);
while ($i < $len) {
  $j = $i % 150;
  if ($partImg[$j] == 2) {
    $partImg[$j] = $img[$i];
  }
  $i++;
}

for ($i = 0; $i < 150; $i++) {
  if ($i%25 == 0) echo "\n";
  echo $partImg[$i] == 0 ? ' ' : 'X';
}
echo "\n";
?>