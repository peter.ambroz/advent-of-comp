<?php
require('Intcode.php');
$data = explode(',', file_get_contents('input07.txt'));

$p = [];
$max = 0;
for ($p[0]=5; $p[0]<10; $p[0]++) {
  for ($p[1]=5; $p[1]<10; $p[1]++) {
    if ($p[1] == $p[0]) continue;
    for ($p[2]=5; $p[2]<10; $p[2]++) {
      if ($p[2] == $p[0] || $p[2] == $p[1]) continue;
      for ($p[3]=5; $p[3]<10; $p[3]++) {
        if ($p[3] == $p[0] || $p[3] == $p[1] || $p[3] == $p[2]) continue;
        for ($p[4]=5; $p[4]<10; $p[4]++) {
          if ($p[4] == $p[0] || $p[4] == $p[1] || $p[4] == $p[2] || $p[4] == $p[3]) continue;
          
          $code = [];
          for ($i=0; $i<5; $i++) {
            $code[$i] = new Intcode($data, false);
            $code[$i]->setInput($p[$i]);
          }
          
          $in = 0;
          $pc = 0;
          $thisPc = $code[$pc];
          while(!$thisPc->isHalted()) {
            printf("PC %d\n", $pc);
            try {
              $thisPc->run();
            } catch (Exception $e) {
//              usleep(500000);
              switch ($e->getCode()) {
                case 3:
                  $thisPc->setInput($in);
                  continue 2;
                case 4:
                  $in = $thisPc->getResult();
                  break;
                default:
                  echo $e->getMessage();
                  break;
              }
            }
            $pc = ($pc+1)%5;
            $thisPc = $code[$pc];
          }
          
          if ($in > $max) $max = $in;
        }
      }
    }
  }
}

echo "MAX: ".$max."\n";
