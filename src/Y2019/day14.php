<?php
function conn($from, $to)
{
  global $elem;
  $empty = ['in' => [], 'out' => [], 'lft' => 0];
  if (!isset($elem[$from])) {
    $elem[$from] = $empty;
  }
  if (!isset($elem[$to])) {
    $elem[$to] = $empty;
  }
  $elem[$from]['out'][$to] = 0;
  $elem[$to]['in'][$from] = 0;
}

function build($n)
{
  global $elem;
  global $rec;

  $rdata = $rec[$n];
  foreach (array_keys($rdata['i']) as $ingr) {
    conn($n, $ingr);
    if ($ingr !== 'ORE') {
      build($ingr);
    }
  }
}

function trace($n)
{
  global $rec;
  global $elem;

  if ($n === 'FUEL') {
    return 1;
  }
  
  $sum = 0;
  foreach (array_keys($elem[$n]['in']) as $prev) {
    $rdata = $rec[$prev];
    $q = trace($prev);
    
    $lft = min($elem[$prev]['lft'], $q);
    $q -= $lft;
    $elem[$prev]['lft'] -= $lft;
    
    $mul = (int)ceil($q / $rdata['q']);
    $now = $rdata['i'][$n] * $mul;
    
    $elem[$prev]['lft'] += (($rdata['q'] * $mul) - $q);
    $sum += $now;
  }
  
  return $sum;
}

$rec = json_decode(file_get_contents('input14.json'), true);
$elem = [];

build('FUEL');
$used = trace('ORE');
echo $used."\n";

$fuels = 0;
while ($used < 1000000000000) {
  $fuels++;
  $used += trace('ORE');
}

echo $fuels."\n";