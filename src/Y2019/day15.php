<?php
require('Intcode.php');

function getPos($x, $y, $go)
{
  global $map;
  global $ext;
  
  switch ($go) {
    case 1: $y--; if ($y < $ext[0]) $ext[0] = $y; break;
    case 2: $y++; if ($y > $ext[2]) $ext[2] = $y; break;
    case 3: $x--; if ($x < $ext[3]) $ext[3] = $x; break;
    case 4: $x++; if ($x > $ext[1]) $ext[1] = $x; break;
  }
  
  return [$x, $y, sprintf('%d_%d', $x, $y)];
}

function where($x, $y, $head, $go)
{
  global $map;
  
  $try = [
    1 => sprintf('%d_%d', $x, $y-1),
    3 => sprintf('%d_%d', $x-1, $y),
    2 => sprintf('%d_%d', $x, $y+1),
    4 => sprintf('%d_%d', $x+1, $y),
  ];
  
  $right = [
    0 => -1, //invalid
    1 => 4,
    2 => 3,
    3 => 1,
    4 => 2
  ];
  
  $left = [
    0 => -1, //invalid
    1 => 3,
    2 => 4,
    3 => 2,
    4 => 1
  ];
  
  foreach ($try as $dir => $idx) {
    if (!isset($map[$idx])) {
      $map[$idx] = -1;
    }
  }

  $rh = $right[$head];
  $lh = $left[$head];
  
  if ($map[$try[$rh]] == 0) {
    if ($map[$try[$head]] != 0) {
      return [$head, $head];
    } else {
      return [$lh, $lh];
    }
  } elseif ($map[$try[$rh]] == -1) {
    return [$head, $rh];
  } else {
    return [$rh, $rh];
  }
  /*
  mam ju po pravici:
    - da sa rovno? chod.
    - neda sa rovno? otoc sa vlavo a urob krok vpred.
  
  nemam ju po pravici:
    - otoc sa vpravo a chod rovno.
    */
}

function redraw($px, $py)
{
  global $map;
  global $ext;
  
  $chmap = [
    0 => '#',
    1 => '.',
    2 => '!',
    3 => ' ',
    4 => 'X',
    5 => 'S',
  ];
  
  echo "\n";
  for ($y = $ext[0]; $y <= $ext[2]; $y++) {
    for ($x = $ext[3]; $x <= $ext[1]; $x++) {
      $char = $map[$x.'_'.$y] ?? 3;
      if ($x == 0 && $y == 0) $char = 5;
      if ($x == $px && $y == $py) $char = 4;
      echo $chmap[$char] ?? '?';
    }
    echo "\n";
  }
}

$data = explode(',', file_get_contents('input15.txt'));
$code = new Intcode($data, false);
$code->setDebug(false);

$map = ['0_0' => 1];
$x = 0;
$y = 0;
$go = 1;
$head = 1;

$dx = 0;
$dy = 0;
$ext = [0, 0, 0, 0];
$destFound = false;

$steps = 0;

while (true) {
  try {
    $code->run();
  } catch (Exception $e) {
    switch($e->getCode()) {
      case 3:
        $code->setInput($go);
        break;
      case 4:
        $steps++;
        [$nx, $ny, $nxy] = getPos($x, $y, $go);
        $result = $code->getResult();
        $map[$nxy] = $result;
        switch($result) {
          case 0:
            break;
          case 2:
            $head = $go;
            $destFound = true;
            $dx = $x;
            $dy = $y;
          case 1:
            $x = $nx;
            $y = $ny;
            break;
        }
        [$head, $go] = where($x, $y, $head, $go);
        redraw($x, $y);
//        usleep(1000);
        break;
      default:
        echo $e->getMessage();
        break 2;
    }
    if ($destFound) {
      echo 'Found 2 @ '.$dx.'_'.$dy."\n";
 //     break;
    }
  }
}