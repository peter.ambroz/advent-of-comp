#!/usr/bin/php
<?php

$year = sprintf('Y%04s', $argv[1] ?? date('Y'));
$tasks = (int) ($argv[2] ?? 25);

$dirName = sprintf('%s/src/%s', __DIR__, $year);

if (file_exists($dirName)) {
    die($dirName . " already exists\n");
}

mkdir($dirName);

foreach (range(1, $tasks) as $day) {

    $nameSpace = sprintf('App\\%s', $year);
    $className = sprintf('D%02s', $day);

    $fileName = sprintf('%s/%s.php', $dirName, $className);

    file_put_contents($fileName, <<<EOF
<?php

declare(strict_types=1);

namespace $nameSpace;

use App\Day;

class $className extends Day
{
    public function run(): void
    {
        \$data = \$this->inputLines();
        
    }

    public function runB(): void
    {
        \$data = \$this->inputLines();

    }
}

EOF);
}
